/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id: main.tag 2673 2017-02-01 15:04:08Z clerger $
 * Copyright (C) 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  frenchmg.tag -- Small French XTAG from Meta Grammar
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-include 'header.tag'.

:-require 'metamof.tag'.
:-require 'addons.tag'.
:-require 'disamb.pl'.

:-op(  700, xfx, [?=]). % for default value
:-xcompiler(( X ?= V :- ( \+ var(X) xor X = V))). %% Setting a default value


%%?-recorded( L::lctag(_,_,_) ), format('lctag ~w\n',[L]),fail.

:-light_tabular fast_reader/1.

fast_reader(L) :- domain('-fast_reader',L).

?-  process_options,
    load_lctag_data,
    show_time(latency),
  xargv(L),
  (domain('-multi',L) -> record(opt(multi)) ; true),
  \+ fast_reader(L),
  (   recorded('S'(SId)) xor SId='E1' ),
%%  ( use_feature_cost -> persistent!add(use_feature_cost) ; true ),
%  ( use_cluster_feature -> persistent!add(use_cluster_feature) ; true ),
  every((
	 'T'(TId,TContent),
	 format('<token> ~wF~w ~w\n',[SId,TId,TContent])
	)),
  wait((clean_sentence)),
%  wait(( add_potential_coma )),
  ( domain('-dump_lattice',L) ->
    dump_lattice(0),
    fail
  ;
    true
  ),
  ( guide ->
    every(( guide!node(Left,Right,Form,Cat,_),
	    Cat \== '_',
	    recorded( 'C'(Left,lemma{ cat => _Cat },_), Addr),
	    \+ Cat = _Cat,
	    delete_address(Addr)
	  ))
  ;
    true
  ),
  every(( 'C'(Left,lemma{ truelex => TId },Right),
	  record_without_doublon('rC'(Right,Left)),
	  (TId = [XTId|_] xor TId = XTId),
	  record_without_doublon(tid2pos(XTId,Left)),
	  record_without_doublon(pos2tid(Left,XTId)),
	  true
	)),
%  fail,
%  every(( recorded(loaded_tree(Tree)),format('loaded tree ~w\n',[Tree]))),
%  every(( recorded(LCTAG_MAP::lctag_map(_,_,_)), format('lctag map ~w\n',[LCTAG_MAP]))),
%  fail,
%  every(( recorded(LCTAG_RMAP::lctag_map_reverse(_,_,_)), format('lctag map reverse ~w\n',[LCTAG_RMAP]))),
%  every((lctag_ok(Tree),format('lctag_true ~w\n',[Tree]))),
%%  every((recorded(lctag(Type,X,Name,_)),
%%	 format('LCTAG type=~w cat=~w tree=~w\n',[Type,X,Name]))),
  %%  wait((assert_anchor_points)),
  wait((
	tag_filter_init,
	tag_filter(_)
	  )),
  ( domain('-disamb',L) ->
    do_at_end(parsing_post_process)
  ;
    do_at_end(parsing_cleaning)
  ),
  %  show_time(preprocess),
  %  every(( recorded( OK::ok_tree(_,_) ), format('ok_tree ~w\n',[OK]) )),
  recorded('N'(N)), A=0,
  (
      %% First try to get a full parse for a verbal sentence
      %%      tag_phrase(top='S'{ sat => (+), extraction => extraction[-,wh]} at - 'S',A,N)
      %% short sentence are systematically tried,
      %% otherwise mask good interpretations for sentences such as
      %% 'ce calme' or 'la voile'
      register_started(0),
      register_rightmost_parsed_token(0),
      %%   ( N < 30 xor Mode = mode[~ [(-)]] ),
      tag_phrase((top='S'{  extraction => extraction[~ [rel,cleft]],
			    %			 extraction => extraction[~ [rel]],
			    %% mode => Mode,
			    %% sat => (+),
			    control => (-)
			 } at - 'S'
%	      ,
%	       (<=> ponctw, <=> poncts) @*,
%	       '$skip'
	      ),
		 A,N),
   (domain('-first_answer',L) ->
	'$interface'('DyALog_Agenda_Delete_All',[return(none)]),
	format('cutting after first answer\n',[]),
	true
    ;
    true
   ),
   record_without_doublon(exists_full_parse),
   ( true
   ;
     register_answer,
     fail
   ),

   true
  )
  .

:-mode(register_started/1,+(+)).

register_started(Level) :-
    \+ recorded( exists_full_parse )
.

try_alternate_trees(TId) :-
    %% fail,
    /*
    ( recorded(reserved(_)),
      \+ recorded(used_reserved)
     ->
	  every(( recorded(reserved(Reserved)),
		  record(Reserved)
		)),
	  record(used_reserved)
     ;
     true
    ),
*/
    (
	'$answers'(register_rightmost_parsed_token(_TId))
     ; '$answers'(register_rightmost_token(_TId))
    ),
    term_range(_TId,1000,_TId2),
    \+ (recorded(done_alternate_trees(_TId2)), _TId2 > _TId),
%    format('try_alternate_trees ~w\n',[_TId]),
    every((
		 %		 fail,
		 recorded(tid2pos(_TId,_Pos)),
%		 _LowPos is _Pos-20,
		 term_range(0,_Pos,_XPos),
		 recorded(reserved(CC:: 'C'(_XPos,lemma{ truelex => _XTId },_))),
		 _XPos =< _Pos,
%		 format('%% activate ~w\n',[CC]),
		 record(CC),
		 erase(reserved(CC)),
		 true
	     )),
    term_range(_TId,1000,__TId),
    (
	(recorded('SUPERTAG'(__TId,_,_)) ;
	 (recorded(reserved_ok_tree(_,Left)), recorded(pos2tid(Left,__TId)))
	),
	__TId > _TId,
	true
     ->
	    TId is _TId+1,
	    % to be sure to advance between each call to try_alternate_trees
	    record_without_doublon(done_alternate_trees(TId)),
	    register_rightmost_token(TId),
	    true
     ;
/*
     \+ (recorded('SUPERTAG'(__TId,_,_)),
         __TId > _TId,
	 true
	),
*/
     TId = 1000,
     true
    ),
    \+ recorded( exists_full_parse ),
    format('activate alternate trees tid=~w\n',[TId]),
    true
.

register_rightmost_token(TId).

?-'$answers'(register_started(0)),
   wait,
   register_activate_delayed_lctag_callers,
   wait,
   register_started(1),
   fail
.

register_activate_delayed_lctag_callers.

?-'$answers'(register_started(1)),
%   format('retry after register_started\n',[]),
   xargv(L),
   wait,
   ( domain('-no_use_reserved',L) ->
	 true
    ;
    \+ recorded( exists_full_parse ),
    (recorded(rightmost_parsed_token(_TId)) ->
	 format('start reserved rightmost=~w\n',[_TId])
     ;
     true
    ),
    register_started_lctag,
    wait,
    \+ recorded( exists_full_parse ),
    recorded('N'(N)),
    register_alternate_range(10),
    try_alternate_trees(_),
    wait,
    \+ recorded( exists_full_parse ),
    %   N >= 5,
    register_alternate_range(20),
    wait,
    \+ recorded( exists_full_parse ),
    %   N >= 18,
    register_alternate_range(100),
    wait
   ),
   register_started(2),
   fail
.

register_started_lctag.

register_alternate_range(_).


:-std_prolog check_not_parsed_sentence/2.

check_not_parsed_sentence(A,N) :-
	tab_item_term( I, 'S'{ mode => mode[~ [(-)]],
			   extraction => extraction[~ [rel,cleft]],
			   sat => (+),
			   control => (-)
			 }(A,N) ),
	\+ recorded( I )
	.
/*
?- '$answers'(register_started),
   wait,
   argv(L),
   ( domain('-nocorrect',L)
   xor
   \+ recorded( exists_full_parse ),
   recorded('N'(_N)), _A=0,
     tab_item_term( I, 'S'{ mode => mode[~ [(-)]],
			    extraction => extraction[~rel],
			    sat => (+),
			    control => (-)
			  }(_A,_N) ),
     \+ recorded( I ),
     format('*** Try correction mode\n',[]),
     '$allpos'(AllPos),
     every((
	    domain(Pos,AllPos),
	    \+ recorded( added_correction ),
	    try_correction(Pos)
	   )
	  ),
     wait
   ),
   register_robust1,
   fail
   .
*/

?- '$answers'(register_started(2)),
   wait,
   xargv(L),
   every(( domain('-nocorrect',L)
		 xor
		 (%fail,
		     recorded(rightmost_parsed_token(_TId)),
		     recorded(tid2pos(_TId,_XPos))
		  ->
		      Pos is max(-1,_XPos-30)
		  ;
		  Pos = 0
		 ),
%	   format('start correction startpos=~w\n',[Pos]),
	   correction_loop(0,Pos)
	 )),
   wait,
   register_started(robust1),
   every(( recorded(added_correction_info(Left1,Left2,Msg,Cat,Token,_)),
	   format('+++ Success correction_anchor ~w left1=~w left2=~w cat=~w token=~w\n',[Msg,Left1,Left2,Cat,Token]),
	   true
	 )),
   fail
   .

correction_loop(Cycle,OldPos) :-
	Cycle < 10,
	\+ recorded( exists_full_parse ),
	recorded('N'(_N)), _A=0,
	tab_item_term( I, 'S'{ mode => mode[~ [(-)]],
			       extraction => extraction[~ [rel,cleft]],
			       sat => (+),
			       control => (-)
			     }(_A,_N) ),
	\+ recorded( I ),
	erase( added_correction ),
	erase( added_correction2 ),
	%	format('*** cycle ~w oldpos=~w\n',[Cycle,OldPos]),
	( recorded(rightmost_parsed_token(_TId)),
	  recorded(tid2pos(_TId,_Pos)) ->
	      Pos is max(OldPos + 1,_Pos-30)
	 ;
	 Pos is OldPos+1
	),
	'$allpos'(AllPos),
	domain(Pos,AllPos),
%	format('*** Try correction mode pos=~w #corr=~w\n',[Pos,Cycle]),
%	Pos > OldPos,
%	format('test pos=~w\n',[Pos]),
	\+ recorded( added_correction ),
	( %fail,
	  try_correction(Pos,Cycle),
	  fail
	;
	  wait,
	  ( recorded( added_correction ) ->
	    NewCycle is Cycle + 1,
	    NewPos = OldPos
	  ;
	    NewCycle = Cycle,
	    NewPos = Pos
	  ),
	  correction_loop(NewCycle,NewPos)
	)
	.

%%:-light_tabular try_correction/0.

try_correction(Left,_Cycle) :-
	%% try correct aggreement in Nomimal Phrases (N2)
	'D'(nc,Left),
	_Left is Left - 2,
	term_range(_Left,Left,TLeft),
	tab_item_term('*RITEM*'(Call,Ret), 'N2'{}(TLeft,Right) ),
	recorded( '*SACITEM*'(Call), Addr ),
	\+ recorded( '*RITEM*'(Call,Ret) ) ,
	'C'(Left,
	    lemma{ cat => nc,
		   lemma => Lemma,
		   lex => Lex,
		   truelex => TLex,
		   anchor => Anchor,
		   top => Top
		 },
	    Next),
	register_correction( nc, (Left:TLeft), Next, Lex, Anchor,
			     nc{},
			     Top,
			     TLex,
			     Lemma,
			     'N2 aggreement1',
			     _Cycle
			   )
	.


try_correction(Left,_Cycle) :-
	%% try correct aggreement in Nomimal Phrases (N2)
	'D'(det,Left),
	tab_item_term('*RITEM*'(Call,Ret), 'N2'{}(Left,Right) ),
	recorded( '*SACITEM*'(Call), Addr ),
	\+ recorded( '*RITEM*'(Call,Ret) ) ,
	'C'(Left,
	    lemma{ cat => det,
		   lemma => Lemma,
		   lex => Lex,
		   truelex => TLex,
		   anchor => Anchor,
		   top => Top
		 },
	    Next),
	register_correction( det, Left, Next, Lex, Anchor,
			     det{},
			     Top,
			     TLex,
			     Lemma,
			     'N2 aggreement2',
			     _Cycle
			   )
	.


try_correction(Left,_Cycle) :-
	%% try correct aggreement in Nomimal Phrases (N2)
	'D'(number,Left),
	tab_item_term('*RITEM*'(Call,Ret), 'N2'{}(Left,Right) ),
	recorded( '*SACITEM*'(Call), Addr ),
	\+ recorded( '*RITEM*'(Call,Ret) ) ,
	'C'(Left,
	    lemma{ cat => number,
		   lemma => Lemma,
		   lex => Lex,
		   truelex => TLex,
		   anchor => Anchor,
		   top => Top
		 },
	    Next),
	register_correction( number, Left, Next, Lex, Anchor,
			     number{},
			     Top,
			     TLex,
			     Lemma,
			     'N2 aggreement3',
			     _Cycle
			   )
	.

try_correction(P1,_Cycle) :-
    %% some categories may be reified as noun after 'le'
    'D'(det,P1),
    'C'(P1,lemma{ cat => det, lemma => le, top => det{ gender => masc, number=>sg} },P2),
    ('C'(P2,lemma{ lemma => OpenLemma, cat => ponctw },P3),
     domain(OpenLemma,['"','''','�']) ->
         true
     ;
     P3 = P2
     ),
    'C'(P3,lemma{ cat => Cat,
		  lemma => Lemma,
		  lex => Lex,
		  truelex => TLex,
		  anchor => Anchor,
		  top => Top
		},
	P4),
     \+ 'C'(P3,lemma{ cat => cat[nc,np,adj,ncpred,number] },_),
     tab_item_term('*RITEM*'(Call,Ret), 'N2'{}(P1,Right) ),
     recorded( '*SACITEM*'(Call), Addr ),
     \+ recorded( '*RITEM*'(Call,Ret) ) ,
     register_correction(nc,
			 P3,P4,Lex,Anchor,
			 nc{},
			 Top,
			 TLex,
			 Lemma,
			 'noun_promotion',
			 _Cycle
			)
	.
     
%% correction on subject-verb aggreement
try_correction(_Left,_Cycle) :-
    'D'(v,_Left),
	K::'C'( _Left,
	       lemma{ cat => v,
		      top => Top::v{ mode => Mode::mode[~ [infinitive,participle]],
				     tense => Tense,
				     aux_req => Aux,
				     diathesis => Diathesis,
				     lightverb => Lightverb
				   },
		      lemma => Lemma,
		      lemmaid => LemmaId,
		      lex => Lex,
		      truelex => TLex,
		      anchor => Anchor
		    },
	       _Right
	      ),
	%%        \+ recorded(added_correction(K)),
	format('try subj-aggreement at ~w lemma=~w\n',[_Left,Lemma]),
	constrained_almost_in(Left^Right^('S'{}(Left,Right)),_Left,_Right,Addr,lemma{ cat => prep}),
        \+ covered_by(N2Left^N2Right^('N2'{}(N2Left,N2Right)),_Left,_Right),
	register_correction( v,_Left,_Right,Lex,Anchor,
			     v{ mode => Mode,
				tense => Tense,
				aux_req => Aux,
				diathesis => Diathesis,
				lightverb => Lightverb
			      },
			     Top,
			     TLex,
			     Lemma,
			     'subject-verb aggreement',
			     _Cycle
			   ),
	true
	.

%% correction on subject-participle aggreement
try_correction(_Left,_Cycle) :-
	'D'(v,_Left),
	'C'( _Left,
	       lemma{ cat => v,
		      top => Top::v{ mode => Mode::participle,
				     tense => Tense,
				     aux_req => Aux::estre,
				     diathesis => Diathesis,
				     lightverb => Lightverb
				     %estre => Estre
				   },
		      lemma => Lemma,
		      lemmaid => LemmaId,
		      lex => Lex,
		      truelex => TLex,
		      anchor => Anchor
		    },
	       _Right
	     ),
	climb_verbs(_Left,__Left),
%%	format('climb from ~w to ~w\n',[_Left,__Left]),
	%% one may a participiale or an infinitive recognized from _Left
	%% so we climb back the verbs
	almost_in(Left^Right^('S'{}(Left,Right)),__Left,_Right,Addr),
	\+ covered_by(N2Left^N2Right^('N2'{}(N2Left,N2Right)),_Left,_Right),
	register_correction( v,_Left:__Left,_Right,Lex,Anchor,
			     v{ mode => Mode,
				tense => Tense,
				aux_req => Aux,
				diathesis => Diathesis,
				lightverb => Lightverb
				%estre => Estre
			      },
			     Top,
			     TLex,
			     Lemma,
			     'subject-participle aggreement',
			     _Cycle
			   ),
	true
	.

%/*
%% correction on subject-attribute aggreement
try_correction(_Left,_Cycle) :-
	'D'(adj,_Left),
	'C'( _Left,
	       lemma{ cat => adj,
		      top => Top::adj{},
		      lemma => Lemma,
		      lemmaid => LemmaId,
		      lex => Lex,
		      truelex => TLex,
		      anchor => Anchor
		    },
	       _Right
	     ),
%	format('potential adj ~w at ~w\n',[Lemma,_Left]),
	climb_verbs(_Left,__Left),
%	format('climb from ~w to ~w\n',[_Left,__Left]),
	%% one may a participiale or an infinitive recognized from _Left
	%% so we climb back the verbs
	almost_in(Left^Right^('S'{}(Left,Right)),__Left,_Right,Addr),
	\+ covered_by(N2Left^N2Right^('N2'{}(N2Left,N2Right)),_Left,_Right),
	XLeft is __Left - 5,
	register_correction( adj,_Left:XLeft,_Right,Lex,Anchor,
			     adj{},
			     Top,
			     TLex,
			     Lemma,
			     'subject-attribute aggreement',
			     _Cycle
			   ),
	true
	.
%*/

try_correction(_Left,_Cycle) :-
%	'D'(Cat0,_Left),
    %	format('here ~w\n',[_Left]),
        'D'(v,_Left),
	'C'(_Left,
	    lemma{ cat => v,
		   top => Top,
		   lemma => Lemma,
%		   lemmaid => LemmaId,
		   lex => Lex,
		   truelex => TLex,
		   anchor => Anchor::tag_anchor{ name => ht{ arg1 => Arg1::arg{ function => Fun1,
										kind => Kind1,
										pcas => PCas1
										},
							     arg2 => Arg2::arg{ kind => Kind2,
										function => Fun2,
										pcas => PCas2
									      },
							     arg0 => Arg0,
							     cat => Cat,
							     refl => Refl,
							     diathesis => Dia,
							     distrib => Distrib
							   }
%						 coanchors => [LemmaId]
					       }
		 },
	    _Right
	   ),
%	( Top =  v{ mode => Mode } xor Top = ncpred{}),
%	format('try0 to correct left=~w lemma=~w anchor=~w\n',[_Left,Lemma,Anchor]),
	( (Fun1 \== (-), \+ Kind1 = (-))
	xor (Fun2 \== (-), \+ Kind2 = (-))
	),
%	format('try1 to correct left=~w lemma=~w\n',[_Left,Lemma]),
	climb_verbs(_Left,__Left),
%	format('try2 to correct left=~w lemma=~w\n',[_Left,Lemma]),
	almost_in(Left^Right^('S'{}(Left,Right)),__Left,_Right,Addr),
%	format('try3 to correct left=~w lemma=~w\n',[_Left,Lemma]),
	\+ covered_by(N2Left^N2Right^('N2'{}(N2Left,N2Right)),_Left,_Right),
%	format('try4 to correct left=~w lemma=~w\n',[_Left,Lemma]),
	term_subsumer(Kind1,(-),XKind1),
	term_subsumer(PCas1,(-),XPCas1),
	term_subsumer(Kind2,(-),XKind2),
	term_subsumer(PCas2,(-),XPCas2),
%	format('ready to correct left=~w lemma=~w\n',[_Left,Lemma]),
	register_correction( v,_Left:__Left,_Right,Lex,
			     tag_anchor{ name => ht{ arg1 => arg{ function => Fun1,
								  kind => XKind1,
								  pcas => XPCas1
								},
						     arg2 => arg{ kind => XKind2,
								  function => Fun2,
								  pcas => XPCas2
								},
						     arg0 => Arg0,
						     cat => Cat,
						     refl => Refl,
						     diathesis => Dia,
						     distrib => Distrib
						   },
%					 coanchors => [LemmaId],
					 equations => []
				       },
			     Top,
			     Top,
			     TLex,
			     Lemma,
			     'optional args',
			     _Cycle
			   )
	.

:-light_tabular climb_verbs/2.
:-mode(climb_verbs/2,+(+,-)).

climb_verbs(0,0).

climb_verbs(LeftIn,LeftOut) :-
	LeftIn > 0,
	%	L is LeftIn - 1,
	'rC'(LeftIn,L),
	( 'C'(L,lemma{ cat => cat[v,aux]},LeftIn) ->
	      climb_verbs(L,LeftOut)
	 ; 'C'(L,lemma{ cat => cat[adv,advneg]},LeftIn) ->
	       climb_verbs(L,LeftOut)
	 ;
	 LeftOut=L
	)
	.
		      
%% correction on subject-auxiliaries
try_correction(_Left,_Cycle) :-
    'D'(aux,_Left),
	'C'( _Left,
	     lemma{ cat => aux,
		    top => Top::aux{ mode => Mode::mode[~ [infinitive,participle]],
				     tense => Tense,
				     aux_req => Aux,
				     form_aux => Form_Aux,
				     diathesis => Diathesis,
				     lightverb => Lightverb
				   },
		    lemma => Lemma,
		    lemmaid => LemmaId,
		    lex => Lex,
		    truelex => TLex,
		    anchor => Anchor
		  },
	     _Right
	   ),
	%%	weak_pos(_Right),
	( covered_by( Left^Right^ ('Infl'{}(Left,Left) * 'Infl'{}(Left,Right)),
		      _Left,_Right
		    ),
	  started_before(Left^('Infl'{}(Left,Left) * 'Infl'{}(Left,Right)),
			 _Left,Addr),
          _Right1 is _Right+1
         xor fail),
	\+ covered_by(SLeft^SRight^ ('S'{}(SLeft,SRight)),_Left,_Right1),
	register_correction(aux,_Left,_Right,Lex,Anchor,
			    aux{ mode => Mode,
				 tense => Tense,
				 aux_req => Aux,
				 form_aux => Form_Aux,
				 diathesis => Diathesis,
				 lightverb => Lightverb
			       },
			    Top,
			    TLex,
			    Lemma,
			    'subject-auxiliary aggreement',
			    _Cycle
			   )
	.


%% correction on auxiliaries
try_correction(_Left1,_Cycle) :-
    'D'(v,_Left1),
	'C'( _Left1,
	     lemma{ cat => v,
		    top => Top::v{ mode => Mode::mode[infinitive],
				   tense => Tense,
				   aux_req => Aux,
				   diathesis => Diathesis,
				   lightverb => Lightverb
				 },
		    lemma => Lemma,
		    lemmaid => LemmaId,
		    lex => Lex,
		    truelex => TLex,
		    anchor => Anchor
		  },
	     _Right1
	   ),
	( _Left2 is _Left1 - 3,
	  term_range(_Left2,_Left1,_TLeft),
	  domain(_Left,_TLeft),
	  K::'C'(_Left,lemma{ lemma => AuxLemma, cat => aux},_Right),
	  %%	weak_pos(_Right),
	  covered_by( Left^Right^ ('Infl'{}(Left,Left) * 'Infl'{}(Left,Right)),
		      _Left,_Right
		    ),
	  started_before(Left^('Infl'{}(Left,Left) * 'Infl'{}(Left,Right)),
			 _Left,Addr)
			xor fail),
	\+ covered_by(SLeft^SRight^ ('S'{}(SLeft,SRight)),_Left,_Right1),
	register_correction(v,(_Left1:_Left),_Right1,Lex,Anchor,
			    v{ mode => participle,
			       tense => Tense,
			       aux_req => Aux,
			       diathesis => Diathesis,
			       lightverb => Lightverb
			     },
			    Top,
			    TLex,
			    Lemma,
			    'auxiliary-infinitive',
			    _Cycle
			   )
	.


%% correction on modal-participle
try_correction(_Left1,_Cycle) :-
    'D'(v,_Left1),
	'C'( _Left1,
	     W::lemma{ cat => v,
		    top => Top::v{ mode => Mode::mode[participle],
				   tense => Tense,
				   aux_req => Aux,
				   diathesis => Diathesis,
				   lightverb => Lightverb
				 },
		    lemma => Lemma,
		    lemmaid => LemmaId,
		    lex => Lex,
		    truelex => TLex,
		    anchor => Anchor
		  },
	     _Right1
	   ),
	(( _Left2 is _Left1 - 3,
	   term_range(_Left2,_Left1,_TLeft),
	   domain(_Left,_TLeft),
	  'C'(_Left,
	      lemma{ cat => v,
		     anchor => tag_anchor{ name => ht{ arg1 => arg{ kind => vcomp } }}
		   },
	      _Right), 
	  %%	weak_pos(_Right),
	  covered_by( Left^Right^ ('V'{}(Left,Left) * 'V'{}(Left,Right)),
		      _Left,_Right
		    ),
	  started_before(Left^('V'{}(Left,Left) * 'V'{}(Left,Right)),
			 _Left,Addr))
			xor fail),
	\+ covered_by(SLeft^SRight^ ('S'{}(SLeft,SRight)),_Left,_Right1),
	register_correction(v,(_Left1:_Left),_Right1,Lex,Anchor,
			    v{ mode => infinitive,
			       tense => Tense,
			       aux_req => Aux,
			       diathesis => Diathesis,
			       lightverb => Lightverb
			     },
			    Top,
			    TLex,
			    Lemma,
			    'modal-participle',
			    _Cycle
			   )
	.



%% correction on advneg-participle
try_correction(_Left1,_Cycle) :-
    'D'(v,_Left1),
	'C'( _Left1,
	     W::lemma{ cat => v,
		    top => Top::v{ mode => Mode::mode[participle],
				   tense => Tense,
				   aux_req => Aux,
				   diathesis => Diathesis,
				   lightverb => Lightverb
				 },
		    lemma => Lemma,
		    lemmaid => LemmaId,
		    lex => Lex,
		    truelex => TLex,
		    anchor => Anchor
		  },
	     _Right1
	   ),
	_Left2 is _Left1 - 3,
	term_range(_Left2,_Left1,_TLeft),
	domain(_Left,_TLeft),
	'C'(_Left, lemma{ cat => advneg },_Right), 
	%%	weak_pos(_Right),
	almost_in(Left^Right^('S'{}(Left,Right)),_Left,_Right1,Addr),
	\+ covered_by(N2Left^N2Right^('N2'{}(N2Left,N2Right)),_Left,_Right1),
	register_correction(v,(_Left1:_Left),_Right1,Lex,Anchor,
			    v{ mode => infinitive,
			       tense => Tense,
			       aux_req => Aux,
			       diathesis => Diathesis,
			       lightverb => Lightverb
			     },
			    Top,
			    TLex,
			    Lemma,
			    'advneg-participle',
			    _Cycle
			   )
	.

%% correction an adjective aggreement
try_correction(_Left,_Cycle) :-
    'D'(adj,_Left1),
	'C'( _Left,
	     lemma{ cat => adj,
		    top => Top::adj{ aux_req => Aux,
				     pre_det => Pre_Det,
				     degree => Degree,
				     position => Position
				   },
		    lemma => Lemma,
		    lex => Lex,
		    truelex => TLex,
		    anchor => Anchor
		  },
	     _Right
	   ),
	( covered_by( Left^Right^ ('N'{}(Left,Left) * 'N'{}(Left,Right)),
		    _Left,_Right
		    )
		    xor
	 covered_by( Left^Right^ ('det'{}(Left,Left) * 'det'{}(Left,Right)),
		     _Left,_Right
		   )
	),
	\+ covered_by(SLeft^SRight^ ('N2'{}(SLeft,SRight)),_Left-1,_Right+1),
	register_correction(adj,_Left,_Right,Lex,Anchor,
			    adj{ aux_req => Aux,
				 pre_det => Pre_Det,
				 degree => Degree,
				 position => Position
			       },
			    Top,
			    TLex,
			    Lemma,
			    'adjective aggreement',
			    _Cycle
			   )
	.

%% correction on potential lightverbs
try_correction(P1,_Cycle) :-
%	fail,
	'D'(v,P1),
	'C'(P1,
	    lemma{ cat => v,
		   lemma => VLemma,
		   top => v{ lightverb => LV, mode => Mode }
		 },
	    _P2
	   ),
	LV \== (-),
	( P2=_P2
	; 'C'(_P2,
	      lemma{ cat => cat[adv,adj,advneg] },
	      P2
	     )
	),
	'C'(P2,
	    lemma{ lex => Lex,
		   truelex => TLex,
		   lemma => Lemma,
		   top => Top,
		   cat => nc
		 },
	    P3
	   ),
	\+ 'C'(P2,lemma{cat => ncpred, top => ncpred{ lightverb => LV }},P3),
	\+ 'C'(P2,lemma{cat => cat[adv,det,advneg]},_),
%	format('ready1 to correct left=~w vlemma=~w lemma=~w\n',[P1,VLemma,Lemma]),
	almost_in(Left^Right^('S'{}(Left,Right)),P1,P3,Addr),
%	format('ready2 to correct left=~w vlemma=~w lemma=~w\n',[P1,VLemma,Lemma]),
	( Mode = participle ->
	      covered_by( _P1^__P2^ ('Infl'{}(_P1,_P1) * 'Infl'{}(_P1,__P2)),P1,P1),
%	      format('found infl ~w ~w\n',[_P1,_P2]),
	      true
	     ;
	     _P1 = P1
	),	
	\+ covered_by(N2Left^N2Right^('N2'{}(N2Left,N2Right)),_P1,P3),
%	format('ready3 to correct left=~w vlemma=~w lemma=~w\n',[P1,VLemma,Lemma]),
	register_correction( ncpred,P2:P1,P3,Lex,
			     tag_anchor{},
			     ncpred{
				    lightverb => LV
				   },
			     Top,
			     TLex,
			     Lemma,
			     'lightverb',
			     _Cycle
			   )
	.
	
%% try correction on missing closing coma (for incises)
try_correction(P1,_Cycle) :-
%    fail,
	'D'(ponctw,P1),
	'C'(P1,lemma{ cat => ponctw, lemma => (',') },_),
	tab_item_term(T , K:: (incise{}(P1,Right) * incise{}(Left2,Right2))),
	T = '*RAI*'!'$ft'(CallFoot,RetFoot,CallAux,RetAux),
%	format('K=~w T=~w\n',[K,T]),
	CAI = '*CAI*'!'$ft'(CallAux),
	recorded(CAI),
%	format('found CAI=~w\n',[CAI]),
	CFI = '*CFI*'!'$ft'(CallAux,CallFoot),
	recorded(CFI),
%	format('found CFI=~w\n',[CFI]),
	RFI = '*RFI*'!'$ft'(CallFoot,CallAux,RetFoot),
	recorded(RFI),
%	format('found K=~w RFI=~w\n',[K,RFI]),
	\+ ( 'D'(ponctw,_P),
	     'C'(_P,lemma{ cat => ponctw, lemma => (',') },_),
	     P1 < _P,
	     _P =< Right2
	   ),
	'C'(Right2,lemma{ cat => cat[det,np,cln] },_),
	format('+++ add potential coma at ~w\n',[Right2]),
	potential_coma(Right2),
	fail
	.

:-std_prolog weak_pos/1.

weak_pos(Pos1) :-
	pos_count(Pos1,_,_,Ins1,Total1),
	Pos3 is Pos1+1,
	pos_count(Pos3,_,_,Ins3,Total3),
	( Pos1 = 0
	xor
	Pos2 is Pos1-1,
	  pos_count(Pos2,_,_,Ins2,Total2)
	)
	.

:-std_prolog covered_by/3.

covered_by(Left^Right^Const,_Left,_Right) :-
%    format('try covered by ~w _left=~w _right=~w\n',[Const,_Left,_Right]),
    tab_item_term(Item,Const),
    _Left2 is max(0,_Left-5),
    term_range(_Left2,_Left,Left),
    _Right2 is _Right+5,
    term_range(_Right,_Right2,Right),
%    format('=>recorded0 left=~w right=~w ~w\n',[Left,Right,Item]),
    recorded(Item),
%    format('=>recorded left=~w right=~w ~w\n',[Left,Right,Item]),
%    Left =< _Left,
%    _Right =< Right,
%    format('=>recorded2 left=~w/~w right=~w/~w ~w\n',[Left,_Left,Right,_Right,Item]),
    true
.

:-std_prolog started_before/3.

started_before(Left^Const,_Left,Addr) :-
    _Left2 is min(0,_Left-5),
    term_range(_Left2,_Left,Left),
    tab_item_term('*RITEM*'(Call,Ret),Const),
    recorded('*SACITEM*'(Call),Addr),
    Left =< _Left,
    true
.

:-std_prolog almost_in/4.

almost_in(Left^Right^Const,_Left,_Right,Addr) :-
%%	weak_pos(_Left),
%	format('here1 left=~w right=~w\n',[_Left,_Right]),
    ( covered_by(Left^Right^Const,_Left,_Right)
     ->
%	  format('covered by ~w ~w\n',[Left,Right]),
	  fail
     ;
%	format('here2 not covered\n',[_Left,_Right]),
	started_before(Left^Const,_Left,Addr) ->
%	format('here3 started at ~w\n',[Left]),
	    true
     ;
     fail
    )
	.


:-std_prolog constrained_almost_in/5.

constrained_almost_in(Left^Right^Const,_Left,_Right,Addr,Constraint) :-
%%	weak_pos(_Left),
%    format('here1 left=~w right=~w\n',[_Left,_Right]),
    ( covered_by(Left^Right^Const,_Left,_Right),
      \+ 'C'(_,Constraint,Left),
      (Left < _Left xor _Right < Right),
      true
     ->
%	  format('covered by ~w ~w\n',[Left,Right]),
	  fail
     ;
%	format('here2 not covered\n',[_Left,_Right]),
	started_before(Left^Const,_Left,Addr) ->
%	format('here3 started at ~w\n',[Left]),
	    true
     ;
     fail
    )
	.


:-light_tabular pos_count/5.
:-mode(pos_count/5,+(+,-,-,-,-)).

pos_count(Pos,Calls,Rets,Ins,Total) :-
	mutable(MCalls,0),
	mutable(MRets,0),
	mutable(MIns,0),
	every((
	       recorded( O ),
	       domain(O,
		      ['*RITEM*'(Call,Ret),
		       '*RITEM*'(Call,Ret) :> _,
		       '*CITEM*'(Call,Call),
		       '*SACITEM*'(Call)
		      ]),
	       ( tab_item_term('*RITEM*'(Call,Ret),T (Left,Right))
	       ; Call = verbose!coanchor(_,Left,Right,_,_)
	       ; Call = verbose!anchor(_,Left,Right,_,_,_,_)
	       ; Call = verbose!lexical(_,Left,Right,_,_)
	       ),
	       ( Left == Pos, O = (_ :> _) ->
		 mutable_inc(MIns,_)
	       ; Left == Pos ->
		 mutable_inc(MCalls,_)
	       ; Right == Pos ->
		 %% format('\t\tRETS ~w\n',[O]),
		 mutable_inc(MRets,_)
	       ;
		 fail
	       )
	      )),
	mutable_read(MCalls,Calls),
	mutable_read(MRets,Rets),
	mutable_read(MIns,Ins),
	Total is Calls+Rets+Ins,
	format('pos count pos=~w calls=~w rets=~w ins=~w total=~w\n',[Pos,Calls,Rets,Ins,Total]),
	true
.

?-  '$answers'(register_started(robust1)),
     xargv(L),
     \+ fast_reader(L),
     domain('-robust',L),
     wait,
     recorded('N'(_N)), _A=0,
     (   tab_item_term( I, 'S'{ mode => mode[~ [(-)]],
				extraction => extraction[~ [rel,cleft]],
				sat => (+),
				control => (-)
			      }(_A,_N) ),
	 \+ recorded( I ),
	 \+ recorded( exists_full_parse ) ->
	 %% If no full parse, then try partial parses
	 format('*** Try robust mode\n',[]),
	 /*
	 mutable(MaxCovered,0,true),
	 every((
		tab_item_term('*RITEM*'(Call,Ret), _NT (Left,Right)),
		recorded( '*RITEM*'(Call,Ret) ),
		XRight is Right - 1,
		mutable_read(MaxCovered,_Max),
		XRight > _Max,
		mutable(MaxCovered,XRight)
	       )),
	 mutable_read(MaxCovered,__Max),
	 every(( mutable_read(MaxCovered,_Max),
		 _NewMax is _Max + 1,
		 recorded( 'C'(_NewMax,lemma{ cat => coo },_) ),
		 mutable(MaxCovered,_NewMax)
	       )),
	 every((
		mutable_read(MaxCovered,Max),
%%		format('max covered ~w\n',[Max]),
		term_range(0,Max,TCovered),
		domain(Pos,TCovered),
		%%		format('is_covered ~w\n',[Pos]),
		record_without_doublon( is_covered(Pos) ),
		true
	       )),
	 */
	 record(robust),
	 %% we activate reserve second-choice lexeme, discarded because of tagging information
%	 every(( recorded(reserved(Reserved)),
%		 record(Reserved)
%	       )),
	 wait(( %%make_intlist(AllPos,0,_N),
		%%domain(_Left,AllPos),
		ANA::analyze(_,_,_),
		%%	     format('Analyze ~w\n',[ANA]),
		true
	      )),
	 \+ recorded( I ),		% check that nothing new has been computed !
	 ( extract_best_solution(A,N,U),
	   %%	format('best sol ~w ~w ~w\n',[A,N,U]),
	   true
	 ; 
	   '$answers'(extract_best_solution(_L1,_R1,_)),
	   '$answers'(extract_best_solution(_L2,_R2,_)),
	   _L2 < _R1,
	   _R1 =< _R2,
	   _L1 =< _L2,
	   \+ (_L1 == _L2,_R2 == _R1 ),
	   %%	U=unknown,
	   %%	format('potential ~w ~w (l1=~w r1=~w; l2=~w,r2=~w)\n',[_L2,_R1,_L1,_R1,_L2,_R2]),
	   %%	U=unknown,
	   %%	extract_analyze(A,N,U),
	   %%	N is A+1,
	   %% ( N =< _L2, A >= _L1
	   %% ; A >= _R1, N =< _R2
	   %% ),
	   ( try_fill_second_best(_L1,_L2,A,N,U)
	   ;  try_fill_second_best(_R1,_R2,A,N,U)
	   ),
	   \+ ( '$answers'(extract_best_solution(_L3,_R3,_)),
		_L3 =< A,
		N =< _R3,
		(_R3 =< _L2 ; _L3 >= _R1)
	   ),
	   %%	format('extra sol ~w ~w ~w\n',[A,N,U]),
	   true
	 ),
	 '$answers'(analyze(A,N,U))
     ;
	 fail
     )
     .

:-std_prolog try_fill_second_best/5.

try_fill_second_best(XL,XR,L,R,U) :-
	( extract_second_best_solution(XL,XR,L,R,U),
	  true
	; '$answers'(extract_second_best_solution(XL,XR,_L1,_R1,_)),
	  '$answers'(extract_second_best_solution(XL,XR,_L2,_R2,_)),
	  _L2 < _R1,
	  _R1 =< _R2,
	  _L1 =< _L2,
	  \+ (_L1 == _L2,_R2 == _R1 ),
	  /*
	  extract_analyze(L,R,U),
	  ( R =< _L2, L >= _L1
	  ; L >= _R1, R =< _R2
	  ),
	  */
	  ( try_fill_second_best(_L1,_L2,L,R,U)
	  ;  try_fill_second_best(_R1,_R2,L,R,U)
	  ),
	  \+ ( '$answers'(extract_second_best_solution(XL,XR,_L3,_R3,_)),
	       _L3 =< L,
	       R =< _R3,
	       (_R3 =< _L2 ; _L3 >= _R1)
	     ),
%%	  analyze(L,R,U)
	  true
	;
	  fail
	)
	.
     
analyze(A,N,U) :-
	tag_phrase( ( top=U::'S'{ extraction => extraction[~ [rel,cleft]],
				  control => (-),
%				  xarg => xarg{ trace => (-) },
				  mode => Mode
				} at - 'S'
		    % ,'$skip'
		    ),A,N),
	N > A+1,
	%%	format('Analyze found at ~w ~w ~w\n',[A,N,U]),
	( Mode == imperative ->
	  %% avoid false imperative sentences just at the end of a sentence in robust mode
	  \+ (recorded('N'(_N), _N =< A+2 ))
	;
	  true
	),
	%% format('\tAnalyze kept\n',[]),
	true
	.

analyze(A,N,U) :- tag_phrase(top=U::comp{} at - comp,A,N), N > A.

analyze(A,N,U) :- tag_phrase(top=U::'PP'{} at - 'PP',A,N), N > A.

analyze(A,N,U) :- tag_phrase(top=U::'CS'{} at - 'CS',A,N), N > A.

%%analyze(A,N,U) :- tag_phrase(top=U::'N2'{ sat => (+) } at - 'N2',A,N), N > A.


%%analyze(A,N,U) :- tag_phrase(- <=> poncts, A, N), N > A.
%%analyze(A,N,U) :- tag_phrase(- <=> ponctw, A, N), N > A.

%%analyze(A,N,U) :- tag_phrase(<=> adv,A,N), N > A.

% analyze(A,N,U) :- tag_phrase(- <=> csu, A, N), N > A.
% analyze(A,N,U) :- tag_phrase(- <=> pri, A, N), N > A.
% analyze(A,N,U) :- tag_phrase(- <=> prel, A, N), N > A.
% analyze(A,N,U) :- tag_phrase(- <=> pro, A, N), N > A.
% analyze(A,N,U) :- tag_phrase(- <=> adj, A, N), N > A.
% analyze(A,N,U) :- tag_phrase(- <=> advneg, A, N), N > A.
% analyze(A,N,U) :- tag_phrase(- <=> prep, A, N), N > A.
% analyze(A,N,U) :- tag_phrase(- <=> coo, A, N), N > A.
% analyze(A,N,U) :- tag_phrase(- <=> v, A, N), N > A.
analyze(A,N,unknown) :- tag_phrase(- unknown, A, N),N > A.

:-std_prolog extract_analyze/3.

extract_analyze(A,N,U) :-
	tab_item_term(I,analyze(A,N,U)),
	recorded(I,_),
%	recorded(is_covered(A)),
%	XN is N-1,
%	recorded(is_covered(XN)),
	true
	.

%%:-std_prolog extract_best_solution/3.
:-light_tabular extract_best_solution/3.
:-mode(extract_best_solution/3,+(-,-,-)).

extract_best_solution(L,R,U) :-
	extract_analyze(L,R,U),
%	format('candidate best sol ~w ~w ~w\n',[L,R,U]),
	\+ ( extract_analyze(L2,R2,U2),
	     L2 =< L, R =< R2,
%	     \+ (U = U2),
	     \+ (L=L2, R=R2)
	    ),
%	format('best sol ~w ~w ~w\n',[L,R,U]),
	true
	.


:-light_tabular extract_second_best_solution/5.
:-mode(extract_second_best_solution/5,+(+,+,-,-,-)).

extract_second_best_solution(XL,XR,L,R,U) :-
	extract_analyze(L,R,U),
	XL =< L,
	R =< XR,
%%	(XL < L xor R < XR),
	(\+ ( extract_analyze(L2,R2,_),
	      L2 =< L, R =< R2,
	      XL =< L2, R2 =< XR,
	      \+ (L=L2, R=R2)
	    )
	)
	.


%%?-tag_hypertag(Family,HyperTag),tag_family_load(Family,Cat).

%%?-true.

%% Normalization of some cat

cat_normalize(det,det{ wh=> Wh,
		       poss => Pos,
		       numberposs => NPos,
		       dem => Dem,
		       countable => Count,
		       person => Pers
		     }) :-
	Wh ?= (-),
	Pos ?= (-),
	NPos ?= (-),
	Dem ?= (-),
	Count ?= (-),
	Pers ?= (-)
	.

cat_normalize(nc,nc{time=> Time,
		    hum => Hum,
		    def => Def,
		    semtype => SType
		   }) :-
	Time ?= (-), Hum ?= (-), Def ?= (+), SType ?= (-).

cat_normalize(prep,prep{pcas=> Pcas }) :- Pcas ?= (+).
%%cat_normalize(csu, csu{ que => Que }) :- Que ?= (-).

/*
cat_normalize(v,v{mode => Mode, person=> Person, number=> Number }) :-
	(   \+ var(Person) xor Person = 3 ),
	(   \+ var(Number) xor Number = sg )
	.
*/

/*
cat_normalize(v,v{ aux_req => AuxReq }) :-
	AuxReq ?= 'avoir'
	.
*/
cat_normalize(csu,csu{que=> Que}) :-
	Que ?= (-).
cat_normalize(pri,pri{case => Case }) :-
	Case ?= case[~ [nom,acc]].
cat_normalize(prel,prel{case => Case }) :-
	Case ?= case[~ [nom,acc]].
cat_normalize(adv,adv{adv_kind => Kind, chunk_type => Chunk }) :-
	Kind ?= adv_kind[~ [tr�s,intensive,modnc,modtime,equalizer,modpro,loc,time,direction,qmod]],
	Chunk ?= (-)
	.
cat_normalize(pres,pres{chunk_type => Chunk }) :-
	Chunk ?= (-)
	.
cat_normalize(adj,adj{ pre_det => X, degree => D, proadj => ProAdj }) :-
	X ?= (-),
	D ?= (-),
	ProAdj ?= (-)
	.

%cat_normalize(v,v{ lightverb => X, estre => Y}) :-
%	X ?= (-),
%	Y ?= (-)
%	.

cat_normalize(pro,pro{ def => Def, numberposs => NPos}) :-
	Def ?= (+),
	NPos ?= (-)
	.

cat_normalize(epsilon,epsilon{ chunk_type => CT }) :-
	CT ?= (-)
	.

cat_normalize(cld,cld{ number =>Num, person => Pers }) :-
	%% for y !
	Num ?= sg,
	Pers ?= (-)
	.


cat_normalize(predet,predet{ wh => Wh }) :-
	Wh ?= (-)
	.


%:-light_tabular add_potential_coma/0.

add_potential_coma :-
    'D'(Cat::cat[det],N),
    'C'(N,lemma{ cat => Cat, lemma => Lemma },M),
     \+ 'C'(M,lemma{ cat => v },_),
     \+ 'C'(N,lemma{ cat => cat[prep,v] },_),
     N > 0,
     P is N - 1,
     'C'(P,lemma{ cat => cat[nc,np,adj]},N),
     \+ 'D'(P,prep),
     format('add potential coma at ~w ~w\n',[N,Lemma]),
%     fail,
      potential_coma(N)
.
      



/*
add_potential_coma :-
    'D'(cln,N),
    'C'(N,lemma{ cat => cln },_),
    N > 0,
    P is N - 1,
    \+ 'C'(P,lemma{ lex => cat[ponctw,poncts] },N),
%    format('register potential coma at ~w\n',[N]),
    potential_coma(N)
.
*/

:-light_tabular clean_sentence/0.

delete_address(X) :- '$interface'( 'object_delete'(X:ptr),[return(bool)]).

/*
clean_sentence :-
	'D'(v,N),
	'C'(N,lemma{ cat=> v,
		     lemma=>Lemma,
		     top=> v{ mode=>participle,
			      diathesis => passive,
			      gender=> Gender,
			      number=> Number
			    }
		   },M),
	\+ var(Lemma),
	\+ domain(Lemma,[uw,uwSe]),
	every(( recorded(_K::'C'(N,lemma{ cat=>adj,
					  lemma => Lemma,
					  top => adj{gender => Gender,
						     number=>Number} },M),
			 Adr),
%%		format('Remove adj ~w\n',[_K]),
		delete_address(Adr)))
	.
*/

/*
clean_sentence :-
	'C'(N,
	   lemma{ lex => Lex,
		  cat => adj,
		  lemma => Lemma,
		  top => adj{ gender => Gender, number => Number, person => Person }
		},
	    M),
	\+ var(Lemma),
	every(( recorded('C'(N,
			     lemma{ lex => Lex,
				    cat => nc,
				    lemma => Lemma,
				    top => nc{ gender => Gender, number => Number, person => Person }
				  },
			     M),
			 Adr),
		delete_address(Adr)
	      )).
*/

/*
clean_sentence :-
	'C'(N,
	   lemma{ lex => Lex,
		  cat => adj,
		  lemma => Lemma,
		  top => adj{ gender => Gender, number => Number, person => Person }
		},
	    M),
	\+ K::'C'(N,
		  lemma{ lex => Lex,
			 cat => nc,
			 lemma => Lemma,
			 top => nc{ gender => Gender, number => Number, person => Person }
		       },
		  M),
%%	format('Record ~w\n',[K]),
	record( K )
	.
*/

:-require 'format.pl'.

:-finite_set(de,['De',de,'d''']).
:-finite_set(la,['La',la,'l''']).

/*
%% Sxpipe should now do a better identification
clean_sentence :-
	%% A simple recongnizer for proper nouns
	'C'(L1,lemma{ cat => np, lex => Lex1, truelex => TrueLex1 },R1),
	%% Should do all possible combinaisons !
	%%	\+ 'C'(_,lemma{ cat => np},L1),
	@*{ goal => (
			'C'(_L,lemma{ cat => np, lex => _Lex, truelex => _True_Lex },_R),
			name_builder('~w ~w',[_Lex_In,_Lex],_Lex_Out),
			name_builder('~w ~w',[_True_Lex_In,_True_Lex],_True_Lex_Out)
		    xor	'C'(_L,lemma{ lex => _Part::de[] , truelex => _True_Part},_L1),
			(   'C'(_L1,lemma{ cat => np, lex => _Lex, truelex => _True_Lex},_R),
			    name_builder('~w ~w ~w',[_Lex_In,_Part,_Lex],_Lex_Out),
			    name_builder('~w ~w ~w',[_True_Lex_In,_True_Part,_True_Lex],_True_Lex_Out)
			xor 'C'(_L1,lemma{ lex => _Part2::la[], truelex => _True_Part2 },_L2),
			    'C'(_L2,lemma{ cat => np, lex => _Lex, truelex => _True_Lex },_R),
			    name_builder('~w ~w ~w ~w',[_Lex_In,_Part,_Part2,_Lex],_Lex_Out),
			    name_builder('~w ~w ~w ~w',[_True_Lex_In,_True_Part,_True_Part2,_True_Lex],_True_Lex_Out)
			)
		    ),
	    from => 1,
	    collect_first => [R1,Lex1,TrueLex1],
	    collect_last  => [R,LexLast,TrueLexLast],
	    collect_loop  => [_L,_Lex_In,_True_Lex_In],
	    collect_next  => [_R,_Lex_Out,_True_Lex_Out]
	  },
	\+ 'C'(R,lemma{ cat => np},_),
%%	format('Found NP sequence ~w -> ~w: ~w\n',[L1,R,LexLast])
	record_without_doublon('C'(L1,lemma{lemma => '_Uw', lex=>LexLast,truelex=>TrueLexLast,cat=>np},R))
	.
*/

:-finite_set(open_quote,['�','"','''']).
:-finite_set(close_quote,['�','"','''']).
/*	
clean_sentence :-
	'C'(A, lemma{ lex => Open:: open_quote[], truelex => Q1 }, B ),
	'C'(B, lemma{ lex => Lex, cat => Cat, lemma => Lemma, top => Top, anchor => Anchor, truelex => TrueLex },C),
	'C'(C, lemma{ lex => Close:: close_quote[], truelex => Q2 }, D ),
%%	format('Found ~w ~w ~w : ~w -> ~w\n',[Open,Lex,Close,A,D]),
	domain(Open:Close,['�':'�','"':'"','''':'''']),
	name_builder('~w ~w ~w',[Open,Lex,Close],QuotedLex),
	name_builder('~w ~w ~w',[Q1,TrueLex,Q2],QuotedTrueLex),
	record_without_doublon( 'C'(A,
				    lemma{ lex => QuotedLex,
					   cat => Cat,
					   lemma => Lemma,
					   top => Top,
					   anchor => Anchor,
					   truelex => QuotedTrueLex
					 },
				    D)
			      )
	.
*/

%% Hack to handle ncpred with lightverbs !
%% Transfer subcat frame from ncpred to v+lightverb
clean_sentence :-
	'D'(v,C),
	recorded( KK:: 'C'(C, lemma{ cat => v,
				     top => Top::v{ lightverb => _Light },
				     lex => Lex,
				     truelex => TLex,
				     lemma => TLemma,
				     lemmaid => TLemmaId,
				     %%				lemma => Lemma,
				     anchor => tag_anchor{ name => ht{ diathesis => Dia::active,
								       %% arg1 => arg{ kind => _Arg1Kind },
								       cat => Cat
								     },
							   coanchors => []
							 }
				   },
			   D),
		  Addr),
%%		  format('Check recorded ~w\n',[KK]),
	\+ recorded( added(KK) ),
	% be sure we deal with an instantiated entry and not some underspecified one
%%	format('HERE1 ~w\n',[KK]),
	\+ var(_Light),
%%	format('HERE2 ~w\n',[KK]),
	%%	_Arg1Kind == npredobj,
%%	erase(KK),
        delete_address(Addr),
%%	format('HERE3 ~w\n',[KK]),
	every(('D'(ncpred,A),
	       'C'(A,
		   lemma{ top => ncpred{ lightverb => Light },
			  lemma => Lemma,
			  lemmaid => LemmaId,
			  anchor => Anchor::tag_anchor{ name => HT::ht{ diathesis => Dia,
									cat => Cat,
									refl => Refl,
									arg1 => Arg1,
									arg2 => Arg2::arg{ kind => Kind2 },
									arg0 => Arg0, % subject
									distrib => Distrib
								      }
						      }
			},
		   B),
	       D =< A,
	       _Light == Light,
	       %%	format('Delete ~w Before Adding ~w\n',[Addr,K]),
	       %%	every(( delete_address(Addr) )),
	       %% Seems better to move original arg1 to arg2 when possible
	       %% new arg1 would correspond to a kind of object, filled by ncpred
	       ( fail, Kind2 == (-) ->
		   XArg1= Arg2,
		   XArg2= Arg1
	       ;   
		   XArg1= Arg1,
		   XArg2= Arg2
	       ),
	       record_without_doublon( K::'C'(C,
			      lemma{ cat => v,
				     top => Top,
				     lex => Lex,
				     truelex => TLex,
%%				     lemma => Lemma,
				     lemma => TLemma,
%%				     lemmaid => TLemmaId,
				     lemmaid => TLemma,
				     anchor => tag_anchor{ name => ht{ diathesis => Dia,
								       cat => Cat,
								       refl => Refl,
								       arg0 => Arg0,
								       arg1 => XArg1,
								       arg2 => XArg2,
								       distrib => Distrib
								     },
							   equations => [],
							   coanchors => [LemmaId]
							 }
				   },
			      D) ),
	       record(added(K)),
%	       format('Adding ~w\n',[K]),
	       true
	      )),
	%% delete_address seems to be buggy, don't know why !
	%% delete_address(Addr),
	%% record_without_doublon( edge_disable(Addr) ),
	true
	.

clean_sentence :-
	%%	fail,
	%% should work for lightverbs derived from synt_head (active)
	%% eg: il lui pr�te main forte
	%% should add something for passive cases
	%% eg: main forte lui est pr�t�
	'D'(ncpred,A),
	recorded( 'C'(A,
		      lemma{ top => ncpred{ lightverb  => Light },
			     lemma => Lemma,
			     lemmaid => LemmaId,
			     anchor => Anchor::tag_anchor{ name => HT::ht{ diathesis => Dia,
									   cat => Cat,
									   refl => Refl,
									   arg1 => Arg1,
									   arg2 => Arg2::arg{ kind => Kind2 },
									   arg0 => Arg0, % subject
									   distrib => Distrib
									 }
							 }
			   },
		      B)),
	\+ var(Light),
	( fail, Kind2 == (-) ->
	  XArg1= Arg2,
	  XArg2= Arg1
	;   
	  XArg1= Arg1,
	  XArg2= Arg2
	),
	'D'(v,C),
	recorded( KK:: 'C'(C, lemma{ cat => v,
				     top => Top::v{ lightverb => _Light },
				     lex => Lex,
				     lemma => Light,
				     lemmaid => LightId,
				     truelex => TLex,
				     %%				lemma => Lemma,
				     anchor => tag_anchor{ name => ht{ diathesis => Dia::active,
								       %% arg1 => arg{ kind => _Arg1Kind },
								       cat => Cat
								     },
							   coanchors => []
							 }
				   },
			   D),
		  Addr),
	D =< A,
	var(_Light),
	_Light = Light,
	record_without_doublon( K::'C'(C,
				       lemma{ cat => v,
					      top => Top,
					      lex => Lex,
					      truelex => TLex,
				%			      lemma => toto,
				%			      lemma => Lemma,
					      lemma => Light,
				%	      lemmaid => LightId,
					      lemmaid => Light,
					      anchor => tag_anchor{ name => ht{ diathesis => Dia,
										cat => Cat,
										refl => Refl,
										arg0 => Arg0,
										arg1 => XArg1,
										arg2 => XArg2,
										distrib => Distrib
									      },
								    equations => [],
								    coanchors => [LemmaId]
								  }
					    },
				       D) ),
	record( added(K) ),
%%	format('Adding ~w\n',[K]),
	true
	.

:-finite_set(lex_noun_det,[un,une,la]).

%MR-22102019: commented block under
/*
clean_sentence :-
	%% remove spurious noun or pronoun interpretations for determiners
	%% such as 'un' 'une' ...
	%% these ones could be filtered using a tagger prior to parsing
	'D'(Cat::cat[pro,nc],A),
	recorded( KK::'C'(A,
			  lemma{ lex => Lex :: lex_noun_det[],
				 cat => Cat
			       },
			  B) ),
%%	domain(Cat,[pro,nc]),
	recorded( 'C'(B,lemma{ cat => Cat2::cat[nc,adj,np]},C) ),
%%	domain(Cat2,[nc,adj,np]),
%%	format('Remove ~w\n',[KK]),
	erase(KK)
	.
*/	

%% *** TMP HACK for causative
%% until a correct entry for faire is added in Lefff
clean_sentence :-
	%%	fail,
	'D'(v,A),
	recorded( 'C'(A,
		      lemma{ lex => Lex,
			     cat => v,
			     lemma => Lemma,
			     truelex => TrueLex,
			     top => Top::v{ lightverb => (-), diathesis => active }
			   },
		      B)
		),
	domain(Lemma,[faire,laisser]),
	Anchor = tag_anchor{
			    name => ht{arg0 => arg{kind => kind[subj,(-)],
						   pcas => (-),
						   function => subj
						  },
				       arg1 => arg{kind => kind[vcompcaus],
						   pcas => (-),
						   function => obj
						  },
				       arg2 => arg{ kind => kind[(-)],
						    pcas => (-),
						    function => (-)
						  },
				       diathesis => active,
				       cat => v,
				       imp => (-),
				       refl => (-)},
			    coanchors => [],
			    equations => [] },
	record_without_doublon(
			       KK::'C'(A,
				       lemma{ lex => Lex,
					      cat => v,
					      lemma => Lemma,
					      truelex => TrueLex,
					      top => Top,
					      anchor => Anchor
					    },
				       B)
			      ),
%	format('Added ~w\n',[KK]),
	true
	.


%% *** TMP HACK for progressive aspect with aller
%% until a correct entry for aller is added in Lefff
clean_sentence :-
	%%	fail,
	'D'(v,A),
	recorded( 'C'(A,
		      lemma{ lex => Lex,
			     cat => v,
			     lemma => aller,
			     truelex => TrueLex,
			     top => Top::v{ lightverb => (-), diathesis => active }
			   },
		      B)
		),
	Anchor = tag_anchor{
			    name => ht{arg0 => arg{kind => kind[subj,(-)],
						   pcas => (-),
						   function => subj
						  },
				       arg1 => arg{kind => kind[vcompprog],
						   pcas => (-),
						   function => obl
						  },
				       arg2 => arg{ kind => kind[(-)],
						    pcas => (-),
						    function => (-)
						  },
				       diathesis => active,
				       cat => v,
				       imp => (-),
				       refl => (-)},
			    coanchors => [],
			    equations => [] },
	record_without_doublon(
			       KK::'C'(A,
				       lemma{ lex => Lex,
					      cat => v,
					      lemma => aller,
					      truelex => TrueLex,
					      top => Top,
					      anchor => Anchor
					    },
				       B)
			      ),
%%	format('Added ~w\n',[KK]),
	true
	.


%% tmp hack for ilimp which is not always precise
clean_sentence :-
	'D'(ilimp,A),
	\+ 'D'(cln,A),
	recorded( 'C'(A,
		      lemma{ lex => Lex,
			     cat => ilimp,
			     lemma => ilimp,
			     truelex => TrueLex
			     },
		      B)
		),
	record( 'C'(A,
		    lemma{
			  lex => Lex,
			  truelex => TrueLex,
			  lemma => cln,
			  cat => cln,
			  top  => cln{case => nom,
				      gender => masc,
				      number => sg,
				      person => 3},
			  anchor  => tag_anchor{ name => ht{arg0 => arg{kind => (-), pcas => (-)}, arg1 => arg{kind => (-), pcas => (-)}, arg2 => arg{kind => (-), pcas => (-)}, refl => (-)}, coanchors => [], equations => [] }
		   },
		    B)
	      ),
	true
	.

clean_sentence :-
	'D'(cln,A),
	recorded( 'C'(A,
		      lemma{
			    lex => Lex,
			    truelex => TrueLex,
			    lemma => cln,
			    cat => cln,
			    top  => cln{case => nom,
					gender => masc,
					number => sg,
					person => 3}
			   },
		      B)
		),
	\+ 'D'(ilimp,A),
	domain(Lex,[il,'-il','-t-il']),
	'D'(clr,_C),
	domain(C,_C),
	C < A + 2,
	record( 'C'(A,
		    lemma{ lex     => Lex,
			   truelex => TrueLex,
			   lemma   => ilimp,
			   lemmaid => ilimp,
			   cat     => ilimp,
			   top     => ilimp{gender => masc, number => sg, person => 3},
			   anchor  => tag_anchor{ name => _, coanchors => [], equations => [] }
			 },
		    B
		   )
		)
	.

clean_sentence :-
	'D'(cln,A),
	recorded( 'C'(A,
		      lemma{
			    lex => Lex,
			    truelex => TrueLex,
			    lemma => cln,
			    cat => cln,
			    top  => cln{case => nom,
					gender => masc,
					number => sg,
					person => 3}
			   },
		      B)
		),
        \+ 'D'(ilimp,A),
	domain(Lex,[il,'-il','-t-il']),
	'D'(cat[adj,v],C),
	recorded( 'C'(C,
		      lemma{
			      cat => cat[adj,v],
			      anchor => tag_anchor{ name => ht{ imp => (+) }}
			  },
		      _
		)),
	C < A + 3,
        C > A,
        \+ recorded( 'C'(A,lemma{ cat => ilimp },_) ),
%%	format('adding ilimp at pos ~w\n',[A]),
	record( 'C'(A,
		    lemma{ lex     => Lex,
			   truelex => TrueLex,
			   lemma   => ilimp,
			   lemmaid => ilimp,
			   cat     => ilimp,
			   top     => ilimp{gender => masc, number => sg, person => 3},
			   anchor  => tag_anchor{ name => _, coanchors => [], equations => [] }
			 },
		    B
		   )
		)
	.

%% some cleaner to remove impossible or very unlikey sequences

%MR-22102019: commented block under
/*
clean_sentence :-
%%	fail,
	%% cln followed by a verb (or aux) is very probable
	%% in the case, we remove the alternante readings
	%% but beware of 'je soussign� ...'
	%% and nous nous trouvons ...
	'D'(cln,Left),
	'C'(Left,lemma{ cat => cln },Right),
	'C'(Right,lemma{ cat => cat[aux,v] },_),
	erase( 'C'(Right, lemma{ cat => cat[~ [aux,v]] },_) ),
	%% if cln=tu: erase verb reading for it !
	('D'(v,Left) ->
	 format('erasing verb reading at ~w\n',[Left]),
	 erase( 'C'(Left,lemma{ cat => v },Right) )
	;
	 true
	)
	.
*/

%MR-22102019: commented block under
/*
clean_sentence :-
	fail,
	%% not activated:
	%% - block some agreement mismatches
	%% eg: elles sont venus
	%% - block some truly acceptable sequences
	%% eg: c'�tait partie perdue
	%%
	%% furthermore, doesn't not seem to reduce the number of items
	'D'(v,Left),
	'C'(Left,
	    lemma{ cat => aux,
		   top => aux{ form_aux => Aux }},
	    Right),
	'C'(Right,
	    lemma{ cat => v,
		   lex => Lex,
		   top => v{ mode => participle,
			     aux_req => Aux			     
			   }
		 },
	    _),
	% \+ domain(Lex,[partie,parties]),
	\+ 'C'(Right,lemma{ cat => cat[adv,advneg] },_),
	erase('C'(Left,lemma{ cat => v },Right) ),
	true
	.
*/

%MR-22102019: commented blocks under
/*
clean_sentence :-
%%	fail,
	'D'(clneg,Left),
	'C'(Left,lemma{ cat => clneg },Right),
	erase('C'(Right,lemma{ cat => cat[~ [aux,v,cla,clg,cll,clr,cld,cld12,cld3,advneg,adv,epsilon,sbound,meta]] },_))
	.

clean_sentence :-
%%	fail,
	'D'(clr,Left),
	'C'(Left,lemma{ cat => clr }, Right),
	erase('C'(Right,lemma{ cat => cat[~ [aux,v,cla,clg,cll,clr,cld,cld12,cld3,epsilon,sbound,meta]] },_))
	.

clean_sentence :-
%%	fail,
	'D'(Cat::cat[cla,cld,cld12,cld3],Left),
	'C'(Left,lemma{ cat => Cat },Right),
	\+ 'C'(Left,lemma{ cat => cat[~ [cla,cld,cld12,cld3]] },_),
	erase('C'(Right,lemma{ cat => cat[~ [aux,v,cla,clg,cll,clr,cld,cld12,cld3,epsilon,sbound,meta]] },_))
	.

clean_sentence :-
%%	fail,
	'D'(Cat::cat[cla,cld,cld12,cld3],Left),
	recorded('C'(Left,lemma{ cat => Cat },Right), Addr),
	\+ 'D'(cat[aux,v,cla,cld,cld12,cld3,clg,cll,adv],Right),
	%% adv: old French: facile de les mieux nourrir
	( Left == 0 -> true
	; Left1 is Left-1,
	  \+ 'C'(Left1,
		 lemma{ cat => v,
			top => v{ mode => imperative }
		      },
		 Left
		),
	  \+ 'C'(Left1,
		 lemma{ cat => cat[cla,cld,cld12,cld3,clg,cll] },
		 Left )
	),
%%	format('delete cl at ~w\n',[Left]),
	delete_address(Addr)
	.
*/

clean_sentence :-
	'D'(prep,Left),
	'C'(Left,lemma{ cat => prep, lemma => de },Right),
	'C'(Right,lemma{ cat => v, top => v{ mode => infinitive } },_),
	erase('C'(Left,lemma{ cat => det },Right))
	.

clean_sentence :-
%%	fail,
	'D'(csu,Left),
	'C'(Left,lemma{ cat => csu, lemma => Lemma },Right),
	\+ domain(Lemma,['de ce que']),
	Middle is Left + 1,
	Right > Middle,
	erase( 'C'(Left,lemma{ cat => cat[~ [adv,advneg]] },_) )
	.

clean_sentence :-
	'D'(title,Left),
	'C'(Left,lemma{ cat => title}, Right),
	erase( 'C'(Left,lemma{ cat => np, lemma => '_PERSON' },Right) )
	.

%% for hour expressions such as "entre midi et deux"
%% would be better to recognize these expressions in sxpipe ?
clean_sentence :-
	'D'(nc,Left),
	'C'(Left,lemma{ cat => nc, lemma => Lemma},L1),
	domain(Lemma,[midi,minuit]),
	'C'(L1,lemma{ cat => coo, lemma => et},L2),
	'C'(L2,lemma{ cat => nc,
		      lemma => '_NUMBER',
		      lex => Lex,
		      truelex => TrueLex,
		      lemmaid => LemmaId,
		      anchor => Anchor
		    },L3),
	record( 'C'(L2,
		    lemma{ cat =>nc,
			   lemma => '_NUMBER',
			   lex => Lex,
			   truelex => TrueLex,
			   lemmaid => LemmaId,
			   anchor => Anchor,
			   top => nc{ time => artf}
			 },
		    L3
		   )
	      )
	.

%% agglutinate sequence of np built on the same token
clean_sentence :-
	'D'(np,Left),
	'C'(Left,
	    XL::lemma{ cat => np,
		       lex => Lex,
		       truelex => TrueLex,
		       lemmaid => LemmaId,
		       top => Top,
		       anchor => Anchor
		     },
	    Right),
	follow_compound_np(Right,TrueLex,Right2),
	\+ recorded( 'C'(Left,lemma{ cat => np },Right2) ),
	record( 'C'(Left,XL,Right2) )
	.

%% agglutinate sequence of nc built on the same token when there is a dash
clean_sentence :-
	'D'(nc,P1),
	'C'(P1,
	    lemma{ cat => nc,
		   lex => Lex1,
		   lemma => Lemma1,
		   truelex => TrueLex,
		   top => Top1,
		   anchor => Anchor1
		 },
	    P2
	   ),
	'C'(P2,
	    lemma{ cat => nc,
		   lex => Lex2,
		   lemma => Lemma2,
		   truelex => TrueLex
		 },
	    P3
	   ),
	\+ 'C'(_,
	       lemma{ truelex => TrueLex },
	       P1
	      ),
	\+ 'C'(P1,
	       lemma{ truelex => TrueLex },
	       P3
	      ),
	'T'(TrueLex,_TrueLex),
	rx!tokenize(_TrueLex,'-',[_,_]),
	name_builder('~w-~w',[Lex1,Lex2],Lex),
	name_builder('~w-~w',[Lemma1,Lemma2],Lemma),
	record_without_doublon( 'C'(P1,
				    lemma{ cat => nc,
					   lex => Lex,
					   lemma => Lemma,
					   lemmaid => Lemma,
					   truelex => TrueLex,
					   top => Top1,
					   anchor => Anchor1
					 },
				    P3
				   )
			      )
	.

%% agglutinate sequence with shared last token for _NUMMBER (eg.: 20 millions)
%% to fix a pb in old DAGs !
clean_sentence :-
    'D'(number,P1),
    recorded( 'C'(P1,
		 Entry::lemma{ cat => Cat, %::cat[~ [det]],
			       lex => '_NUMBER',
			       lemma => '_NUMBER',
			       lemmaid => '_NUMBER',
			       top => Top,
			       anchor => Anchor,
			       truelex => TLex::[_|_]
			     },
		 P2
		),
	     Addr
	   ),
    ( 'C'(P2,
	  lemma{ truelex => TLex2 },
	  P3
	 ),
      domain(TLex2,TLex) ->
	  record_without_doublon(K::'C'(P1,Entry,P3)),
%	  format('register ~w\n',[K]),
	  delete_address(Addr),
	  true
     ;
     fail
    ),
    true
	.


%% tmp: de l' as partitive det (missing in SxPipe)
clean_sentence :-
	'D'(det,L),
	'C'(L,lemma{ lemma => de, cat => prep, truelex => TL1 },M),
	'C'(M,lemma{ lex => 'l''', truelex => TL2},R),
	( TL1 = TL2 ->
	  TrueLex = TL1
	;
	  truelex_concat(TL1,TL2,TrueLex)
	),
	record( 'C'(L,
		    lemma{ lex => 'de l''',
			 truelex => TrueLex,
			   lemma => du,
			   lemmaid => du,
			   cat => det,
			   top => det{def => '-', det => (+), number => sg},
			   anchor => tag_anchor{ name => _, coanchors => [], equations => [] }
			 },
		    R)
	      )
	.

%% tmp: some compound nouns such as 'chauffeur - routier" are recognized as sequences rather than multi-word expression
%% (a pb with sxpipe)
clean_sentence :-
	'D'(nc,P1),
	'C'(P1,lemma{ cat => nc, truelex => TL1, lex => L1, lemma => Lemma1, top => Top, anchor => Anchor }, P2),
	'C'(P2,
	    lemma{ cat => ponctw,
		   lex => '-',
		   lemma => Lemma2,
		   truelex => TL2
		 },
	    P3
	   ),
	'C'(P3,lemma{ cat => nc, lex => L3, truelex => TL3, lemma=> Lemma3},P4),
	\+ 'C'(P1,lemma{},P4),
	\+ 'C'(P3,lemma{ cat => cat[det,prep,coo] },_),
	truelex_concat(TL2,TL3,_TrueLex),
	truelex_concat(TL1,_TrueLex,TrueLex),
	name_builder('~w - ~w',[L1,L3],Lex),
	name_builder('~w~w~w',[Lemma1,Lemma2,Lemma3],Lemma),
	record_without_doublon( 'C'(P1,
				    K::lemma{ cat => nc,
					      truelex => TrueLex,
					      lex => Lex,
					      lemma => Lemma,
					      lemmaid => Lemma,
					      top => Top,
					      anchor => Anchor
					    },
				    P4)
			      ),
%	format('addded l=~w K=~w\n',[P1,K]),
	true
	.
	
%% some sequences <np nc> should be recognized as np (such as UIMM actualit� or Paris plage)
clean_sentence :-
	'D'(np,P1),
	'C'(P1,lemma{ cat => np, truelex => TL1, lex => L1, lemma => Lemma1, top => Top }, P2),
	(	K1:: 'C'(P2,
			lemma{ cat => nc,
			       lex => L2,
			       lemma => Lemma2,
			       truelex => TL2
			     },
			P3
		       )
         ;
	 recorded(reserved(K1))
        ),
	\+ 'C'(P1,lemma{},P3),
	\+ 'C'(P2,lemma{ cat => cat[det,prep,coo,adj,aux] },_),
	\+ domain(Lemma2,['_DATE_artf','_DATE_arto','_DATE_def','_DATE_idef','_DATE_year','_HEURE',
			  '_EMAIL',
			  '_DIMENSION',
			  '_TEL',
			  '_URL'
			 ]),
	% ('C'(_P1,lemma{ cat => det },P1) ->
	%  ('C'(_,lemma{ cat => prep },P1) xor fail)
	% ;
	%  true
	% ),
	(P1 \== 0 xor \+ 'C'(0,lemma{ cat => cat[~ [np]] },_)),
	\+ 'C'(P1,lemma{ cat => cat[det,prep,pro,nc,adj,v,adv] },_),
	%%	name_builder('~w ~w',[TL1,TL2],TrueLex),
%	TrueLex = [TL1,TL2],
	truelex_concat(TL1,TL2,TrueLex),
	name_builder('~w ~w',[L1,L2],Lex),
	record_without_doublon( 'C'(P1,
				    K::lemma{ cat => np,
					      truelex => TrueLex,
					      lex => Lex,
					      lemma => 'Uw',
					      lemmaid => 'Uw',
					      top => np{}
					    },
				    P3)
			      ),
	record_without_doublon( pseudo_lex(P1,K,P3) ),
%	format('addded l=~w K=~w\n',[P1,K]),
	true
	.
 
:-std_prolog truelex_concat/3.

truelex_concat(TL1,TL2,TL3) :-
	( TL1 = [A|B] ->
	  truelex_concat(B,TL2,XB),
	  TL3 = [A|XB]
	; TL1 = [] ->
	  ( TL2 = [_|_] -> TL3 = TL2
	  ; TL3 = [TL2]
	  )
	; TL2 = [_|_] ->
	  TL3 = [TL1|TL2]
	; TL3 = [TL1,TL2]
	)
	.


%% some sequences <_Uw _Uw> should be recognized as np 
clean_sentence :-
	'D'(np,P1),
	'C'(P1,lemma{ cat => np,
		      truelex => TL1,
		      lex => L1,
%		      lemma => 'Uw',
		      top => Top
		    },
	    P2),
	( K1:: 'C'(P2,
	    lemma{ cat => np,
		   lex => L2,
%		   lemma => 'Uw',
		   truelex => TL2
		 },
	    P3
		 ) ; recorded(reserved(K1))
	),
	\+ ('C'(P1,lemma{},_P3), _P3 >= P3),
	\+ 'C'(P2,lemma{ cat => cat[det,prep,coo,adj,aux] },_),
	(P1 \== 0 xor \+ 'C'(0,lemma{ cat => cat[~ [np]] },_)),
%	\+ 'C'(P1,lemma{ cat => cat[det,prep,pro,nc,adj,v,adv] },_),
	truelex_concat(TL1,TL2,TrueLex),
	name_builder('~w ~w',[L1,L2],Lex),
	record_without_doublon( 'C'(P1,
				    K::lemma{ cat => np,
					      truelex => TrueLex,
					      lex => Lex,
					      lemma => 'Uw',
					      lemmaid => 'Uw',
					      top => np{}
					    },
				    P3)
			      ),
%	format('addded l=~w K=~w\n',[P1,K]),
	true
	.

clean_sentence :-
	'D'(aux,P1),
	'C'(P1,lemma{ cat => aux, truelex => Truelex, lex => a},P2),
	\+ 'C'(P1,lemma{ cat => prep, lex => �}, P2),
	'C'(P2,lemma{ cat => v, top => v{ mode => infinitive }},_),
	record_without_doublon( 'D'(prep,P1) ),
	record_without_doublon( 'C'(P1,
				        lemma{ lex     => '�',
					       truelex => Truelex,
					       lemma   => '�',
					       lemmaid => '�',
					       cat     => prep,
					       top     => prep{pcas => prep[loc,'�']},
					       anchor  => tag_anchor{ name => ht{arg0 => arg{function => obj,
											     kind => kind[acomp,sadv,scomp,vcomp,obj],
											     pcas => (-)},
										 arg1 => arg{extracted => (-), function => (-), kind => (-), pcas => (-), real => (-)},
										 arg2 => arg{extracted => (-), function => (-), kind => (-), pcas => (-), real => (-)}, distrib => (-), refl => (-)}, coanchors => [], equations => [] }
	 },
				    P2
				   )
			      )
	.

clean_sentence :-
	'D'(v,P1),
	'C'(P1,lemma{ cat => v, lemma => sembler, truelex => TrueLex, lex => semble},P2),
	'C'(P2,lemma{ cat => cln, lex => Lex::'-t-il'},P3),
	\+ 'D'(ilimp,P2),
	record_without_doublon( 'C'(P2,
				    lemma{ cat => ilimp,
					   truelex => TrueLex,
					   lex => Lex,
					   lemmaid => ilimp,
					   lemma => ilimp,
					   top     => ilimp{gender => masc, number => sg, person => 3},
					   anchor  => tag_anchor{ name => _, coanchors => [], equations => [] }
					 },
				    P3
				   )
			      )
	.

clean_sentence :-
    'D'(ponctw,P1),
    'C'(P1,lemma{ lemma => '(', truelex => T1 },P2),
    'C'(P2,lemma{ cat => Cat,
		  lemma => Lemma,
		  lemmaid => LemmaId,
		  truelex => T2,
		  top => Top,
		  anchor => Anchor,
		  lex => Lex
		},
	P3),
    'C'(P3,lemma{ lemma => ')', truelex => T3},P4),
    domain(Cat,[cln,cla,cld,clr,pri,prel,coo,det]),
    XT = [T1,T2,T3],
    record_without_doublon(
	    K::'C'(P1,
		lemma{ cat => Cat,
		       lex => Lex,
		       lemma => Lemma,
		       lemmaid => LemmaId,
		       truelex => XT,
		       top => Top,
		       anchor => Anchor
		     },
		P4
		  )),
    record_without_doublon('D'(Cat,P1)),
    %%    format('add ~w\n',[K]),
    true
	.

%% some noums linked to adjectives seem to be missing the right gender
%% e.g.: espion exist as a feminne noum in Lefff but not as a maculine one
clean_sentence :-
    'D'(adj,P1),
    'D'(nc,P1),
    'C'(P1,lemma{ lemma => Lemma,
		  lemmaid => LemmaId,
		  lex => Lex,
		  cat => adj,
		  truelex => TLex,
		  top => adj{ gender => Gender1, number => Number }
		},
	P2),
    'C'(P1,lemma{ lemma => Lemma,
		  lemmaid => LemmaId,
		  lex => Lex,
		  cat => nc,
		  truelex => TLex,
		  top => Top::nc{ gender => Gender2, number => Number },
		  anchor => Anchor
		},
	P2),
    \+ Gender1 = Gender2,
     record_without_doublon(
	     'C'(P1,
		 lemma{ lemma => Lemma,
			lemmaid => LemmaId,
			lex => Lex,
			cat => nc,
			truelex => TLex,
			anchor => Anchor,
			top => nc{ gender => Gender1, number => Number }
		      },
		 P2
		))
	.

%% sometimes dashes '-' are not joined with the preceding or following words, leading to pb
clean_sentence :-
    'D'(v,P1),
    'C'(P1,lemma{ cat => v, lemma => VLemma },P2),
    'C'(P2, lemma{ truelex => T2 },_P3),
    'T'(T2,'-'),
    ('C'(_P3,lemma{ cat => sbound, truelex => T2 },P3) xor P3=_P3),
%    format('try add inverted subj ~w\n',[P2]),
    ('C'(P3, lemma{ lemma => Lemma,
		   lemmaid => LemmaId,
		   lex => Lex,
		   cat => cln,
		   truelex => TLex,
		   top => Top,
		   anchor => Anchor
		  },
	 P4) ->
	 record_without_doublon(
		 'C'(P2, lemma{ lemma => Lemma,
				lemmaid => LemmaId,
				lex => Lex,
				cat => cln,
				truelex => [T2,TLex],
				top => Top,
				anchor => Anchor
			      },
		     P4)
	     )
     ; 'C'(P3,lemma{ truelex => T3 },P4),
       'T'(T3,'t'),
       'C'(P4,lemma{ truelex => T4 },P5),
       'T'(T4,'-'),
       'C'(P5, lemma{ lemma => Lemma,
		      lemmaid => LemmaId,
		      lex => Lex,
		      cat => cln,
		      truelex => TLex,
		      top => Top,
		      anchor => Anchor
		    },
	   P6),
%      format('add inverted subj ~w ~w ~w vlemma=~w lex=~w\n',[P2,P6,[T2,T3,T4,TLex],VLemma,Lex]),
       record_without_doublon(
	       'C'(P2, lemma{ lemma => Lemma,
			      lemmaid => LemmaId,
			      lex => Lex,
			      cat => cln,
			      truelex => [T2,T3,T4,TLex],
			      top => Top,
			      anchor => Anchor
			    },
		   P6)
	   ),
       (VLemma = sembler,
	Lex = il ->
%	    format('add ~w\n',[K]),
	    record_without_doublon(
		    K::'C'(P2, lemma{ lemma => ilimp,
				   lemmaid => ilimp,
				   lex => Lex,
				   cat => ilimp,
				   truelex => [T2,T3,T4,TLex],
				   top => ilimp{gender => masc, number => sg, person => 3},
				   anchor => tag_anchor{ name => _, coanchors => [], equations => [] }
				 },
			P6)
		)
	;
	true
       )
    )
	.

%MR-14112019: commented block under
/*
clean_sentence :-
    'D'(v,P1),
    'C'(P1,lemma{ cat => v, top => v{ mode => imperative }},P2),
    'C'(P2, lemma{ truelex => T2 },_P3),
    'T'(T2,'-'),
    ('C'(_P3,lemma{ cat => sbound, truelex => T2 },P3) xor P3=_P3),
    'C'(P3, lemma{ lemma => Lemma,
		   lemmaid => LemmaId,
		   lex => Lex,
		   cat => Cat,
		   truelex => TLex,
		   top => Top,
		   anchor => Anchor
		 },
	P4),
    domain(Cat,[cla,cld,clg,cll]),
    record_without_doublon(
	    'C'(P2, lemma{ lemma => Lemma,
			   lemmaid => LemmaId,
			   lex => Lex,
			   cat => Cat,
			   truelex => [T2,TLex],
			   top => Top,
			   anchor => Anchor
			 },
		P4)
	)
	.
*/

%% some gerundive behaves as adjective but have no such entries in Lefff
clean_sentence :-
    fail,
    %% do not work well: the creation of the adjective comes too late to activate the trees
    %% it seems better to add some missing entries in missing.lex 
    %% (and think about a productive rule in lefff or in the meta-grammar)
    'D'(v,C),
    recorded( 'C'(C,
		  lemma{ cat => v,
			 top => v{ diathesis => active, mode => gerundive},
			 lex => Lex,
			 truelex => TLex,
			 lemma => TLemma,
			 lemmaid => TLemmaId,
			 anchor => tag_anchor{ name => ht{ arg1 => arg{} } }
		       },
		  D)
	    ),
    \+ recorded( 'C'(C,lemma{ cat => adj, lex => Lex },D)),
    format('adding ~w\n',[K]),
    record_without_doublon( K:: 'C'(C,
				    lemma{ cat => adj,
					   top => adj{ gender => masc, number => sg},
					   lex => Lex,
					   truelex => TLex,
					   lemma => TLemma,
					   lemmaid => TLemmaId,
					   anchor => tag_anchor{ name => ht{
								     distrib => (-),
								     refl => (-)},
								 coanchors => [],
								 equations => [] }},
				    D
				   )
			  ),
    record(added(K)),
    record_without_doublon('D'(adj,C)),
    true
	.

    
:-std_prolog follow_compound_np/3.

follow_compound_np(Left,TrueLex,Right) :-
    ( K::'C'(Left,lemma{ truelex => TrueLex },Middle)
     ;
     recorded(reserved(K))
    ),
    ( follow_compound_np(Middle,TrueLex,Right) xor Middle = Right )
.

:-std_prolog name_builder/3.

name_builder(Format,Args,Name) :-
        ( string_stream(_,S),
	  format(S,Format,Args),
	  flush_string_stream(S,Name),
	  close(S)
	xor
	   format('** pb name builder: ~w ~w ~w\n',[Format,Args,Name]),
	   close(S),
	   fail
	)
        .

:-extensional autoload_check_hypertag/3.

autoload_check_hypertag(v,ht{},ht{ arg1 => arg{ kind => npredobj } }).

:-std_prolog anchor_hypertag/2.

anchor_hypertag(ht{ anchor => Anchor }, Anchor ).

:-std_prolog record_without_doublon/1.

record_without_doublon( A ) :-
        (   recorded( A ) xor record( A ))
        .


:-std_prolog tag_autoload_adj/7.

tag_autoload_adj(Cat,Top,Bot,Left,Right,Left1,Right1) :-
	( Cat = 'Infl' ->
	  ( \+ (\+ Bot = 'Infl'{ mode => adjective }) ->
	    %% format('Search autoload adj cat=~w left=~w left1=~w bot =~w\n',[Cat,Left,Left1,Bot]),
	    'C'(Left,
		Lemma::lemma{ cat => v,
			      anchor => tag_anchor{
						   name => ht{ arg1 => arg{ kind => Kind1 },
							       arg2 => arg{ kind => Kind2} }}
			    },
		_Left1
	       ),
	    %%	    format('Found0 autoload_adj ~w left=~w lemma=~w\n',[Cat,_Left,Lemma]),
	    (Kind1 = acomp xor Kind2 = acomp),
	    Right=Right1,
	    term_range(_Left1,1000,Left1),
	    %%	    format('Found1 autoload_adj ~w left=~w lemma=~w\n',[Cat,_Left,Lemma]),
	    true
	  ;
	    Left=Left1,
	    Right=Right1
	  )
	;
	    Left=Left1,
	    Right=Right1
	)
	.


:-light_tabular special_lctag/2.
:-mode(special_lctag/2,+(+,-)).

special_lctag(Name,Left) :-
	'C'(M,lemma{ cat => Cat, lex => Lex },Left),
	( Cat = coo
	;
	  Lex = (','),
	  K is Left+100,
	  term_range(Left,K,Range),
	  'C'(Range,lemma{ cat => coo },_)
	),
	domain(Name,[follow_coord_aux,follow_coord_aux2])
	.

special_lctag('33 empty_spunct shallow_auxiliary',Left) :-
	( recorded( 'N'(Left) )
	xor 'C'(Left,lemma{ lex => ponct['�','"'] },_)
	)
	.

:-std_prolog check_extra_constraints/6.

check_extra_constraints(Cat,Top,HyperTag,Coanchors,Left,Right) :-
	( fail,
	  Cat == v ->
	  Top = v{ mode => Mode, lightverb => Light },
	  (	Light \== (-),
		Coanchors = [LemmaId] ->
		%%		format('test lightverb ~w left=~w lemma=~w ht=~w\n',[Light,Left,LemmaId,HyperTag]),
		( 'C'( _Left,
		       lemma{ lemmaid => LemmaId },
		       _Right
		     ),
		  Right =< _Left,
		  _Left =< Right + 5
		xor fail
		),
%%		format('found at ~w\n',[_Left]),
		true
	    ;	
		true
	    ),
	    HyperTag = ht{ arg1 => arg{ extracted => X1, pcas => PCas1, real => Real1, kind => Kind1 },
			   arg2 => arg{ extracted => X2, pcas => PCas2, real => Real2, kind => Kind2 },
			   arg0 => arg{ extracted => X0, pcas => PCas0, real => Real0, kind => Kind0 },
			   diathesis => Diathesis
			 },
% 	  format('Test extra left=~w pcas1=~w pcas2=~w kind1=~w kind2=~w real1=~w real2=~w light=~w X0=~w X1=~w X2=~w\n',
% 		 [Left,PCas1,PCas2,Kind1,Kind2,Real1,Real2,Light,X0,X1,X2]
% 		),
	    (	%fail,
		Diathesis = active,
		\+ (X1 = arg[~ [-]]),
		\+ (Mode=imperative),
		\+ ( Kind1 = (-)),
		\+ (PCas1 == (-)),
%%		Real1 = cat[~ [cla,cld3,cld12,clr,clg,cll,-]],
		\+ ( test_some_clitics(_XR1),_XR1 =< Left)
	    ->	
		( test_some_prep(PCas1,_L1), Right =< _L1 xor fail)
	    ;	
		true
	    ),
	    (	Diathesis = passive,
		\+ (X0 = arg[~ [-]]),
		\+ (Kind0 = (-)),
		\+ (PCas0 == (-)),
		Real0 = cat[~ [cla,cld3,cld12,clr,clg,cll,-]]
	    ->	
		( test_some_prep(PCas0,_L0), Right =< _L0 xor fail )
	    ;	
		true
	    ),
	  %%	  format('real2=~w pcas2=~w\n',[Real2,PCas2]),
	    (	Diathesis = active,
		\+ (X2 = arg[~ [-]]),
		\+ (Mode=imperative),
		\+ ( Kind2 = (-)),
		\+ (PCas2 == (-)),
%%		Real2 = cat[~ [cla,cld3,cld12,clr,clg,cll,-]],
		\+ ( test_some_clitics(_XR2), _XR2 =< Left )
	    ->
		%% format('Test prep ~w at ~w x2=~w real2=~w\n',[PCas2,Right,X2,Real2]),
		( test_some_prep(PCas2,_L2), Right =< _L2 xor fail )
	    ;	
		true
	    ),
%%	    format('activated left=~w mode=~w ht=~w\n',[Left,Mode,HyperTag]),
	    true
	;   
	    true
	)
	.

:-light_tabular test_some_clitics/1.
:-mode(test_some_clitics/1,+(-)).

test_some_clitics(Right) :-
	'D'(Cat,Left),
	'C'(Left,lemma{ cat => Cat::cat[clr,clg,cll,cld3,cld12] },Right).

:-light_tabular test_some_prep/2.
:-mode(test_some_clitics/2,+(+,-)).

test_some_prep(Prep,Left) :-
	'D'(prep,Left),
	'C'(Left,lemma{ cat => prep, top => prep{ pcas => Prep } },_)
	.

%% distance constraints
%% should be generated by analyzing a corpus
%% we provide tmp approx

distance_constraint(det,3).
distance_constraint(nc,6).
distance_constraint(np,5).
distance_constraint(adv,5).
distance_constraint(advneg,5).
distance_constraint(aux,5).
distance_constraint(prep,3).
distance_constraint(csu,3).
distance_constraint(pro,3).
distance_constraint(predet,3).
distance_constraint(pri,5).
distance_constraint(xpro,3).
distance_constraint(adjPref,3).
distance_constraint(suffAdj,3).
distance_constraint(number,5).
distance_constraint(ce,3).
distance_constraint(pres,3).

:-extensional blocking_anchors/2.

%blocking_anchors('86 adj_as_cnoun anchor:agreement n:agreement',[nc,np]).
%blocking_anchors('87 anchor:agreement n:agreement cnoun_as_noun_mod_name shallow_auxiliary',[adj]).

%:-light_tabular register_answer/1.

register_answer.

:-dyalog_ender(my_ender).

:-std_prolog my_ender/0.

my_ender :-
	xargv(L),
	( domain('-stats',L),
	  table_analysis
	;
	  fail,
	  domain('-disamb',L),
	  true
	)
	.

:-std_prolog table_analysis/0.

:-require 'forest.pl'.

table_analysis :-
	format('table analysis\n',[]),
	every((
	       recorded( O ),
	       ( O='*RITEM*'(Call,Ret), Kind = 'ritem'
	       ; O='*SACITEM*'(Call), Kind = 'sacitem'
	       ; O='*RITEM*'(Call,Ret) :> _, Kind = 'trans'
	       ),
	       ( Call =.. [Pred,Pos|_], number(Pos),
		 \+ domain(Pred,[tag_filter])
	       ; Call = verbose!coanchor(_,Left,Pos,_,_), Pred = coanchor
	       ; Call = verbose!anchor(_,Left,Pos,_,_,_,_), Pred = anchor
	       ; Call = verbose!lexical(_,Left,Pos,_,_), Pred = lexical
	       ),
	       (recorded(tablecount(Pos,Kind,Pred,M))
	       xor mutable(M,0),
		record(tablecount(Pos,Kind,Pred,M))
	       ),
	       mutable_read(M,V),
	       W is V + 1,
	       mutable(M,W)
	      )),
	recorded('N'(N)),
	every(( term_range(0,N,D),
		domain(Pos,D),
		recorded( tablecount(Pos,Kind,Pred,M) ),
		mutable_read(M,V),
		format('\t~w ~w ~w: ~w\n',[Pos,Kind,Pred,V])
	      )),

	every(( recorded(O,Addr),
		\+ domain(O,['*RITEM*'('call_tab_item_term/2',_),
			     '*RITEM*'(tag_filter(_),_),
			     '*RITEM*'(clean_sentence,_),
			     lctag(_,_,_,_),
			     lctag_ok(_),
			     lctag_map_reverse(_,_,_),
			     lctag_map(_,_,_),
			     loaded_tree(_),
			     anchor_point(_,_,_),
			     require_anchor(_),
			     autoload_check_hypertag(_,_,_),
			     forest!forest_type(_,_),
			     '$allpos'(_),
			     'C'(_,_,_),
			     'D'(_,_),
			     'N'(_),
			     'S'(_),
			     tablecount(_,_,_,_),
			     ok_cat(_,_,_),
			     ok_tree(_,_),
			     distance_constraint(_,_)
			    ]),
		( O = (Head :> _) ->
		  PO = (Head :> [])
		;
		  PO = O
		),
		format('~w [label="~w"];\n',[Addr,PO]),
%%		format('*** handling ~w\n',[O]),
		forest!forest(Addr,Forest),
		Forest = and(Add_X,Y),
		( Y = LY:Add_Y xor Y = Add_Y ),
		( Add_X = 0 xor format('~w -> ~w;\n',[Add_X,Addr]) ),
		( Add_Y = 0 xor	format('~w -> ~w;\n',[Add_Y,Addr]) ),
		true
	      )),
	
	true
	.


%% a way to register something to trigger at the very end
:-xcompiler
do_at_end(G) :- (wait(true), G, fail; true).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% insertion of exotic.tag for advanced management of coordinations

/*
%% Forward subst only on simple pos
my_subst_handler(P,Bot,Left,Right,N) :-
	Left = gen_pos(_,_),
	Right = gen_pos(_,_),
%%	format('Trying ~w ~w ~w ~w\n',[P,Left,Right,N]),
	fail,
	tag_phrase( '$protect'(id=N and bot=Bot at P), Left, Right ),
	%% The next two lines should be commented and replaced by more
	%% complex conditions if one wish to synchronize derivations
	%% inside substitued P
	%% For instance for: Pierre mange une pomme rouge et Paul une verte
	number(Left),
	number(Right),
%	format('Found ~w ~w ~w\n',[P,Left,Right,N]),
	true
	.
*/

%% Setting a watch coord
?-watch_coord,fail.

watch_coord :-
	xargv(Argv),
	domain('-exotic',Argv),
	record(exotic_on),
	'D'(coo,Left),
	'C'(_Left,lemma{ cat => coo, lemma => Lemma},Right),
	domain(Lemma,[et,ou,mais]),
%%	format('Detect coo at ~w ~w\n',[Left,Right]),
	NT='S',			% restrict to S-coords
	( 'C'(Left,lemma{ cat => ponctw, lex => ','},_Left) xor _Left = Left ),
	tag_phrase('$answers'(NT),Left1,Left),
	number(Left1),
	number(Left),
	Left1 > Left - 20,
%	format('Try coord for ~w left1=~w left=~w right=~w\n',[NT,Left1,Left,Right]),
	%% Here: should be completed to retrieve derivation (using forest predicates)
	%% and use the information (anchor, tree, ...) to build more complex gen_pos
	tag_phrase(NT,gen_pos(Right,Left1),gen_pos(Right2,Left)),
	Right2 > Right,
%	format('Found coord for ~w left1=~w left=~w right=~w right2=~w\n',[NT,Left1,Left,Right,Right2]),
	true
	.

%% Try to reuse a coordination handled by watch_coord
/*
my_subst_handler(P,Bot,Left,Right,N) :-
	tag_phrase('$answers'(id=N at P),Left,Right1),
	tag_phrase(id=coord at <=> coo,Right1,Left2),
	tag_phrase('$answers'(id=N at P),gen_pos(Left2,Left),gen_pos(Right,Right1)),
	true
	.
*/


my_subst_handler(P,Bot,Left,Right,N) :-
	recorded(exotic_on),
	P='S',
	number(Left),
	_Right is Left-1,
	'C'(_Right,lemma{ cat => coo },Left),
	( 'C'(_Right2,lemma{ cat => ponctw, lex => ',' },_Right) xor _Right2 = _Right),
	tag_phrase('$answers'(id=N and bot=Bot at P),gen_pos(Left,_Left),gen_pos(Right,_Right2)),
%	format('found nt=~w left=~w right=~w\n',[P,gen_pos(Left,_Left),gen_pos(Right,_Right)]),
	true
	.

/*
%% Try ellipsis on subject in coord 
my_subst_handler(P::np,
		 Bot::np,
		 Left::gen_pos(L1,L2),
		 Right::gen_pos(L1,R2),
		 N::subject
		) :-
	format('Ellipsing subject at ~w ~w\n',[Left,Right]),
	tag_phrase('$answers'(id=N and bot=Bot at P),L2,R2),
	tag_phrase(id=trace at <=> trace,L1,L1),
	format('Found ellipsed subject at ~w ~w\n',[Left,Right])
%%	tag_phrase('$protect'(id=N at P),L2,R2)
	.
*/


%% Use already recognized constituants before coordination
my_subst_handler(P,
		 Bot,
		 Left::gen_pos(L1,L2),
		 Right::gen_pos(R1,R2),
		 N
		) :-
	number(L2),
	number(L1),
	L2 < L1-1,
	( tag_phrase('$answers'(id=duplicate and bot=Bot at P),L2,R2)
	; N==subject,
	  '$answers'(verbose!coanchor(_,L2,R2,cln{},_))
	),
	R2 < L1,
%%	tag_phrase('$protect'(id=N at P),L2,R2),
%%	format('try sync ~w (~w,~w) at ~w bot=~w N=~w\n',[P,L2,R2,L1,Bot,N]),
	tag_phrase('$protect'(id=N and bot=_Bot at P),L1,R1),
	L1 < R1,
	(Bot = _Bot xor true),
	true
%	tag_phrase(id=N and bot=Bot at P,L1,R1)
	.


my_subst_handler(P,
		 Bot,
		 Left::gen_pos(L1,L2),
		 Right::gen_pos(L1,R2),
		 subject
		) :-
				% ellipsis on subject !
	number(L2),
	number(L1),
	L2 < L1-1,
	tag_phrase('$answers'(id=subject and bot=Bot at P),L2,R2),
	R2 < L1,
	L2 < R2,
	true
	.

my_subst_handler(P,
		 Bot::'N2'{ wh => (+) },
		 Left::gen_pos(L1,L2),
		 Right::gen_pos(L1,R2),
		 object
		) :-
				% ellipsis on object when interrogative
	number(L2),
	number(L1),
	L2 < L1-1,
	tag_phrase('$answers'(id=object and bot=Bot at P),L2,R2),
	R2 < L1,
	L2 < R2,
	true
	.


authorized_ellipsis(v).
authorized_ellipsis(aux).
authorized_ellipsis(pri).
%authorized_ellipsis(prel).

coanchor_authorized_ellipsis(cln).
coanchor_authorized_ellipsis(cld).
coanchor_authorized_ellipsis(cla). % il le trouve idiot et elle pr�tentieux
coanchor_authorized_ellipsis(clg). % il en mange deux et elle, trois.
coanchor_authorized_ellipsis(clr). % il se souvient de sa maison et elle de son �cole.
coanchor_authorized_ellipsis(pri).
coanchor_authorized_ellipsis(prel).

coanchor_authorized_ellipsis(ncpred).


%% exit server mode
?- 
   '$interface'('DyALog_Server_Active',[]),
   recorded(exit_server),
   format('closing server. bye !\n',[]),
   exit(1),
   true
.

:-std_prolog dump_lattice/1.

dump_lattice(I) :-
	recorded('N'(N)),
	I < N,
	every(( 'C'(I,lemma{ lex => Lex,
			     lemma => Lemma,
			     cat => Cat,
			     truelex => _TLex,
			     top => Top
			   },J),
		(_TLex = [_|_] -> TLex = _TLex ; TLex = [_TLex]),
		( Cat = cat[v,aux] ->
		  conll_fullcat(Cat,'V',OId,N,FullCat,_)
		;
		  FullCat = conll_fullcat[~ ['V','VINF','VPP','VS','VIMP','VPR']]
		),
		mutable(MFeat,[],true),
		every((
		       Top \== [],
		       domain(F,[tense,person,number,mode]),
		       inlined_feature_arg(Top,F,_,V),
				%	       \+ (F=mode, FullCat = conll_fullcat['VPP','VPR']),
		       \+ ( F=tense,
			    FullCat \== conll_fullcat['VPP','VPR'],
			    (inlined_feature_arg(Top,_Mode,_,conditional), _Mode = mode)),
		       \+ ( Cat=cat[adv,advneg,prep],
			    domain(F,[number,gender,person])
			  ),
		       feature2depconll(F,V,FV,FullCat),
		       mutable_list_extend(MFeat,FV)
		      )),
		mutable_read(MFeat,_MSTag),
		conll_fvlist2fv(_MSTag,MSTag),
		format('~L\t~L\n',[['~w','\t'],[I,J,Lex,Lemma,Cat,Cat,MSTag],['~w','+'],TLex])
	      )),
	K is I+1,
	dump_lattice(K)
	.

:-light_tabular potential_start_coma/1.
:-mode(potential_start_coma/1,+(+)).

%% finding potential missing starting coma
potential_start_coma(I) :-
    fail,
    %    format('try potential start comma at ~w\n',[I]),
    I > 0,
    'C'(I,lemma{ cat => cat[prep,adv]},_),
    'C'(_,lemma{ cat => prep},I),
%    format('potential start comma at ~w\n',[I]),
    true
	.

