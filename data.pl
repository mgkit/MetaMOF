/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2015 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  data.pl -- Lexical data for disamb and conversions
 *
 * ----------------------------------------------------------------
 * Description
 *  Disambiguation and conversion may require some lexical data to
 *  take decisions.
 *  We collect these lists in this file
 * ----------------------------------------------------------------
 */

:-include 'header.tag'.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Lexical properties

:-subset( nominal, cat[nc,cln,pro,np,pri,prel,ncpred,xpro,ce,ilimp,caimp] ).
:-subset( xnominal, cat[nc,cln,pro,np,pri,prel,ncpred,adj,xpro,ce,ilimp,caimp] ).

:-subset( terminal, cat[~ [-,'CS','N','N2','PP','S','V']] ).

:-finite_set(unknown,['uw','_Uw']).
:-finite_set(true_time,[arto,artf,arti]).
:-finite_set(date,['_DATE_arto','_DATE_artf','_DATE_year','_DATE_def','_DATE_indef']).

:-finite_set(entities,['_PERSON',
		       '_PERSON_m',
		       '_PERSON_f',
		       '_PRODUCT',
		       '_ORGANIZATION',
		       '_COMPANY',
		       '_NP',
		       '_NP_WITH_INITIALS',
		       '_LOCATION',
		       '_NUMBER',
		       '_NUM',
		       '_ROMNUM',
		       '_PATENTID'
		      ]).

:-subset(number,entities['_NUMBER','_NUM','_ROMNUM']).

:-finite_set(numerals,[premier,deux,second,trois,quatre,cinq,six,sept,huit,neuf,dix,onze,douze,treize,quatorze,quinze,seize,
		       vingt,trente,quarante,cinquante,soixante
		      ]).

:-finite_set(de_form,[de,du,des,'de la','de l''']).

:-finite_set(million,['millions','milliards','million','milliard','centaine','centaines','millier','milliers',dizaine,'dizaines']).
:-finite_set(month,[janvier,f�vrier,mars,avril,mai,juin,juillet,ao�t,septembre,octobre,novembre,d�cembre]).
:-finite_set(day,['lundi','mardi','mercredi','jeudi','vendredi','samedi','dimanche']).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Agglutinates

:-extensional agglutinate/3.

agglutinate('�','le__det','au').
agglutinate('�','les__det','aux').
agglutinate('�','le','au').
agglutinate('�','les','aux').
agglutinate('�','lequel','auquel').
agglutinate('�','lesquels','auxquels').
agglutinate('�','lesquelles','auxquelles').
agglutinate('de','le__det','du').
agglutinate('de','le','du').
agglutinate('de','lequel','duquel').
agglutinate('de','lesquels','desquels').
agglutinate('de','lequelles','desquelles').
agglutinate('de','les__det','des').
agglutinate('en','les__det','�s').
agglutinate('de','les','des').
agglutinate('en','les','�s').
agglutinate('�','ledit','audit').
agglutinate('�','lesdits','auxdits').
agglutinate('�','lesdites','audites').
agglutinate('de','ledit','dudit').
agglutinate('de','lesdits','dudits').
agglutinate('de','lesdites','auxdites').

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Conversion of pronouns for Passage/Easy

:-extensional pri/2.

pri('combien?','GR').
pri('quand?','GR').
%%pri('o�?','GP').
pri('o�?','GR').
pri('pourquoi?','GR').
pri('comment?','GR').
pri('commentComp?',GR).

:-extensional prel/2.

prel('dont','GP').
prel('o�','GP').

%% Prep:

%% v
%% pendant

%% nc

%% adj

%% favor adv with following verb rather than with aux

%% verify MOD-N entre dans 'quel X' 'chaque X'

%% verifier status de combien

%% Verifier MOD-N sur Revues dans am:33

%% Etrange SUJ-V sur am:43

%% Mauvais COMP dans am:54 + beaucoup d'autres phenomenes etranges

%% Verifier entre COD-V ou CPL-V pour "de Sinf"

%% Eviter interpretations de "prep que' comme GP et preferer 'csu' donc COMP

%% Renforcer encore "GN de GN" plutot que argument de verbe (COD-V)

%% am:243 traitement coordonant ternaire

%% am:244 coord sur ADV

%% am:246 coord sur ADJ

:-extensional not_a_prep/1.

not_a_prep('il y a').

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Some preferences to select a part-of-speech for some words

:-extensional catpref/4.

catpref(que,_,csu,1000).
catpref(comme,_,csu,500).
catpref(mais,mais,coo,1000).
catpref(donc,donc,coo,1000).
catpref(plus,plus,adv,1500).
catpref(pas,pas,nc,-1000).
catpref(_,plus,coo,-1000).
catpref(aussi,aussi,adv,1000).
catpref(pour,pour,prep,1000).
catpref(alors,alors,adv,1000).
%catpref(avoir,avoir,v,1000).
%catpref(avoir,avoir,aux,1000).
catpref(concernant,concerner,v,1000).
catpref(la,_,nc,-1000).
catpref('par exemple',_,adv,3000).
catpref('eh bien',_,pres,2000).
catpref(avec,avec,prep,1000).
catpref(cent,_,det,1000).
catpref('au moins',_,adv,3000).
%%catpref('en particulier',_,adv,2000).
catpref('alors',_,adv,100).
catpref('m�me',_,adv,100).
catpref('m�me',_,pro,-1000).
catpref('d'' abord',_,adv,100).
catpref('en outre',_,adv,3000).
%%catpref('en effet',_,adv,2000).
catpref('� peine',_,adv,3000).
catpref('surtout',_,adv,2000).
catpref('maintenant',maintenant,adv,1000).
catpref('trop',trop,adv,1000).
catpref('sans doute',_,adv,2000).
catpref('en m�me temps',_,adv,2000).
catpref('sans cesse',_,adv,2000).
catpref('de m�me',_,adv,2000).
catpref('au mieux',_,adv,2500).
catpref('tout � l'' heure',_,adv,2500).
catpref('tout � fait',_,adv,2000).
%%catpref('en vigueur',_,adv,3000).
catpref(_,'afin de',prep,4000).
%%catpref(_,'� partir de',prep,2000).
%%catpref(_,'autour de',prep,1000).
catpref(_,'au dehors',adv,1500).
%%catpref(_,'au dehors de',prep,1000).
catpref('candidat','candidat',nc,1000).
catpref(_,'de plus en plus',adv,5000).
catpref(_,'de moins en moins',adv,5000).
catpref(_,'quelque chose',pro,3000).
catpref(_,'quelqu''un',pro,3000).
catpref(_,'fille',nc,500).
catpref(_,'dernier',adj,100).
catpref(_,'prochain',adj,200).
catpref(_,'europ�en',adj,1000).
catpref('il y a','il y a',prep,-2000).
catpref('en vain','en vain',adv,3000).
catpref(_,'autre',adj,500).
%%catpref(_,'en revanche',adv,2000).
catpref(_,'conform�ment �',prep,-2000).
%%catpref(_,'par ailleurs',adv,2000).
catpref('tout d''abord',_,adv,3000).
catpref(_,'en moyenne',adv,2000).
catpref(_,'d''ailleurs',adv,3000).
catpref(pas,pas,advneg,1000).
catpref(_,'par contre',adv,3000).
catpref(_,pendant,prep,2000).
catpref(_,'pr�s de',prep,2000).
catpref(_,'non',adv,2000).
catpref(_,'oui',adv,2000).
catpref('m�me','m�me',adv,800).
%%catpref(_,'d''accord',adv,1000).
%% catpref('avoir','avoir',nc,-500).
catpref(_,sur,prep,800).
catpref(_,mort,cat[adj,nc],500).

catpref(_,durant,prep,2000).
catpref(est,est,cat[nc],-1000).

catpref(_,'de plus',adv,1600).
catpref(_,tout,nc,-1000).

%% rather adj than past participle
catpref(_,clos,adj,1500).
catpref(_,'mort',adj,1500).
catpref(_,'maudit',adj,1500).
catpref(_,suivant,adj,200).
catpref(_,cependant,adv,1500).
catpref(nombre,_,v,-2000).
catpref(nombres,_,v,-2000).
%%catpref(_,si,csu,1000).
catpref(face,face,ncpred,500).	% to counterbalance 'face �' in faire face �

catpref(_,'ce que',csu,-2000).

catpref(_,'� ce que',csu,-5000).
catpref(_,'de ce que',csu,-5000).


catpref(_,aussi,csu,-4000).
catpref(_,si,adv,1200).
catpref(bref,bref,nc,-200).
catpref(�tre,�tre,nc,-200).
%%catpref(_,'plus du',det,-500).
catpref(_,plus,predet,1000).	%as strong as adv version
%% � m�me de: prep

catpref(_,tel,adj,1000).

%% ?? 'quelque chose de p�trifi�' construction specifique sur 'quelque chose de'

%% run27 lemonde:63 etrange !
%% run27 lemonde:78
%%       lemonde:189
%%       lemonde:205 double sujet dont un sur aux

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% To rewrite some specialized cat into more abstract ones

:-extensional  cat_abstract/2.

cat_abstract(ilimp,cln).
cat_abstract(caimp,cln).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Passage compound form -- October 2009

:-extensional passage_compound/1.

passage_compound('un peu').
passage_compound('quelque peu').
passage_compound('un petit peu').
passage_compound('pas du tout').
passage_compound('quelque fois').
passage_compound('nulle part').
passage_compound('bien entendu').
passage_compound('le cas �ch�ant').
passage_compound('autrement dit').
passage_compound('y compris').
passage_compound('peut-�tre').
passage_compound('n''est-ce pas').
passage_compound('� vrai dire').
passage_compound('� verse').
passage_compound('un tant soit peu').
passage_compound('qui plus est').
passage_compound('le cas �ch�ant').
passage_compound('autrement dit').
passage_compound('y compris').
passage_compound('peust-estre').
passage_compound('tout �  fait').
passage_compound('tout de m�me').
passage_compound('tout de suite').
passage_compound('tout � l''heure').
passage_compound('tout d''abord').
passage_compound('tout � coup').
passage_compound('tout d''un coup').
passage_compound('tout au plus').
passage_compound('tout � l''heure').
passage_compound('tout � fait').
passage_compound('tout-�-fait').
passage_compound('tout le temps').
passage_compound('tout le long').
passage_compound('peu � peu').
passage_compound('petit � petit').
passage_compound('tour � tour').
passage_compound('pied � pied').
passage_compound('mot � mot').
passage_compound('peu � peu').
passage_compound('�a et l� ').
passage_compound('bel et bien').
passage_compound('a priori').
passage_compound('a posteriori').
passage_compound('a fortiori').
passage_compound('de facto').
passage_compound('grosso modo').
passage_compound('bis').
passage_compound('ex aequo').
passage_compound('a priori').
passage_compound('le moins').
passage_compound('le plus').
passage_compound('le mieux').
passage_compound('l�-dessus').
passage_compound('l�-dedans').
passage_compound('ci-dessus').
passage_compound('ci-apr�s').
passage_compound('ci-dessous').
passage_compound('l�-dessous').
passage_compound('l�-bas').
passage_compound('quand m�me').
passage_compound('du tout').
passage_compound('de suite').
passage_compound('par contre').
passage_compound('pour de bon').
passage_compound('d''ici peu').
passage_compound('de rechef ').
passage_compound('au jour le jour').
passage_compound('ainsi de suite').
passage_compound('bon an mal an').
passage_compound('tel quel').
passage_compound('afin de').
passage_compound('� commencer par').
passage_compound('� compter de').
passage_compound('� fleur de').
passage_compound('� m�me ').
passage_compound('� moins de').
passage_compound('� partir de').
passage_compound('� savoir').
passage_compound('� travers').
passage_compound('� raison de').
passage_compound('aupr�s de').
passage_compound('au vu et au su de').
passage_compound('en ce qui concerne').
passage_compound('face �').
passage_compound('faute de').
passage_compound('gr�ce �').
passage_compound('histoire de').
passage_compound('le long de').
passage_compound('lors de').
passage_compound('quant �').
passage_compound('quitte �').
passage_compound('sans compter').
passage_compound('sauf �').
passage_compound('suite �').
passage_compound('tout au long de').
passage_compound('vis-�-vis de ').


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Preferences for attachement

%% should use a SQLite database rather than this enumeration
%% now the case with -restrictions and -restrictions2 (see features.pl)

:-extensional prep_pref/4.

prep_pref(mois,nc,apr�s,100). %% 5
prep_pref(infection,nc,par,100). %% 4
prep_pref(d�bat,nc,sur,100). %% 4
prep_pref(consid�ration,nc,dans,100). %% 4
prep_pref(acc�s,nc,�,100). %% 4
prep_pref(bonjour,nc,�,100). %% 4
prep_pref(relations,nc,avec,100). %% 3
prep_pref(pr�l�vement,nc,sur,100). %% 3
prep_pref(mail,nc,sur,100). %% 3
prep_pref(adh�sion,nc,�,100). %% 3
prep_pref(accord,nc,avec,100). %% 3
prep_pref(merci,nc,pour,100). %% 3
prep_pref(�largissement,nc,�,100). %% 2
prep_pref(soutien,nc,�,200). %% 2
prep_pref(relation,nc,entre,200). %% 2
prep_pref(passage,nc,�,200). %% 2
prep_pref(opposant,nc,�,200). %% 2
prep_pref(livre,nc,sur,200). %% 2
prep_pref(invitation,nc,�,200). %% 2
prep_pref(donn�e,nc,sur,200). %% 2
prep_pref(contraste,nc,avec,200). %% 2
prep_pref(continuit�,nc,avec,200). %% 2
prep_pref(confiance,nc,�,200). %% 2
prep_pref(avis,nc,sur,200). %% 2
prep_pref(attention,nc,sur,200). %% 2
prep_pref(aide,nc,�,200). %% 2
prep_pref(�quilibre,nc,entre,200). %% 1

%% for PP on AdjP, we have to counter the negative effect of -PP_ON_ADJP
prep_pref(ok,adj,pour,630). %% 7
prep_pref(partant,adj,pour,630). %% 4
prep_pref(relatif,adj,�,630). %% 3
prep_pref(pr�t,adj,�,630). %% 3
prep_pref(proche,adj,de,630). %% 3
prep_pref(n�cessaire,adj,de,630). %% 3
prep_pref(inf�rieur,adj,�,630). %% 3
prep_pref(responsable,adj,de,630). %% 2
prep_pref(pr�sent,adj,dans,630). %% 2
prep_pref(naturel,adj,de,630). %% 2
prep_pref(issu,adj,de,630). %% 2
prep_pref(indispensable,adj,pour,630). %% 2
prep_pref(favorable,adj,�,630). %% 2
prep_pref(disponible,adj,dans,630). %% 2
prep_pref(disponible,adj,sur,630). %% 2
prep_pref(digne,adj,de,630). %% 2
prep_pref(contraire,adj,�,630). %% 2
prep_pref(capable,adj,de,630). %% 2
prep_pref(atteint,adj,de,630). %% 2
prep_pref(accessibles,adj,�,630). %% 2
prep_pref(�pris,adj,de,630). %% 1
prep_pref(seul,adj,avec,630). %% 2
prep_pref(utilisable,adj,par,630). %% 1
prep_pref(utilisable,avec,par,630). %% 1
prep_pref(utile,adj,de,630). %% 1
prep_pref(sup�rieure,adj,�,630). %% 1
prep_pref(suffisant,adj,pour,630). %% 1
prep_pref(sourd,adj,�,600). %% 1
prep_pref(sensible,adj,�,600). %% 1
prep_pref(r�ticents,adj,�,630). %% 1
prep_pref(riche,adj,en,600). %% 1
prep_pref(respectueux,adj,de,630). %% 1
prep_pref(repr�sentatif,adj,de,600). %% 1
prep_pref(mort,adj,de,630). %% 1
prep_pref(impossible,adj,de,600). %% 1
prep_pref(identique,adj,�,630). %% 1
prep_pref(heureux,adj,de,600).
prep_pref(gauche,adj,de,600). %% 1
prep_pref(droit,adj,de,200). %% 1

%% Some preferences for adj before nouns
:-extensional ante_adj_pref/2.

ante_adj_pref(petit,200).
ante_adj_pref(cher,200).
ante_adj_pref(grand,200).
ante_adj_pref(bon,200).
ante_adj_pref(premier,200).
ante_adj_pref(dernier,200).
ante_adj_pref(second,200).
ante_adj_pref(prochain,200).
ante_adj_pref(jeune,200).
ante_adj_pref(pauvre,200).
ante_adj_pref(vieil,200).
ante_adj_pref(beau,200).
ante_adj_pref(nouveau,200).
ante_adj_pref(ancien,200).
ante_adj_pref(gros,200).
ante_adj_pref(principal,200).
ante_adj_pref('_NUMBER',200).
%% extracted from frwiki (PassageEval2/DEP3)
ante_adj_pref(autre,200). %% ratio=88.93% n=82384
ante_adj_pref(nombreux,200). %% ratio=83.89% n=41236
ante_adj_pref(seul,200). %% ratio=83.46% n=40808
ante_adj_pref('m�me',200). %% ratio=91.44% n=21008
ante_adj_pref(meilleur,200). %% ratio=89.95% n=19861
ante_adj_pref(haut,200). %% ratio=58.24% n=17795
ante_adj_pref(bon,200). %% ratio=87.75% n=16363
ante_adj_pref(beau,200). %% ratio=76.01% n=13442
ante_adj_pref(saint,100). %% ratio=55.40% n=13382
ante_adj_pref(certain,200). %% ratio=78.78% n=12415
ante_adj_pref(futur,200). %% ratio=68.39% n=8558
ante_adj_pref('v�ritable',200). %% ratio=88.97% n=8462
ante_adj_pref(double,200). %% ratio=65.07% n=7257
ante_adj_pref(super,200). %% ratio=82.92% n=6576
ante_adj_pref(vrai,200). %% ratio=70.35% n=5623
ante_adj_pref(mauvais,200). %% ratio=82.76% n=4699
ante_adj_pref(vaste,200). %% ratio=73.00% n=4170
ante_adj_pref(fameux,200). %% ratio=79.80% n=3841
ante_adj_pref(immense,200). %% ratio=70.91% n=3070
ante_adj_pref(faux,200). %% ratio=75.72% n=2998
ante_adj_pref(ultime,200). %% ratio=60.68% n=2986
ante_adj_pref(excellent,200). %% ratio=82.11% n=2929
ante_adj_pref('�norme',200). %% ratio=66.95% n=2720
ante_adj_pref(moindre,200). %% ratio=60.18% n=2358
ante_adj_pref(prochain,200). %% ratio=64.54% n=2098
ante_adj_pref(magnifique,100). %% ratio=58.81% n=2088
ante_adj_pref(bref,100). %% ratio=52.76% n=2009
ante_adj_pref(joli,200). %% ratio=73.80% n=1309
ante_adj_pref(triple,200). %% ratio=69.52% n=1135
ante_adj_pref(superbe,200). %% ratio=65.88% n=970
ante_adj_pref(pire,200). %% ratio=72.76% n=826
ante_adj_pref(formidable,100). %% ratio=55.83% n=763
ante_adj_pref(demi,200). %% ratio=61.41% n=583
ante_adj_pref(innombrable,200). %% ratio=66.86% n=513
ante_adj_pref(fervent,100). %% ratio=53.51% n=456
ante_adj_pref(brave,200). %% ratio=53.18% n=393
ante_adj_pref(vilain,200). %% ratio=69.46% n=334
ante_adj_pref(bienheureux,200). %% ratio=75.69% n=255
ante_adj_pref(abominable,100). %% ratio=58.93% n=168
ante_adj_pref(adorable,200). %% ratio=70.06% n=167
ante_adj_pref('pi�tre',200). %% ratio=88.55% n=166
ante_adj_pref('avant-dernier',200). %% ratio=82.53% n=166
ante_adj_pref('v�n�rable',100). %% ratio=53.42% n=146
ante_adj_pref(richissime,200). %% ratio=63.57% n=140
ante_adj_pref(ravissant,200). %% ratio=74.24% n=132
ante_adj_pref(maint,200). %% ratio=68.42% n=95
ante_adj_pref('�ni�me',200). %% ratio=62.92% n=89
ante_adj_pref('dix-septi�me',200). %% ratio=85.00% n=80
ante_adj_pref('dix-huiti�me',200). %% ratio=93.59% n=78
ante_adj_pref('dix-neuvi�me',200). %% ratio=90.41% n=73
ante_adj_pref(incorrigible,200). %% ratio=62.00% n=50
ante_adj_pref(inqualifiable,200). %% ratio=66.67% n=15

ante_adj_pref(large,200). %% ratio=49.08% n=14998


:-extensional rpref/4.

%% Added
rpref('Jeux Olympiques','de','_DATE_arto',243). %% 59367
rpref('viser','�','article',146). %% 21456
rpref('conseil','de','_DATE_arto',138). %% 19083
rpref('article','de','r�glement',132). %% 17566
rpref('pr�voir','�','article',115). %% 13441
rpref('naissance','en','_DATE_year',113). %% 12772
rpref('commission','de','_DATE_arto',109). %% 12006
rpref('entrer','en','vigueur',107). %% 11536
rpref('modifier','par','r�glement',106). %% 11309
rpref('droits','de','homme',102). %% 10450
rpref('page','dans','section',99). %% 9957
rpref('conform�ment','�','article',99). %% 9949
rpref('mise','en','oeuvre',99). %% 9927
rpref('proposition','de','commission',99). %% 9815
rpref('remplacer','par','texte',96). %% 9365
rpref('d�c�s','en','_DATE_year',95). %% 9125
rpref('mettre','en','oeuvre',91). %% 8405
rpref('viser','�','paragraphe',91). %% 8313
rpref('point','de','vue',90). %% 8161
rpref('commission','de','communaut�',89). %% 7969
rpref('membre','de','commission',87). %% 7607
rpref('avis','de','comit�',86). %% 7416
rpref('num�ro','de','conseil',85). %% 7358
rpref('d�cision','de','commission',83). %% 7032
rpref('directive','de','conseil',83). %% 6909
rpref('annexe','de','r�glement',81). %% 6670
rpref('obligatoire','dans','�l�ment',81). %% 6643
rpref('mettre','en','place',78). %% 6132
rpref('publication','�','journal',76). %% 5887
rpref('prendre','en','consid�ration',75). %% 5692
rpref('jour','de','publication',75). %% 5671
rpref('disposition','de','article',74). %% 5569
rpref('autorit�','de','�tat',74). %% 5520
rpref('num�ro','de','commission',74). %% 5481
rpref('paragraphe','de','r�glement',73). %% 5447
rpref('application','de','article',73). %% 5417
rpref('ordre','de','jour',73). %% 5334
rpref('d�cision','de','conseil',72). %% 5296
rpref('figurer','�','annexe',72). %% 5250
rpref('si�cle','avant','_Uv',70). %% 4966
rpref('modalit�','de','application',67). %% 4520
rpref('�tre','en','mesure',66). %% 4452
rpref('article','de','directive',66). %% 4435
rpref('million','de','euro',66). %% 4367
rpref('jour','suivant','celui',66). %% 4359
rpref('organisation','de','march�',65). %% 4330
rpref('entr�e','en','vigueur',65). %% 4319
rpref('mener','�','cat�gorie',65). %% 4243
rpref('veiller','�','ce',64). %% 4184
rpref('application','de','r�glement',64). %% 4173
rpref('voir','pour','article',63). %% 4045
rpref('chemin','de','fer',63). %% 3978
rpref('ville','de','_Uv',62). %% 3959
rpref('conform�ment','�','disposition',62). %% 3955
rpref('sortir','en','_DATE_year',62). %% 3893
rpref('r�glement','de','commission',62). %% 3845
rpref('article','de','trait�',62). %% 3844
rpref('viser','�','point',60). %% 3692
rpref('prendre','en','compte',60). %% 3634
rpref('conform�ment','�','proc�dure',60). %% 3602
rpref('d�c�s','en','_NUMBER',59). %% 3564
rpref('cours','de','ann�e',59). %% 3554
rpref('p�riode','de','enqu�te',59). %% 3546
rpref('r�publique','de','_LOCATION',59). %% 3522
rpref('march�','dans','secteur',59). %% 3517
rpref('r�glement','de','base',58). %% 3463
rpref('n�','de','commission',57). %% 3363
rpref('sens','de','article',57). %% 3362
rpref('r�glement','de','conseil',57). %% 3353
rpref('annexe','de','directive',57). %% 3321
rpref('communiquer','�','commission',57). %% 3298
rpref('vertu','de','article',56). %% 3241
rpref('pr�voir','�','r�glement',56). %% 3186
rpref('comit�','de','gestion',56). %% 3155
rpref('cours','de','p�riode',56). %% 3149
rpref('conseil','de','union',55). %% 3122
rpref('fin','en','_DATE_year',55). %% 3085
rpref('pays','en','d�veloppement',55). %% 3072
rpref('modifier','par','directive',54). %% 2960
rpref('site','de','_Uv',54). %% 2941
rpref('fin','de','ann�e',53). %% 2830
rpref('journal','de','communaut�',53). %% 2828
rpref('na�tre','en','_DATE_year',53). %% 2809
rpref('_DATE_year','de','conseil',52). %% 2791
rpref('nom','de','groupe',52). %% 2772
rpref('rapport','de','_PERSON_m',52). %% 2745
rpref('pr�sident','en','exercice',52). %% 2730
rpref('mettre','�','disposition',51). %% 2688
rpref('_NUMBER','�','_NUMBER',51). %% 2664
rpref('pr�sident','de','conseil',51). %% 2626
rpref('conseil','de','_LOCATION',51). %% 2624
rpref('�tre','de','accord',51). %% 2618
rpref('journal','de','_Uv',51). %% 2615
rpref('pager','dans','cat�gorie',50). %% 2577
rpref('voter','en','faveur',50). %% 2573
rpref('district','de','_Uv',50). %% 2572
rpref('ann�e','de','calendrier',50). %% 2562
rpref('pr�senter','de','int�r�t',50). %% 2561
rpref('aide','de','�tat',50). %% 2551
rpref('pr�sident','de','commission',50). %% 2523
rpref('disposition','de','r�glement',49). %% 2496
rpref('pr�senter','pour','_Uv',49). %% 2495
rpref('mise','en','place',49). %% 2485
rpref('province','de','_LOCATION',49). %% 2452
rpref('cour','de','justice',49). %% 2432
rpref('plan','de','action',49). %% 2414
rpref('l�gislation','de','�tat',48). %% 2396
rpref('�tre','dans','cas',48). %% 2390
rpref('naissance','en','_NUMBER',48). %% 2383
rpref('pr�voir','par','r�glement',48). %% 2381
rpref('pays','de','origine',48). %% 2340
rpref('exercice','de','conseil',48). %% 2340
rpref('importation','de','produit',48). %% 2336
rpref('provenance','de','pays',48). %% 2333
rpref('partir','de','_DATE_arto',48). %% 2309
rpref('nom','de','commission',48). %% 2304
rpref('remplacer','�','article',47). %% 2283
rpref('num�ro','sous','_Uv',47). %% 2258
rpref('r�gion','de','_Uv',47). %% 2222
rpref('notifier','sous','num�ro',46). %% 2207
rpref('sein','de','_Uv',46). %% 2193
rpref('certificat','de','importation',46). %% 2182
rpref('organisme','de','intervention',46). %% 2176
rpref('restitution','�','exportation',45). %% 2114
rpref('�tre','en','effet',45). %% 2113
rpref('sein','de',entities['_ORGANIZATION','_COMPANY'],45). %% 2112
rpref('�tre','en','train',45). %% 2100
rpref('proposition','de','r�solution',45). %% 2091
rpref('r�giment','de','infanterie',45). %% 2087
rpref('avis','de',entities['_ORGANIZATION','_COMPANY'],45). %% 2087
rpref('viser','�','annexe',45). %% 2079
rpref('paragraphe','de','directive',45). %% 2072
rpref('membre','de','_Uv',45). %% 2071
rpref('pourcent','de','prix',45). %% 2054
rpref('base','de','donn�e',45). %% 2044
rpref('pr�voir','�','paragraphe',44). %% 2018
rpref('strat�gie','de',entities['_ORGANIZATION','_COMPANY'],44). %% 2015
rpref('site','de',entities['_ORGANIZATION','_COMPANY'],44). %% 2012
rpref('destinataire','de','d�cision',44). %% 2005
rpref('sein','de','commission',44). %% 1995
rpref('communication','de','commission',44). %% 1987
rpref('satisfaire','�','exigence',44). %% 1965
rpref('membre','de','_ORGANIZATION',44). %% 1960
rpref('directive','de','commission',44). %% 1953
rpref('mise','sur','march�',44). %% 1945
rpref('publier','�','journal',43). %% 1914
rpref('index','de','cat�gorie',43). %% 1910
rpref('acc�s','dans','index',43). %% 1910
rpref('produit','de','p�che',43). %% 1900
rpref('ann�e','de','d�c�s',43). %% 1899
rpref('d�finir','�','article',43). %% 1891
rpref('part','de','march�',43). %% 1883
rpref('article','de','accord',43). %% 1851
rpref('milliard','de','euro',42). %% 1844
rpref('journal','de','_ORGANIZATION',42). %% 1838
rpref('destinataire','de','pr�sente',42). %% 1837
rpref('territoire','de','�tat',42). %% 1836
rpref('histoire','de','_LOCATION',42). %% 1828
rpref('importation','en','provenance',42). %% 1805
rpref('demande','de','certificat',42). %% 1789
rpref('�tre','en','_DATE_year',42). %% 1778
rpref('d�but','de','ann�e',41). %% 1745
rpref('valeur','de','mati�re',41). %% 1743
rpref('application','de','r�gime',41). %% 1741
rpref('application','de','disposition',41). %% 1735
rpref('proposition','de','directive',41). %% 1727
rpref('na�tre','�','_LOCATION',41). %% 1727
rpref('�tat','de','droit',41). %% 1726
rpref('commune','de','_Uv',41). %% 1719
rpref('championnat','de','monde',41). %% 1708
rpref('march�','de','travail',41). %% 1704
rpref('situer','dans','d�partement',41). %% 1694
rpref('annexe','de','d�cision',41). %% 1694
rpref('respect','de','droits',41). %% 1691
rpref('canton','de','_LOCATION',41). %% 1687
rpref('_PERSON_m','de','_LOCATION',40). %% 1676
rpref('journal','de','union',40). %% 1666
rpref('d�lai','de','mois',40). %% 1657
rpref('article','de','d�cision',40). %% 1655
rpref('protection','de','environnement',40). %% 1646
rpref('commission','de','affaire',40). %% 1634
rpref('mati�re','de','position',40). %% 1627
rpref('conformer','�','pr�sente',40). %% 1623
rpref('chronologie','de','si�cle',40). %% 1618
rpref('paragraphe','de','trait�',40). %% 1616
rpref('provenance','de','_LOCATION',40). %% 1614
rpref('huile','de','olive',40). %% 1610
rpref('p�riode','de','an',40). %% 1604
rpref('organisation','de','producteur',40). %% 1602
rpref('applicable','�','partir',40). %% 1601
rpref('gaz','de','�chappement',39). %% 1591
rpref('site','de','Quid',39). %% 1587
rpref('paragraphe','de','article',39). %% 1586
rpref('conseil','de','communaut�',39). %% 1586
rpref('trait�','de','_LOCATION',39). %% 1581
rpref('�ge','de','an',39). %% 1554
rpref('d�but','de','r�gne',39). %% 1552
rpref('conform�ment','�','annexe',39). %% 1549
rpref('conform�ment','�','paragraphe',39). %% 1548
rpref('disposition','pour','conformer',39). %% 1545
rpref('aliment','pour','animal',39). %% 1542
rpref('lier','�','commune',39). %% 1541
rpref('fois','de','plus',39). %% 1533
rpref('adh�sion','de','_LOCATION',39). %% 1531
rpref('prix','de','vente',39). %% 1530
rpref('fixer','�','article',39). %% 1530
rpref('mettre','en','�vidence',39). %% 1523
rpref('�num�rer','�','annexe',39). %% 1522
rpref('programme','de','action',39). %% 1521
rpref('groupe','de','travail',38). %% 1509
rpref('mettre','�','jour',38). %% 1507
rpref('localisation','sur','carte',38). %% 1503
rpref('taux','de','int�r�t',38). %% 1502
rpref('comit�','de','_Uv',38). %% 1501
rpref('viser','�','alin�a',38). %% 1493
rpref('transport','de','marchandise',38). %% 1490
rpref('r�pondre','�','question',38). %% 1481
rpref('mesure','de','possible',38). %% 1476
rpref('appel','de','offre',38). %% 1473
rpref('ministre','de','affaire',38). %% 1471
rpref('r�gion','de','_LOCATION',38). %% 1466
rpref('secteur','de','viande',38). %% 1465
rpref('destiner','�','couvrir',38). %% 1459
rpref('conseil','de','ministre',38). %% 1455
rpref('titre','de','article',38). %% 1454
rpref('violation','de','droits',38). %% 1446
rpref('r�gime','de','aide',37). %% 1437
rpref('�conomie','de','march�',37). %% 1435
rpref('type','de','v�hicule',37). %% 1432
rpref('comit�','de','r�gion',37). %% 1431
rpref('date','de','entr�e',37). %% 1427
rpref('�tat','de','_ORGANIZATION',37). %% 1422
rpref('disposition','de','paragraphe',37). %% 1419
rpref('v�hicule','�','moteur',37). %% 1416
rpref('mettre','en','vigueur',37). %% 1416
rpref('acte','de','adh�sion',37). %% 1414
rpref('�change','de','information',37). %% 1401
rpref('rapport','de','commission',37). %% 1392
rpref('pomme','de','terre',37). %% 1386
rpref('principe','de','subsidiarit�',37). %% 1384
rpref('millilitre','de','solution',37). %% 1384
rpref('certificat','de','exportation',37). %% 1370
rpref('source','de','�nergie',37). %% 1369
rpref('animal','de','esp�ce',36). %% 1368
rpref('Plan','de','_LOCATION',36). %% 1364
rpref('royaume','de','_LOCATION',36). %% 1357
rpref('sommet','de','_LOCATION',36). %% 1353
rpref('conseil','de',entities['_ORGANIZATION','_COMPANY'],36). %% 1352
rpref('reprendre','�','annexe',36). %% 1349
rpref('sud','de','_LOCATION',36). %% 1347
rpref('modifier','par','d�cision',36). %% 1347
rpref('chiffre','de','affaire',36). %% 1345
rpref('question','de','savoir',36). %% 1344
rpref('charte','de','droits',36). %% 1339
rpref('transmettre','�','commission',36). %% 1337
rpref('int�r�t','pour','_Uv',36). %% 1336
rpref('bataille','de','_LOCATION',36). %% 1327
rpref('valeur','�','importation',36). %% 1321
rpref('localisation','de','_LOCATION',36). %% 1320
rpref('d�partement','de','_LOCATION',36). %% 1313
rpref('�tat','de','_Uv',36). %% 1312
rpref('sein','de','union',36). %% 1307
rpref('partir','de','_NUMBER',36). %% 1298
rpref('championnat','de','_LOCATION',36). %% 1298
rpref('pr�sident','de','_Uv',35). %% 1293
rpref('disposition','de','directive',35). %% 1293
rpref('demander','�','commission',35). %% 1293
rpref('gouvernement','de','_LOCATION',35). %% 1292
rpref('montant','de','aide',35). %% 1289
rpref('�tablissement','de','cr�dit',35). %% 1288
rpref('prestation','de','service',35). %% 1286
rpref('protection','de','donn�e',35). %% 1285
rpref('d�cision','de','_DATE_arto',35). %% 1285
rpref('mettre','�','point',35). %% 1283
rpref('mettre','sur','march�',35). %% 1282
rpref('proposition','de','r�glement',35). %% 1281
rpref('membre','de','famille',35). %% 1262
rpref('campagne','de','commercialisation',35). %% 1262
rpref('droits','de','douane',35). %% 1261
rpref('pr�sident','de','_LOCATION',35). %% 1256
rpref('lutte','contre','terrorisme',35). %% 1255
rpref('application','de','directive',35). %% 1254
rpref('jour','de','ann�e',35). %% 1252
rpref('date','de','_DATE_arto',35). %% 1251
rpref('texte','de','int�r�t',35). %% 1246
rpref('num�ro','de','_DATE_arto',35). %% 1246
rpref('m�thode','de','analyse',35). %% 1242
rpref('pays','de','_Uv',35). %% 1234
rpref('�tre','�','origine',35). %% 1233
rpref('conclusion','de','accord',34). %% 1223
rpref('march�','de','produit',34). %% 1220
rpref('publier','en','_DATE_year',34). %% 1218
rpref('aller','�','encontre',34). %% 1214
rpref('expiration','de','d�lai',34). %% 1213
rpref('r�alisation','de','objectif',34). %% 1209
rpref('position','de','conseil',34). %% 1206
rpref('mesure','�','prendre',34). %% 1199
rpref('pr�voir','par','d�cision',34). %% 1197
rpref('disposition','de','droit',34). %% 1186
rpref('pr�voir','�','annexe',34). %% 1185
rpref('trait�','de','_ORGANIZATION',34). %% 1182
rpref('programme','de','travail',34). %% 1181
rpref('appliquer','dans','cas',34). %% 1180
rpref('syst�me','de','qualit�',34). %% 1177
rpref('politique','en','mati�re',34). %% 1174
rpref('politique','de','p�che',34). %% 1168
rpref('d�rogation','�','article',34). %% 1167
rpref('cadre','de','programme',34). %% 1167
rpref('arr�ter','par','�tat',34). %% 1164
rpref('d�put�','de','_LOCATION',34). %% 1160
rpref('pi�ce','de','th��tre',34). %% 1159
rpref('protection','de','consommateur',34). %% 1157
rpref('cadre','de','adjudication',34). %% 1157
rpref('joueur','de','rugby',33). %% 1150
rpref('accompagner','de','r�f�rence',33). %% 1142
rpref('�tat','de','union',33). %% 1139
rpref('droits','�','importation',33). %% 1137
rpref('�galit�','de','chance',33). %% 1135
rpref('viande','de','volaille',33). %% 1130
rpref('accompagner','de','publication',33). %% 1130
rpref('exigence','en','mati�re',33). %% 1127
rpref('autorit�','de','pays',33). %% 1126
rpref('navire','de','p�che',33). %% 1124
rpref('abbaye','de','_LOCATION',33). %% 1121
rpref('peine','de','mort',33). %% 1120
rpref('syst�me','de','contr�le',33). %% 1119
rpref('modalit�','de','r�f�rence',33). %% 1119
rpref('conform�ment','�','r�glement',33). %% 1116
rpref('titre','de','r�glement',33). %% 1112
rpref('r�pondre','�','exigence',33). %% 1110
rpref('commission','de','budget',33). %% 1108
rpref('faveur','de','rapport',33). %% 1102
rpref('mati�re','de','s�curit�',33). %% 1101
rpref('repr�sentant','de','�tat',33). %% 1100
rpref('d�livrance','de','certificat',33). %% 1098
rpref('secteur','de','c�r�ale',33). %% 1096
rpref('produit','�','base',33). %% 1095
rpref('cadre','de','_Uv',33). %% 1094
rpref('cadre','de','proc�dure',33). %% 1092
rpref('�tat','de','origine',33). %% 1091
rpref('proposition','de','amendement',32). %% 1088
rpref('commission','de','environnement',32). %% 1088
rpref('mettre','sur','pied',32). %% 1084
rpref('�tre','de','avis',32). %% 1084
rpref('r�pondre','�','condition',32). %% 1082
rpref('application','de','mesure',32). %% 1082
rpref('obtenir','�','partir',32). %% 1077
rpref('portant','de','application',32). %% 1074
rpref('�quipe','de','_LOCATION',32). %% 1072
rpref('pacte','de','stabilit�',32). %% 1069
rpref('adopter','dans','domaine',32). %% 1068
rpref('lecture','en','premier',32). %% 1067
rpref('nord','de','_LOCATION',32). %% 1066
rpref('rapprochement','de','l�gislation',32). %% 1065
rpref('cr�er','en','_DATE_year',32). %% 1064
rpref('pr�sident','de',entities['_ORGANIZATION','_COMPANY'],32). %% 1063
rpref('utiliser','�','fin',32). %% 1059
rpref('bureau','de','douane',32). %% 1058
rpref('accord','de','coop�ration',32). %% 1058
rpref('adh�sion','�','union',32). %% 1057
rpref('d�cision','de','_Uv',32). %% 1055
rpref('pr�judice','de','disposition',32). %% 1051
rpref('demande','de','aide',32). %% 1050
rpref('sommet','de',entities['_ORGANIZATION','_COMPANY'],32). %% 1048
rpref('suivre','de','accord',32). %% 1047
rpref('avoir','pour','objet',32). %% 1046
rpref('application','de','paragraphe',32). %% 1045
rpref('circulation','de','marchandise',32). %% 1042
rpref('ressortissant','de','pays',32). %% 1039
rpref('marge','de','dumping',32). %% 1038
rpref('�tre','de','nature',32). %% 1038
rpref('fin','de','compte',32). %% 1037
rpref('accord','de','association',32). %% 1037
rpref('ville','de','_LOCATION',32). %% 1035
rpref('chronologie','de','_Uv',32). %% 1034
rpref('r�aliser','par','_PERSON_m',32). %% 1032
rpref('�tat','de','_LOCATION',32). %% 1031
rpref('chronologie','de','_LOCATION',32). %% 1031
rpref('repr�sentant','de','commission',32). %% 1030
rpref('d�cision','de',entities['_ORGANIZATION','_COMPANY'],32). %% 1029
rpref('base','de','viande',32). %% 1028
rpref('inf�rieur','�','pourcent',32). %% 1027
rpref('�valuation','de','risque',32). %% 1026
rpref('�tat','de','accueil',32). %% 1025
rpref('respect','de','disposition',32). %% 1024
rpref('avoir','pour','effet',32). %% 1024
rpref('accord','de','_LOCATION',31). %% 1023
rpref('d�c�s','�','_Uv',31). %% 1021
rpref('sein','de','comit�',31). %% 1018
rpref('pr�sidence','de','conseil',31). %% 1017
rpref('joueur','de','tennis',31). %% 1016
rpref('�tre','�','�gard',31). %% 1016
rpref('conseil','de','s�curit�',31). %% 1016
rpref('cat�gorie','en','_DATE_year',31). %% 1016
rpref('personnalit�','en','_DATE_year',31). %% 1014
rpref('�tre','en','fait',31). %% 1014
rpref('niveau','de','protection',31). %% 1009
rpref('mener','�','bien',31). %% 1008
rpref('entreprise','de','assurance',31). %% 1007
rpref('d�cision','de','comit�',31). %% 1006
rpref('degr�','de','_NUMBER',31). %% 1005
rpref('soci�t�','de','information',31). %% 1003
rpref('avoir','pour','but',31). %% 1000
rpref('arr�ter','selon','proc�dure',31). %% 997
rpref('�tat','de','_NUMBER',31). %% 996
rpref('prix','�','exportation',31). %% 994
rpref('club','de','football',31). %% 993
rpref('agir','de','question',31). %% 993
rpref('octroi','de','aide',31). %% 992
rpref('proposer','par','commission',31). %% 991
rpref('prendre','en','charge',31). %% 988
rpref('p�riode','de','_DATE_arto',31). %% 988
rpref('pays','de','_NUMBER',31). %% 986
rpref('maire','de','_LOCATION',31). %% 985
rpref('soin','de','sant�',31). %% 983
rpref('fabrication','�','partir',31). %% 982
rpref('voter','contre','rapport',31). %% 980
rpref('sein','de','conseil',31). %% 979
rpref('�l�ment','de','preuve',31). %% 979
rpref('�tre','�','_LOCATION',31). %% 978
rpref('processus','de','paix',31). %% 972
rpref('�le','de','_LOCATION',31). %% 972
rpref('usine','de','produit',31). %% 970
rpref('temps','de','travail',31). %% 970
rpref('objet','de','dumping',31). %% 969
rpref('�tre','en','cas',31). %% 969
rpref('exportation','de','produit',31). %% 968
rpref('conform�ment','�','directive',31). %% 968

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 'functions' in appositions

%% Initiated from data provide by Yayoi Nakamura-Delloye (Dec 2010)

:-extensional appos_function/1.

appos_function(d�put�).
appos_function(d�l�gu�).
appos_function(militant).
appos_function(�lu).
appos_function(docteur).
appos_function(professeur).
appos_function(pr�sident).
appos_function('vice-pr�sident').
appos_function(directeur).
appos_function(secr�taire).
appos_function(repr�sentant).
appos_function('porte-parole').
appos_function(s�nateur).
appos_function(maire).
appos_function(chef).
appos_function(patron).
appos_function('PDG').
appos_function(entraineur).
appos_function(ministre).
appos_function('Premier ministre').
appos_function(candidat).
appos_function(membre).
appos_function(rapporteur).
appos_function(fondateur).
appos_function(leader).
appos_function(expert).
appos_function(journaliste).
appos_function(ambassadeur).
appos_function(avocat).
appos_function(dirigeant).
appos_function(coll�gue).
appos_function(responsable).
appos_function(propri�taire).
appos_function(m�diateur).
appos_function(homologue).
appos_function(coordinateur).
appos_function(professeur).
appos_function(politologue).
appos_function(pr�d�cesseur).
appos_function(successeur).
appos_function(num�ro).
appos_function(pilier).
appos_function(�missaire).
appos_function(manager).
appos_function(cadre).
appos_function(sp�cialiste).
appos_function(recteur).
appos_function(doyen).
appos_function(champion).
appos_function(commandant).
appos_function(analyste).
appos_function(administrateur).

appos_function(meneur).
appos_function(gardien).
appos_function(attaquant).

appos_function(projet).
appos_function(syst�me).
appos_function(programme).
appos_function(logiciel).

appos_function(ambassadeur).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Lefff is missing good info on se_moyen ?

:-extensional conll_se_moyen/1.

conll_se_moyen(inscrire).
conll_se_moyen(�lever).
conll_se_moyen(trouver).
conll_se_moyen(produire).
conll_se_moyen(traduire).
conll_se_moyen(retrouver).
conll_se_moyen(�tablir).
conll_se_moyen(poursuivre).
conll_se_moyen(pr�senter).
conll_se_moyen(manifester).
conll_se_moyen(imposer).
conll_se_moyen(multiplier).
conll_se_moyen(terminer).
conll_se_moyen(exprimer).
conll_se_moyen(engager).
conll_se_moyen(am�liorer).
conll_se_moyen(accro�tre).
conll_se_moyen(faire).
conll_se_moyen(ouvrir).
conll_se_moyen(expliquer).
conll_se_moyen(exercer).
conll_se_moyen(g�n�raliser).
conll_se_moyen(enliser).
conll_se_moyen(engouffrer).
conll_se_moyen(�largir).
conll_se_moyen(endetter).
%conll_se_moyen(enfonce).
conll_se_moyen(effriter).
conll_se_moyen(effondrer).
conll_se_moyen(enthousiasmer).
conll_se_moyen(empiler).
conll_se_moyen(�mousser).
conll_se_moyen(effectuer).
conll_se_moyen(�courter).
conll_se_moyen(doubler).
conll_se_moyen(distinguer).
conll_se_moyen(diversifier).
conll_se_moyen(diffuser).
conll_se_moyen(dessiner).
conll_se_moyen(d�rouler).
conll_se_moyen(d�rober).
conll_se_moyen(d�finir).
conll_se_moyen(creuser).
conll_se_moyen(cr�er).
conll_se_moyen(constituer).
conll_se_moyen(consommer).
conll_se_moyen(consacrer).
conll_se_moyen(conjuguer).
conll_se_moyen(conclure).
conll_se_moyen(concerter).
conll_se_moyen(clarifier).
conll_se_moyen(colleter).
conll_se_moyen(chiffrer).
conll_se_moyen(comporter).
conll_se_moyen(composer).
conll_se_moyen(confirmer).
conll_se_moyen(caract�riser).
conll_se_moyen(borner).
conll_se_moyen(articuler).
conll_se_moyen(arranger).
conll_se_moyen(abstraire).
conll_se_moyen(accentuer).
conll_se_moyen(accompagner).
conll_se_moyen(affaiblir).
conll_se_moyen(situer).
conll_se_moyen(sp�cialiser).
conll_se_moyen(tasser).
conll_se_moyen(tisser).
conll_se_moyen(accomoder).
conll_se_moyen(stabiliser).
conll_se_moyen(solder).
conll_se_moyen(ranger).
conll_se_moyen(prononcer).
conll_se_moyen(inverser).

:-extensional conll_se_obj/1.

conll_se_obj(f�liciter).
conll_se_obj(occuper).
conll_se_obj(s�parer).
conll_se_obj(rapprocher).
conll_se_obj(nourrir).
conll_se_obj(charger).
conll_se_obj(doter).
conll_se_obj(replier).
conll_se_obj(amorcer).
conll_se_obj(gonfler).
conll_se_obj(enichir).
conll_se_obj(coucher).
conll_se_obj(tailler).
conll_se_obj(pourvoir).
conll_se_obj(tailler).
conll_se_obj(qualifier).
conll_se_obj(financer).
conll_se_obj(encombrer).

:-extensional conll_se_aobj/1.

conll_se_aobj(demander).
conll_se_aobj(succ�der).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Conll expansion for sxpipe compound

conll_simple_expansion(['l''', 'un'],[det: 1, head]).  %% n=54
conll_simple_expansion(['l''', 'une'],[det: 1, head]).  %% n=23
conll_simple_expansion(['michel', 'sapin'],[head, mod: -1]).  %% n=19
conll_simple_expansion(['air', 'france'],[head, mod: -1]).  %% n=19
conll_simple_expansion(['la', 'poste'],[det: 1, head]).  %% n=16
conll_simple_expansion(['les', 'autres'],[det: 1, head]).  %% n=16
conll_simple_expansion(['jean_-_claude', 'trichet'],[head, mod: -1]).  %% n=14
conll_simple_expansion(['mais', 'aussi'],[head, mod: -1]).  %% n=14
conll_simple_expansion(['m�me', 'si'],[obj: 1, head]).  %% n=13
conll_simple_expansion(['france', 't�l�com'],[head, mod: -1]).  %% n=13
conll_simple_expansion(['soci�t�', 'g�n�rale'],[head, mod: -1]).  %% n=13
conll_simple_expansion(['arabie', 'saoudite'],[head, mod: -1]).  %% n=13
conll_simple_expansion(['et', 'donc'],[head, mod: -1]).  %% n=11
conll_simple_expansion(['edouard', 'balladur'],[head, mod: -1]).  %% n=10
conll_simple_expansion(['pas', 'encore'],[mod: 1, head]).  %% n=10
conll_simple_expansion(['afrique', 'du', 'sud'],[head, dep: -1, obj: -1]).  %% n=9
conll_simple_expansion(['zenith', 'data', 'systems'],[head, dep: -1, dep: -2]).  %% n=9
conll_simple_expansion(['jacques', 'delors'],[head, mod: -1]).  %% n=8
conll_simple_expansion(['r�serve', 'f�d�rale'],[head, mod: -1]).  %% n=8
conll_simple_expansion(['us', 'air', 'force'],[head, mod: -1, mod: -2]).  %% n=8
conll_simple_expansion(['am�rique', 'du', 'nord'],[head, dep: -1, obj: -1]).  %% n=8
conll_simple_expansion(['elf', 'aquitaine'],[head, mod: -1]).  %% n=8
conll_simple_expansion(['commission', 'europ�enne'],[head, mod: -1]).  %% n=8
conll_simple_expansion(['vieux', 'continent'],[mod: 1, head]).  %% n=8
conll_simple_expansion(['pavel', 'maly'],[head, mod: -1]).  %% n=7
conll_simple_expansion(['pour', 'autant'],[head, obj: -1]).  %% n=7
conll_simple_expansion(['d''', 'entre'],[head, obj: -1]).  %% n=7
conll_simple_expansion(['banque', 'mondiale'],[head, mod: -1]).  %% n=7
conll_simple_expansion(['euro', 'disney'],[head, mod: -1]).  %% n=7
conll_simple_expansion(['club', 'de', 'paris'],[head, dep: -1, obj: -1]).  %% n=6
conll_simple_expansion(['d''', 'espagne'],[head, obj: -1]).  %% n=6
conll_simple_expansion(['british', 'airways'],[dep: 1, head]).  %% n=6
conll_simple_expansion(['john', 'major'],[head, mod: -1]).  %% n=6
conll_simple_expansion(['bernard', 'tapie'],[head, mod: -1]).  %% n=6
conll_simple_expansion(['d''', 'italie'],[head, obj: -1]).  %% n=6
conll_simple_expansion(['ou', 'encore'],[head, mod: -1]).  %% n=6
conll_simple_expansion(['depuis', 'longtemps'],[head, obj: -1]).  %% n=6
conll_simple_expansion(['ibm', 'france'],[head, mod: -1]).  %% n=6
conll_simple_expansion(['michel', 'giraud'],[head, mod: -1]).  %% n=5
conll_simple_expansion(['xavier', 'de_boishebert'],[head, mod: -1]).  %% n=5
conll_simple_expansion(['gaz', 'de', 'france'],[head, dep: -1, obj: -1]).  %% n=5
conll_simple_expansion(['comme', 'si'],[head, obj: -1]).  %% n=5
conll_simple_expansion(['raymond', 'lacombe'],[head, mod: -1]).  %% n=5
conll_simple_expansion(['d''_ici', '�'],[head, obj: -1]).  %% n=5
conll_simple_expansion(['peter', 'sutherland'],[head, mod: -1]).  %% n=5
conll_simple_expansion(['alain', 'denvers'],[head, mod: -1]).  %% n=5
conll_simple_expansion(['de', 'plus_de'],[arg: 1, head]).  %% n=5
conll_simple_expansion(['gordon', 'roddick'],[head, mod: -1]).  %% n=5
conll_simple_expansion(['daniel', 'yergin'],[head, mod: -1]).  %% n=5
conll_simple_expansion(['premier', 'ministre'],[mod: 1, head]).  %% n=5
conll_simple_expansion(['fonds', 'mon�taire', 'international'],[head, mod: -1, mod: -2]).  %% n=5
conll_simple_expansion(['la', 'plupart', 'des'],[det: 1, head, dep: -1]).  %% n=5
conll_simple_expansion(['les', 'uns'],[det: 1, head]).  %% n=4
conll_simple_expansion(['jean', 'arthuis'],[head, mod: -1]).  %% n=4
conll_simple_expansion(['jacques', 'hersant'],[head, mod: -1]).  %% n=4
conll_simple_expansion(['la', 'redoute'],[det: 1, head]).  %% n=4
conll_simple_expansion(['jacques', 'de_larosi�re'],[head, mod: -1]).  %% n=4
conll_simple_expansion(['au', 'total'],[head, obj: -1]).  %% n=4
conll_simple_expansion(['l''', 'air', 'liquide'],[det: 1, head, mod: -1]).  %% n=4
conll_simple_expansion(['m.', 'de'],[head, det: -1]).  %% n=4
conll_simple_expansion(['helmut', 'schlesinger'],[head, mod: -1]).  %% n=4
conll_simple_expansion(['sous', 'la', 'forme', 'd'''],[head, det: 1, obj: -2, dep: -1]).  %% n=4
conll_simple_expansion(['tout', 'comme'],[mod: 1, head]).  %% n=4
conll_simple_expansion(['m.', 'pierre'],[head, mod: -1]).  %% n=4
conll_simple_expansion(['maison', 'blanche'],[head, mod: -1]).  %% n=4
conll_simple_expansion(['yves', 'saint', 'laurent'],[head, mod: -1, mod: -2]).  %% n=4
conll_simple_expansion(['general', 'motors'],[dep: 1, head]).  %% n=4
conll_simple_expansion(['wall', 'street'],[dep: 1, head]).  %% n=4
conll_simple_expansion(['guy', 'ros�s'],[head, mod: -1]).  %% n=4
conll_simple_expansion(['roberto', 'procopio'],[head, mod: -1]).  %% n=4
conll_simple_expansion(['et', 'encore'],[head, mod: -1]).  %% n=4
conll_simple_expansion(['francis', 'werner'],[head, mod: -1]).  %% n=4
conll_simple_expansion(['le', 'reste'],[det: 1, head]).  %% n=4
conll_simple_expansion(['le', 'd�bat'],[det: 1, head]).  %% n=4
conll_simple_expansion(['non', 'sans'],[mod: 1, head]).  %% n=4
conll_simple_expansion(['steven', 'ross'],[head, mod: -1]).  %% n=4
conll_simple_expansion(['philippe', 'lagayette'],[head, mod: -1]).  %% n=3
conll_simple_expansion(['bernard', 'lambert'],[head, mod: -1]).  %% n=3
conll_simple_expansion(['bourse', 'de', 'londres'],[head, dep: -1, obj: -1]).  %% n=3
conll_simple_expansion(['maintes', 'fois'],[det: 1, head]).  %% n=3
conll_simple_expansion(['nations', 'unies'],[head, mod: -1]).  %% n=3
conll_simple_expansion(['edward', 'hall'],[head, mod: -1]).  %% n=3
conll_simple_expansion(['caisse', 'des', 'd�p�ts'],[head, dep: -1, obj: -1]).  %% n=3
conll_simple_expansion(['la', 'cour'],[det: 1, head]).  %% n=3
conll_simple_expansion(['au', 'bout', 'de'],[head, obj: -1, dep: -1]).  %% n=3
conll_simple_expansion(['parti', 'lib�ral'],[head, mod: -1]).  %% n=3
conll_simple_expansion(['jean_-_fran�ois', 'colas'],[head, mod: -1]).  %% n=3
conll_simple_expansion(['place', 'de', 'londres'],[head, dep: -1, obj: -1]).  %% n=3
conll_simple_expansion(['union', 'centriste'],[head, mod: -1]).  %% n=3
conll_simple_expansion(['deutsche', 'bank'],[dep: 1, head]).  %% n=3
conll_simple_expansion(['d''', 'une', 'fa�on', 'g�n�rale'],[head, det: 1, obj: -2, mod: -1]).  %% n=3
conll_simple_expansion(['gilles', 'oury'],[head, mod: -1]).  %% n=3
conll_simple_expansion(['organisation', 'de', 'coop�ration', 'et', 'de', 'd�veloppement', '�conomique'],[head, dep: -1, obj: -1, coord: -2, dep_coord: -1, obj: -1, mod: -1]).  %% n=3
conll_simple_expansion(['en', 'place'],[head, obj: -1]).  %% n=3
conll_simple_expansion(['george', 'soros'],[head, mod: -1]).  %% n=3
conll_simple_expansion(['au', 'point', 'que'],[head, obj: -1, dep: -1]).  %% n=3
conll_simple_expansion(['cor�e', 'du', 'sud'],[head, dep: -1, obj: -1]).  %% n=3
conll_simple_expansion(['time', 'warner'],[head, mod: -1]).  %% n=3
conll_simple_expansion(['et', 'm�me'],[head, mod: -1]).  %% n=3
conll_simple_expansion(['en', 'vigueur'],[head, obj: -1]).  %% n=3
conll_simple_expansion(['�', 'terme'],[head, obj: -1]).  %% n=3
conll_simple_expansion(['cr�dit', 'agricole'],[head, mod: -1]).  %% n=3
conll_simple_expansion(['nicolas', 'guilbert'],[head, mod: -1]).  %% n=3
conll_simple_expansion(['d''_ici', '�_la_fin_de'],[head, obj: -1]).  %% n=3
conll_simple_expansion(['le', 'monde'],[det: 1, head]).  %% n=3
conll_simple_expansion(['d''', 'epinal'],[head, obj: -1]).  %% n=3
conll_simple_expansion(['banque', 'centrale', 'europ�enne'],[head, mod: -1, mod: -2]).  %% n=3
conll_simple_expansion(['prud''', 'hommes'],[mod: 1, head]).  %% n=3
conll_simple_expansion(['la', 'baule'],[det: 1, head]).  %% n=3
conll_simple_expansion(['soci�t�', 'marseillaise', 'de', 'cr�dit'],[head, mod: -1, dep: -2, obj: -1]).  %% n=3
conll_simple_expansion(['�', 'moins_de'],[head, obj: -1]).  %% n=3
conll_simple_expansion(['robert', 'maxwell'],[head, mod: -1]).  %% n=3
conll_simple_expansion(['olivier', 'bouissou'],[head, mod: -1]).  %% n=3
conll_simple_expansion(['patrick', 's�bastien'],[head, mod: -1]).  %% n=3
conll_simple_expansion(['philip', 'morris'],[head, mod: -1]).  %% n=3
conll_simple_expansion(['avant', 'que'],[head, dep: -1]).  %% n=2
conll_simple_expansion(['au', 'centre', 'd'''],[head, obj: -1, dep: -1]).  %% n=2
conll_simple_expansion(['charbonnages', 'de', 'france'],[head, dep: -1, obj: -1]).  %% n=2
conll_simple_expansion(['c�te', 'd''', 'azur'],[head, dep: -1, obj: -1]).  %% n=2
conll_simple_expansion(['jean', 'stromboni'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['herv�', 'barr�'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['catherine', 'viannay'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['au', 'lieu', 'de'],[head, obj: -1, dep: -1]).  %% n=2
conll_simple_expansion(['arthur', 'dunkel'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['bernard', 'haemmerlin'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['tudor', 'banus'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['depuis', 'que'],[obj: 1, head]).  %% n=2
conll_simple_expansion(['reed', 'international'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['reporters', 'sans', 'fronti�res'],[head, dep: -1, obj: -1]).  %% n=2
conll_simple_expansion(['d.', 'i.', 'oparine'],[head, mod: -1, mod: -2]).  %% n=2
conll_simple_expansion(['fran�ois', 'guillaume'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['m.', 'carlo', 'azeglio', 'ciampi'],[head, mod: -1, mod: -2, mod: -3]).  %% n=2
conll_simple_expansion(['fran�ois', 'mitterrand'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['compte', 'tenu', 'des'],[head, mod: -1, de_obj: -1]).  %% n=2
conll_simple_expansion(['eug�ne', 'forget'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['jean', 'peyrelevade'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['m.', 'michel'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['fernand', 'corradetti'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['union', 'professionnelle', 'artisanale'],[head, mod: -1, mod: -2]).  %% n=2
conll_simple_expansion(['et', 'aussi'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['werner', 'sombart'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['bureau', 'international', 'du', 'travail'],[head, mod: -1, dep: -2, obj: -1]).  %% n=2
conll_simple_expansion(['jean', 'pr�tre'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['fabrice', 'th�obald'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['jean_-_charles', 'pellerin'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['au_cours', 'desquelles'],[head, dep: -1]).  %% n=2
conll_simple_expansion(['au', 'c�t�', 'de'],[head, obj: -1, dep: -1]).  %% n=2
conll_simple_expansion(['emiliano', 'zapata'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['edith', 'cresson'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['daniel', 'hoeffel'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['pierre', 'guillen'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['henri', 'nallet'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['silver', 'moon'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['le', 'mieux'],[det: 1, head]).  %% n=2
conll_simple_expansion(['les', 'r�formateurs'],[det: 1, head]).  %% n=2
conll_simple_expansion(['sachant', 'que'],[head, obj: -1]).  %% n=2
conll_simple_expansion(['au', 'cours', 'des'],[head, obj: -1, dep: -1]).  %% n=2
conll_simple_expansion(['de', 'chez'],[head, obj: -1]).  %% n=2
conll_simple_expansion(['ronald', 'reagan'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['chargeurs', 'sa'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['organisation', 'de', 'coop�ration', 'et', 'de', 'd�veloppement', '�conomiques'],[head, dep: -1, obj: -1, coord: -2, dep_coord: -1, obj: -1, mod: -1]).  %% n=2
conll_simple_expansion(['au', 'milieu', 'de'],[head, obj: -1, dep: -1]).  %% n=2
conll_simple_expansion(['dassault', 'aviation'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['caisse', 'des', 'd�p�ts', 'et', 'consignations'],[head, dep: -1, obj: -1, coord: -1, dep_coord: -1]).  %% n=2
conll_simple_expansion(['france', 'motors'],[head, dep: -1]).  %% n=2
conll_simple_expansion(['au', 'point', 'de'],[head, obj: -1, dep: -1]).  %% n=2
conll_simple_expansion(['michel', 'leclerc'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['guy', 'azoulay'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['les', 'anglais'],[det: 1, head]).  %% n=2
conll_simple_expansion(['assurances', 'g�n�rales', 'de', 'france'],[head, mod: -1, dep: -2, obj: -1]).  %% n=2
conll_simple_expansion(['d''', 'o�'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['l''', '�quipe'],[det: 1, head]).  %% n=2
conll_simple_expansion(['anita', 'roddick'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['au', 'plus'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['jean_-_fran�ois', 'gavalda'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['christophe', 'delavenne'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['mamie', 'nova'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['thomson', 'consumer', 'electronics'],[head, dep: -1, dep: -2]).  %% n=2
conll_simple_expansion(['louis', 'mermaz'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['canard', 'encha�n�'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['daniel', 'vauvilliers'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['prix', 'nobel'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['mohamed', 'boudiaf'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['serge', 'bensimon'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['air', 'inter'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['�', 'tout', 'prix'],[head, det: 1, obj: -2]).  %% n=2
conll_simple_expansion(['xavier', 'grenet'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['helmut', 'kohl'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['au', 'milieu', 'des'],[head, obj: -1, dep: -1]).  %% n=2
conll_simple_expansion(['martin', 'bouygues'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['d''', 'ici', '�'],[head, mod: -1, arg: -2]).  %% n=2
conll_simple_expansion(['chris', 'patten'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['parti', 'communiste'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['val�ry', 'giscard', 'd''', 'estaing'],[head, mod: -1, dep: -2, obj: -1]).  %% n=2
conll_simple_expansion(['d''', 'angleterre'],[head, obj: -1]).  %% n=2
conll_simple_expansion(['de', 'loin'],[head, obj: -1]).  %% n=2
conll_simple_expansion(['thierry', 'aulagnon'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['communaut�', 'des', 'etats', 'ind�pendants'],[head, dep: -1, obj: -1, mod: -1]).  %% n=2
conll_simple_expansion(['au', 'bord', 'de'],[head, obj: -1, dep: -1]).  %% n=2
conll_simple_expansion(['conseil', 'sup�rieur', 'de', 'l''', 'audiovisuel'],[head, mod: -1, dep: -2, det: 1, obj: -2]).  %% n=2
conll_simple_expansion(['fernando', 'collor', 'de', 'mello'],[head, mod: -1, det: -2, mod: -3]).  %% n=2
conll_simple_expansion(['alain', 'buhler'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['thierry', 'gandillot'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['michel', 'camdessus'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['michel', 'gilet'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['la', 'chambre'],[det: 1, head]).  %% n=2
conll_simple_expansion(['us', 'air'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['en', 'fin', 'de'],[head, obj: -1, dep: -1]).  %% n=2
conll_simple_expansion(['bbc', 'world', 'service'],[head, dep: -1, dep: -2]).  %% n=2
conll_simple_expansion(['los', 'angeles'],[dep: 1, head]).  %% n=2
conll_simple_expansion(['british', 'gas'],[dep: 1, head]).  %% n=2
conll_simple_expansion(['frankfurter', 'allgemeine', 'zeitung'],[dep: 2, dep: 1, head]).  %% n=2
conll_simple_expansion(['luc', 'guyau'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['serge', 'tchuruk'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['michel', 'gaillard'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['machine', 'arri�re'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['au', 'cours', 'de'],[head, obj: -1, dep: -1]).  %% n=2
conll_simple_expansion(['d''', 'autant'],[head, obj: -1]).  %% n=2
conll_simple_expansion(['place', 'de', 'paris'],[head, dep: -1, obj: -1]).  %% n=2
conll_simple_expansion(['jean_-_pierre', 'soisson'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['jean_-_fran�ois', 'duhot'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['cerro', 'paranal'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['st�phane', 'k�lian'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['xx', 'si�cle'],[mod: 1, head]).  %% n=2
conll_simple_expansion(['boulevard', 'haussmann'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['michel', 'quesnot'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['brice', 'martin'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['maurice', 'strong'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['louis', 'fontvieille'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['gilles', 'laurent'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['cr�dit', 'national'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['r�publique', 'du', 'kazakhstan'],[head, dep: -1, obj: -1]).  %% n=2
conll_simple_expansion(['jacques', 'attali'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['adrien', 'zeller'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['au', 'm�me', 'titre', 'que'],[head, mod: 1, obj: -2, dep: -1]).  %% n=2

