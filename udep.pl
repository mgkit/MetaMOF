/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2015, 2017, 2021 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  udep.pl -- Universal Dependencies
 *
 * ----------------------------------------------------------------
 * Description
 * Conversion to the Universal Dependency schema (for French)
 * see at http://universaldependencies.github.io/docs/fr/dep/index.html
 * ----------------------------------------------------------------
 */

:-include 'header.tag'.
%:-require 'features.pl'.
:-require 'conll.pl'.

:-finite_set(udep_POS,[
		 'ADJ',		% adjectives
		 'ADP',		% adposition
		 'ADV',		% adverbs
		 'AUX',         % auxiliary verb 
		 'CCONJ',        % coordinating conjunction
		 'DET',         % determiner
                 'INTJ',        % interjection
		 'NOUN',        % noun
                 'NUM',         % numeral
                 'PART',        % particle
                 'PRON',        % pronoun
                 'PROPN',       % proper noun
                 'PUNCT',       % punctuation
                 'SCONJ',       % subordinating conjunction
                 'SYM',         % symbol
		 'VERB',        % verb
                 'X'            % other
	     ]).

%% The possible features for Universal Dependencies
%% the values for these features are not yet defined for French
%% should reuse as much as possible what was done for Passage (passage.pl)
:-finite_set(udep_FEAT,[
		 'Animacy', % animacy
		 'Aspect', % aspect
		 'Case', % case
		 'Definite', % definiteness or state
		 'Degree', % degree of comparison
		 'Gender', % gender
		 'Mood', % mood
		 'Negative', % whether the word can be or is negated
		 'NumType', % numeral type
		 'Number', % number
		 'Person', % person
		 'Poss', % possessive
		 'PronType', % pronominal type
		 'Reflex', % reflexive
		 'Tense', % tense
		 'VerbForm', % form of verb or deverbative
		 'Voice' % voice
	     ]).


udep_emit :-
    conll_edge_reset,     % resuse edge management from CONLL but do a reset
    emit_multi('UDEP'),
    sentence(SId),
    recorded(mode(Mode)),
    ( recorded(has_best_parse) ->
	  Best = yes
     ;
     Best = no
    ),
%%    format('##starting\n',[]),
    udep_ids(0,1),
%%    format('##done ids\n',[]),
    udep_collect_node_info(1),
%%    format('##done node info\n',[]),
    every(( udep_relation_manager )),

    %% check for multiple roots
    (Mode = robust ->
	 every(( node{ id => _NId },
		 node2udep(_NId,_Pos),
                 \+ conll_edge(_Pos,_,_,_,_,_),
		 conll_node_info(_Pos,_CLex,_CLemma,'PUNCT',_FullCat,_MSTag),
		 (_Pos > 1 -> _HPos is _Pos -1 ; _HPos is _Pos + 1),
		 conll_edge_register(_Pos,_HPos,punct,'R_root_robust',[up],0)
	       )),
	 true
     ;
     every(( node{ id => _NId },
	     node2udep(_NId,_Pos),
             \+ conll_edge(_Pos,_,_,_,_,_),
	     record_without_doublon(udep_potential_root(_Pos))
	   )),
     ( recorded(udep_potential_root(_Pos1)),
       recorded(udep_potential_root(_Pos2)),
       _Pos1 \== _Pos2 ->
	   %% select best root
	   ( ( domain(_CCat,['VERB','CCONJ','NOUN','ADJ','X','ADV'])
	      ; _CCat = udep_POS[~ 'PUNCT']
	     ),
	     recorded(udep_potential_root(_Pos)),
	     conll_node_info(_Pos,_CLex,_CLemma,_CCat,_FullCat,_MSTag) ->
		 record(udep_best_potential_root(_Pos))
	    ;
	    true
	   )
      ;
      true
     )
    ),

    
    format('## sentence=~w mode=~w best=~w\n',[SId,Mode,Best]),
    udep_emit(1),
    every(( node{ id => NId },
	    ((xnode2udep_in(NId,PosIn) ; recorded(node2udep_alt(NId,PosIn))),
	     record(node2conll_source(NId,PosIn))
	     ;
	     xnode2udep_out(NId,PosOut),
	     record(node2conll_target(NId,PosOut))
	    )
	  )),
    conll_emit_cost,
    true
.

:-extensional
      udep_mwe/2,
  udep_token/2.

:-std_prolog udep_ids/2.

%% Universal Dependencies use a 2 level schema to differentiate tokens from wordform
%% however, the scheme is not fully comptatible with MAF (!)
%% a multi word expression covering several tokens will be expressed on several lines (one per token)
%% e.g: 'afin de' will give
%% n     afin
%% n+1   de
%% an agglutinate token (covered by two word forms) such as 'aux' will give
%% n-n+1 aux
%% n     �
%% n+1   les
%% therefore '�' and 'les' play the role of tokens when they are actually word forms, but 'afin' and 'de' are tokens !
%% another consequence is that Positions for UDep are not in direct correspondance with Token Ids

%% in more complex cases, where a mwe include an agglutinate such as 'au dessus des'
%% 1-2 au
%% 1   �
%% 2   le
%% 3   dessus
%% 4-5 des
%% 4   de
%% 5   les

:-extensional
      node2udep/2,
  udep2node/2,
  node2udep_in/2,		% entering head pos in a MWE
  node2udep_out/2,		% exit head pos in a MWE
  udep_agglutinate_start/3,
  udep_agglutinate_end/3,
  udep_agglutinate/3,
  udep_mwe_token/2,
  udep_relation/3,
  udep_is_aux_verb/1
.

:-std_prolog udep_relation_manager/0.
:-rec_prolog udep_rule/3.

udep_ids(Left,Pos) :-
%%    format('##process pos=~w at left=~w\n',[Pos,Left]),
    ( N::node{ id => NId,
	       lemma => Lemma,
	       cat => Cat,
	       cluster => cluster{ left => Left, right => Right, lex => TIds }
	     },
      Right > Left ->
	  %%	  format('## found node=~w\n',[N]),
	  format('## found pos=~w node=~w lemma=~w tids=~w\n',[Pos,NId,Lemma,TIds]),
	  (Lemma = '_SENT_BOUND' ->
	       XPos = Pos
	   ; TIds = [_|_] ->
		 %% mwe expression
		 udep_mwe_ids(TIds,Pos,XPos,NId,Right,Out),
%%		 format('## ids ~w => out=~w\n',[TIds,Out]),
		 udep_mwe_expand(NId,Out,Pos)
	   ; domain(Lemma,['jusqu''�','jusqu''en','jusqu''alors']) ->
		 %% pseudo agglutinate specific to UD_French
		 %% probably to be modified in some future
		 udep_mwe_ids([TIds,TIds],Pos,XPos,NId,Right,Out),
		 %%		 format('ids ~w => out=~w\n',[TIds,Out]),
		 udep_mwe_expand(NId,Out,Pos),
		 PosEnd is Pos+1,
		 record(udep_agglutinate(Pos,PosEnd,TIds)),
		 true
	   ; node{ lemma => NextLemma, cat => NextCat, cluster => cluster{ left => Right, lex => TIds, token => Token } },
	     format('## test aggl lemma=~w nextlemma=~w token=~w\n',[Lemma,NextLemma,Token]),
	     NextLemma \== '_SENT_BOUND' ->
		 (Cat = cat[advPref,adjPref] ->
		      record(udep_redirect(NId,Pos)),
		      XPos = Pos
		 ; (agglutinate(_,_,Token)
				xor agglutinate(_,NextLemma,_)
				xor rx!tokenize(Token,'''',[_,_|_])
		    ) ->
		    %% agglutinate: next node built on same token
		    format('## agglutinate lemma=~w nextlemma=~w token=~w\n',[Lemma,NextLemma,Token]),
			(udep_agglutinate_start(TIds,_,_) xor record(udep_agglutinate_start(TIds,Pos,NId))),
			record(udep2node(Pos,NId)),
			record(node2udep(NId,Pos) ),
			XPos is Pos + 1
		  ;
		  format('## record redirect ~w => ~w\n',[NId,Pos]),
		  record(udep_redirect(NId,Pos)),
		  XPos = Pos
		 )
	   ; 			% base case !
	   %% check if closing an agglutinate
	   (udep_agglutinate_start(TIds,PosStart,_) ->
		record(udep_agglutinate_end(TIds,Pos,NId)),
		record(udep_agglutinate(PosStart,Pos,TIds))
	    ;
	    true
	   ),
	   record( udep2node(Pos,NId) ),
	   record( node2udep(NId,Pos) ),
	   XPos is Pos + 1
	  ),
	  udep_ids(Right,XPos)
     ;
     true
    )
.

:-std_prolog udep_mwe_ids/6.

udep_mwe_ids([TId|TIds],PosIn,PosOut,NId,Right,Out) :-
    record(udep2node(PosIn,NId)),
    record(udep_mwe_token(PosIn,TId)),
    /*
    (recorded(node2udep(NId,_)) ->
	 %% a head for the mwe has been identified
	 %% we use the first token to be the head
	 %% but we should reuse expansion (conll_expansion) to identify an internal structure for the MWE
	 true
     ;
     record(node2udep(NId,PosIn))
    ),
    */
    XPos is PosIn+1,
    (TIds = [_|_] ->
	 'T'(TId,Lex),
	 '$interface'(lowercase(Lex:string),[return(LLex:string)]),
	 (agglutinate(LexA,LexB,LLex),
	  \+ LexA=sbound[] ->
	      XXPos is XPos+1,
	      record(udep_agglutinate_start(TId,PosIn,NId)),
	      record(udep_agglutinate_end(TId,XPos,NId)),
	      record(udep_agglutinate(PosIn,XPos,TId)),
	      record(udep2node(XPos,NId)),
	      record(udep_mwe_token(XPos,TId)),
	      record(udep_mwe_aggl(PosIn,LexA)),
	      record(udep_mwe_aggl(XPos,LexB)),
	      udep_mwe_ids(TIds,XXPos,PosOut,NId,Right,Out2),
	      Out = [lex(LexA),lex(LexB)|Out2]
	  ;
	  udep_mwe_ids(TIds,XPos,PosOut,NId,Right,Out2),
	  Out = [TId|Out2]
	 )
     ; node{ cluster => cluster{ left => Right, lex => TId } } ->
	   %% the last token of the MWE is an agglutinate
	   record(udep_agglutinate_start(TId,PosIn,NId)),
	   PosOut = XPos,
	   Out = [TId]
     ;
     PosOut=XPos,
     Out = [TId]
    )
.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% handling mwe

:-std_prolog udep_mwe_expand/3.

udep_mwe_expand(NId,TIds,Pos) :-
    lex2conll_forms(TIds,NLexL),
    record(udep_mwe(NId,NLexL)),
%%    format('## mwe ~w : ~w\n',[NId,NLexL]),
    (%fail,
	udep_mwe_simple_expansion(NLexL,Expansion) -> true
     ; udep_mwe_complex_expansion(NLexL,NId,Expansion) -> true
     ;
     fail,			% not yet ready
     udep_mwe_alt_expansion(NLexL,NId,Expansion) -> true
     %% ; udep_mwe_base_expansion(CLexL,NId,Expanson) -> true
     ; udep_mwe_default_expansion(NLexL,NId,Expansion,_)
    ),
%%    format('## mwe expand ~w => ~w\n',[NId,Expansion]),
    udep_expansion_data(Expansion,NId,Pos,_),
    true
.

:-std_prolog udep_expansion_data/4.

udep_expansion_data(Exp,NId,Pos,PosOut) :-
    ( Exp = head ->
	  PosOut is Pos+1,
	  (recorded(node2udep(NId,_)) ->
	       record(node2udep_alt(NId,Pos))
	   ;
	   record(node2udep(NId,Pos))
	  )
     ; Exp = head_in ->
	   PosOut is Pos+1,
	   record( node2udep_in(NId,Pos))
     ; Exp = (Type : head_in) ->
	   PosOut is Pos+1,
	   record( node2udep_in(NId,Pos))
     ; Exp = head_out ->
	   PosOut is Pos+1,
%	   record(node2udep(NId,Pos)),
	   record(node2udep_out(NId,Pos))
     ; Exp = (Type : head) ->
	   PosOut is Pos+1,
	   (recorded(node2udep(NId,_)) ->
		record(node2udep_alt(NId,Pos))
	    ;
	    record(node2udep(NId,Pos))
	   ),
	   record(udep_mwe_head_type(Pos,Type))
     ; Exp = (Type : head_out) ->
	   PosOut is Pos+1,
	   %	   record(node2udep(NId,Pos)),
	   record(node2udep_out(NId,Pos)),
	   %	   record(udep_mwe_head_type(Pos,Type)),
	   true
     ; Exp = (Type : intro ) ->
	   PosOut is Pos + 1,
	   record(udep_mwe_intro(NId,Pos,Type)),
	   record(node2udep_out(NId,Pos))
     ; Exp = (Type : Delta : Constraints) ->
	   PosOut is Pos+1,
	   Head is Pos + Delta,
	   %	  record( udep_expansion_edge(Pos,Type,Head) )
	   conll_edge_register(Pos,Head,Type,'R_mwe',Constraints,0)
     ; Exp = (Type : Delta) ->
	   PosOut is Pos+1,
	   Head is Pos + Delta,
	   %	  record( udep_expansion_edge(Pos,Type,Head) )
	   conll_edge_register(Pos,Head,Type,'R_mwe',[dep_frozen],0)
     ; Exp = (Type :> Delta) ->
	   PosOut is Pos+1,
	   Head is Pos + Delta,
	   %	  record( udep_expansion_edge(Pos,Type,Head) )
	   conll_edge_register(Pos,Head,Type,'R_mwe',[],0)
     ; Exp = [Exp1|Exp2] ->
	   udep_expansion_data(Exp1,NId,Pos,Pos2),
	   udep_expansion_data(Exp2,NId,Pos2,PosOut)
     ; Exp = [] ->
	   PosOut=Pos,
	   true
     ; Exp = (Exp1 @ (Cat,FullCat)) ->
	   record( udep_cat(Pos,Cat,FullCat) ),
	   udep_expansion_data(Exp1,NId,Pos,PosOut)
     ;
     fail
    )
.

:-extensional udep_mwe_simple_expansion/2.

:-rec_prolog udep_mwe_complex_expansion/3.

udep_mwe_complex_expansion(K::[N,million[]],NId,[(nummod:1) @ ('NUM','adj'),head @ ('NOUN','nc') ]) :-
    (is_number(N) xor domain(N,numerals[]))
.

%% 20 mille milliard
udep_mwe_complex_expansion(K::[N1,N2,million[]],NId,[(nummod:2) @ ('NUM','adj'),(nummod:1) @ ('NUM','adj'), head @ ('NOUN','nc') ]) :-
	(is_number(N1) xor domain(N1,numerals[]))
.


udep_mwe_complex_expansion([N,month[]],_,
			   [ (nummod:1) @ ('NUM', 'adj'), % 5 Janvier
			     head
			   ]
		       ) :- is_number(N).
udep_mwe_complex_expansion([month[],Year],_,[head,(nummod: -1) @ ('NUM','nc')]) :- is_number(Year). % Janvier 2013
udep_mwe_complex_expansion([day[],N,month[]],_, % Mercredi 26 Novembre
			[head @ ('NOUN', 'nc'),
			 (nummod: 1) @ ('NUM', 'adj'),
			 (fixed: -2) @ ('NOUN', 'nc')
			]
		       ) :- is_number(N).

udep_mwe_complex_expansion([day[],N,month[],Year],_, % Mercredi 26 Novembre 2014
			[head @ ('NOUN', 'nc'),
			 (nummod: 1) @ ('NUM', 'adj'),
			 (fixed: -2) @ ('NOUN', 'nc'),
			 (nummod: -1) @ ('NUM', 'nc')
			]
		       ) :- is_number(N),
			    is_number(Year).

udep_mwe_complex_expansion([N,month[],Year],_, % 26 Novembre 2014
			[(nummod: 1) @ ('NUM', 'adj'),
			 head @ ('NOUN', 'nc'),
			 (nummod: -1) @ ('NUM', 'nc')
			]
		       ) :- is_number(N),
			    is_number(Year).

udep_mwe_complex_expansion([N1,'-',N2,month[]],_,% 10 - 12 d�cembre
			[(nummod : 3) @ ('NUM','adj'),
			 (punct : 2) @ ('PUNCT','ponctw'),
			 (nummod : 1) @ ('NUM', 'adj'),
			 head @ ('NOUN', 'nc')
			]
		       ) :- is_number(N1),
			    is_number(N2)
.

udep_mwe_complex_expansion([month[],Next],_,
			   [head @ ('NOUN', 'nc'),
			    (amod: -1) @ ('ADJ', 'adj')]
			  ) :- domain(Next,[prochain,suivant,dernier]).

udep_mwe_complex_expansion([day[],Next],_,
			   [head @ ('NOUN', 'nc'),
			    (amod: -1) @ ('ADJ', 'adj')]
			  ) :- domain(Next,[prochain,suivant,dernier]).


udep_mwe_complex_expansion([N,Next],NId,
			   [(nummod : 1) @ ('NUM', 'adj'),
			    head @ ('NOUN', 'nc')]
			  ) :-
    node{ id => NId, lemma => Lemma},
    domain(Lemma,['_DATE_arto','_DATE_artf']),
%    is_number(N),
    domain(Next,['si�cle'])
.

udep_mwe_complex_expansion([Hour,h,Min],NId,
			   [(nummod : 1) @ ('NUM', 'adj'),
			    head @ ('NOUN', 'nc'),
			    (nummod : -1) @ ('NUM','adj')
			   ]
			  ) :-
    node{ id => NId, lemma => Lemma},
    domain(Lemma,['_HEURE']),
    is_number(Hour),
    is_number(Min)
.

udep_mwe_complex_expansion([Hour,h],NId,
			   [(nummod : 1) @ ('NUM', 'adj'),
			    head @ ('NOUN', 'nc')
			   ]
			  ) :-
    node{ id => NId, lemma => Lemma},
    domain(Lemma,['_HEURE']),
    is_number(Hour)
.

%% present in English reference guide, but unclear if really used
udep_mwe_complex_expansion([N1,'-',N2],_,% 10 - 12 as 10 � 12
			[head @ ('NUM','adj'),
			 (case : 1) @ ('PUNCT','ponctw'),
			 (nummod : -2) @ ('NUM', 'adj')
			]
		       ) :- is_number(N1),
			    is_number(N2)
.

udep_mwe_complex_expansion([Prep,Noun],NId,[(case : 1) @ ('ADP','prep'),  head @ ('NOUN',nc)]) :-
    node{id => NId, cat => Cat, cluster => cluster{ left => Left, right => Right}},
    Cat \== det,
    (domain(Prep,[de,des,du,'d''',en])
	   xor
	   'C'(Left,lemma{ cat => prep },Middle),
     'C'(Middle,lemma{ cat => nc },Right)
    )
.


udep_mwe_complex_expansion([de,Det],NId,[head @ ('DET','det'),  head]) :-
    domain(Det,[la,'l''']),
    N::node{id => NId, cat => det}
.

udep_mwe_complex_expansion([Det,Pron],NId,[(expl : 1) @ ('DET','det'),  head]) :-
    domain(Det,['l''']),
    node{ id => NId, cat => cat[pro,cln]}
	.

udep_mwe_complex_expansion(['-',t,'-',CL],
			   NId,
			   [(punct : 1) @ ('PUNCT',cln),
			    (expl : 2) @ ('ADV',cln),
			    (punct : -1) @ ('PUNCT', cln),
			    head
			   ]).

udep_mwe_complex_expansion([Prep1,Noun,Prep2],_,
			   [ (case : 1) @ ('ADP',prep),
			     (nmod : intro) @ ('NOUN',nc),
			     head_in @ ('ADP',prep)
			   ]
			  ) :-
	domain(Prep1,[�,en]),
	domain(Prep2,[de,des,du,'d'''])
	.
/*
udep_mwe_complex_expansion([�,le,Noun,Prep2],_,
			   [ (mwe : 2) @ ('ADP',prep),
			     (det : 1) @ ('DET',det),
			     head @ ('NOUN',nc),
			     (mwe : -1) @ ('ADP',prep)
			   ]
			  ) :-
	domain(Prep2,[de,des,du,'d'''])
	.
*/

udep_mwe_complex_expansion([�,le,Noun,Prep2],_,
			   [ (case : 2) @ ('ADP',prep),
			     (det : 1) @ ('DET',det),
			     (nmod : intro) @ ('NOUN',nc),
			     head_in @ ('ADP',prep)
			   ]
			  ) :-
	domain(Prep2,[de,des,du,'d'''])
	.

udep_mwe_complex_expansion([Prep,Que],NId,
			   [(case : 1) @ ('ADP',prep),
			    head
			   ]
			  ) :-
    node{ id => NId, cat => csu },
    domain(Que,[que,'qu'''])
	.

%% jusqu'� (peusdo-agglutinates for udep)
udep_mwe_complex_expansion([X,X],NId,
			   [(case: head) @ ('ADP',prep),head]
			  ).

:-std_prolog udep_mwe_alt_expansion/3.
:-std_prolog udep_mwe_alt_expansion_aux/8.

udep_mwe_alt_expansion(L,NId,Expansion) :-
    % fail,
    % format('try conll alt expansion ~w ~w\n',[L,NId]),
    N::node{ id => NId, cluster => cluster{ id => CId, left => Left, right => Right} },
    % format('try conll alt expansion ~w ~w left=~w right=~w\n',[L,NId,Left,Right]),
    ( edge{ source => N, target => node{ cluster => cluster{ left => TLeft }}}, TLeft >= Right ->
	  MaxRight is Right+5,
	  term_range(Right,MaxRight,_Right),
	  _Left = Left
     ; edge{ source => N, target => node{ cluster => cluster{ right => TRight }}}, TRight =< Left ->
	   MinLeft is Left-5,
	   term_range(MinLeft,Left,_Left),
	   Right = _Right
     ;
     _Right = Right,
     _Left = Left
    ),
    recorded(span2best_parse_node(Span::[_Left,_Right|_],_NId)),
    NId \== _NId,
    % format('try conll alt expansion ~w ~w left=~w right=~w nid=~w\n',[L,NId,Left,_Right,_NId]),
    '$answers'(best_parse(_NId,_,[_Left,_Right|_],_,DStruct::dstruct{ w => _W})),
    % format('try conll alt expansion ~w ~w ~w dstruct=~w\n',[Left,Right,L,DStruct]),
    % \+ ( '$answers'( best_parse(__NId,_,[Left,Right|_],_,dstruct{ w => __W }) ),
    %      NId \== __NId,
    %      __W > _W
    %    ),
    dstruct2lists(DStruct,CIds,EIds::[_,_|_],DIds),
    \+ domain(CId,CIds),
    % format('try conll alt expansion ~w ~w ~w ~w eids=~w\n',[L,Left,Right,L,EIds]),
    mutable(MEdges,[],true),
    every(( domain(EId,EIds),
	    (recorded(erased(E::edge{ id => EId, target => node{ cluster => cluster{ left => P0, right => P1 } }})) xor E),
	    %%	    format('\tregister ~w\n',[P0:E]),
	    _Info ::= (P0:E),
	    mutable_list_extend(MEdges,_Info)
	  )),
    mutable_read(MEdges,Edges),
    % format('try conll alt expansion aux ~w ~w ~w ~w edges=~w\n',[L,Left,Right,L,Edges]),
    udep_mwe_alt_expansion_aux(L,Expansion,Left,Right,Left,Edges,_NId,[]),
    %    format('conll alt expansion ~w ~w ~w => ~w\n',[Left,Right,L,Expansion]),
    true
.

udep_mwe_alt_expansion_aux([Lex|Rest],Exp,Left,Right,P0,Edges,NId,LastTokenId) :-
    P1 is P0+1,
    % format('\t\ttry at ~w\n',[P0]),
    ( domain( (P0: E::edge{ id => EId,
			    label => Label,
			    type => Type,
			    target => node{ cat => TCat,
					    id => TNId,
					    cluster => cluster{ left => P0, right => P1, lex => TokenId }
					  },
			    source => node{ cat => SCat,
					    cluster => cluster{ left => SLeft, right => SRight, lex => STokenId }
					  }
			  }
	      ),
	      Edges
	    ) ->
	  Delta is STokenId - TokenId,
	  %% Delta=0 should not arise ! (but it does)
	  Delta \== 0
     ; domain( _: edge{ source => node{ cluster => cluster{ left => P0, right => P1, lex => TokenId }}}, Edges) ->
	   Exp1 = head
     ; fail
    ),
    % format('\t\tfound ~w ~w ~w\n',[P0,EId,Exp1]),
    ( Exp1 == head -> true
     ; SCat = prep, Type = subst ->
	   udep_easy_cat(TCat,XCat,_),
	   Exp1 = ((case : Delta : []) @ (XCat, TCat))
     ; TCat = prep ->
	   Exp1 = ((nmod : Delta) @ ('ADP', prep))
     ; TCat = det ->
	   Exp1 = ((det : Delta) @ ('DET', det))
     ; TCat = coo ->
	   Exp1 = ((cc : Delta) @ ('CCONJ',TCat))
     ; SCat = coo, Label = label[coord2,coord3] ->
	   udep_easy_cat(TCat,XCat,_),
	   Exp1 = (conj : Delta) @ (XCat,TCat)
     ;
     udep_easy_cat(TCat,XCat,Type),
     Exp1 = (Type : Delta) @ (XCat,TCat)
    ),
    %    format('\t\tfound2 ~w ~w ~w rest=~w\n',[P0,EId,Exp1,Rest]),
    %	format('alt expansion left=~w lex=~w exp1=~w e=~w\n',[P0,Lex,Exp1,E]),
    ( P1=Right ->
	  Exp = [Exp1]
     ; TokenId = LastTokenId ->
	   udep_mwe_alt_expansion_aux([Lex|Rest],Exp,Left,Right,P1,Edges,NId,TokenId)
     ;
     Exp = [Exp1|Exp2],
     udep_mwe_alt_expansion_aux(Rest,Exp2,Left,Right,P1,Edges,NId,TokenId)
    )
.

:-extensional udep_easy_cat/3.

udep_easy_cat(cat[nc,ncpred,title],'NOUN',nmod).
udep_easy_cat(np,'PROPN',nmod).
udep_easy_cat(cat[v],'VERB',cconj).
udep_easy_cat(cat[aux],'AUX',aux).
udep_easy_cat(adj,'ADJ',amod).
udep_easy_cat(adv,'ADV',advmod).
udep_easy_cat(cat[det,number],'DET',det).
udep_easy_cat(cat[ponctw,poncts],'PUNCT',punct).
udep_easy_cat(prep,'ADP',case).
udep_easy_cat(cat[csu,que],'SCONJ',mark).
udep_easy_cat(cat[coo],'C',cc).


:-std_prolog udep_mwe_default_expansion/4.

udep_mwe_default_expansion(L,NId,Expansion,Delta) :-
    (L=[L1|L2] ->
%	 XDelta is Delta + 1,
	 udep_mwe_default_expansion(L2,NId,Expansion2,XDelta),
	 Delta is XDelta+1,
	 (XDelta = 0 ->
%	     Delta = 0 ->
	      Part1 = head
	  ;
	  (node{id => NId, cat => np} -> Type = 'flat' ; Type = fixed),
	  Part1 = (XType :> XDelta)
	 ),
	 (domain(L1,
		 ['.',',',';','!','?','(',')','[',']','!?','??','?!','...','"','''','-',':','_','(...)','�','�','/','--','---']
		) ->
	      XType = punct,
	      Part2 = Part1 @ ('PUNCT','punctw')
	  ;
	  udep_mwe_default_cat(L1,Cat,XType) ->
	      Part2 = Part1 @ Cat
	  ;
	  XType = Type,
	  Part2 = Part1
	 ),
	 Expansion = [ Part2 | Expansion2]
     ;
     Expansion = [],
     Delta = 0,
     true
    )
.

:-extensional udep_mwe_default_cat/3.

udep_mwe_default_cat(le,('DET',det),det).
udep_mwe_default_cat('l''',('DET',det),det).
udep_mwe_default_cat(la,('DET',det),det).
udep_mwe_default_cat(les,('DET',det),det).
udep_mwe_default_cat(�,('ADP',prep),case).
udep_mwe_default_cat(de,('ADP',prep),case).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% collecting POS and FEATs info

:-extensional udep_nform/2.

udep_nform('\\?','?').
udep_nform('\\+','+').
udep_nform('\\*','*').
udep_nform('\\(','(').
udep_nform('\\)',')').

:-std_prolog udep_collect_node_info/1.

udep_collect_node_info(Pos) :-
    (udep2node(Pos,NId) ->
	 N::node{ id => NId,
		  lemma => Lemma,
		  cat => Cat,
		  form => Form,
		  cluster => cluster{ lex => TIds, token => Token }
		},
	 (recorded(udep_mwe_aggl(Pos,_XForm)) -> true
	  ; udep_mwe_token(Pos,TId) ->
		'T'(TId,_XForm)
	  ; udep_agglutinate_start(TIds,_,_) ->
		% part of an agglutinate => use wordform rather than token
%%		format('in agglutinate ~w\n',[N]),
		_XForm = Form
	  ;
	  'T'(TIds,_XForm)
	 ),
%%	 format('xform ~w\n',[_XForm]),
	 (udep_nform(_XForm,XForm) xor XForm = _XForm),
	 (recorded(udep_cat(Pos,CPOSTAG,POSTAG)) -> true
	  ; domain(Lemma,['_NUMBER','_NUM','_ROMNUM','_DATE_year']) ->
		CPOSTAG = udep_POS['NUM']
	  ; domain(XForm,['%','$','�','�','�','+','-','*','=','�','�','#']) ->
	  %		CPOSTAG = udep_POS['SYM']
	    CPOSTAG = udep_POS['NOUN']
	  ; Cat = cat[nc,np],
	    '$interface'(is_all_uppercase(XForm:string),[])
	    ->
		CPOSTAG = udep_POS['X']
	  ;  udep_POS(N,CPOSTAG) ->
		 true
	  ;
	  format('no udep POS for ~w\n',[N]),
	  CPOSTAG = udep_POS['X']
	 ),
	 POSTAG ?= Cat,
	 udep_feats(N,CPOSTAG,Feats),
	 conll_lemma(Lemma,Token,CLemma),
	 record( conll_node_info(Pos,XForm,CLemma,CPOSTAG,POSTAG,Feats) ),
	 XPos is Pos +1,
	 udep_collect_node_info(XPos)
     ;
     true
    )
.

%% inspired by conll_cat and conll_fullcat
:-rec_prolog udep_POS/2.

udep_POS(node{ cat => cat[nc,ncpred], lemma => Lemma, cluster => cluster{ left => Left, lex => TLex}},POS) :-
    (
	'T'(TLex,Lex),
	is_acronym(Lex,_),
	\+ domain(Lex,['%','�','$'])
     ->
	    POS = udep_POS['X']
    ;
        fail,
	Left > 0,
	capitalized_cluster(TLex) ->
	    POS = udep_POS['PROPN']
     ;
     Lemma = '_ETR' ->
	 POS = udep_POS['X']
     ;
     POS = udep_POS['NOUN']
    )
.
udep_POS(node{ cat => cat[title], form => Form, cluster => cluster{ id => CId}},POS) :-
    POS = udep_POS['NOUN']
    /*
    ( recorded(erased(node{ cat => nc, cluster => cluster{ id => CId}})) ->
	  POS = udep_POS['NOUN']
     ;
     POS = udep_POS['X']
    )
    */  
.

%udep_POS(node{ cat => etr }, udep_POS['X']).
udep_POS(node{ cat => cat[np], cluster => cluster{ id => CId, lex => TLex }},POS) :-
    ( fail,			% the rules between PROPN and X are unclear !
      'T'(TLex,Lex),
      is_acronym(Lex,_) ->
      POS = udep_POS['X']
    ; recorded(erased(node{ cat => nc, cluster => cluster{ id => CId}})) ->
      POS = udep_POS['NOUN']
    ;
    POS = udep_POS['PROPN']
    )
.

udep_POS(node{ cat => Cat::cat[cln,cla,cld,clr,clg,ilimp,cll,pro,prel,pri,xpro,ce,caimp], lemma => Lemma},POS) :-
    (Cat=pri,
     domain(Lemma,['comment?','combien?','o�?']) ->
	 POS=udep_POS['ADV']
     ;
     POS=udep_POS['PRON']
    )
.
udep_POS(N::node{ id => NId, cat => cat[cll] }, POS) :-
    (chain(N << lexical @ cll << node{ lemma => avoir, cat => v } >> lexical @ impsubj >> node{}) ->
	 POS = 'PRON'
    ;
    POS = 'ADV'
    )
.
udep_POS(node{ cat => cat[adv,advneg,que_restr,predet,adjPref,advPref]},udep_POS['ADV']).
udep_POS(node{ cat => cat[suffAdj]},udep_POS['ADJ']).
udep_POS(node{ cat => cat[ponctw,poncts]},udep_POS['PUNCT']).
udep_POS(node{ cat => cat[csu,que]},udep_POS['SCONJ']).
udep_POS(node{ cat => cat[prep]},udep_POS['ADP']).
udep_POS(node{ cat => cat[pres]},udep_POS['INTJ']).
udep_POS(N::node{ id => NId, cat => cat[adj], lemma => Lemma},POS) :-
    (domain(Lemma,['_-l�','_-ci']) ->
	 POS = udep_POS['PART']
     ; chain( N << adj @ det << node{}) ->
	   POS = udep_POS['DET']
     ; chain( N::node{ id =>  NId } >> subst @ det >> node{} ) ->
	   POS = udep_POS['NOUN']
     ; Lemma = temps ->		% in exp: il est temps de|que S
	   POS = udep_POS['NOUN']
     ;
     POS = udep_POS['ADJ']
    ).

udep_POS(N::node{ id => NId, cat => cat[v], lemma => Lemma},POS) :-
    (chain(N << adj << node{ cat => Cat::cat[v,adj]}) ->
	 % record_without_doublon(udep_is_aux_verb(NId)),
	 (Cat = adj ->
	      POS = udep_POS['VERB']
	 ; udep_modal(Lemma) ->
	   % record_without_doublon(udep_is_aux_verb(NId)),
	   POS = udep_POS['AUX']
	  ;
	  POS = udep_POS['VERB']
	 )
     ; chain(N >> subst @ xcomp >> node{ cat => v }),
%       udep_modal(Lemma),
       \+ chain( N >> lexical @ label[prep,csu] >> node{})
       ->
%       record_without_doublon(udep_is_aux_verb(NId)),
       POS = udep_POS['AUX']
     ; chain(N >> subst @ comp >> node{ }) ->
%       record_without_doublon(udep_is_aux_verb(NId)),
       POS = udep_POS['AUX']
     ;
     POS = udep_POS['VERB']
    )
.

:-extensional udep_modal/1.

udep_modal(voloir).
udep_modal(p�oir).
udep_modal(pouvoir).
udep_modal(devoir).
udep_modal(savoir).
udep_modal(soloir).
udep_modal(pouvoir).
udep_modal(sembler).
udep_modal(sanbler).
udep_modal(senbler).
udep_modal(aler).
udep_modal(pooit).

udep_POS(node{ id => NId, cat => cat[aux]},udep_POS['AUX']) :- record_without_doublon(udep_is_aux_verb(NId)).
udep_POS(node{ cat => cat[det]},udep_POS['DET']).
udep_POS(node{ cat => cat[clneg]},udep_POS['ADV']).
udep_POS(node{ cat => cat[coo]},udep_POS['CCONJ']).
udep_POS(node{ cat => '_', lemma => Lemma, cluster => cluster{ token => Token}},udep_POS['PUNCT']) :-
    domain(Lemma,['.',',',';','!','?','(',')','[',']','!?','??','?!','...','"','''','-',':','_','(...)','�','�','/'])
	  xor
	  domain(Token,['.',',',';','!','?','(',')','[',']','!?','??','?!','...','"','''','-',':','_','(...)','�','�','/'])
.
udep_POS(node{ cat => '_', lemma => 'etc.' },udep_POS['X']).
udep_POS(node{ cat => '_', lemma => ce },udep_POS['PRON']).
udep_POS(node{ cat => '_', lemma => en },udep_POS['ADP']).
udep_POS(node{ cat => '_', lemma => ou },udep_POS['CCONJ']).
udep_POS(node{ cat => '_', lemma => '_SMILEY' },udep_POS['SYM']).
udep_POS(node{ cat => '_', lemma => '_META_TEXTUAL_GN' },udep_POS['NOUN']).

:-std_prolog udep_feats/3.

udep_feats(node{ id => NId, cat => Cat, lemma => Lemma},CPosTag,Feats) :-
    ( node2op(NId,OId),
      recorded(op{ id => OId, top => Top })
	      xor
	      recorded(node2top(NId,Top))
	      xor
	      Top =[],
      OId = '_'
    ),
    mutable(MFeat,[]),
    ( Cat=cat[v,aux],
      recorded(node2top(NId,XTop)),
      XTop \== [],
      XTop_F = mode,
      inlined_feature_arg(XTop,XTop_F,_,XMode),
      domain(XMode,[participle,gerundive]) ->
%	  format('mode ~w\n',[XMode]),
	  true
     ;
     XMode=[]
    ),

    every((
		 (Cat = cln -> FV = 'Case=Nom'
		 ; Cat = cla -> FV = 'Case=Acc'
		 ; Cat = cat[clneg,advneg] -> FV = 'Negative=Neg'
		 ; Cat = clr -> FV = 'Reflexive=Yes'
		 ; CPosTag = 'PRON', domain(Lemma,[rien,personne,aucun,nul]) -> FV= 'Negative=Neg'
		 ; CPosTag = 'PRON', domain(Lemma,[celui,ce,cela]) -> FV= 'PronType=Dem'
		 ; Top = det{ dem => (+) } -> FV = 'PronType=Dem'
		 ; Top = det{ poss => (+) } -> FV = 'Pos=Yes' 
		 ; fail
		 ),
		 mutable_list_extend(MFeat,FV)
	     )),

    
    every((
%		 format('nid=~w top=~w\n',[NId,Top]),
		 ( Top \== [],
		   domain(F,[tense,person,number,mode,gender]),
		   inlined_feature_arg(Top,F,_,V),
		 \+ (V = 'not_an_acceptable_value'),
		 \+ ( F=tense,
%		      FullCat \== conll_fullcat['VPP','VPR'],
		      (inlined_feature_arg(Top,_Mode,_,conditional), _Mode = mode)),
		 \+ ( Cat=cat[adv,advneg,prep],
		      domain(F,[number,gender,person])
		    ),
		 (F=mode, XMode \== [] ->
		      feature2udep(F,XMode,FV,_CPosTag) % a bug seem to exist in DyALog with finite set: to investigate
		  ;
		  feature2udep(F,V,FV,_CPosTag) % a bug seem to exist in DyALog with finite set: to investigate
		 )
		     %% format('try F=~w V=~w POS=~w\n',[F,V,CPosTag]),
		     %% format('=> FV=~w\n',[FV]),
		     %% _CPosTag = CPosTag, %% this unification seems to always fail: this is a bug !
		 ),
		 mutable_list_extend(MFeat,FV)
	     )),
    mutable_read(MFeat,_MSTag),
    conll_fvlist2fv(_MSTag,Feats)
.
	  
:-extensional feature2udep/4.

feature2udep(gender,masc,'Gender=Masc',udep_POS['NOUN','ADJ','PROPN','DET']).
feature2udep(gender,fem,'Gender=Fem',udep_POS['NOUN','ADJ','PROPN','DET']).

feature2udep(number,pl,'Number=Plur',udep_POS['VERB','AUX','NOUN','ADJ','DET','PROPN']).
feature2udep(number,sg,'Number=Sing',udep_POS['VERB','AUX','NOUN','ADJ','DET','PROPN']).

feature2udep(person,1,'Person=1',udep_POS['VERB','AUX','PRON']).
feature2udep(person,2,'Person=2',udep_POS['VERB','AUX','PRON']).
feature2udep(person,3,'Person=3',udep_POS['VERB','AUX','PRON']).

feature2udep(tense,future,'Tense=Fut',udep_POS['VERB','AUX']).
feature2udep(tense,'future-perfect','Tense=Fut',udep_POS['VERB','AUX']).
feature2udep(tense,present,'Tense=Pst',udep_POS['VERB','AUX']).
feature2udep(tense,past,'Tense=Past',udep_POS['VERB','AUX']).
feature2udep(tense,'past-historic','Tense=Past',udep_POS['VERB','AUX']).
feature2udep(tense,imperfect,'Tense=Impft',udep_POS['VERB','AUX']).

feature2udep(mode,conditional,'Tense=Cond',udep_POS['VERB','AUX']).

feature2udep(mode,indicative,'Mood=Ind|VerbForm=Fin',udep_POS['VERB','AUX']).
feature2udep(mode,subjonctive,'Mood=Subj|VerbForm=Fin',udep_POS['VERB','AUX']).
feature2udep(mode,infinitive,'VerbForm=Inf',udep_POS['VERB','AUX']).
feature2udep(mode,gerundive,'VerbForm=Ger',udep_POS['VERB','AUX']).
feature2udep(mode,participle,'VerbForm=Par',udep_POS['VERB','AUX']).
feature2udep(mode,imperative,'Mood=Imp|VerbForm=Fin',udep_POS['VERB','AUX']).

%feature2udep(type,card,'s=card',_).
feature2udep(type,def,'Definite=Def',_).
%feature2udep(type,dem,'s=card').
%feature2udep(type,excl,'s=excl',_).
feature2udep(type,ind,'Definite=Ind',_).
feature2udep(type,int,'PronType=Int',_).
%feature2udep(type,ord,'s=ord',_).
%feature2udep(type,part,'s=part',_).
feature2udep(type,poss,'Poss=Yes',_).
%feature2udep(type,qual,'s=qual',_).
feature2udep(type,rel,'PronType=Rel',_).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Emitting

:-std_prolog udep_emit/1.

udep_emit(Pos) :-
    (udep2node(Pos,NId) ->
	 (udep_agglutinate(Pos,PosEnd,TId) ->
	      'T'(TId,AggForm),
	      (rx!tokenize(AggForm,'_',[AggForm]) ->
		   format('~w-~w\t~w\t_\t_\t_\t_\t_\t_\t_@_\t_\n',[Pos,PosEnd,AggForm])
	       ;
	       true
	      )
	  ;
	  true
	 ),
	 N::node{ id => NId, cat => Cat },
	 conll_node_info(Pos,XForm,Lemma,_CPOSTAG,POSTAG,FEATS),
	 (is_number(XForm) ->
	      CPOSTAG = 'NUM'
	  ; XForm = '-' ->
		CPOSTAG = 'PUNCT',
		XDepRel = 'punct'
	  ;
	  CPOSTAG = _CPOSTAG
	 ),
	 (conll_edge(Pos,Head,DepRel,Name,Reroot,EId) ->
	      true 
	  ; 
	  ( recorded(mode(robust)) ->
		true
	   ; ( recorded( udep_root(RootPos) )
	      ; recorded( udep_best_potential_root(RootPos) ), RootPos \== Pos
	     )
	     ->
		 ( edge{ target => N,
			 label => _Label,
			 type => __Type::edge_kind[~ virtual],
			 source => __Head::node{ cat => _SCat, lemma => _SLemma }
		       },
		   conll_up_till_non_empty(__Head,__XHead::node{ id => __XHNId}),
		   node2udep(__XHNId,Head),
		   _Type = __Type, _XLemma = Lemma, _XCat = Cat
		xor
		udep2node(RootPos,RootNId),
		   edge{ target => node{ id => RootNId, lemma => _XLemma, cat => _XCat },
			 label => _Label,
			 type => __Type::edge_kind[~ virtual],
			 source => node{ cat => _SCat, lemma => _SLemma }
		       },
		   _Type = __Type
   	       xor
	       _Label = none,
		   _SCat = none,
		   _SLemma = none,
		   _Type = none,
		   _XLemma = Lemma, _XCat = Cat
		 ),
		 Head ?= RootPos,
		 (CPOSTAG = 'PUNCT' -> DepRel = punct
		 ; CPOSTAG = 'CCONJ' -> DepRel = 'cc:nc'
		 ; CPOSTAG = 'SCONJ' -> DepRel = mark
		 ; CPOSTAG = 'PRON' -> DepRel = mark
		 ; DepRel = dep ),
		 Name = 'R_deproot',
		 format('## *** multiple root ~w and ~w cat=~w lemma=~w uplabel=~w uptype=~w upcat=~w uplemma=~w\n',
			[RootPos,Pos,_XCat,_XLemma,_Label,_Type,_SCat,_SLemma])
	   ;
	   record( conll_root(Pos) )
	  ),
	  Head ?= 0,
	  DepRel ?= root,
	  EId ?= -1,
	  Name ?= 'R_root'  
	 ),
	 ( edge{ id => EId, label => FRMG_Label } xor FRMG_Label = ''),
	 %% field 1 ID: Pos
	 %% field 2 FORM: XForm
	 %% field 3 LEMMA: Lemma
	 %% field 4 CPOSTAG: CPOSTAG
	 %% field 5 POSTAG: Cat
	 %% field 6 FEATS <to be done>
	 %% field 7 HEAD <to be done>
	 %% field 8 DEPREL <to be done>
	 XDepRel ?= DepRel,
	 format('~w\t~w\t~w\t~w\t~w\t~w\t~w\t~w\t~w@~w\t~w\n',
		[Pos,XForm,Lemma,CPOSTAG,POSTAG,FEATS,Head,XDepRel,Name,FRMG_Label,EId]),
	 XPos is Pos + 1,
	 udep_emit(XPos)
     ;
     true
    )
.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Getting dependencies

:-features(udep!relation,[dep,head,type,name,reroot,eid,xrels]).
:-rec_prolog
      udep!relation{},
  udep_proxy_rule/2
.

:-std_prolog udep_proxy/2.

udep_relation_manager :-
    N::node{ id => NId },
    (xnode2udep_in(NId,DPos) ; recorded(node2udep_alt(NId,DPos))),
    udep_self_or_proxy(DPos,N,
		       udep!relation{ type => DepRel,
				      dep => Proxy,
				      head => Head,
				      name => Name,
				      reroot => Reroot,
				      eid => EId,
				      xrels => XRels
				    }
		      ),
    Head \== N,
    udep_handle_relation(
	    udep!relation{ type => DepRel,
			   dep => N,
			   head => Head,
			   name => Name,
			   reroot => Reroot,
			   eid => EId,
			   xrels => XRels
			 }
	)
.

:-std_prolog udep_self_or_proxy/3.

udep_self_or_proxy(DPos,N::node{ id => NId},Rel) :-
    (
	SelfRel::udep!relation{ type => DepRel,
				dep => N,
				head => Head::node{ id => HNId},
				name => Name,
				reroot => Reroot,
				eid => EId,
				xrels => XRels
			      },
	Rel = SelfRel,
	record_without_doublon(udep_rel_processed(DPos))
		  %     \+ conll_edge(DPos,_,_,_,_,_),
     ;
     %% DPos has still no governor, time to try a proxy node !
     \+ recorded(udep_rel_processed(DPos)),
     \+ conll_edge(DPos,_,_,_,_,_),
     udep_proxy_rule(N,Proxy::node{ id => ProxyNId}),
%%     Proxy \== N,
     format('## *** try proxy relation init=~w n=~w proxy=~w\n',[DPos,NId,ProxyNId]),
     udep_self_or_proxy(DPos,Proxy,Rel),
%     format('## *** found proxy rel ~w\n',[Rel]),
     true
    )
.
   

:-std_prolog udep_handle_relation/1.

udep_handle_relation(K::udep!relation{
			     type => DepRel,
			     dep => _N::node{ id => _NId},
			     head => _Head::node{ id => _HNId},
			     name => Name,
			     reroot => Reroot,
			     eid => EId,
			     xrels => XRels
			 }
		    ) :-
    Reroot ?= [],
    XRels ?= [],
    conll_up_till_non_empty(_Head,Head::node{id => HNId }),
    conll_down_till_non_empty(_N,N::node{id => NId }),
%%    format('## +++ PRE edge register ~w\n',[K]),
    (xnode2udep_in(NId,DPos) ; recorded(node2udep_alt(NId,DPos))),
    xnode2udep_out(HNId,HPos),
%%    format('## +++ map in(~w)=~w out(~w)=~w\n',[NId,DPos,HNId,HPos]),
    (recorded(udep_mwe_head_type(DPos,XDepRel)) xor XDepRel = DepRel),
    (udep_reverse_relation(N,Head,NewDepRel) ->
	 %% used for clefted construction
	 conll_edge_register(HPos,DPos,NewDepRel,Name,Reroot,EId)
     ; domain(intro(IntroNId),Reroot),
       recorded(udep_mwe_intro(IntroNId,IntroPos,IntroType)) ->
	   conll_edge_register(DPos,IntroPos,IntroType,Name,Reroot,EId),
	   conll_edge_register(IntroPos,HPos,XDepRel,Name,Reroot,EId)
     ; domain(reverse,Reroot) ->
	   conll_edge_register(HPos,DPos,XDepRel,Name,Reroot,EId)
     ;
     conll_edge_register(DPos,HPos,XDepRel,Name,Reroot,EId)
    ),
    every(( (XRels = udep!relation{} -> XRel = XRels ; domain(XRel,XRels)),
	    udep_handle_relation(XRel)
	  ))
.
    

:-xcompiler
xnode2udep_in(NId,Pos) :-
    (node2udep_in(NId,Pos) ->
	 %%     format('node2udep_source ~w ~w\n',[NId,Pos])
	 true
     ;
     node2udep(NId,Pos) ->
	 true
     ; recorded(udep_redirect(NId,Pos))
    ).

:-xcompiler
xnode2udep_out(NId,Pos) :-
    node2udep_out(NId,Pos) xor node2udep(NId,Pos) xor recorded(udep_redirect(NId,Pos)).

udep_proxy(N,Proxy) :-
    udep_proxy_rule(N,Proxy1),
    ( udep_proxy(Proxy1,Proxy)
		%     xor
     ;
     Proxy=Proxy1
    )
.

udep_proxy_rule(N::node{},Proxy::node{}) :-
    edge{ type => subst,
	  target => N,
	  source => Proxy::node{ tree => Tree}
	},
    domain('quoted_as_N2',Tree)
.

udep_proxy_rule(N::node{ cat => cat[adj,adv,prep,comp] },Proxy::node{ cat => cat[v]}) :-
    edge{ type => subst,
	  label => comp,
	  target => N,
	  source => Proxy
	},
    chain(Proxy >> _ @ label[object,xcomp,start] >> node{})
.

udep_proxy_rule(N::node{ cat => cat[nc,np] },Proxy::node{ cat => cat[comp]}) :-
    edge{ type => subst,
	  target => N,
	  source => Proxy
	},
    node!empty(Proxy),
    true
.


udep_proxy_rule(N::node{},Proxy::node{}) :-
    %% relative with no antecedent: qui dort d�ne
    edge{ type => subst,
	  label => 'SRel',
	  target => N,
	  source => Proxy::node{ tree => Tree }
	},
    node!empty(Proxy),
    domain(relnom_as_noun,Tree)
.

udep_proxy_rule(N::node{},Proxy::node{}) :-
    chain(N << subst @ 'N2' << node{ cat => prep, lemma => de }
	  << adj @ 'PP' << node{ cat => prep } >> subst @ 'N2' >> Proxy ),
%%    format('## *** try proxy ~w for ~w\n',[N,Proxy]),
    true
.

udep_proxy_rule(N::node{ cat => prel},Proxy::node{}) :-
    %% partitive relative: il a de quoi manger
    chain(N << lexical @ prel << node{} << adj << node{} << subst @ 'SRel' << Proxy::node{ tree => Tree}),
    node!empty(Proxy),
    domain(partitive_relative,Tree)
.

udep_proxy_rule(N::node{ cat => prep }, Proxy::node{ cat => prep}) :-
    chain(N >> subst @ 'PP' >> Proxy)
.

%udep_proxy_rule(N::node{},Proxy::node{}) :-
%    chain(N << subst @ quoted_S << Proxy)
%.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => NCat::cat[aux,v], lemma => TLemma },
	       head => Head,
	       name => 'R_aux',
	       reroot => Reroot,
	       eid => EId } :-
    edge{ id => EId,
	  type => adj,
	  label => label['Infl','V'],
	  target => N,
	  source => _Head::node{ id => SNId, cat => SCat::cat[v,aux,adj]}
	},
%    udep_is_aux_verb(NId),
    %    udep_main_verb(_Head,Head),
    _Head = Head,
    ( TLemma = avoir ->
	  Type = aux
     ; SCat = adj ->
	   Type = cop
     ; SCat = aux ->
	   Type = aux
     ; NCat=aux,
       udep_passive_verb(SNId) ->
	   Type = 'aux:pass'
     ; NCat = aux ->
	   Type = aux
     ; % fail,
       udep_modal(TLemma) ->
	   Type = aux
     ;
     Reroot = [reverse],
     Type = xcomp
    ),
    Reroot ?= [redirect,dep_frozen]
.

:-light_tabular udep_passive_verb/1.

udep_passive_verb(NId) :-
    node2live_ht(NId,HTId),
    check_ht_feature(HTId,diathesis,passive)
.

:-light_tabular udep_main_verb/2.
:-mode(udep_main_verb/2,+(+,-)).

udep_main_verb(V::node{ id => NId },MainV) :-
    ( recorded(udep_is_aux_verb(NId)),
      chain(V << (adj @ label['Infl','V']) << V2::node{ cat => cat[v,aux,adj]})
     ->
     udep_main_verb(V2,MainV)
    ; chain(V >> adj @ label['V'] >> V3::node{ cat => cat[v]}) ->
      udep_main_verb(V3,MainV)
    ;
    MainV = V
    )
.

:-light_tabular udep_main_verb_alt/2.
:-mode(udep_main_verb_alt/2,+(+,-)).

udep_main_verb_alt(V::node{ id => NId },MainV) :-
    ( recorded(udep_is_aux_verb(NId)),
      chain(V << (adj @ label['Infl']) << V2::node{ cat => cat[v,aux,adj]})
     ->
     udep_main_verb_alt(V2,MainV)
    ;
    MainV = V
    )
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat, lemma => TLemma, form => TForm, cluster => cluster{ lex => TIds }  },
	       head => MainVerb,
	       name => 'R_subj',
	       eid => EId,
	       xrels => XRels
	     } :-
    edge{ id => EId,
	  label => Label::label[subject,impsubj],
	  source => V::node{ tree => VTree },
	  target => N
	},
    udep_main_verb(V,MainVerb::node{ id => VId }),
    (fail,			% from the guide should be expl, but not the case in dataset !
     Label = impsubj ->
	 Type = expl
     ; TLemma = ce,
       domain(cleft_verb,VTree)
       ->
	   % Type =expl		% from the guide sould be expl but not the case in dataset !
	   Type = nsubj
     ; TCat = cln,
       chain(MainVerb >> subst @ subject >> AltSubj::node{}),
       AltSubj \== N ->
	   % secondary inverted subject seems to be noted as expletive
	   % La s�paration aura-t-elle lieu ?
	   Type = expl
     ; chain(V >> EId2 : lexical @ causative_prep >> Prep::node{ cat => prep }) ->
	   Type = nmod,
	   XRels = udep!relation{ type => case,
				  dep => Prep,
				  head => N,
				  name => 'R_subj_causative',
				  reroot => [redirect,dep_frozen],
				  eid => EId2
				}
     ; TCat = cat[v,aux] ->
	   (udep_passive_verb(VId) ->
		Type = 'csubj:pass'
	    ;
	    Type = csubj
	   )
     ; TCat = cat[csu,prep] ->
	   fail			% handled by other rules
     ; udep_passive_verb(VId) ->
       Type = 'nsubj:pass'
     ; TForm \== il,
       edge{ id => _EId,
	     label => object,
	     source => V,
	     target => node{ cluster => cluster{ lex => TIds } } } ->
       % format('## tlemma = ~w\n',[N]),
       Type = 'nsubj:obj'
     ;
     Type = nsubj
    )
.
    
%% need a rule for [nv]subjpass (passive deep subject [introduced by par])

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat },
	       head => Head,
	       name => 'R_dobj',
	       eid => EId
	     } :-
    edge{ id => EId,
	  label => label[object,ncpred],
	  type => edge_kind[lexical,subst],
	  source => V::node{},
	  target => N
	},
    ( fail,			% UD2
      chain(V >> ( _ @ comp ) >> _Head::node{ cat => _HCat }) ->
	  Type = nsubj,
	  (_HCat = adj ->
	       Head = _Head
	   ; _HCat = prep ->
		 chain(_Head >> subst >> Head::node{})
	   ; conll_down_till_non_empty(_Head,Head)
	  )
     ;
     udep_main_verb_alt(V,Head),
     Type = obj
    )
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::cat[cld] , cluster => cluster{ token => Token }},
	       head => MainVerb,
	       name => 'R_iobj',
	       eid => EId
	     } :-
    edge{ id => EId,
	  label => label[preparg],
	  type => edge_kind[lexical],
	  source => V::node{ id => VNId},
	  target => N
	},
    udep_main_verb_alt(V,MainVerb),
    ( 
	node2live_ht(VNId,HTId),
	check_xarg_feature(HTId,Arg::args[arg1,arg2],Fun::function[],_,hascl[cld12,cld3])
			  xor chain(V >> _ @ object >> node{}) % genitive: il lui coupe la t�te
			  xor \+ domain(Token,[y,en])
    ),
%%    format('test HId=~w arg=~w fun=~w\n',[HTId,Arg,Fun]),
%%    Arg = args[arg1,arg2],
    Type = iobj
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::cat[adv] },
	       head => MainVerb,
	       name => 'R_preparg_adv',
	       eid => EId
	     } :-
    chain( N << EId : lexical @ preparg << V::node{}),
    udep_main_verb_alt(V,MainVerb),
    Type = advmod
.



udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::cat[cll] },
	       head => MainVerb,
	       name => 'R_cll',
	       eid => EId
	     } :-
    edge{ id => EId,
	  label => label[cll],
	  type => edge_kind[lexical],
	  source => V::node{},
	  target => N
	},
    udep_main_verb_alt(V,MainVerb),
    Type = 'obl:advmod'
    %% Eric: really unclear, distribute between obl:advnod obl advmod 
.

    

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat, lemma => TLemma },
	       head => Head,
	       name => 'R_det',
	       reroot => Reroot,
	       eid => EId,
	       xrels => XRels
	     } :-
    (edge{ id => EId,
	  label => det,
	  type => subst,
	  source => Head::node{},
	  target => N
	 }
     ;
     chain(N << EId : adj @ det << node{ cat => det } << subst @ det << Head)
    ),
    TCat \== predet,
    (node2udep(NId,Pos),
     conll_node_info(Pos,_,_,'NUM',_,_) ->
	 Type = 'nummod',
	 (chain(N >> EId2: lexical @ det1 >> Det1::node{}) ->
	      XRels = [
		  udep!relation{ type => Type,
				 dep => Det1,
				 head => Head,
				 name => 'R_det',
				 eid => EId2
			       }
	      ]
	  ;
	  true
	 )
     ; TCat = nc, 		% det complexe: (une multitude de) gens sont venus
       chain(N >> lexical @ de >> node{ cat => prep}) ->
	   Reroot = [reverse,redirect,dep_frozen],
	   Type = nmod
     ; fail, TLemma = 'son' ->
       Type = 'nmod:poss'
     ; domain(TLemma,[aucun]) ->
	   Type = advmod
     ;
     Type = det
    )
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::cat[adj,suffAdj], lemma => TLemma },
	       head => Head,
	       name => 'R_amod',
	       eid => EId
	     } :-
    edge{ id => EId,
	  type => adj,
	  source => Head::node{ cat => cat[nc,np,title,ncpred,pro,adj,pri] },
	  target => N
	},
    (node2udep(NId,Pos),
     conll_node_info(Pos,_,_,'NUM',_,_) ->
	 Type = 'nummod'
     ;
     Type = amod
    )
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::cat[prep], lemma => TLemma },
	       head => Head,
	       name => 'R_prep_case_or_mark',
	       reroot => Reroot,
	       eid => EId
	     } :-
    edge{ id => EId,
	  type => Kind::edge_kind[subst,lexical],
	  label => Label,
	  source => N,
	  target => _Head::node{ id => _HNId }
	},
    (TLemma = de,
     chain(N << edge_kind[subst,adj] << node{ id => _KId }),
     udep_number_node(_KId)
     ->
	 Type = expl,
	 _Head2 = _Head
    ; udep_number_node(_HNId),
      chain(_Head >> _ >> De::node{ cat => Prep, lemma => de } >> subst >> _Head2::node{}) ->
      Type = case
     ;
     fail,
     TLemma = 'comme' -> Type = 'mark' % 'comme X' seems to be seen as a clause in the guide (but not in the data)
     ; Label = label['N2','adjP','PP'] ->
       ( domain(TLemma,[des,au,del,el,as,al,doi,du,ou]) ->
	 Type = 'case:det'
       ;
       Type = 'case'
       ),
       _Head2 = _Head
     ; Label = 'S' ->
	   (udep_prep_as_mark(TLemma) ->
		%% should be always 'mark' but the data shows that it is only true for some rare prepositions
		Type = 'mark'
	    ;
	    Type = 'case'
	   ),
	   _Head2 = _Head
     ; Kind = lexical ->
       Type = case,
       _Head2 = _Head
     ; fail
    ),
    _Head2 ?= _Head,
    conll_down_till_non_empty(_Head2,Head),
    Reroot ?= [redirect,dep_frozen],
    true
.

:-extensional udep_prep_as_mark/1.

udep_prep_as_mark(pour).
udep_prep_as_mark(apr�s).
udep_prep_as_mark(avant).
udep_prep_as_mark(en).
udep_prep_as_mark(sans).

:-light_tabular udep_number_node/1.
:-mode(udep_number_node/1,+(+)).

udep_number_node(NId) :-
%    node2udep(NId,Pos),
    %    conll_node_info(Pos,_,_,'NUM',_,_)
    node{ id => NId, lemma => Lemma::number[] }
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::cat[prep], lemma => TLemma },
	       head => Head,
	       name => 'R_prep_rel',
	       eid => EId
	     } :-
    edge{ id => EId,
	  type => lexical,
	  label => prep,
	  target => N,
	  source => V::node{}
	},
    chain(V >> lexical @ label[preparg,prel] >>  Head::node{ cat => prel }),
    Type = case
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::cat[prep], lemma => Lemma },
	       head => Head,
	       name => 'R_en_mark',
	       eid => EId
	     } :-
    edge{ id => EId,
	  type => lexical,
	  label => en,
	  source => Participial::node{},
	  target => N
	},
    (chain(Participial >> subst @ 'SubS' >> Head::node{cat => v})
     ; chain(Participial >> subst @ label[coord2,coord3] >> Head::node{cat => v})
    ),
    Type = mark
.

%% complex det: (une multitude de) gens sont venus
udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::cat[prep], lemma => Lemma::de },
	       head => Head,
	       name => 'R_prep_det',
	       eid => EId
	     } :-
    edge{ id => EId,
	  type => lexical,
	  label => de,
	  source => Quant::node{ cat => QCat },
	  target => N
	},
    QCat \== predet,
    (chain(Quant << subst @ det << Head::node{}) xor chain(Quant << adj @ det << node{ cat => det } << subst @ det << Head::node{})),
    Type = case
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::cat[prep], lemma => Lemma::de },
	       head => Head,
	       name => Name::'R_prep_partitive',
	       eid => EId,
	       xrels => XRels
	     } :-
    edge{ id => EId,
	  type => lexical,
	  label => prep,
	  source => _Head::node{ cat => _HCat },
	  target => N
	},
    (_HCat = cat[pri,pro] ->
	 Head = _Head
     ;
     chain(_Head >> EId2 : subst @ 'SRel' >> V::node{ cat => v } >> adj >> node{} >> lexical @ prel >> Head::node{}),
     XRels = [
	 udep!relation{ type => acl,
			dep => V,
			head => Head,
			name => Name,
			eid => EId2
		      }
     ]
    ),
    Type = case
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::cat[prep], lemma => Lemma::de },
	       head => Head,
	       name => 'R_prep_quantity_mod',
	       eid => EId
	     } :-
    edge{ id => EId,
	  type => lexical,
	  label => prep,
	  source => Quant::node{ cat => adv },
	  target => N
	},
    chain( N << EId : lexical @ prep << node{ cat => adv } << adj @ 'N2' << Head::node{}),
    Type = case
.


udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::cat[csu], lemma => TLemma },
	       head => Head,
	       name => 'R_csumod_mark',
	       reroot => [redirect,dep_frozen],
	       eid => EId
	     } :-
    edge{ id => EId,
	  type => subst,
	  label => 'SubS',
	  source => N,
	  target => _Head
	},
    Type = mark,
    conll_down_till_non_empty(_Head,Head)
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat, lemma => TLemma },
	       head => Head,
	       name => 'R_CS',
	       eid => EId
	     } :-
    edge{ id => EId,
	  type => subst,
	  label => 'SubS',
	  source => Intro::node{ cat => csu },
	  target => N
	},
    chain(Intro << adj << _Head::node{}),
    conll_up_till_non_empty(_Head,Head),
    (TCat = prep ->
	 Type = nmod
     ;
     Type = advcl
    )
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat, lemma => TLemma },
	       head => Head,
	       name => 'R_CS_noun',
	       eid => EId
	     } :-
    chain(N << subst << node{ cat => comp } << EId : subst @ 'SubS' << Intro::node{ cat => csu }),
    chain(Intro << adj << Head::node{}),
    Type = nmod
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::cat[csu], lemma => TLemma },
	       head => Head,
	       name => 'R_csu_xcomp_mark',
	       eid => EId
	     } :-
    edge{ id => EId,
	  type => lexical,
	  label => csu,
	  source => V::node{ id => VId, cat => cat[v,adj] },
	  target => N
	},
    chain(V >> (subst @ xcomp) >> Head::node{}),
    
    Type = mark
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::cat[prep], lemma => TLemma },
	       head => Head,
	       name => 'R_prep_xcomp_mark',
	       reroot => [redirect,dep_frozen],
	       xrels => XRels,
	       eid => EId
	     } :-
    edge{ id => EId,
	  type => lexical,
	  label => prep,
	  source => V::node{ id => VId, cat => cat[v,adj,adv] },
	  target => N
	},
    chain(V >> (subst @ xcomp) >> _Head::node{}),
    %% following the guideline, it should be 'mark', but in the dataset, we mostly found 'case' !
    %%    Type = mark,
    (chain(V >> EId2 : lexical @ ce >> Ce::node{ cat => ce }),
     chain(V >> lexical @ csu >> Que::node{ cat => que })
     ->
	 Head = Que,
	 Type = fixed,
	 XRels = udep!relation{ type => fixed,
				dep => Ce,
				head => Que,
				name => 'R_prep_xcomp_mark',
				eid => EId2
			      }
     ;	 TLemma = pour ->
	     Type = mark,
	     Head = _Head
     ;
     Type = case,
     Head = _Head
    )
.


udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::cat[v,adj,nc,np], lemma => TLemma, cluster => cluster{ left => NLeft} },
	       head => Head,
	       name => 'R_xcomp',
	       reroot => Reroot,
	       eid => EId
	     } :-
    edge{ id => EId,
	  type => subst,
	  label => xcomp,
	  source => _Head::node{ id => VId, cat => HCat::cat[v,adj,nc,adv], lemma => HLemma,
				cluster => cluster{ left => HLeft }},
	  target => N
	},
     %% avoid citations (as direct xcomp)
    HLeft =< NLeft,
     \+ chain(_Head >> lexical >> node{ lemma => ':' }),
     (TCat = cat[nc,np] ->
	  (HLemma = tel,
	   chain(_Head << adj @ 'N2' << Head::node{}) ->
	       true
	   ;
	   Head = _Head
	  ),
	  Type = nmod
      ; HCat = cat[nc] ->
	Type = acl,
	Head = _Head
     ; HCat = adv ->
       Type = advcl,
       Head = _Head
     ; chain(N >> (_ @ label[subject,impsubj]) >> node{}) ->
       Type = ccomp,
       	Head = _Head
     ; udep_modal(HLemma),
       \+ chain(_Head >> lexical @ label[csu,prep] >> node{})
       ->
	   Type = aux,
	   Reroot = [reverse,redirect,dep_frozen],
	   Head = _Head
     ;
     Type = xcomp,
     Head = _Head
     ),
     Head ?= _Head
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat, lemma => TLemma, cluster => cluster{ left => NLeft}},
	       head => Head,
	       name => 'R_xcomp_direct',
	       reroot => Reroot,
	       eid => EId
	     } :-
    ( S = N
     ; chain( N << subst @ comp << S)
     ; chain( N << subst << Comp::node{ cat => comp } << subst @ comp << S::node{} >> subst @ start >> node{} )
     ; chain( N << subst @ quoted_PP << node{} << subst << Comp::node{ cat => comp } << subst @ comp << S::node{} >> subst @ start >> node{} )
     ; chain( N << subst @ 'SubS' << node{ cat => csu, lemma => que} << subst @ 'CS' << CS::node{}), S=CS
    ),
    chain( S << EId : subst @ xcomp << Head::node{ cat => HCat, cluster => cluster{ left => HLeft }}),
    HCat \== coo,
    Type = parataxis,
    ( HLeft > NLeft ->
	  Reroot = [reverse]
     ;
     chain(Head >> lexical >> Sep::node{ lemma => ':', cluster => cluster{ left => SLeft }}),
     HLeft =< SLeft,
     SLeft =< NLeft
    )
.
    
    

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::cat[v,adj], lemma => TLemma },
	       head => Head,
	       name => 'R_xsubj',
	       eid => EId
	     } :-
    edge{ id => EId,
	  type => subst,
	  source => Intro::node{ id => IntroId, cat => IntroCat::cat[prep,csu] },
	  target => N
	},
    chain(Intro << subst @ subject << Head::node{ id => VId }),
    ( chain(Head >> impsub >> node{}) ->
	  (IntroCat = prep -> Type = xcomp ; Type = ccomp)
     ; udep_passive_verb(VId) ->
	   Type = 'csubj:pass'
     ;
     Type = csubj
    )
.


udep!relation{ type => cop,
	       dep => V::node{ id => NId, cat => TCat::cat[v], lemma => TLemma::estre },
	       head => Head,
	       name => 'R_comp',
	       reroot => [redirect,dep_frozen],
	       eid => EId
	     } :-
    edge{ id => EId,
	  type => edge_kind[subst,lexical],
	  label => comp,
	  source => V,
	  target => _Head::node{ cat => _HCat }
	},
    \+ chain( V >> _ @ label[object,xcomp] >> node{} ), % ats but not ato
    (_HCat = cat[adj,prel,pri,cla,v,nc,np] ->
	 Head =  _Head
     ; chain( _Head >> subst @ 'N2' >> _Head2::node{} ) ->
	    conll_down_till_non_empty(_Head2,Head::node{})
     ; _HCat = prep,
       (chain( _Head >> edge_kind[subst,lexical] >> Head) xor Head = _Head)
    )
.

udep!relation{ type => xcomp,
	       dep => Head,
	       head => V::node{ id => NId, cat => TCat::cat[v], lemma => TLemma },
	       name => 'R_comp_alt',
	       reroot => Reroot,
	       eid => EId
	     } :-
    edge{ id => EId,
	  type => edge_kind[subst,lexical],
	  label => comp,
	  source => V,
	  target => _Head::node{ cat => _HCat }
	},
    \+ TLemma = estre,
    \+ chain( V >> _ @ label[object,xcomp] >> node{} ), % ats but not ato
    (_HCat = cat[adj,prel,pri,cla,v,nc,np] ->
	 Head =  _Head
     ; chain( _Head >> subst @ 'N2' >> _Head2::node{} ) ->
	    conll_down_till_non_empty(_Head2,Head::node{})
     ; _HCat = prep,
       (chain( _Head >> edge_kind[subst,lexical] >> Head) xor Head = _Head)
    )
.


udep!relation{ type => cop,
	       dep => V::node{ id => NId, cat => TCat::cat[aux], lemma => estre },
	       head => Head,
	       name => 'R_comp_special',
	       reroot => [redirect,dep_frozen],
	       eid => EId
	     } :-
    chain( V << EId : adj @ 'S' << S::node{ cat => 'S' } >> subst @ comp >> _Head::node{}),
    node!empty(S),
    conll_down_till_non_empty(_Head,Head)
.


udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat, lemma => TLemma },
	       head => Head,
	       name => 'R_comp_ncarg_object',
	       eid => EId
	     } :-
    edge{ label => 'N2',
	  type => subst,
	  source => Comp::node{ cat => comp },
	  target => N
	},
    chain(Comp << subst @ comp << V::node{ cat => v} >> _ @ label[object,xcomp] >> node{}),
    Head = V,
    Type = xcomp
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::cat[adj,prel,v,pri,cla], lemma => TLemma },
	       head => Head,
	       name => 'R_comp_adjarg_object',
	       eid => EId
	     } :-
    edge{ label => comp,
	  type => edge_kind[subst,lexical],
	  source => Head::node{ cat => v },
	  target => N
	},
    chain(Head >> _ @ label[object,xcomp] >> node{}),
    (domain(TLemma,['comment?',comment]) ->
	 Type = advmod
     ;
     Type = xcomp
    )
.


udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::cat[prel], lemma => TLemma },
	       head => Head,
	       name => 'R_prel_arg',
	       eid => EId
	     } :-
    (edge{ id => EId,
	  type => lexical,
	  label => preparg,
	  source => Head::node{ id => HNId},
	  target => N
	 }
     ; chain( N << EId : lexical @ prel << Head )
    ),
    \+ chain(Head >> subst @ 'N2Rel' >> node{}),
    (fail,			% seems to be an error in the French annotation guide !
     udep_passive_verb(HNId),
     chain(Head >> lexical @ prep >> node{ lemma => par }) ->
	 Type = 'nsubj:pass'
     ; TLemma = dont ->
	   Type = iobj
     ;
     Type =nmod
    )
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat, lemma => TLemma },
	       head => Head2,
	       reroot => Reroot,
	       name => 'R_prep_arg',
	       xrels => XRels,
	       eid => EId
	     } :-
    edge{ id => EId,
	  type => Kind::edge_kind[subst,lexical],
	  source => _Intro::node{ id => IntroNId, cat => prep, lemma => IntroLemma },
	  target => _N
	},
    (chain(_Intro << adj @ 'PP' << Intro::node{ cat => prep }) ->
	 %%  for paths such as : de Paris � Rome
	 true
     ;
     Intro = _Intro
    ),
    conll_down_till_non_empty(_N,N::node{ id => NId }),
    (IntroLemma = de,
     chain( _Intro << _ @ _ << Head),
     udep_number_node(HNId) ->
	 Head = Head2,
	 Type = nummod,
	 Reroot = [reverse,redirect]
     ; chain( Intro << adj @ label['N2','adjP'] << Head::node{ cat => HCat, id => HNId }) ->
	   (TCat = v ->
		Type = acl
	    ;TCat = adj ->
		 Type = amod
	    ; HCat = adj ->
	      Type = obl
	    ;
	    Type = nmod
	   )
     ; chain( Intro << subst @ label['PP',wh] << node{} << adj << Head),
       HCat = cat[v,adj,coo,prep,nc,aux,adv] ->
       (TCat = v -> Type = advcl ;
	HCat = cat[v,adj,prep,aux,adv] -> Type = obl ;
	Type = nmod)
     ;  chain( Intro << subst @ 'PP' << _H1::node{ cat => 'S'} >> subst @ start >> node{}) ->
	    conll_up_till_non_empty(_H1,Head),
	    Type = nmod
     ;  chain( Intro << subst @ 'PP' << node{ cat => cat['S','VMod'] } << adj << _H1::node{ cat => 'S'} >> subst @ start >> node{}) ->
	    conll_up_till_non_empty(_H1,Head),
	    Type = nmod
     ; chain( Intro << comp << V::node{ cat => v } >> (_ @ object) >> node{}) ->
	   Type = xcomp,
	   Head = V
     ;  chain( Intro << (subst @ preparg) << Head ),
	HCat = cat[v,adj,adv,aux] ->
	    ( TCat = v -> Type = xcomp
	     ; TCat = adj -> Type = amod
	     ; fail,
	       IntroLemma = par,
	       udep_passive_verb(HNId) -> Type = 'nsubj:pass'
	     ; Type = obl
	    )
     ;
     fail
    ),
    (HCat = cat[v,aux] -> udep_main_verb_alt(Head,Head2) ; Head2 = Head),
    Reroot ?= [intro(IntroNId)]
.

udep!relation{ type => 'appos',
	       dep => N::node{ id => NId },
	       head => Head,
	       name => 'R_genitive',
	       eid => EId
	     } :-
    edge{ id => EId,
	  type => subst,
	  label => 'NGen',
	  target => _N,
	  source => _Head::node{}
	},
    conll_up_till_non_empty(_Head,Head::node{}),
    conll_down_till_non_empty(_N,N)
    .
    
	  

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::cat[pri], lemma => TLemma },
	       head => Head,
	       name => 'R_prep_wh',
	       eid => EId
	     } :-
    chain(N << EId : subst @ preparg << _Head::node{}),
    domain(TLemma,['comment?','o�?','quand?','combien?','pourquoi?',
		   comment,pourquoi,quand,o�,combien]),
    udep_main_verb_alt(_Head,Head),
    Type = advmod
.
    
udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::cat[v,adj], lemma => TLemma },
	       head => Head,
	       name => 'R_rel',
	       eid => EId
	     } :-
    edge{ id => EId,
	  type => subst,
	  label => 'SRel',
	  target => N,
	  source => Intro::node{}
	},
    (Intro = node{ cat => ce, lemma => ce } -> Head = Intro
    ; chain( Intro << (adj @ label['N2',adv,adj]) << Head::node{} )
    ; chain( Intro << (adj @ 'S') << V::node{ cat => v} >> _ @ subject >> Head::node{}  )
    ),
    Type = 'acl:relcl'
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::cat[v], lemma => TLemma },
	       head => Head,
	       name => 'R_participiale',
	       eid => EId
	     } :-
    edge{ id => EId,
	  target => N,
	  source => Intro::node{ cat => IntroCat},
	  label => 'SubS',
	  type => subst
	},
    IntroCat \== csu,
    \+  udep_find_subject(N,_),
    (chain( Intro << adj @ 'N2' << Head::node{ cat => HCat } ) ->
	 Type = acl
     ; IntroCat = coo,
       chain(Intro << adj @ coo << node{ cat => coo } << adj << Head::node{} ) ->
	   Type = acl
     ; chain( Intro << adj << V::node{cat => VCat::cat[v,adj]} ),
       ( VCat = v,
	 udep_find_subject(V,Head) ->
	     Type = acl
	; Head = V,
	  Type = advcl
       )
    )
.

:-std_prolog udep_find_subject/2.

udep_find_subject(V::node{},Subject::node{}) :-
    ( chain(V >> (_ @ subject) >> Subject) ->
	  true
     ; chain( V >> adj @ label['Infl','V'] >> V2::node{}),
       udep_find_subject(V2,Subject)
    )
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::cat[adv,advneg,pri,'adjPref','advPref'], lemma => TLemma },
	       head => Head,
	       name => 'R_adv',
	       reroot => Reroot,
	       eid => EId
	     } :-
    edge{ id => EId,
	  target => N,
	  source => _Head::node{cat => _HCat },
	  label => Label,
	  type => Kind::edge_kind[adj,lexical]
	},
    (TCat = pri ->
	 domain(TLemma,['comment?','o�?','quand?','combien?','pourquoi?',
		       comment,pourquoi,quand,o�,combien])
     ; Label = label[pas] ->
	   Kind = lexical
     ; Label = label[advneg] ->
	   true
     ;
     Kind = adj
    ),
    (_HCat = cat[prep,csu] ->
	 Reroot = [dep_frozen]
     ;
     true
    ),
    (_HCat = cat[comp] ->
	 conll_down_till_non_empty(_Head,Head)
     ; Label = pas,
       _HCat = coo,
       chain(_Head >> subst @ label[coord3,coord2,coord] >> _Head2::node{}) ->
	   conll_down_till_non_empty(_Head2,Head)
     ;
     conll_up_till_non_empty(_Head,Head)
    ),
    (domain(TLemma,[oui,non,bref]),
     (_HCat = cat[v,aux,coo]
     xor chain(_Head >> subst @ start >> node{}) % attached on short sentence
     )
     ->
	 Type = discourse
     ; TCat = advneg ->
	 Type = advmod
     ;
     Type = advmod
    )
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::cat[nc], lemma => TLemma },
	       head => Head,
	       name => 'R_timemod',
	       eid => EId
	     } :-
    edge{ id => EId,
	  target => N,
	  source => _Head::node{},
	  label => time_mod,
	  type => subst
	},
    conll_up_till_non_empty(_Head,Head::node{ tree => HTree }),
    Type = nmod
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat, lemma => TLemma, form => TForm },
	       head => Head,
	       name => 'R_punct',
	       reroot => Reroot,
	       eid => EId
	     } :-
    node2udep(NId,Pos),
    conll_node_info(Pos,_,_,CPOSTAG,_,_),
    domain(CPOSTAG,['PUNCT','SYM']),
    (CPOSTAG = 'PUNCT' ->
	 Type = punct
     ;
     Type = dep
    ),
    edge{ id => EId,
	  target => N,
	  label => Label,
	  source => _Head::node{ cat => _HCat },
	  type => lexical
	},
    (_HCat = coo ->
	 chain(_Head << adj << _Head2),
	 conll_up_till_non_empty(_Head2,Head)
     ; _HCat = incise,
       domain(TForm,['(',')','[',']','<','>']),
       chain(_Head << adj << _Head2::node{}) ->
	   (conll_down_till_non_empty(_Head2,Head)
				     xor chain(_Head2 >> lexical >> Head::node{})
				     xor conll_up_till_non_empty(_Head2,Head)
	   )
     ; domain(TForm,['"','''','�','�']) ->
	   ( conll_down_till_non_empty(_Head,Head) xor conll_up_till_non_empty(_Head,Head)),
	   true
     ;
     conll_up_till_non_empty(_Head,_Head2),
     udep_main_verb(_Head2,Head)
    ),
    (Label = skip ->
	 Reroot = [up]
     ;
     true
    )
.

udep!relation{ type => mark,
	       dep => N::node{ id => NId, cat => TCat::cat[que,csu], lemma => TLemma },
	       head => Head,
	       name => 'R_que_xcomp',
	       eid => EId
	     } :-
    domain(TLemma,[que,si]),
    edge{ id => EId,
	  type => lexical,
	  label => Label,
	  target => N,
	  source => V::node{cat => cat[v,adj,nc]}
	},
    domain(Label,[csu,siwh]),
    chain(V >> subst @ xcomp >> Head::node{})
.
    

udep!relation{ type => cc,
	       dep => N::node{ id => NId, cat => TCat::coo, lemma => TLemma, cluster => cluster{left => N_Left} },
	       head => Head,
	       name => 'R_coo',
	       eid => EId
	     } :-
    ( edge{ id => EId,
	    type => edge_kind[subst,lexical],
	    source => N,
	    target => _Head,
	    label => 'coord3'
	  }
    ;
    edge{ id => EId,
	  type => lexical,
	  target => N,
	  source => N2 :: node{ cat => coo, cluster => cluster{ left => N2_Left } },
	  label => coo21
	},
    edge{ type => edge_kind[subst,lexical],
	  source => N2,
	  target => _Head::node{ cluster => cluster{ left => _Head_Left}} ,
	  label => 'coord2'
	},
    N_Left < _Head_Left,
    _Head_Left < N2_Left
    ),
    conll_down_till_non_empty(_Head,Head)
.

udep!relation{ type => conj,
	       dep => N::node{ id => NId, cat => TCat, lemma => TLemma, cluster => cluster{ left => N_Left } },
	       head => Head,
	       name => 'R_coord',
	       eid => EId,
	       xrels => XRels
	     } :-
    (TCat = prep,
     TLemma = de,
     chain(N << adj @ 'PP' << Proxy::node{ cat => prep }) ->
	 N2 = Prep
     ;
     N2 = N
    ),
    edge{ id => EId,
	  type => edge_kind[lexical,subst],
	  label => Label::label[coord2,coord3,xcomp],
	  source => COO::node{ cat => coo, cluster => cluster{ left => COO_Left }},
	  target => N2
	},
    chain(COO << adj << _Head::node{}),
    (Label = xcomp ->
	 chain(_Head >> subst @ xcomp >> Head::node{}),
	 (chain(COO >> EId2 : lexical @ (Intro_L::label[prep,csu]) >> Intro::node{ cluster => cluster{ left => Intro_Left }}),
	  Intro_Left < N_Left,
      \+ (
          chain(COO >> lexical @ Intro_L >> node{ cluster => cluster{ left => _Intro_Left }}),
	  Intro_Left < _Intro_Left,
	  _Intro_Left =< N_Left
      )
      ->
	  XRels = [
	      udep!relation{ type => mark,
			     dep => Intro,
			     head => N,
			     name => 'R_coord',
			     eid => EId2
			   }
	  ]
      ;
      true
     )
     ;
     conll_up_till_non_empty(_Head,Head)
    )
.

udep!relation{ type => conj,
	       dep => N::node{ id => NId, cat => TCat, lemma => TLemma },
	       head => Head,
	       name => 'R_enum',
	       eid => EId
	     } :-
    chain(N << EId : edge_kind[lexical,subst] @ coord << node{ cat => _HCat } << adj << Head::node{ cat => HCat }),
    _HCat \== 'S'		% S=parataxis
.

udep!relation{ type => 'cc:nc',
	       dep => COO::node{ id => TId, cat => coo },
	       head => Head::node{},
	       name => 'R_starter_coord',
	       eid => EId
	     } :-
    chain( COO << EId : edge_kind[lexical] @ L << Head::node{ cat => 'S'} ),
    domain(L,[starter,coo]),
    true
    .

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::clneg, lemma => TLemma, cluster => cluster{ lex => TIds } },
	       head => Head,
	       name => 'R_ne',
	       reroot => [dep_frozen], % it seems that ne_clneg stay attached to the main verb
	       eid => EId
	     } :-
    edge{ id => EId,
	  type => edge_kind[lexical],
	  source => _Head,
	  target => N
	},
    ( edge{ id => _EId,
	    label => object,
	    source => _Head,
	    target => node{ cluster => cluster{lex => TIds }}} ->
      %% agglutinates like nel
      Type = obj
    ;
    Type = advmod
    ),
    udep_main_verb_alt(_Head,Head)
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat, lemma => TLemma },
	       head => Head,
	       name => 'R_appos_pos',
	       eid => EId
	     } :-
    edge{ id => EId,
	  type => edge_kind[lexical,subst],
	  label => label['Np2','N2app'],
	  source => _Head,
	  target => N
	},
    conll_up_till_non_empty(_Head,Head),
    (TCat = np ->
	 %% for titles, should be related to a semantic property title
	 Type = flat
    ;
    Type = appos
    )
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::prep, lemma => TLemma::de },
	       head => Head,
	       name => 'R_predet_de',
	       eid => EId,
	       xrels => XRels
	     } :-
    edge{ id => EId,
	  type => lexical,
	  label => de,
	  source => PreDet::node{ cat => predet },
	  target => N
	},
    (chain(PreDet << subst @ det << Head::node{}) ->
	 Type = det
     ;
     chain(PreDet << adj @ det << Head::node{}) ->
	 Type = advmod
     ;
     fail
    ),
    XRels = [
	udep!relation{ type => fixed,
		       dep => PreDet,
		       head => N,
		       name => 'R_predet',
		       eid => EId
		     }
    ]
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::cat[nc,np], lemma => TLemma },
	       head => Head,
	       name => 'R_appos_alt',
	       eid => EId
	     } :-
    edge{ id => EId,
	  type => adj,
	  label => cat['N','N2'],
	  source => Head::node{ cat => cat[pro,nc,np] },
	  target => N
	},
    Type = fixed
.


udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::cat[prel], lemma => TLemma },
	       head => Head,
	       name => 'R_cleft_que',
	       eid => EId
	     } :-
    edge{ id => EId,
	  label => 'CleftQue',
	  type => lexical,
	  target => N,
	  source => Intro::node{}
	},
    (chain(Intro >> adj @ label['S','S2'] >> node{ cat => aux, lemma => estre}) ->
	 Head = Intro
     ; chain(Intro << adj << Intro2::node{}),
       conll_up_till_non_empty(Intro2,Head::node{}),
       chain(Head >> adj @ label['S','S2'] >> node{ cat => aux, lemma => estre})
    ),
    Type = mark
.

udep!relation{ type => cop,
	       dep => Est::node{ id => NId, cat => TCat::cat[aux], lemma => TLemma::estre, tree => Tree },
	       head => Head,
	       name => 'R_cleft_estce',
	       reroot => [redirect,dep_frozen],
	       eid => EId
	     } :-
    edge{ id => EId,
	  type => adj,
	  label => Label::label['S','S2'],
	  target => Est,
	  source => V::node{ cat => cat[v,adj], cluster => cluster{ left => VLeft }}
	},
    (Label = 'S2',
     chain(V >> lexical @ 'CleftQue' >> node{ cluster => cluster{ left => Left }}),
     chain(V >> edge_kind[lexical,subst] >> Arg::node{ cluster => cluster{ right => ARight }}),
     ARight =< Left
     ->
	 _Head = Arg
     ; Label = 'S',
       chain(V >> adj >> Mod::node{} >> lexical @ 'CleftQue' >> node{ cat => prel }) ->
	   conll_down_till_non_empty(Mod,_Head::node{ cat => _HCat} )
     ; chain(Est >> lexical @ csu >> node{ cat => que, lemma => que }),
       chain(Est << adj << _Head1::node{}) ->
       (node!empty(_Head1),
	chain(_Head1 >> subst @ comp >> QueComp::node{ cat => pri}) ->
	    _Head = QueComp
	;
	_Head = _Head1
       )
     ; chain(V >> adj @ 'S' >> node{} >> lexical >> PRI::node{ cat => pri }) ->
	   Head = PRI
     ;
     fail
    ),
    (_HCat = cat[prep,csu],
     chain(_Head >> edge_kind[subst,lexical] >> Head::node{}) ->
	 true
     ;
     Head = _Head
    )
.

:-rec_prolog udep_reverse_relation/3.

%% could be rewritten using the reverse constraint in Reroot ?
udep_reverse_relation(N::node{ cluster => cluster{ right => NRight }},
		      V::node{ cat => cat[v,adj], cluster => cluster{ left => VLeft} },
		      ccomp
		     ) :-
    NRight =< VLeft,
    chain(V >> adj @ (Label::label['S','S2']) >> node{ cat => aux, lemma => estre }),
    (Label = 'S2' ->
	 chain(V >> lexical @ 'CleftQue' >> CleftQue::node{ cluster => cluster{ left => Left }}),
	 NRight =< Left
     ; Label = 'S',
       chain(V >> adj >> Mod::node{} >> lexical @ 'CleftQue' >> CleftQue),
       NRight =< Left
    )
.

udep!relation{ type => advmod,
	       dep => N::node{ id => NId, cat => TCat::cat[que_restr], lemma => TLemma, cluster => cluster{ right => QRight } },
	       head => Head,
	       name => 'R_que_restr',
	       eid => EId
	     } :-
    edge{ id => EId,
	  label => advneg,
	  type => lexical,
	  source => _Head::node{ cat => _HCat },
	  target => N
	},
%    format('que restr head=~w\n',[_Head]),
    (node!empty(_Head),
     chain(_Head << adj << node{ cat => cat[v,adj]}) ->
	 %% que_rest applied on a verb modifier
	 conll_down_till_non_empty(_Head,Head)
     ; _HCat = cat[adj,v] ->
	   %% que_restr applied on a verb argument => need to retrieve it !
	   (chain(_Head >> edge_kind[subst,lexical] @ _ >> _Head2::node{ cluster => cluster{ left => ALeft, right => ARight }})
	    ; chain(_Head << adj @ 'V' << _Head2)
	   ),
	   QRight =< ALeft,
	   \+ ( chain(_Head >> _ >> _N::node{ cluster => cluster{ left => _Left, right => _Right}}),
                \+ _N = _Head2,
                QRight =< _Left,
		_Right =< ALeft
	      ),
	   conll_down_till_non_empty(_Head2,Head)
     ;
     Head = _Head
    )
.
    
udep!relation{ type => parataxis,
	       dep => N::node{ id => NId, cat => TCat, lemma => TLemma},
	       head => Head,
	       name => 'R_juxt',
	       eid => EId
	     } :-
    ( edge{ id => EId,
	  type => subst,
	  label => 'S',
	  target => N,
	  source => _Sep::node{}
	  }
     ; chain( N << subst << node{ cat => comp } << subst @ comp << Start2::node{ cat => 'S' } >> subst @ start >> node{}),
       chain(Start2 << EId : subst @ 'S' << _Sep)
     ; chain( N << subst @ comp << Start2::node{ cat => 'S' } >> subst @ start >> node{}),
       chain(Start2 << EId : subst @ 'S' << _Sep)
     ; chain( N << subst @ 'SubS' << node{ cat => csu, lemma => que } << subst @ 'CS' << node{} << EId : subst @ 'S' << _Sep)
    ),
    conll_up_till_sep_S(_Sep,Sep::node{}),
    ( conll_up_till_non_empty(Sep,Head) -> true
     ; chain(Sep << adj << Start::node{} >> subst @ start >> node{ cat => start}),
       chain(Start>> subst @ comp >> Short::node{}),
       conll_down_till_non_empty(Short,Head)
    )
.

udep!relation{ type => parataxis,
	       dep => N::node{ id => NId, cat => TCat, lemma => TLemma},
	       head => Head,
	       name => 'R_S_incise',
	       eid => EId
	     } :-
    edge{ id => EId,
	  type => subst,
	  label => 'S_incise',
	  target => N,
	  source => _Mod::node{}
	},
    conll_up_till_non_empty(_Mod,Head)
.

udep!relation{ type => advmod,
	       dep => N::node{ id => NId, cat => TCat, lemma => TLemma},
	       head => Head,
	       name => 'R_S_incise_short',
	       eid => EId
	     } :-
    chain(N << subst << node{} << subst @ comp << _H::node{} >> subst @ start >> node{}),
    chain(_H << EId : subst @ 'S_incise' << Head::node{})
.

udep!relation{ type => parataxis,
	       dep => N::node{ id => NId, cat => TCat, lemma => TLemma},
	       head => Head,
	       name => 'R_S_enum',
	       eid => EId
	     } :-
    edge{ id => EId,
	  type => subst,
	  label => 'coord',
	  target => N,
	  source => Head::node{ cat => 'S'}
	}
.

udep!relation{ type => parataxis,
	       dep => N::node{ id => NId, cat => TCat::ce, lemma => TLemma},
	       head => Head,
	       name => 'R_ce_rel',
	       eid => EId
	     } :-
    chain( N << EId : subst @ ce_rel << Head::node{} )
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::cll, lemma => TLemma},
	       head => Head,
	       name => 'R_ilya_y',
	       eid => EId
	     } :-
    edge{ id => EId,
	  type => lexical,
	  label => cll,
	  source => Head::node{ lemma => avoir, cat => v, tree => VTree},
	  target => N
	},
    domain(verb_ilya_as_time_mod,VTree),
    Type = expl
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::v, lemma => TLemma::avoir, tree => TTree},
	       head => Head,
	       name => 'R_ilya_a',
	       eid => EId
	     } :-
    edge{ id => EId,
	  type => adj,
	  source => _Head::node{},
	  target => N
	},
    domain(verb_ilya_as_time_mod,TTree),
    conll_up_till_non_empty(_Head,Head),
    Type = advcl
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::cat[nc,np,pro,adj], lemma => TLemma},
	       head => Head,
	       name => 'R_audience',
	       eid => EId
	     } :-
    edge{ id => EId,
	  type => subst,
	  label => label[audience,person_mod],
	  target => N,
	  source => Intro
	},
    conll_up_till_non_empty(Intro,Head),
    Type = vocative
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::cat[pro,xpro], lemma => TLemma},
	       head => Head,
	       name => 'R_audience',
	       eid => EId
	     } :-
    edge{ id => EId,
	  type => adj,
	  target => N,
	  source => _Head::node{ cat => cat[v,aux,adj,comp]}
	},
    Type = nmod,
    conll_down_till_non_empty(_Head,Head)
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::pres, lemma => TLemma},
	       head => Head,
	       name => 'R_interjection',
	       eid => EId
	     } :-
    edge{ id => EId,
	  type => adj,
	  target => N,
	  source => _Head
	},
    conll_up_till_non_empty(_Head,Head),
    Type = discourse
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::que, lemma => TLemma::que},
	       head => Head,
	       name => 'R_comparative_que',
	       eid => EId
	     } :-
    ( edge{ id => EId,
	    type => lexical,
	    label => que,
	    target => N,
	    source => Supermod::node{ cat => supermod }
	  },
      chain(Supermod >> edge_kind[lexical,subst] @ 'Modifier' >> Mod::node{}),
      chain(Supermod << adj @ supermod << Adv::node{})
     ;
     chain( N << EId : lexical @ que << Mod),
     chain(Mod << adj @ supermod << Adv::node{})
    ),
    Type = mark,
    conll_down_till_non_empty(Mod,Head)
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat, lemma => TLemma},
	       head => Head,
	       name => 'R_comparative_mod',
	       eid => EId
	     } :-
    (chain(N << subst @ 'SubS' << Que::node{ cat => csu , lemma => que }) ->
	 _N = Que
     ;
     _N = N
    ),
    edge{ id => EId,
	  type => edge_kind[lexical,subst],
	  label => 'Modifier',
	  target => _N,
	  source => Supermod::node{ cat => supermod}
	},
%    chain(Supermod >> lexical @ que >> node{}),
    chain(Supermod << adj @ supermod << Adv::node{ cat => cat[adv,adj] } << adj << _Head::node{}),
    conll_up_till_non_empty(_Head,Head::node{ cat => HCat }),
    (TCat = cat[adj,v] ->
	 (HCat = cat[nc] ->
	      Type = acl
	  ;
	  Type = advcl
	 )
     ;
     Type = nmod
    )
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat, lemma => TLemma},
	       head => Head,
	       name => 'R_comparative_pour_mod',
	       eid => EId
	     } :-
    chain(N << EId : subst @ _ << Intro::node{ cat => prep, lemma => pour}
		     << adj @ supermod << Adv::node{ cat => cat[adv,adj] } << adj @ _ << _Head::node{}),
    conll_down_till_non_empty(_Head,Head::node{ cat => HCat }),
    ( TCat = cat[v] ->
	  (HCat = cat[nc,np] ->
	       Type = acl
	   ;
	   Type = advcl
	  )
     ; 
     Type = nmod
    )
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::clg, lemma => TLemma, form => en},
	       head => Head,
	       name => 'R_comparative_mod',
	       eid => EId
	     } :-
    edge{ id => EId,
	  type => lexical,
	  label => clg,
	  target => N,
	  source => Head::node{}
	},
    Type = iobj			% to be refined, following the underlying function
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::prep, lemma => TLemma::�},
	       head => Head,
	       name => 'R_range_�',
	       eid => EId
	     } :-
    edge{ id => EId,
	  type => lexical,
	  label => prep,
	  target => N,
	  source => Num1::node{cat => number}
	},
    chain(Num1 >> lexical @ number2 >> Head::node{ cat => number }),
    Type = case
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::number, lemma => TLemma},
	       head => Head,
	       name => 'R_range_num2',
	       eid => EId
	     } :-
    edge{ id => EId,
	  type => lexical,
	  label => number2,
	  target => N,
	  source => Head::node{cat => number}
	},
    Type = nmod
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::prep, lemma => TLemma::�},
	       head => Head,
	       name => 'R_range_�_alt',
	       xrels => XRels,
	       eid => EId
	     } :-
    chain( N << EId : lexical @ prep << First::node{ cat => adj} >> EId2 : lexical @ range >> Second::node{} ),
    Head = Second,
    Type = case,
    XRels = udep!relation{ type => amod,
			   dep => Second,
			   head => First,
			   name => 'R_range_�_alt',
			   eid => EId2
			 }
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat:: '_', lemma => TLemma, cluster => cluster{ right => Right}},
	       head => Head,
	       name => 'R_skip',
	       reroot => [dep_frozen],
	       eid => EId
	     } :-
    edge{ id => EId,
	  type => lexical,
	  label => skip,
	  target => N,
	  source => _Head
	},
    node2udep(NId,Pos),
    \+ conll_node_info(Pos,_,_,'PUNCT',_,_),
    ( Next::node{ id  => NextId,
		  cluster => cluster{ left => Right }
		},
      node2udep(NextId,_) ->
	  %% try to attach to the next word when possible
	  Head = Next
     ;
     conll_up_till_non_empty(_Head,Head)
    ),
    (TLemma = '_EPSILON' ->
	 Type = reparandum
     ; TLemma = '_META_TEXTUAL_GN' ->
	   Type = nmod
     ; TLemma = '_META_TEXTUAL_GP' ->
	   Type = nmod
     ;
     fail
    )
.


udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::que, lemma => TLemma::que},
	       head => Head,
	       name => 'R_estceque_que',
	       eid => EId
	     } :-
    edge{ id => EId,
	  type => lexical,
	  label => csu,
	  target => N,
	  source => V::node{ cat => aux, lemma => estre, tree => Tree}
	},
    domain(cleft_verb,Tree),
    chain(V >> lexical @ subject >> Head::node{ cat => cln, lemma => ce }),
    Type = fixed
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::cat[clr,cla], lemma => TLemma},
	       head => Head,
	       name => 'R_se',
	       eid => EId
	     } :-
    edge{ id => EId,
	  type => lexical,
	  label => clr,
	  target => N,
	  source => V::node{}
	},
    udep_main_verb_alt(V,Head),
    %% the guide indicate that se could be dobj, maybe iobj and also expl
    %% however only se as dobj is seen in French data
    Type = obj
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::nc, lemma => TLemma},
	       head => Head,
	       name => 'R_quant_mod',
	       reroot => [dep_frozen],
	       eid => EId
	     } :-
    edge{ id => EId,
	  type => adj,
	  target => N,
	  source => Head::node{ cat => cat[adv,prep,csu]}
	},
    %% the guide indicate that se could be dobj, maybe iobj and also expl
    %% however only se as dobj is seen in French data
    Type = nmod
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat, lemma => TLemma},
	       head => Head,
	       name => Name::'R_special_cleft',
	       eid => EId,
	       xrels => XRels
	     } :-
    edge{ id => EId,
	  type => subst,
	  label => subject,
	  target => N,
	  source => Intro::node{ tree => Tree}
	},
    node!empty(Intro),
    chain(Intro >> subst @ comp >> Comp::node{}),
    chain(Intro >> EId2 : adj @ 'S' >> Aux::node{ cat => aux, lemma => estre}),
    (domain(special_cleft_extraction_ante,Tree)
	    xor domain(special_cleft_extraction_post,Tree)
    ),
    Head = Comp,
    ( TCat = v ->
	  Type = csubj
     ;
     Type = nsubj
    ),
    XRels = [
	udep!relation{ type => cop,
		       dep => Aux,
		       head => Comp,
		       name => Name,
		       eid => EId2
		     }
    ]
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat, lemma => TLemma},
	       head => Head,
	       name => Name::'R_participiale_with_subj',
	       eid => EId
	     } :-
    chain( N << EId : subst @ 'SubS' << _Head::node{ cat => _HCat } ),
    \+ _HCat = cat[csu,prep],
    chain( N >> _ @ subject >> node{}),
    conll_up_till_non_empty(_Head,Head),
    Type = advcl
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::nc, lemma => TLemma, tree => TTree},
	       head => Head,
	       name => Name::'R_position',
	       eid => EId
	     } :-
    chain(N << EId : adj << _Head::node{}),
    ( domain(cnoun_as_position_mod,TTree) ->
	  Type = advmod
     ; domain(address_on_s,TTree) ->
	   Type = nmod
     ;
     fail
    ),
    conll_up_till_non_empty(_Head,Head)
.

udep!relation{ type => advmod,
	       dep => N::node{ id => NId, cat => TCat::cat[nc,np], lemma => TLemma, tree => TTree},
	       head => Head,
	       name => Name::'R_address',
	       eid => EId
	     } :-
    chain(N << EId : subst @ label[position,reference] << node{} << adj << _Head::node{}),
    conll_up_till_non_empty(_Head,Head)
.

udep!relation{ type => nmod,
	       dep => N::node{ id => NId, cat => TCat, lemma => TLemma, tree => TTree},
	       head => Head,
	       name => Name::'R_refmark',
	       eid => EId
	     } :-
    chain(N << EId : lexical << Intro::node{ tree => ITree } << adj << Head::node{}),
    domain(refmark_on_N2,ITree)
.



udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::cat[v,adj], lemma => TLemma},
	       head => Head,
	       name => Name::'R_deep_extract',
	       eid => EId,
	       xrels => XRels
	     } :-
    chain( N >> EId : adj @ 'S' >> Head::node{ cat => cat[v], tree => HTree }),
    ( chain( Head >> EId2: lexical @ prep >> Prep::node{ cat => prep } ) ->
	  Type = xcomp,
	  Intro = Prep
     ; chain( Head >> EId2: lexical @ csu >> Que::node{ cat => que } ) ->
	   Type = ccomp,
	   Intro = Que
     ; domain(verb_canonical_xcomp_by_adj,HTree) ->
	   Type = xcomp,
%%	   format('deep extract canonical head=~w dep=~w\n',[N,Head]),
	   XRels = []
     ;
     fail
    ),
    XRels ?= udep!relation{ type => mark,
			    dep => Intro,
			    head => N,
			    name => Name,
			    eid => EId2
			  }
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::cat[prep], lemma => TLemma},
	       head => Head,
	       name => Name::'R_prep_standalone',
	       eid => EId
	     } :-
    chain( N << EId: edge_kind[subst,adj] @ Label << Head::node{ cat => _HCat } ),
    _HCat \== coo,
    Label \== comp,
    \+ (chain( N >> _ @ _Label >> node{ cat => XCat }),
	XCat \== coo
       ),
    Type = advmod
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::cat[adj], lemma => TLemma},
	       head => Head,
	       name => Name::'R_adj_float',
	       eid => EId
	     } :-
    chain( N << EId : adj << _Head::node{ cat => _HCat::cat[v,aux,'N2'] }),
    (_HCat = 'N2',
     chain(_Head << adj @ 'N2' << Head::node{}) ->
	 Type = amod
     ; udep_find_subject(_Head,Head) ->
	   Type = amod
     ;
     udep_main_verb_alt(_Head,Head),
     Type = advcl
    )
.

udep!relation{ type => det,
	       dep => N::node{ id => NId, cat => TCat::cat[adj], lemma => TLemma},
	       head => Head,
	       name => Name::'R_adj_as_det',
	       eid => EId
	     } :-
    (chain( N << EId: adj @ det << node{ cat => det } << subst << Head::node{})
     ; chain( N << EId: lexical @ predet_ante << Head::node{})
     ; chain( N << EId : adj @ 'N' << Head), Head = node{ cat => ce }
    )
.

udep!relation{ type => fixed,
	       dep => N::node{ id => NId, cat => TCat::cat[title], lemma => TLemma},
	       head => Head,
	       name => Name::'R_Monsieur',
	       eid => EId
	     } :-
    chain( N << EId: lexical @ 'Monsieur' << Head::node{})
.

udep!relation{ type => advmod,
	       dep => N::node{ id => NId, cat => TCat, lemma => TLemma},
	       head => Head,
	       name => Name::'R_unsatN2',
	       eid => EId
	     } :-
    chain( N << subst @ starter << Head::node{} >> subst @ start >> node{})
.

udep!relation{ type => ccomp,
	       dep => N::node{ id => NId, cat => TCat, lemma => TLemma},
	       head => Head,
	       name => Name::'R_quoted_S',
	       eid => EId
	     } :-
    ( chain( N << EId : subst @ quoted_S << Head::node{} >> subst @ start >> node{} )
     ; chain( N << EId : subst @ quoted_S << node{}  << adj << Head::node{})
    )
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat, lemma => TLemma},
	       head => Head,
	       name => Name::'R_comparative',
	       eid => EId,
	       xrels => XRels
	     } :-
    chain(N << EId : subst @ 'Comparative' << Comp::node{} >> EId2 : lexical @ que >> Que::node{ cat => que }),
    chain(Comp << adj << Head::node{}),
    Type = nmod,
%%    format('## +++ found comparative at ~w\n',[N]),
    XRels = [udep!relation{ type => case,
			   dep => Que,
			   head => Comp,
			   name => Name,
			   eid => EId2
			  }
	    ],
    true
.

udep!relation{ type => Type,
	       dep => N::node{ id => NId, cat => TCat::prel, lemma => TLemma},
	       head => Head,
	       name => Name::'R_N2Rel',
	       eid => EId,
	       xrels => XRels
	     } :-
    chain( N << EId : lexical @ prel << N2::node{ cat => 'N2' } >> EId2 : subst @ 'N2Rel' >> Head::node{}),
    Type = nmod,
    XRels = udep!relation{ type => appos,
			   dep => Head,
			   head => N2,
			   name => Name,
			   eid => EId2
			 }
.

udep!relation{ type => nmod,
	       dep => N::node{ id => NId, cat => TCat::xpro, lemma => TLemma},
	       head => Head,
	       name => Name::'R_xpro',
	       eid => EId
	     } :-
    chain( N << EId : adj @ 'N2' << Head::node{})
.

%% should also be added for CONLL
udep!relation{ type => fixed,
	       dep => N::node{ id => NId, cat => TCat::que, lemma => TLemma::que},
	       head => Head,
	       name => Name::'R_plus_que',
	       eid => EId
	     } :-
    chain( N << EId : lexical @ que << Head::node{ cat => adv })
.

%% dislocated
udep!relation{ type => dislocated,
	       dep => N::node{},
	       head => Head,
	       name => Name::'R_dislocated',
	       eid => EId
	     } :-
    chain( N << EId : subst @ label[dcln,dcla,dcld,dclg,dcll] << _Head::node{ cat => 'S' } ),
    udep_main_verb(_Head,Head)
    .

%% participle on noun => acl
%% clause on noun => acl
%% relative on noun => acl:relcl
%% participiale related to subject => acl (question: when related to object ?)
%% csu-clause => advcl
%% verbal subject => csubj (or csubjpass)
%% clausal argument (with subject) => ccomp
%% (infinitive) clausal argument (without subject) => xcomp 

%% found using conllu_compounds.pl
%% updated on 2021 for new UD rules


% rule1 l'+en amod: 1+nsubj:head 29/54 53.70%
udep_mwe_simple_expansion(['l''', 'en'], [amod: 1, nsubj:head]).

% rule2 mes+sires det: 1+nsubj:head 25/32 78.12%
udep_mwe_simple_expansion(['mes', 'sires'], [det: 1, nsubj:head]).

% rule3 a+tant case: 1+advmod:head 23/25 92.00%
udep_mwe_simple_expansion(['a', 'tant'], [case: 1, advmod:head]).

% rule4 ja+mes advmod:head+advmod: -2 8/22 36.36%
% udep_mwe_simple_expansion(['ja', 'mes'], [advmod:head, advmod: -2]).

% rule5 de+si case:head+advmod: 1 19/20 95.00%
% udep_mwe_simple_expansion(['de', 'si'], [case:head, advmod: 1]).

% rule6 ja+mais advmod:head+advmod: 2 7/16 43.75%
% udep_mwe_simple_expansion(['ja', 'mais'], [advmod:head, advmod: 2]).

% rule7 l'+uis det: 1+obl:head 8/12 66.67%
% udep_mwe_simple_expansion(['l''', 'uis'], [det: 1, obl:head]).

% rule8 l' det:head 7/10 70.00%
% udep_mwe_simple_expansion(['l'''], [det:head]).


udep_mwe_simple_expansion(['por','quoi'],[case: 1, obl:head]).
udep_mwe_simple_expansion(['por','coi'],[case: 1, obl:head]).
udep_mwe_simple_expansion(['par','mi'],[case: 1, obl:head]).
udep_mwe_simple_expansion(['toz','jorz'],[det: 1, obl:head]).
udep_mwe_simple_expansion(['a','tant'],[case: 1, advmod:head]).
udep_mwe_simple_expansion(['tote','voies'],[det: 1, obl:head]).
udep_mwe_simple_expansion(['maintes','foiz'],[det: 1, obl:head]).
