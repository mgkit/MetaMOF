/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2015, 2016, 2017 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  depxml.pl -- DepXML schema
 *
 * ----------------------------------------------------------------
 * Description
 * Emission following DepXML schema (XML format), the native schema for FRMG
 * ----------------------------------------------------------------
 */

:-include 'header.tag'.
:-require 'best.pl'.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% DepXML

:-extensional node2wid/2.

:-light_tabular source2wid/3.
:-mode(source2wid/3,+(+,+,-)).

source2wid(NId,CId,WId) :-
	( node2wid(NId,WId) -> true
	; node{ id => NId1, cluster => cluster{ id => CId } },
	  NId1 \== NId ->
	  source2wid(NId1,CId,WId)
	;
	  edge{ source => node{ id => NId1, cluster => cluster{ id => CId1 } },
		target => node{ id => NId }
	      } ->
	  source2wid(NId1,CId1,WId)
	; recorded( erased(edge{ source => node{ id => NId1, cluster => cluster{ id => CId1 } },
				 target => node{ id => NId }
			       } ) ) ->
	  source2wid(NId1,CId1,WId)
	;
	  WId = none
	)
	.

:-light_tabular target2wid/3.
:-mode(target2wid/3,+(+,+,-)).

target2wid(NId,CId,WId) :-
	( node2wid(NId,WId) -> true
	; node{ id => NId1, cluster => cluster{ id => CId }},
	  NId1 \== NId ->
	  target2wid(NId1,CId,WId)
	;
	  edge{ target => node{ id => NId1, cluster => cluster{ id => CId1 } },
		source => node{ id => NId }
	      } ->
	  target2wid(NId1,CId1,WId)
	;
	  recorded( erased(edge{ target => node{ id => NId1, cluster => cluster{ id => CId1 } },
				source => node{ id => NId }
			      }
			 )
		 ) ->
	  target2wid(NId1,CId1,WId)
	;
	  WId = none
	)
	.


:-std_prolog depxml_emit/1.

depxml_emit(Disamb) :-
	( Disamb == disamb -> Stage = 'DEPXML' ; Stage = 'NODIS'),
	emit_multi(Stage),
	sentence(SId),
	recorded(mode(Mode)),
	event_ctx(Ctx,0),
	Handler=default([]),
	event_process(Handler,start_document,Ctx),
%%	event_process(Handler,pi{name=>xml,value=> [version:'1.0',encoding:'latin1']},Ctx),
	event_process(Handler,xmldecl,Ctx),
	( recorded(mode(R)) ->
	  Attrs = [mode:R]
	;
	  Attrs = []
	),
	( recorded(has_best_parse(AllW)) ->
	      recorded('N'(N)),
	      WperWord is AllW / N,
	      Attrs2 = [w:AllW,nw:WperWord|Attrs]
	 ;
	 Attrs2 = Attrs
	),
	( recorded(opt(cost)) ->
	  Tasks = [depclusters,depnodes,depedges,depops,dephts,depcost]
	;
	   Tasks = [depclusters,depnodes,depedges,depops,dephts]
	),
	xml!wrapper(Handler,
		    Ctx,
		    'dependencies',
		    [id:SId|Attrs2],
		    event_process(Handler,Tasks,Ctx)
		   ),
	xevent_process(Handler,end_document,Ctx,Handler),
	format('\n',[]),
	true
	.

dstats_emit :-
	emit_multi('DSTATS'),
	mutable(MClusters,0,true),
	every(( cluster{},
		mutable_add(MClusters,1)
	      )),
	mutable_read(MClusters,NClusters),
	%%
	mutable(MNodes,0,true),
	mutable(MDerivs,0,true),
	every(( node{ deriv => Derivs },
		mutable_add(MNodes,1),
		length(Derivs,K),
		mutable_add(MDerivs,K)
	      )),
	mutable_read(MNodes,NNodes),
	mutable_read(MDerivs,NDerivs),
	%%
	mutable(MEdges,0,true),
	every(( edge{},
		mutable_add(MEdges,1)
	      )),
	mutable_read(MEdges,NEdges),
	%%
	Amb is 1.0 * (1 + NEdges - NClusters) / NClusters,
	AvgDerivs is 1.0 * NDerivs / NNodes,
	format('Dependency stats: ~w ambiguity ~w clusters ~w nodes ~w edges ~w derivs ~w avgderivs\n',
	       [Amb,NClusters,NNodes,NEdges,NDerivs,AvgDerivs]),
	true
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Event Process for printing DEP XML objects

event_process( H::default(Stream), depclusters, Ctx1, Ctx2 ) :-
	mutable(Ctx,Ctx1,true),
	every(( C::cluster{},
		event_process(depxml(Stream), C, Ctx ))),
	mutable_read(Ctx,Ctx2)
	.

event_process( H::depxml(Stream),
	       cluster{ id => Id,
			left => Left,
			right => Right,
			token => Token,
			lex => Lex
		      },
	       Ctx1,
	       Ctx2
	     ) :-
	(Id = root ->
	 Ctx1 = Ctx2
	;
	 cid2xmlid(Id,CId),
	 tokenid2fulltoken(Lex,FullToken),
	 event_process(default(Stream),
		       element{ name => cluster,
				attributes => [ id: CId,
						left: Left,
						right: Right,
						token: Token,
					%	lex: Lex
						lex: FullToken
					      ]
			      },
		       Ctx1,
		       Ctx2
		      )
	).

:-light_tabular tokenid2fulltoken/2.
:-mode(tokenid2fulltoken/2,+(+,-)).

tokenid2fulltoken(TokenId,Token) :-
%	format('tokenid2fulltoken ~w\n',[TokenId]),
	(
	 TokenId = '' ->
	 Token = ''
	; TokenId = [_|_] ->
	 tokenid2fulltoken_aux(TokenId,LTokens),
	 name_builder('~L',[['~w',' '],LTokens],Token)
	;
	 sentence(SId),
	 'T'(TokenId,_Token),
	 name_builder('~wF~w|~w',[SId,TokenId,_Token],Token)
	),
%	format('=> ~w : ~w\n',[TokenId,Token]),
	true
	.

:-std_prolog tokenid2fulltoken_aux/2.

tokenid2fulltoken_aux(LTokenId,LTokens) :-
	( LTokenId = [TokenId|LTokenId2] ->
	  tokenid2fulltoken(TokenId,Token),
	  LTokens = [Token|LTokens2],
	  tokenid2fulltoken_aux(LTokenId2,LTokens2)
	;
	  LTokens = []
	)
	.

event_process( H::default(Stream), depnodes, Ctx1, Ctx2 ) :-
	mutable(Ctx,Ctx1,true),
	every(( N::node{}, event_process(depxml(Stream), N, Ctx ))),
	mutable_read(Ctx,Ctx2)
	.

event_process( H::depxml(Stream),
	       N::node{ id => Id,
		     cat => Cat,
		     xcat => XCat,
		     tree => Tree,
		     lemma => Lemma,
		     lemmaid => LemmaId,
		     deriv => Derivs,
		     cluster => cluster{ id =>CId },
		     form => _Form,
		     w => W
		   },
	       Ctx1,
	       Ctx2
	     ) :-
	( Id = root ->
	  Ctx1 = Ctx2
	;
	  name_builder('~L',[['~w',' '],Tree],L_Tree),
	  nid2xmlid(Id,NId),
	  cid2xmlid(CId,XCId),
	  (_Form = ellipsis(Form) xor _Form = Form),
	  Attrs =  [ id: NId,
		     cat: Cat,
		     (tree): L_Tree,
		     lemma: Lemma,
		     lemmaid: LemmaId,
		     cluster: XCId,
		     form: Form
		   ],
	  ( var(XCat) -> Attrs1=Attrs ; Attrs1 = [xcat:XCat|Attrs]),
	  ( Derivs = [[]] ->
%%		format('no derivs ~w\n',[N]),
	    Attrs2 = Attrs1
	  ;
	    mutable(MWAll,[],true),
	    ( recorded(disambiguated) ->
	      mutable(M,[],true),
	      every((
		     node2live_deriv(Id,DId),
		     mutable_list_extend(M,DId),
		     recorded(opt(cost)),
		     %% format('emit did ~w ~w\n',[Id,DId]),
		     deriv2best_parse(DId,dstruct{ w => _W, constraint => _Cst }),
		     mutable_list_extend(MWAll,_W)
		    )),
	      mutable_read(M,Derivs2)
	    ;
	      Derivs2 = Derivs
	    ),
	    ( Derivs2 = [_|_] ->
	      name_builder('~L',[['d~w',' '],Derivs2],L_Derivs),
	      _Attrs2 = [deriv:L_Derivs|Attrs1],
	      ( mutable_read(MWAll,WAll2::[_|_]) ->
		name_builder('~L',[['~w',' '],WAll2],L_WAll),
		Attrs2 = [wall:L_WAll|_Attrs2]
	      ;
		Attrs2 = _Attrs2
	      )
	     ;
%%	     format('no derivs alt ~w\n',[N]),
	      Attrs2=Attrs1
	    )
	  ),
	  ( W=[] ->
	    Attrs3 = Attrs2
	  ;	
	    name_builder('~U',[['~w:~w',' '],W],L_W),
	    %%		format('Emitting attr ~w => ~w\n',[W,L_W]),
	    Attrs3=[w:L_W|Attrs2]
	  ),
	  event_process(default(Stream),
			element{ name => node,
				 attributes => Attrs3
			       },
			Ctx1,
			Ctx2
		       )
	).

event_process( H::default(Stream), depedges, Ctx1, Ctx2 ) :-
	mutable(Ctx,Ctx1,true),
	every(( E::edge{}, event_process(depxml(Stream), E, Ctx ))),
	mutable_read(Ctx,Ctx2)
	.

:-light_tabular cid2xmlid/2.
:-mode(cid2xmlid/2,+(+,-)).

cid2xmlid(CId,XCId) :-
	sentence(SId),
	(C::cluster{ id => CId, left => Left, right => Right } xor recorded(erased(C))),
	name_builder('~wc_~w_~w',[SId,Left,Right],XCId)
	.


:-light_tabular nid2xmlid/2.
:-mode(nid2xmlid/2,+(+,-)).

nid2xmlid(NId,XNId) :-
%%	\+ var(NId),
	sentence(SId),
	name_builder('~wn~w',[SId,NId],XNId)
	.

:-light_tabular eid2xmlid/2.
:-mode(eid2xmlid/2,+(+,-)).

eid2xmlid(EId,XEId) :-
	sentence(SId),
	name_builder('~we~w',[SId,EId],XEId)
	.

:-light_tabular oid2xmlid/2.
:-mode(oid2xmlid/2,+(+,-)).

oid2xmlid(OId,XOId) :-
	sentence(SId),
	name_builder('~wo~w',[SId,OId],XOId)
	.

:-light_tabular hid2xmlid/2.
:-mode(hid2xmlid/2,+(+,-)).

hid2xmlid(HId,XHId) :-
	sentence(SId),
	name_builder('~wht~w',[SId,HId],XHId)
	.

event_process( H::depxml(Stream),
	       E::edge{ id => Id,
			source => node{ id => SId },
			target => node{ id => TId },
			type => Type,
			label => Label,
			deriv => Derivs,
			secondary => Secondary
		      },
	       Ctx1,
	       Ctx2
	     ) :-
	( Id = root(_) ->
	  Ctx1 = Ctx2
	;
	  ('$answers'(edge_cost(Id,_W,_Vector,_Cst)),
	   (_Cst = [] xor edge{ id => _Cst }) ->
	   _Attrs = [w: _W,
		     ws: _Vector]
	  ;
	   _Attrs=[]
	  ),
%%	  ( (var(SId) xor var(TId)) -> format('pb edge ~w\n',[E]) ; true ),
	  eid2xmlid(Id,EId),
	  nid2xmlid(SId,NSId),
	  nid2xmlid(TId,NTId),
	  Attrs =  [ id: EId,
		     source: NSId,
		     target: NTId,
		     type: Type,
		     label: Label
		   | _Attrs
		   ],
	  event_process(default(Stream),
			start_element{ name => edge,
				       attributes => Attrs
				     },
			Ctx1,
			Ctx3
		       ),
	  mutable(Ctx,Ctx3,true),
	  every(( domain(Deriv,Derivs),
		  event_process(H,deriv(Deriv,Id),Ctx)
		)),
	  mutable_read(Ctx,Ctx4),
	  event_process(default(Stream), end_element{ name => edge },
			Ctx4,
			Ctx5),
	  ( Secondary = [_|_] ->
		mutable(CtxSec,Ctx5,true),
		every((
			     domain(E2::edge{id => EId2, deriv => Derivs2, source => node{ id => SId2 }, target => node{ id => TId2}},Secondary),
%			     format('try emit secondary ~w\n',[E2]),
			     (\+ recorded(disambiguated),
			      true ->
				  event_process(H,E2,CtxSec)
			      ; node{ id => SId2 },
				node{ id => TId2 },
				secondary_alive(EId2),
				domain(DId2,Derivs2),
				recorded( keep_deriv(DId2) )
				->
				    event_process(H,E2,CtxSec)
			      ;
			      true
			     )
			 )),
		mutable_read(CtxSec,Ctx2)
	   ;
	   Ctx2 = Ctx5
	  )
	).


:-xcompiler
secondary_alive(EId) :-
%    format('test secondary alive ~w derivs=~w\n',[EId,Derivs]),
    recorded(secondary2edge(EId,LiveCst,_,_,_,_)),
    (LiveCst = [] -> true
     ; LiveCst = [_EId|SPId] ->
           edge{id => _EId},
           secondary_chain_alive(SPId)
     ;
     secondary_chain_alive(LiveCst)
    )
.

:-std_prolog secondary_chain_alive/1.

secondary_chain_alive(SPId) :-
    recorded(secondary_propagate_bptr(SPId,Info,PrevSPId)),
    %    format('secondary btr chain spid=~w nid=~w top=~w prevspid=~w\n',[SPId,NId,TOP,PrevSPId]),
    ( Info = [] ->
	  true
     ; Info = head(NId,SOP) ->
	   node{ id => NId, deriv => Derivs },
	   domain(DId,Derivs),
	   recorded(keep_deriv(DId)),
	   deriv(DId,EId,XSPan,SOP,_)
     ;
     edge{ id => Info, deriv => Derivs }
    ),
    (PrevSPId = [] xor secondary_chain_alive(PrevSPId))
.

event_process( H::depxml(Stream), deriv(DId,EId), Ctx1, Ctx2 ) :-
    (recorded(disambiguated) -> recorded( keep_deriv(DId) ) ; true),
    (deriv(DId,EId,XSpan,SOP,TOP) -> true
     ; % format('try secondary\n',[]),
       recorded(secondary2edge(EId,_,EId0,SOP,_SOP,direct)),
       % format('test emit secondary edge eid=~w eid0=~w did=~w\n',[EId,EId0,DId]),
       deriv(DId,EId0,XSpan,_SOP,TOP),
%%       edge{ id => EId0 },
%%       format('emit secondary deriv eid2=~w eid0=~w nid2=~w did=~w SOP=~w\n',[EId,EId0,NId2,DId,SOP]),
       true
     ; recorded(secondary2edge(EId,_,EId0,TOP,_SOP,reverse)),
       deriv(DId,EId0,XSpan,_SOP,SOP),
       _Attrs0=[reverse:1],
       true
     ; % format('try secondary\n',[]),
       recorded(secondary2edge(EId,_,EId0,SOP,head(TOP,XSpan),direct)),
%       format('test emit secondary edge eid=~w eid0=~w did=~w\n',[EId,EId0,DId]),
       edge{ id => EId0 },
%%       format('emit secondary deriv eid2=~w eid0=~w nid2=~w did=~w SOP=~w\n',[EId,EId0,NId2,DId,SOP]),
       true
    ),
    _Attrs0 ?= [],
	( recorded(reroot_source(EId,XSpan,NId,_NId)),
	  edge{ id => EId, source => node{ id => NId } } ->
	  nid2xmlid(_NId,_XNId),
%	  format('EMIT REROOT _nid=~w _xnid=~w\n',[_NId,_XNId]),
	  _Attrs1 = [reroot_source : _XNId|_Attrs0]
	;
	  _Attrs1 = _Attrs0
	),
	_Attrs = _Attrs1,
	name_builder('~L',[['~w',' '],XSpan],Span),
	name_builder('d~w',[DId],XDId),
	oid2xmlid(SOP,XSOP),
	oid2xmlid(TOP,XTOP),
	Attrs = [ names: XDId,
		  source_op: XSOP,
		  target_op: XTOP,
		  span: Span
		| _Attrs
		],
	event_process(default(Stream),
		      element{ name => deriv,
			       attributes => Attrs
			     },
		      Ctx1,
		      Ctx2
		     )
	.

event_process( H::default(Stream), depops, Ctx1, Ctx2 ) :-
	mutable(Ctx,Ctx1,true),
	every(( O::op{},
		event_process(depxml(Stream), O, Ctx ))),
	mutable_read(Ctx,Ctx2)
	.

event_process( H::depxml(Stream),
	       op{ id => Id,
		   span => XSpan,
		   cat => Cat,
		   deriv => Derivs,
		   top => Top,
		   bot => Bot
		 },
	       Ctx1,
	       Ctx2
	     ) :-
	( recorded(disambiguated) ->
	  mutable(M,[],true),
	  every((
		 alive_deriv(Derivs,DId),
		 mutable_list_extend(M,DId)
		)),
				%	mutable_read(M,Derivs2::[_|_]),
	  mutable_read(M,Derivs2)
	;
	  Derivs2 = Derivs
	),
	name_builder('~L',[['d~w',' '],Derivs2],L_Derivs),
	oid2xmlid(Id,OId),
	( Top = [] -> Content1 = []
	; Content1 = [narg(top,Top)]
	),
	( Bot = [] -> Content = Content1
	; Content = [narg(bot,Bot)|Content1]
	),
	name_builder('~L',[['~w',' '],XSpan],Span),
	( Content = [] ->
	  event_process(default(Stream),
			element{ name => op,
				 attributes => [ id:OId,
						 cat: Cat,
						 span: Span,
						 deriv: L_Derivs
					       ]
			       },
			Ctx1,
			Ctx2
		       )
	;
	 fs_analyze(Content,Bindings),
%%	 format('\nXML FS ctx1=~w content=~w bindings=~w\n',[Ctx1,Content,Bindings]),
%%	 verbose('FS content: ~w\n',[Content]),
	  event_process(default(Stream),
			start_element{ name => op,
				       attributes => [ id:OId,
						       cat: Cat,
						       span: Span,
						       deriv: L_Derivs
						      ]
				     },
			Ctx1,
			Ctx3
		       ),
	  mutable(Ctx,fs(Ctx3,Bindings)),
	  event_process(default(Stream),Content,Ctx),
	  mutable_read(Ctx,fs(Ctx4,Bindings)),
	  event_process(default(Stream),
			end_element{ name => op },
			Ctx4,
			Ctx2
		       )
	)
	.

:-xcompiler
fs_analyze(T,Bindings) :-
	mutable(M,[],true),
	fs_analyze_aux(T,M),
	mutable_read(M,Bindings1),
	mutable(M,[]),
	every(( domain(Var:2,Bindings1),
		mutable_read(M,_Bindings),
		length(_Bindings,Name),
		mutable(M,[Var:Name|_Bindings])
	      )),
	mutable_read(M,Bindings)
	.

:-std_prolog fs_analyze_aux/2.

fs_analyze_aux(T,M) :-
	( (var(T) xor T =.. ['$SET$'|_]) ->
	  mutable_read(M,Bindings),
	  ( fs_search(T,Bindings,N) ->
	    N2 is N+1
	  ;
	    N2 = 1
	  ),
	  ( N2 == 3 xor mutable(M,[T:N2|Bindings]))
	; atomic(T) ->
	  true
	; T = [_|_] ->
	  every(( domain(_T,T), fs_analyze_aux(_T,M) ))
	; T =.. [_|Args],
	  every(( domain(_T,Args), fs_analyze_aux(_T,M)	))
	)
	.

:-xcompiler
fs_search(Var,Bindings,N) :-
	once(( domain(_Var:N,Bindings),
	       _Var == Var
	     ))
	.

event_process( H::default(Stream),narg(Kind,FS), fs(Ctx1,Bindings),fs(Ctx2,Bindings) ) :-
	event_process(H,
		      start_element{ name => narg,
				     attributes => [type:Kind]
				   },
		      Ctx1,
		      Ctx3),
	event_process(fsxml(Stream), fs(FS), fs(Ctx3,Bindings),fs(Ctx4,Bindings)),
	event_process(H, end_element{ name => narg }, Ctx4,Ctx2 )
	.

event_process( H::fsxml(Stream),fs(FS),fs(Ctx1,Bindings),fs(Ctx2,Bindings)) :-
	event_process(default(Stream),
		      start_element{ name => fs, attributes => [] },Ctx1,Ctx3),
	mutable(Ctx,fs(Ctx3,Bindings),true),
	every(( inlined_feature_arg(FS,F,_,V),
		event_process( fsxml(Stream), feature(F,V), Ctx )
	      )),
	mutable_read(Ctx,fs(Ctx4,Bindings)),
	event_process(default(Stream),
		      end_element{ name => fs }, Ctx4,Ctx2)
	.

event_process( fsxml(Stream), feature(Name,Values),fs(Ctx1,Bindings),fs(Ctx2,Bindings)) :-
	( fs_search(Values,Bindings,Var) ->
	  Attrs = [id:Var],
	  Bound == 1
	;
	  Attrs = []
	),
%	format('\nFSXML ~w bindings=~w ctx1=~w\n',[Values,Bindings,Ctx1]),
	( var(Values), var(Bound) ->
	  Ctx2 = Ctx1
	;
	  event_process(default(Stream),
			start_element{ name => f,
				       attributes => [name:Name|Attrs]
				     },
			Ctx1,
			Ctx3
		       ),
				%	format('\nhello1 ctx3=~w\n',[Ctx3]),
	  ( (atomic(Values) ; is_finite_set(Values)) ->
	    mutable(Ctx,fs(Ctx3,Bindings),true),
	    every(( domain(Value,Values),
		    event_process(fsxml(Stream),val(Value),Ctx)
		  )),
	    mutable_read(Ctx,fs(Ctx4,Bindings))
	  ; var(Values) ->
	    Ctx3=Ctx4
	  ; %% complex term
	    event_process(fsxml(Stream),fs(Values),fs(Ctx3,Bindings),fs(Ctx4,Bindings))
	  ),
	  %%	format('\nhello2 ctx4=~w\n',[Ctx4]),
	  event_process(default(Stream),end_element{ name=> f },Ctx4,Ctx2)
	)
	.

event_process( fsxml(Stream), val(-), fs(Ctx1,Bindings),fs(Ctx2,Bindings) ) :-
	event_process(default(Stream), element{ name => minus },Ctx1,Ctx2).

event_process( fsxml(Stream), val(+), fs(Ctx1,Bindings), fs(Ctx2,Bindings) ) :-
	event_process(default(Stream), element{ name => plus },Ctx1,Ctx2).

event_process( fsxml(Stream), val(V), fs(Ctx1,Bindings), fs(Ctx2,Bindings) ) :-
	\+ domain(V,[+,-]),
	mutable(Ctx,Ctx1,true),
	event_process(default(Stream),
		      [ start_element{ name => val },
			characters{ value => V },
			end_element{ name => val }
		      ],
		      Ctx
		     ),
	mutable_read(Ctx,Ctx2)
	.

event_process( H::default(Stream), dephts, Ctx1, Ctx2 ) :-
	mutable(Ctx,Ctx1,true),
	every(( HT::hypertag{}, event_process(depxml(Stream), HT, Ctx ))),
	mutable_read(Ctx,Ctx2)
	.

event_process( H::depxml(Stream),
	       hypertag{ id => Id,
			 ht => HT,
			 deriv => Derivs
		       },
	       Ctx1,
	       Ctx2
	     ) :-
	( recorded(disambiguated) ->
	  mutable(M,[],true),
	  every((
		 alive_deriv(Derivs,DId),
		 mutable_list_extend(M,DId)
		)),
	  mutable_read(M,Derivs2::[_|_])
	;
	  Derivs2 = Derivs
	),
	name_builder('~L',[['d~w',' '],Derivs2],L_Derivs),
	hid2xmlid(Id,HId),
	mutable(Ctx,Ctx1,true),
	%%	verbose('FS content: ~w\n',[Content]),
	event_process(default(Stream),
		      start_element{ name => hypertag,
				     attributes => [ id:HId,
						     derivs: L_Derivs%, op: OpId
						   ]
				   },
		      Ctx1,
		      Ctx3),
	event_process(fsxml(Stream),fs(HT),fs(Ctx3,[]),fs(Ctx4,[])),
	event_process(default(Stream),
		      end_element{ name => hypertag },
		      Ctx4,
		      Ctx2
		     )
	.

event_process( H::default(Stream), depcost, Ctx1, Ctx2 ) :-
	    mutable(Ctx,Ctx1,true),
	    ( recorded(opt(cost)) ->
	    every( ( '$answers'(edge_cost(EId,W,Ws,_Cst)),
%		     format('\ndepxml cost eid=e~w w=~w ws=~w cst=~w\n',[EId,W,Ws,_Cst]),
		       ( recorded(keep_edge(EId)) ->
			   Kept = yes,
			   E::edge{ id => EId,
				    type => Type,
				    label => Label,
				    source => node{ cat => SCat,
						    cluster => cluster{ id => SCId,
									left => SLeft
								      }
						  },
				    target => node{ cat => TCat,
						    cluster => cluster{ id => TCId,
									right => TLeft
								      }
						  }
				  }
		       ;   
			   Kept = no,
			   recorded( erased(E) )
		       ),
		       ( SLeft < TLeft ->
			   Dir = right
		       ;   
			   Dir = left
		       ),
		     cid2xmlid(SCId,XSCId),
		     cid2xmlid(TCId,XTCId),
		     event_process(H,
				   element{ name => 'cost',
					    attributes => [eid:EId,
							   kept:Kept,
							   w:W,
							   ws:Ws,
							   info: [Dir,SCat,Label,Type,TCat],
							   source: XSCId,
							   target: XTCId
							  ]
					  },
				   Ctx),
		     true
		   ))
	;
	      true
	),
	    mutable_read(Ctx,Ctx2)
	.

