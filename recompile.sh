#!/bin/bash
file="$HOME/exportbuild/src/ofrlex-metamof/dico.xlfg"
if [[ -f "$file" ]];
then
    echo "ofrlex-metamof found"
else
    echo "Repository ofrlex-metamof not found"
fi

aclocal -I $HOME/exportbuild/share/aclocal/
libtoolize --force
autoconf
automake-dyalog --add-missing
./configure --prefix=$HOME/exportbuild/ LDFLAGS=-L/$HOME/exportbuild/lib CPPFLAGS=-I/$HOME/exportbuild/include
make
make install