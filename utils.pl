/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2015 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  utils.pl -- Utilities
 *
 * ----------------------------------------------------------------
 * Description
 * Generic utilities, used for disamb and conversion
 * ----------------------------------------------------------------
 */

:-require('rx.pl').

:-extensional
      opt/1
.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Meta-predicate

:-op(  700, xfx, [?=]). % for default value

:-xcompiler
X ?= V :- (X=V xor true). %% Setting a default value

:-xcompiler
once(G) :- (G xor fail).

:-xcompiler
      exists(G) :- (G xor fail).

:-xcompiler
is_finite_set(T) :- T =.. ['$SET$'|_].

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% XML helpers

:-import{ module=> xml,
	  file => 'libdyalogxml.pl',
	  preds => [
		    event_handler/5,
		    event_process/3,
		    event_process/4,
		    event_ctx/2,
		    event_super_handler/2,
		    read_event/3,
		    attribute_handler/3,
		    attribute_dyalog_handler/4
		   ]
         }.

:-features(ctx,[class,node,constraint,current]).

:-xcompiler
xml!wrapper(Handler,Ctx,Name,Attr,G) :-
	event_process(Handler,start_element{ name => Name, attributes => Attr },Ctx),
	G,
	event_process(Handler,end_element{ name => Name },Ctx)
	.

:-xcompiler
xml!text(Handler,Ctx,Text) :-
	event_process(Handler,characters{ value => Text }, Ctx )
	.

:-std_prolog xevent_process/4.

:-rec_prolog xevent_process/5.

xevent_process(Handler,Event,Ctx,OrigHandler) :-
        ( Event = [_Event|Events2] ->
%%          format('Event process: handler=~w Event=~w\n',[Handler,_Event]),
            xevent_process(OrigHandler,_Event,Ctx,OrigHandler),
%%          format('Event processed: handler=~w Event=~w\n',[Handler,_Event]),
            xevent_process(OrigHandler,Events2,Ctx,OrigHandler)
%%          every((domain(_Event,Event), event_process(Handler,_Event,Ctx)))
        ;   Event == [] ->
            true
        ;
            mutable_read(Ctx,Ctx1),
            (	( xevent_process(Handler,Event,Ctx1,Ctx2,OrigHandler)
                xor event_super_handler(Handler,Super_Handler),
                    xevent_process(Super_Handler,Event,Ctx1,Ctx2,OrigHandler)),
                mutable(Ctx,Ctx2)
            xor true            % 'do nothing' as Default handler
            )
        )
        .

event_process(Handler,Event,Ctx1,Ctx2) :-
	(   xevent_process(Handler,Event,Ctx1,Ctx2,Handler)
	xor  event_super_handler(Handler,Super_Handler),
	    (	xevent_process(Super_Handler,Event,Ctx1,Ctx2,Handler)
	    xor event_process(Super_Handler,Event,Ctx1,Ctx2)
	    )
	).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Lists

:-std_prolog append/3.
append(L1,L2,L3) :-
	(L1=[] -> L2=L3
	; L1=[A|XL1],
	  L3=[A|XL3],
	 append(XL1,L2,XL3)
	).

:-xcompiler
fast_append(L1,L2,L3) :- '$interface'('Easyforest_Fast_Append'(L1:term,L2:term,L3:term),[]).


:-std_prolog list_sorted_add/3.

list_sorted_add(A,L,XL) :-
    (L = [] -> XL = [A]
     ; L = [B|L2],
       (A @< B ->
	    XL = [A|L]
	; A=B ->
	      XL = L
	; list_sorted_add(A,L2,XL2),
	  XL = [B|XL2]
       )
    )
.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Verbosity

:-xcompiler			%verbose
verbose(Msg,Args) :- (\+ opt(verbose) xor format(Msg,Args)).

:-xcompiler			%verbose_cost
verbose_cost(Msg,Args) :- (\+ opt(verbose_cost) xor format(Msg,Args)).

%:-xcompiler((verbose(Msg,Args) :- true )).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Counters

:-std_prolog update_counter/2.

update_counter(Name,V) :-
        ( recorded( counter(Name,M) ) ->
	  mutable_read(M,V),
	  mutable_add(M,1)
%            mutable_inc(M,V)
        ;   V=1,
            mutable(M,2),
            record( counter(Name,M) )
        )
        .

:-std_prolog value_counter/2.

value_counter(Name,V) :-
        ( recorded( counter(Name,M) ) ->
            mutable_read(M,V)
        ;
            V = 1
        )
.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Adding and deleting facts and relations

:-light_tabular tab_item_term/2.
:-mode(tab_item_term/2,+(-,-)).

tab_item_term(I,T) :- item_term(I,T).

:-std_prolog read_dbfile/1.
read_dbfile(File) :-
	open(File,read,S),
	repeat(( read_term(S,T,V),
		 ( T == eof
		 xor
		   record(T),
		   fail
		 )
	       )),
	close(S)
	.

%% to store a fact in DyALog persistent aread, to be restored at the next loop
%% NOTE: after restoration, the fact is removed from the persistent area
:-light_tabular persistent!add/1.

persistent!add(Fact) :-
	'$interface'('DyALog_Persistent_Add'(Fact:term),[return(none)]).

:-xcompiler
persistent!add_fact(Fact) :-
	every(( recorded(Fact),
		persistent!add(Fact),
		true
	      ))
	.

:-xcompiler
record_without_doublon( A ) :-
        (recorded( A ) xor record( A ))
        .

:-std_prolog erase_relation/1.

erase_relation( Rel ) :-
	%% Erase both Call and Return items
	tab_item_term(I::'*RITEM*'(Call,_),Rel),
	every(( recorded(C::'*CITEM*'(Call,Call),Add),
		delete_address(Add)) ),
	every(( recorded(I,Add),
		delete_address(Add),
		true
	      )
	     ),
	true
.

:-xcompiler
abolish(F/N) :- '$interface'( 'Abolish'(F:term,N:int), [return(none)]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Mutables

%% the notation X ::= t force a fesh copy of t
%% it assumes that t is ground

:-op(  800, xfx, [::=]).

:-xcompiler
X ::= Y	:- '$interface'('DyALog_Copy'(Y:term,X:term),[]).

:-xcompiler
fast_mutable(M,T) :-
	X ::= T,
	mutable(M,X)
	.

:-xcompiler
mutable_list_extend(M,X) :- '$interface'('DyALog_Mutable_List_Extend'(M:ptr,X:term),[return(none)]).	

:-xcompiler
mutable_add(M,X) :- '$interface'('DyALog_Mutable_Add'(M:ptr,X:int),[return(none)]).	

:-xcompiler
mutable_min(M,V) :-
	'$interface'('DyALog_Mutable_Min'(M:ptr,V:int),[return(none)])
	.

:-xcompiler
mutable_max(M,V) :-
	'$interface'('DyALog_Mutable_Max'(M:ptr,V:int),[return(none)])
	.

:-xcompiler
mutable_check_min(M,V) :-
	'$interface'('DyALog_Mutable_Check_Min'(M:ptr,V:int),[])
	.

:-xcompiler
mutable_check_max(M,V) :-
	'$interface'('DyALog_Mutable_Check_Max'(M:ptr,V:int),[])
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% defined in tag_generic
:-std_prolog name_builder/3.

:-xcompiler
emit_multi(Stage) :- (opt(multi) -> format('------------ ~w -----------\n',[Stage]) ; true).
