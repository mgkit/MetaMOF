/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2015, 2016, 2017 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  best.pl -- extraction of the best dependency tree
 *
 * ----------------------------------------------------------------
 * Description
 * Extraction of the best dependency tree from the shared dependency forest
 * the weights on edges and nodes are provided by cost.pl
 * ----------------------------------------------------------------
 */

:-include 'header.tag'.
:-require 'cost.pl'.

best_dependency_tree :-
	%% Add a virtual root node
	record( CRoot::cluster{ id => root,
				lex => '',
				token => '',
				left => 0,
				right => 0 }
	      ),
	mutable(MRDerivs,[],true),
	every(( N::node{ id => NId, cat => Cat },
		(\+ N = NRoot),
%		format('potential root nid ~w\n',[NId]),
		( node2op(NId,OId),
		  recorded(root_op(OId)) -> true
		; fail,
		  recorded(mode(robust)) ->
		  Cat = cat[v]
		;
		  fail,
		  span_max(Span),
		  span(NId,Span)
		),
%		format('root nid ~w\n',[NId]),
		_NId ::= root(NId),
		mutable_list_extend(MRDerivs,_NId)
	      )),
	mutable_read(MRDerivs,RDerivs),
	record( NRoot::node{ id => root,
			     cluster => CRoot,
			     cat => root,
			     tree => [virtual],
			     deriv => RDerivs,
			     xcat => root,
			     lemma => '',
			     lemmaid => '',
			     form => '',
			     w => 0
			   }
	      ),
	every(( domain(root(NId),RDerivs),
		NN::node{ id => NId },
		( recorded( mode(robust) )
		xor span_max(Span),
		  span(NId,Span)
		),
		verbose('Add root edge ~E\n',[NN]),
		record_without_doublon( edge{ id => root(NId),
					      source => NRoot,
					      target => NN,
					      label => root,
					      type => virtual,
					      deriv => [root(NId)],
					      secondary => []
					    } )
	      )),
	every( (( recorded( mode(robust) ) ->
		  recorded(edge{ id => root(NId) }),
		  span(NId,Span),
		  Span=[_,_]
		;
		  span_max(Span)
		),
		verbose('try with span ~w\n',[Span]),
		best_parse(root,[],Span,[],_),
		true
	       )
	     ),
	\+ opt(stop(best)),
	every(( \+ recorded( mode(robust) ),
		span_max(Span),
		'$answers'(best_parse(root,[],Span,[],DStruct))
		->
		    verbose('Found root best parse ~w\n', [DStruct]),
		dstruct2lists(DStruct,CIds,EIds,DIds),
		verbose('Found root best parse ~w: cids=~w eids=~w dids=~w\n', [DStruct,CIds,EIds,DIds]),
		(\+ opt(verbose)
		xor every(( domain(_EId,EIds),
			    _E::edge{ id => _EId },
			    '$answers'(edge_cost(_EId,_W,_Vector,_Cst)),
			    (_Cst = [] xor domain(_Cst,EIds)),
			    format('\t~w: ~E ~w\n',[_W,_E,_Vector]) ))),
		keep_only_best(CIds,EIds,DIds),
		record(has_best_parse),
		DStruct = dstruct{ w => AllW},
		record(has_best_parse(AllW)),
		true
	      ;	  find_best_parse_coverage ->
		(   \+ opt(verbose)
		xor every(( _C::cluster{}, format('Cluster ~E\n',[_C]) )),
		    every(( _N::node{}, format('Node ~E\n',[_N]) )),
		    every(( _E::edge{ id => _EId },
			    '$answers'(edge_cost(_EId,_W,_Vector,_Cst)),
			    format('Edge ~w: ~E ~w\n',[_W,_E,_Vector]) ))
		)
	      ;	  
		  verbose('Not found best parse\n', [])
	      ))
	.

:-std_prolog keep_only_best/3.

keep_only_best(CIds,EIds,DIds) :-
%    format('only best ~w\n',[EIds]),
	every(( domain(_EId,EIds),
		record(keep_edge(_EId)))),
	every(( _E::edge{ id => _EId,
			  source => node{ id => SourceId},
			  target => node{ id => TargetId}
			},
		\+ recorded( keep_edge(_EId) ),
%                \+ (recorded(edge_double(_EId,_EId_Orig)), recorded(keep_edge(_EId_Orig))),
		( '$answers'(edge_cost(_EId,_W,_Vector,_Cst))
		xor _W=none,
		  _Vector=[]
		),
		verbose('Erase w=~w edge ~E\n',[_W,_E]),
		erase( _E ),
		erase( source2edge(SourceId,_E) ),
		record( erased_source2edge(SourceId,_E)),
		erase( target2edge(TargetId,_E) ),
		record( erased(_E) )
	      )),
	%% keep nodes related to existing edges or to cluster not covered by the clusters in CIds
	every(( domain(D,DIds),
		record_without_doublon( keep_deriv(D) ) )),
	every(( _N::node{ id => _NId,
			  cluster => _C::cluster{ id => _CId, lex => _Lex, left => _Left, right => _Right }
			},
		( source2edge(edge{ source => _N} )-> true
		; target2edge(edge{ target => _N }) -> true
		; _Lex \== '',
		  ( domain(__CId,CIds)
		  ; recorded( keep_cluster(__CId))),
		  cluster_overlap(_CId,__CId),
		  %%		  __C::cluster{ id => __CId },
		  ( _CId \== __CId
		  xor edge{ source => node{ cluster => cluster{ id => _CId } } }
		  xor edge{ target => node{ cluster => cluster{ id =>_CId } } }
		  )
		  ->
		  verbose('Erase node ~E overlap with ~w\n',[_N,__CId]),
		  erase( _N ),
		  record(erased(_N)),
		  erase( source2edge(_NId,_) ),
		  erase( target2edge(_NId,_) )
		; (_Lex == '' xor _Left == _Right) ->
		  verbose('Erase node ~E emptylex\n',[_N]),
		  erase( _N ),
		  record(erased(_N)),
		  erase( source2edge(_NId,_) ),
		  erase( target2edge(_NId,_) )
		;
		  %% keep a non empty node
		  %% which is neither an edge source nor target
		  record_without_doublon( keep_cluster(_CId) ),
		  true
		)
	      )),
	every(( _C::cluster{ lex => Lex},
%		Lex \== '',
		\+ node{ cluster => _C },
		verbose('Erase cluster ~E\n',[_C]),
		erase(_C),
		erase( sourcecluster2edge(_C) ),
		erase( targetcluster2edge(_C) ),
		record( erased( _C ) )
	      )),
	every(( _O::op{ deriv => ODerivs },
		\+ alive_deriv(ODerivs,_),
		verbose('Erase op ~w\n',[_O]),
		erase(_O),
		record(erased(_O))
	      )),
	every(( _HT::hypertag{ deriv => HDerivs },
		\+ alive_deriv(HDerivs,_),
		verbose('Erase ht ~w\n',[_HT]),
		erase(_HT),
		record(erased(_HT))
	      )),
	(   \+ opt(verbose)
	xor every(( _C::cluster{}, format('Cluster ~E\n',[_C]) )),
	    every(( _N::node{}, format('Node ~E\n',[_N]) )),
	    every(( _E::edge{ id => _EId },
		    '$answers'(edge_cost(_EId,_W,_Vector,_Cst)),
		    format('Edge ~w: ~E\n',[_W,_E]) ))
	)
	.


:-light_tabular dstruct2lists/4.
:-mode(dstruct2lists/4,+(+,-,-,-)).

dstruct2lists( DStruct::dstruct{ node => NId,
                                 deriv => DId,
                                 span => Span,
                                 children => Children },
               [CId|_CIds],
               EIds,
               DIds
             ) :-
        verbose('dstruct ~w\n',[DStruct]),
        (N::node{ id => NId, cluster => cluster{ id => CId } } xor recorded(erased(N)) ),
        dstruct2lists_aux(Children,_CIds,EIds,_DIds),
        ( DId == [] ->
          DIds = _DIds
        ; DIds = [DId|_DIds]
        ),
        true
        .

:-light_tabular edge_best_parse_constraint/2.

edge_best_parse_constraint(EId,Constraints) :-
	( \+ recorded(opt(no_ssfeatures)),
	  edge{ target => N::node{ cat => Cat },
		type => Type,
		id => EId
	      },
	  ( % Cat = prep xor
	  node!empty(N), Type = adj
	  )
	->
	  Constraints = EId
	;
	  Constraints = []
	).

:-rec_prolog dstruct2lists_aux/4.

dstruct2lists_aux([],[],[],[]).
dstruct2lists_aux([dinfo(OId1,EId1,NId1,Constraints)|L2],CIds,[EId1|EIds],DIds) :-
        dstruct2lists_aux(L2,CIds2,EIds2,DIds2),
	edge_best_parse_constraint(EId1,Constraints1),
        '$answers'(best_parse(NId1,OId1,Span1,Constraints1,DStruct1)),
        dstruct2lists(DStruct1,CIds1,EIds1,DIds1),
        append(CIds1,CIds2,CIds),
        append(EIds1,EIds2,EIds),
        append(DIds1,DIds2,DIds),
        true
        .

:-std_prolog find_best_parse_coverage/0.

find_best_parse_coverage :-
	verbose('Try find best coverage\n',[]),
	every((
	       edge{ source => Root::node{ id =>NId,
					   xcat => XCat::cat['S','N2','PP',comp,unknown,'CS'],
					   cat => Cat
					 %% cat => cat[v,nc,np,]
					 } },
	       '$answers'(best_parse(NId,_,Span::[Left,Right],[],
				     DStruct::dstruct{ w => W,
						       deriv => _Deriv,
						       children => _Children
						     })),
	       \+ (Span = [_L,_L] ),
	       verbose('best coverage considering root ~E xcat=~w w=~w cids=~w eids=~w\n',[Root,XCat,W,CIds,EIds]),
	       mutable(_WM,W,true),
	       every(( robust_re_weight(NId,_Children,_Deriv,Span,_W_Reg,_Name),
		       mutable_add(_WM,_W_Reg),
		       edge_in_children(NId,edge{ id => _EId },_Children),
		       (use_model,
			fail ->
			    constrained_edge_features(_EId,[],_Name,_Values),
			    (model!query(_Values,_ModelW,[]) ->
				 mutable_add(_WM,_ModelW),
				 _W_Reg2 is _W_Reg + _ModelW
			     ;
			     _W_Reg2 = _W_Reg
			    )
			;
			_W_Reg2 = _W_Reg
		       ),
		       %	       format('register regional cost ~w ~w ~w\n',[_EId,_Name,_W_Reg2]),
		       record(regional_edge_cost(_EId,_Name,_W_Reg2)),
		       true
		     )),
	       mutable_read(_WM,W1),
	       %% no need to add best parse covering empty span !
	       Length is Right - Left,
	       %%	       Length > 1,
	       ( Length == 1 -> Length3 = 1
	       ; Length < 4 ->  Length3 = 2
	       ; Length < 10 -> Length3 = 3
	       ; Length3 = 4
	       ),
	       verbose('Insert parse length=~w o=~w w=~w\n',[Length,O,W1]),
	       '$interface'('Easyforest_Add_DStruct'(Length3:int,W1:int,DStruct:term),[return(none)])
	      )),
	verbose('Sorted partial best parses ~L\n',[['\n\t~w',''],L]),
	(\+ opt(stop(partial)) xor exit(1)),
	best_parse_traversal(L,CIds,EIds,DIds),
	verbose('Second try: found best parse by gluing ~w ~w\n',[CIds,EIds]),
	(\+ opt(stop(second_try)) xor exit(1)),
	keep_only_best(CIds,EIds,DIds),
	true
	.
	      
:-std_prolog best_parse_traversal/4.

best_parse_traversal( L,
		      CIds,
		      EIds,
		      DIds
		    ) :-
    mutable(MDs,[],true),
    mutable(MW,0,true),
	%% try to find large and good segments to cover the input sentence
	%% segments are tried by decreasing length and decreasing weight
	%% first, we get a list of compatible DStruct
	every(( '$interface'('Easyforest_Get_DStruct'(DStruct1:term,_W1:term),[choice_size(2)]),
		DStruct1 = dstruct{ span => Span::[Left,Right], w => _W },
		mutable_read(MDs,_Ds),
		\+ ( domain(dstruct{ span => _Span::[_Left,_Right] }, _Ds),
		     ( _Left =< Left, Left < _Right
		     xor _Left < Right, Right =< _Right
		     xor Left =< _Left, _Left < Right
		     xor Left < _Right, _Right =< Right
		     )
		   ),
		fast_mutable(MDs,[DStruct1|_Ds]),
		mutable_add(MW,_W1)
	      )),
	mutable_read(MDs,Ds),
	mutable_read(MW,AllW),
	record(has_best_parse(AllW)),
	mutable(MCIds,[],true),
	mutable(MEIds,[],true),
	mutable(MDIds,[],true),
	%% then, we get the lists of corresponding clusters, edges and derivs
	every(( 
		domain(DStruct1,Ds),
		dstruct2lists(DStruct1,CIds1,EIds1,DIds1),
		mutable_read(MCIds,_CIds),
		mutable_read(MEIds,_EIds),
		mutable_read(MDIds,_DIds),
		append(CIds1,_CIds,NewCIds),
		append(EIds1,_EIds,NewEIds),
		append(DIds1,_DIds,NewDIds),
		verbose('Best parse recovery: agglutinate length=~w weight=~w node=~w\n',[Length,W,NId1]),
		mutable(MCIds,NewCIds),
		mutable(MEIds,NewEIds),
		mutable(MDIds,NewDIds)
	      )),
	mutable_read(MCIds,CIds),
	mutable_read(MEIds,EIds),
	mutable_read(MDIds,DIds)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Disambiguisation

:-light_tabular best_parse/5.
:-mode(best_parse/5,+(+,+,+,+,-)).

%% Pb with NId=root IncomingSpan=dummy

best_parse(NId,OId,Span,Constraints,DStruct) :-
	%% best parse for triple <NId,Spam,OId>
	%% actually, Span may be derived from OId (= Op Id)
	%% note that several NId may be head of a same OId
	%% actually, it should be enough to find best parses for OId
	verbose('Try find best parse from ~w entering with span=~w and oid=~w and cst=~w\n',
		[NId,Span,OId,Constraints]),
	N::node{ id => NId, lemma => Lemma, deriv => Derivs },
%	length(Derivs,NDerivs),
	mutable(BPL,[],true),
%	MaxDelta is min(2000,100 * NDerivs),
%	mutable(MDelta,MaxDelta),
	every((
	       
	       best_parse_in( N,
			      W_Below,
			      _DStruct::dstruct{ w => _W,
						 node => NId,
						 span => Span,
						 deriv => Deriv,
						 children => Children,
						 oid => OId,
						 constraint => Constraints
					       }
			    ),

%	       mutable_read(MDelta,_XDelta),
%	       _XDelta2 is max(100,_XDelta - 100),
%	       mutable(MDelta,_XDelta2),
	       
	       (%NDerivs < 10
		%	      xor recorded('N'(SLength)), SLength < 20
		%							xor
	       ( ( potential_coord(NId), _Delta = 2000
	       	 xor Lemma == faire, _Delta = 2000
	       	 xor _Delta = 100
%						  xor _Delta = 0
%						  xor _Delta = 100
	       	 ),
	       	 \+ (
	       	     mutable_read(BPL,_W1_All+dstruct{ w => _W1}),
	       	     W_Below + _Delta < _W1_All 
	       	    )
	       )),

	       
%	       verbose('Found pre parse from ~E for deriv=~w: ~w\n',[N,Deriv,W]),
	       sum_node_cost_regional(NId,Children,Deriv,Span,W_Region),
	       _W is W_Below+W_Region,
	       verbose('Found parse from ~w: ~w\n',[NId,_DStruct]),
%	       format('Found parse from ~w: w=~w below=~w reg=~w\n',[NId,_W,W_Below,W_Region]),
	       
	       ( mutable_read(BPL,_W1_All+dstruct{ w => _W1}),
		 _W =< _W1_All ->
		 true		% keep old best parse
	       ; % add or update with current parse
		 mutable(BPL, _W + _DStruct)
	       )
	      )),
	( mutable_read(BPL,W_All + (DStruct::dstruct{ w => W,
						      node => NId,
						      span => Span,
						      deriv => XDeriv,
						      children => XChildren,
						      oid => OId,
						      constraint => Constraints
						    })),
%	  format('Found best parse from ~E: weight=~w span=~w oid=~w dstruct=~w cst=~w\n',[N,W,Span,OId,DStruct,Constraints]),
	  verbose('Found best parse from ~E: weight=~w span=~w oid=~w dstruct=~w cst=~w\n',[N,W,Span,OId,DStruct,Constraints])
	xor 
	verbose('No best parse for ~w with span=~w and oid=~w\n',[NId,Span,OId]),
	  fail
	),
	record_without_doublon(span2best_parse_node(Span,NId)),
	true
	.

:-xcompiler
best_parse_in( N::node{ id => NId,
			deriv => Derivs
		      },
	       W,
	       DStruct::dstruct{ % w => W_All,
				 node => NId,
				 span => Span,
				 deriv => Deriv,
				 children => Children,
				 oid => OId,
				 constraint => Constraints
			       }
	     ) :-

	verbose('All edges from ~E inspan=~w oid=~w derivs=~w\n',[N,Span,OId,Derivs]),

%%	verbose('All edges: enter n=~w span=~w oid=~w\n',[NId,Span,OId]),
	verbose('All edges: here1 ~w\n',[Derivs]),
	( NId = root ->
	  edge{ id => RootEId::root(NId1),
		deriv => [Deriv],
		type => virtual
	      },
	  node2op(NId1,OId1),
	  op{ id => OId1, span => Span },
	  best_parse(NId1,OId1,Span,[],dstruct{ w => W1}),
%	  '$answers'(edge_cost(RootEId,WE,_,Constraints)),
	  Constraints=[],
	  edge_cost(RootEId,WE,_,Constraints),
	  W is WE+W1,
	  Children ::= [dinfo(OId1,RootEId,NId1,Constraints)]
	; Derivs = [] ->
	  Deriv = [],
	  Edges = [],
	  W = 0,
	  Children = []
	; %op{ id => OId, deriv => XDerivs }  ->
	%	  node_op2deriv(NId,OId,_) ->
	node2op(NId,OId),
	verbose('getting potential0 op=~w for nid=~w\n',[NId,OId]),
	%% check that selected OId is compatible with expected span
	op{ id => OId, span => _Span },
	verbose('getting potential1 op=~w for nid=~w _span=~w span=~w\n',[NId,OId,_Span,Span]),
	(_Span = Span xor Span=[gen_pos(_SL,_),gen_pos(_SR,_)], _Span=[_SL,_SR]),
	true ->
	  verbose('All edges: here2 oid=~w\n',[OId]),
	  %%
	  %% domain(Deriv,XDerivs),
	  %% select a derivation, compatible with OId and NId
	  node_op2deriv(NId,OId,Deriv), 
	  verbose('All edges: deriv n=~w span=~w deriv=~w\n',[NId,Span,Deriv]),
	  '$interface'('Easyforest_DList_Init'(),[return(M:ptr)]),
	  every((
		 %% select all edges for Deriv
		 %% A deriv is characterized by maximal span and children span (OId1)
		 %% several edges may be in competition for a given children span
		 %% => several (EId1,NId1) pairs are possible for a given (OId1,Span1) pair
		 %% domain(info(EId1,OId,OId1,NId1,Span1),DerivEdges),
		 deriv2edge(Deriv,info(EId1,OId,OId1,NId1,Span1)),
		 verbose('deriv nid=~w nid1=~w d=~w e=~w span=~w oid=~w oid1=~w\n',[NId,NId1,Deriv,EId1,Span1,OId,OId1]),
%		 verbose('Edge examine ~E deriv=~w oid=~w\n',[E1,Deriv,OId1]),
		 ( recorded(edge_best_parse(EId1,OId1,Constraints,XW1))
		 xor
		 edge_best_parse_constraint(EId1,Constraints1),
		   best_parse(NId1,OId1,Span1,Constraints1,dstruct{ w => W1, children => Children1 }),
		   verbose('fetch answer edge_cost ~w ~w\n',[EId1,Constraints]),
		   '$answers'(edge_cost(EId1,WE,_,Constraints)),
		   verbose('got answer edge_cost ~w ~w => w=~w\n',[EId1,Constraints,WE]),
		   %% (	 edge_weight_distrib(EId1,NId1,Children1,WE,WDistrib) xor WDistrib = 0),
		   %% XW1 is W1+WE+WDistrib,
		   XW1 is W1+WE,
		   record(edge_best_parse(EId1,OId1,Constraints,XW1))
		 ),
		 verbose('register for nid=~w cst=~w: xw1=~w dinfo=~w\n',[NId,Constraints,XW1,dinfo(OId1,EId1,NId1,Constraints)]),
		 '$interface'('Easyforest_DList_Add'(M :ptr,
						     XW1 :int,
						     OId1 :term,
						     dinfo(OId1,EId1,NId1,Constraints) :term),
			      [return(none)])
		)),
	  '$interface'('Easyforest_DList_Get'(M:ptr,W: -int, Children:term), [])
	;
	  verbose('Pb with ~E derivs=~w\n',[N,Derivs]),
	  fail
	),
	verbose('best_parse_in: deriv n=~w span=~w deriv=~w w=~w children=~w\n',[NId,Span,Deriv,W,Children]),
%	format('best_parse_in: deriv n=~w span=~w deriv=~w w=~w children=~w dstruct=~w\n',[NId,Span,Deriv,W,Children,DStruct]),
	true
	.


