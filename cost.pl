/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2015, 2016, 2017, 2018, 2021 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  cost.pl -- compute edge and regional costs
 *
 * ----------------------------------------------------------------
 * Description
 *    the set of rules used to assign weights to edges and nodes
 *    these rule have been manually crafted but can benefit from
 *    additional weights tuned on a treebank
 * ----------------------------------------------------------------
 */

:-include 'header.tag'.
:-require 'extract.pl'.
:-require 'features.pl'.

:-extensional
      rule_no_cost/1,
  extra_elem_cost/2,
  extra_node_cost/2,
  extra_chunk_cost/4,
  sentence/1,
  span/3	
.

:-xcompiler
span(NId,Span) :-
	node2op(NId,OId),
	op{ id => OId, span => Span }
	.


:-xcompiler
precedes( N1::node{},
	  N2::node{}
	) :-
	N1=node{ cluster=> cluster{ right => Right}},
	N2=node{ cluster => cluster{ left => Left}},
	Right =< Left
	.


%% Special case of _SENT_BOUND introduced by sxpipe
:-finite_set(sbound,['*','-','_','+']).
agglutinate(X::sbound[],'_SENT_BOUND',X).

:-light_tabular part_of_agglutinate/2.
:-mode(part_of_agglutinate/2,+(-,-)).

part_of_agglutinate(Form,Token) :- agglutinate(Form,_,Token).
part_of_agglutinate(Form,Token) :- agglutinate(_,Form,Token).

:-finite_set(quepro,['que?','queComp?']).

:-xcompiler
compute_all_edge_costs :-
    ( recorded(opt(no_ssfeatures)) ->
	  every(( edge{ id => EId },
		  edge_cost(EId,_,_,[]),
		  true
		))
     ;
     every(( edge{ id => EId, source => Source :: node{ id => SNId, cat => SCat } },
	     ( recorded( reroot_source(EId,_,_,XSNId) ) ->
		   XSource :: node{ id => XSNId }
	      ;
	      XSource = Source
	     ),
	     ( ( node!empty(XSource),
		 target2edge(edge{ type => adj, target => XSource }),
		 SType = adj
			     %		    xor SCat = prep, % prep attachement
			     %		      edge{ target => Source }
	       )
	      ->
		  target2edge(edge{ id => _EId, type => SType, target => XSource }),
		  exists(( edge2top(_EId,_OId),
			   edge2sop(EId,_OId)
			 )),
		  Constraints = _EId
	      ;
	      Constraints = []
	     ),
	     %%		format('process ~E constraint=~w\n',[E,Constraints]),
	     edge_cost(EId,_,_,Constraints),
	     %% format('done process ~w cst=~w\n',[EId,Constraints]),
	     true
	   ))
    ).

:-light_tabular edge_cost/4.
:-mode(edge_cost/4,+(+,-,-,+)).

edge_cost(EId,W,L,Constraints) :-
%%	format('computing edge cost eid=e~w constraints=~w\n',[EId,Constraints]),
%%	Edge::edge{ id => EId, source => node{}, target => node{} },
%%	every(( t_edge_cost_elem(_,EId,_,Constraints) )),
	mutable(WM,0,true),
	mutable(LM,[],true),
	mutable(RM,[],true),
%%	mutable(XLM,[],true),
%%	verbose('Computing edge cost ~E\n',[Edge]),
	every((
		     every((
				  t_edge_cost_elem(Name,EId,_WRaw,Constraints),
				  %% verbose('Edge cost ~w ~w ~E\n',[Name,_W,Edge]),
				  verbose_cost('Edge cost eid=e~w cst=~w ~w ~w\n',[EId,Constraints,Name,_WRaw]),
				  %	       (opt(no_hand_cost) -> _W is _WRaw / 10 ; _W = _WRaw),
				  (opt(no_hand_cost) -> _W is max(min(_WRaw,1000),-1000) ; _W = _WRaw),
				  mutable_add(WM,_W),
				  _Next ::= Name:_W,
				  mutable_list_extend(RM,_Next),
				  true
			      )),
		     mutable_read(RM,Names),
		     mutable_read(WM,W0),
		     ( use_model ->
			   constrained_edge_features(EId,Constraints,Name,Values),
%			   format('try ~w w0=~w names=~w values=~w\n',[EId,W0,Names,Values]),
			   every(( model!query(Values,_ModelW,Names),
%				   format('found ~w w=~w name=~w names=~w\n',[EId,_ModelW,Name,Names]),
				   domain(Name:_W,Names),
				   ( recorded(rule_multiply(Name)) ->
					 _U is _W * (_ModelW-1),
					 mutable_add(WM,_U),
					 _XW is _W + _U
				    ;
				    mutable_add(WM,_ModelW),
				    _XW is _W + _ModelW
				   ),
				   verbose_cost('Edge cost mdl_delta eid=e~w cst=~w ~w ~w\n',[EId,Constraints,Name,_ModelW]),
				   _Next ::= Name:_XW,
				   mutable_list_extend(LM,_Next)
				 )),
			   mutable_read(LM,_L),
			   every(( 
				   domain(Name:_W,Names),
				   \+ domain(Name:_,_L),
				   _Next ::= Name:_W,
				   mutable_list_extend(LM,_Next)
				 )),
			   true
		      ;
		      mutable(LM,Names)
		     ),
%%	       mutable_list_extend(XLM,Name),
%%	       _Next ::= Name:_XW,
%%	       mutable_list_extend(LM,_Next),
		     true
		 )),
	mutable_read(LM,L),
	mutable_read(WM,W),
	%%	format('TOTAL edge cost cost eid=e~w constraints=~w w=~w l=~w\n',[EId,Constraints,W,L]),
	verbose_cost('TOTAL Edge cost ~w ~w\n',[EId,W]),
	true
	.

:-light_tabular t_edge_cost_elem/4.
:-mode(t_edge_cost_elem/4,+(-,+,-,+)).

t_edge_cost_elem(XName,EId,W,Constraints) :-
	E::edge{ id => EId,
		 type => Type,
		 label => Label,
		 source => node{ cat => SCat, lemma => SLemma },
		 target => node{ cat => TCat, lemma => TLemma }
	       },
	( edge_cost_elem(E,Name,_W,Constraints)
	; edge_cost_elem_type(Type,E,Name,_W,Constraints)
	; edge_cost_elem_label(Label,E,Name,_W,Constraints)
	; edge_cost_elem_label_type(Label,Type,E,Name,_W,Constraints)
	; edge_cost_elem_tcat(TCat,E,Name,_W,Constraints)
	; edge_cost_elem_scat(SCat,E,Name,_W,Constraints)
	; edge_cost_elem_lc(Label,SCat,TCat,E,Name,_W,Constraints)
	; edge_cost_elem_tc(Type,SCat,TCat,E,Name,_W,Constraints)
	; edge_cost_elem_label_tcat(Label,TCat,E,Name,_W,Constraints)
	; edge_cost_elem_label_scat(Label,SCat,E,Name,_W,Constraints)
	; edge_cost_elem_cats(SCat,TCat,E,Name,_W,Constraints)
	; edge_cost_elem_tlemma(TLemma,E,Name,_W,Constraints)
	; edge_cost_elem_slemma(SLemma,E,Name,_W,Constraints)
	; oracle_edge_cost(E,Constraints,_W),
	  Name = 'Oracle'
	),
	mutable(XMW,_W,true),
	every((
	       extra_indirect_cost_elem(EId,Name,Constraints,W1),
	       mutable_add(XMW,W1)
	      )),
	mutable_read(XMW,_W2),
%%	 format('adding extra cost eid=~w cst=~w name=~w ~w\n',[EId,Constraints,Name,W1]),
	( rule_no_cost(Name) ->
	  W2 is _W2 / 100,
	  rule_decorate(Name,W2,XName)
	;
	  XName = Name
	),
	( \+ opt(only_dummy_rule) xor
	% domain(XName,['+dummy','-RANK1','+CatPref','-LONG9','-LONG0','+CLOSED_conj_or_prep',
	% 	      '+SUBST','-RANK2','-LONG3','+PREP->X','-Spelling','-LONG6','+PREP->n','-RANK3','+ARG',
	% 	      '+->de','+RESTR_PP_V','-incise_on_subS','+post_adj','+LONGLEX2','+RESTR_ADJ','+SUBJ','+CLOSED_det'
	% 	     ])
	%% use most effective rules
	domain(XName,['+dummy','-RANK1','-LONG0','+CatPref_6','-RANK2','+SUBST','-LONG9','+CatPref_7','+CatPref_3',
		      '+CLOSED_conj_or_prep','-LONG3','+CatPref','-RANK3','+PREP->X','+PREP->n','-Spelling','-LONG6',
		      '-RANK4','+CatPref_5','+CatPref_1','+CatPref_4','+ARG','-V->de','+post_adj','+RESTR_ADJ'
		     ]
	      )
	),
	( opt(zero_cost),
	  \+ rule_no_cost(Name)
	->
	  W = 0
	;
	  W = _W2
	),
	true
	.

:-extensional extra_indirect_cost_elem/4.

:-light_tabular rule_decorate/3.
:-mode(rule_decorate/3,+(+,+,-)).

rule_decorate(Name,W,XName) :-
	( W > 0 ->
	  name_builder('~w_~w',[Name,W],XName)
	; W < 0 ->
	  W2 is - W,
	  name_builder('~w_m~w',[Name,W2],XName)
	;
	  XName = Name
	)
	.

:-light_tabular sentence_length/2.
:-mode(sentence_length/2,+(-,-)).

sentence_length(Length,SLength) :-
	span_max([0,Max]),
	Length is Max+1,
	( Length > 20 -> SLength = 20
	; Length > 10 -> SLength = 10
	; SLength = Length
	).

:-rec_prolog
	edge_cost_elem/4,
	edge_cost_elem_type/5,
	edge_cost_elem_label/5,
	edge_cost_elem_label_type/6,
	edge_cost_elem_tcat/5,
	edge_cost_elem_scat/5,
	edge_cost_elem_lc/7,
	edge_cost_elem_tc/7,
	edge_cost_elem_label_tcat/6,
	edge_cost_elem_label_scat/6,
	edge_cost_elem_cats/6,
	edge_cost_elem_tlemma/5,
	edge_cost_elem_slemma/5
	.

%% Penalty for adj dependencies
%% edge_cost_elem( edge{ type => adj }, -1, _).

:-light_tabular rule_weight/3.

:-extensional rule_weight/2.

%% Dummy rule for tuning the disamb process
%% we try to remove all rules but this one !
edge_cost_elem(E::edge{},'+dummy',1,_) :- opt(only_dummy_rule).

:-light_tabular oracle_edge_cost/3.
:-mode(oracle_edge_cost/3,+(+,+,-)).

%% Oracle rule to force the disamb process with extra info
oracle_edge_cost(E::edge{ id => EId,
			source => node{ id => SNId, cat => SCat, lemma => SLemma, tree => STree,
					cluster => cluster{ left => SLeft, right => SRight, lex => SToken}
				      },
			target => node{ id => TNId, cat => TCat, lemma => TLemma, tree => TTree,
					cluster => cluster{ left => TLeft, right => TRight, lex => TToken}
				      },
			type => Type::edge_kind[~ lexical],
			label => Label
		      },
		 Constraints,
		 W
		) :-    
    
	'$answers'(prepare_oracle(Stmt)),
	sentence(SId),
	( recorded(opt(strong_oracle)) ->
	  (Constraints = [] -> XConstraints = -1 ; XConstraints = Constraints),
	  L=[SId,EId,TNId,SNId,XConstraints]
	; recorded(opt(weak_oracle)) ->
	  corpus(Corpus),
	  %% format('oracle edge cost corpus=~w sid=~w ~w~n',[Corpus,SId,E]),
	  ( SToken = [_|_] -> domain(XSToken,SToken) ; SToken = '' -> XSToken = '_' ; XSToken = SToken ),
	  ( TToken = [_|_] -> domain(XTToken,TToken) ; TToken = '' -> XTToken = '_' ; XTToken = TToken ),
	  TCat ?= '_',
	  (L = [Corpus,SId,XTToken,XSToken,TCat,Label,Type]
	  ;  L = [Corpus,SId,XTToken,'_',TCat,'_','_']
	  ),
	  true
	;
	  ( STree = [XSTree|_] xor XSTree = STree),
	  ( TTree = [XTTree|_] xor XTTree = TTree),
	  ( SToken = [_|_] -> name_builder('~L',[['~w','_'],SToken],XSToken) ; SToken = '' -> XSToken = '_' ; XSToken = SToken ),
	  ( TToken = [_|_] -> name_builder('~L',[['~w','_'],TToken],XTToken) ; TToken = '' -> XTToken = '_' ; XTToken = TToken ),
	  (SLemma = '' -> XSLemma = '_' ; XSLemma = SLemma),
	  (TLemma = '' -> XTLemma = '_' ; XTLemma = TLemma),
	  SCat ?= '_',
	  TCat ?= '_',
	  L = [SId,SLeft,SRight,XSToken,TLeft,TRight,XTToken,XSTree,XTTree,SCat,TCat,XSLemma,XTLemma,Label,Type]
	),
	verbose_cost('oracle try eid=~w l=~w\n',[EId,L]),
	sqlite!inlined_reset_and_bind(Stmt,L),
	(sqlite!inlined_tuple(Stmt,[Status]) xor fail),
	( Status == 1 -> W = 20000
	; Status == 0 ->  W = 10000
	; Status == 2 ->  W = 5000
	; Status == -1,
	  W = -20000
	),
	verbose_cost('oracle eid=~w w=~w\n',[EId,W]),
	true
	.

:-light_tabular prepare_oracle/1.


prepare_oracle(Stmt) :-
	recorded(opt(oracle,DB)),
	recorded( opt(strong_oracle) ),
	sqlite!prepare(DB,
		       'select status from oracle where sid=? and eid=? and tnid=? and snid=? and seid=?',
		       Stmt
		      )
	.


prepare_oracle(Stmt) :-
	recorded(opt(oracle,DB)),
	\+ recorded( opt(strong_oracle) ),
	\+ recorded( opt(weak_oracle) ),
	sqlite!prepare(DB,
		       'select status from oracle where sid=? and sleft=? and sright=? and stoken=? and tleft=? and tright=? and ttoken=? and stree=? and ttree=? and scat=? and tcat=? and slemma=? and tlemma=? and label=? and type=?',
		       Stmt
		      )
	.

prepare_oracle(Stmt) :-
    recorded(opt(oracle,DB)),
    recorded( opt(weak_oracle) ),
    sqlite!prepare(DB,
		   'select status from oracle where corpus=? and sid=? and tnid=? and snid=? and tcat=? and label=? and type=?',
		   %%'select status from oracle where corpus=? and sid=? and tnid=? and tcat=?',
		   Stmt)
    .


%:-xcompiler
rule_weight(Name,Weight,Default) :-
%	format('rule weight ~w\n',[Name]),
	( rule_weight(Name,Weight) xor Weight = Default ).

%% Get user-provided extra cost
edge_cost_elem(%none,
	       E::edge{},
	       Name::'UserExtraCost',
	       W,
	       _
	      ) :-
	extra_elem_cost(E,W)
	.

%% Get user-provide extra cost for a node
edge_cost_elem(%none,
	       E::edge{ target => N::node{} },
	       Name::'UserNodeCost',
	       W,
	       _
	      ) :-
	extra_node_cost(N,W)
	.

%% Favor subst and lexical dependencies
edge_cost_elem_type(subst,
	       edge{ type => subst ,
		     source => node{ cluster => cluster{ lex => Lex }}},
	       Name::'+SUBST',
	       W,
	       _) :-
	rule_weight(Name,W,10),
	Lex \== ''.

edge_cost_elem_type(lexical,
	       E::edge{ type => lexical ,
			source => node{ cluster => cluster{ lex => Lex }},
			target => node{ cluster => cluster{ token => Token }}
		      },
	       Name::'+LEXICAL',
	       W,
	       _) :-
	%% should avoid favoring over skippable punctuations
	Token \== '_EPSILON',
	%%	format('+LEXICAL ~E\n',[E]);
	%% format('Try lexical ~E lex=~w tok=~w\n',[E,Lex,Token]),
	rule_weight(Name,W,20),
	Lex \== ''.


edge_cost_elem_type(epsilon,
	       edge{ type => epsilon, target => node{ cat => cat[~ [epsilon,sbound,meta]] } },
	       Name::'-skip_in_robust',
	       W,
	       _
	      ) :-
	recorded( mode(robust) ),
	rule_weight(Name,W,-2000)
	.

/*
edge_cost_elem(
	       label,
	       skip
	      edge{ type => epsilon, label => skip },
	       Name::'+skip',
	       W,
               _
	      ) :- rule_weight(Name,W,20)
	.
*/

%% Favor ncpred
edge_cost_elem_tc(lexical,v,ncpred,
		edge{ type => lexical, source => node{ cat => v, lemma => VLemma }, target => node{ cat => ncpred, lemma => NcLemma } },
		Name,
		W,
		_) :-	
	rule_weight(_Name::'+NCPRED',W,500),
	( Name = _Name
	;
	  name_builder('+NCPRED_~w_~w',[VLemma,NcLemma],Name)
	)
	.

%% Favor ncpred mod
edge_cost_elem_lc(L,ncpred,adj,
		edge{ type => adj,
		      label => L::label[ncpred,mod],
		      source => node{ cat => ncpred },
		      target => node{ cat => adj}
		    },
		Name::'+NCPREDMOD',
		W,
		_) :-
	rule_weight(Name,W,30).


%% but penalize det on ncpred
edge_cost_elem_lc(det,v,det,
		edge{ source => V::node{ cat => v },
		      target => node{ cat => det },
		      label => det,
		      type => subst
		    },
		Name::'-DET_ON_NCPRED',
		W,
		_
	      ) :-
	fail,
	rule_weight(Name,W,-200),
	chain( V >> (lexical @ ncpred) >> node{ cat => ncpred })
	.

%% Favor verbal argument, except if date
edge_cost_elem_label(Label,
		E::edge{ id => EId,
			 label => Label::label[subject,object,preparg,comp,xcomp],
			 source => N1::node{ cat => Cat1 },
			 target => node{ id => NId, lemma => Lemma }},
		     Name::'+ARG',
		     W,
		     _
	      ) :-
	%% CALL EDGE
%	format('+ARG test1 ~w ~w\n',[EId,E]),
	\+ domain(Lemma,date[]),
%	format('+ARG test2 ~w\n',[EId]),
	\+ source2edge(
		       edge{ source => N1,
			     target => node{ cat => start },
			     label => start,
			     type => subst
			   }
		      ),
%	format('+ARG test3 ~w\n',[EId]),
	\+ check_node_top_feature(NId,time,true_time[]),
%	format('+ARG test4 ~w\n',[EId]),
	\+ ( Cat1=adj,
	     Label=xcomp,
	     chain( N1 >> (lexical @ prep) >> node{ cat => prep } )
	   ),
	%	format('+ARG test5 ~w\n',[EId]),
	( Label = object ->
	 rule_weight(Name,W,170)
	;
	rule_weight(Name,W,110)
	)
	.

%% favor modal verb, almost as arg
edge_cost_elem_lc(L,v,v,
	       edge{ label => L::label['V',modal],
		     type => adj,
		     source => node{ cat => v },
		     target => node{ cat => v }
		   },
	       Name::'+Modal',
	       W,
	       _
	      ) :-
	rule_weight(Name,W,1000)
	.

%% Favor preparg->de over object
edge_cost_elem_label(preparg,
		edge{ label => label[preparg],
		      target => node{ form => de },
		      source => node{ lemma => SLemma, cat => SCat }
		    },
		Name::'+ARG_prepobj_de',
		W,
		_
		    ) :-
       rule_weight(Name,W,1000).

%% FAVOR object->de over PP-attach on nouns
edge_cost_elem_label(object,
		edge{ label => label[object],
		      target => N::node{ cat => nc } },
		Name::'+ARG_object_de',
		W,
		_
	      ) :-
	rule_weight(Name,W,W3),
	source2edge( edge{ source => N,
			   target => node{ cat => det,
					   lemma => L,
					   form => F }
			 }
		   ),
	( F == du ->
	  W1=300
	;
	  domain(L,[un,du]),
	  F=de_form[des,'de la','de l'''],
	  W1=400
	),
	%% re-inforce if coord
	( chain( N
	       >> adj @ label['N2',coord]
	       >> node{ cat => coo }
	       >> subst @ coord3
	       >> node{}
	       >> subst @ det
	       >> node{ cat => det,
			form => F2
		      }
	       ),
	  domain(F2,[des,'de la','de l''']) ->
	W2=300
	;
	  W2  = 0
	),
	W3 is W1 + W2
	.


%% FAVOR acomp->de over PP-attach on nouns
edge_cost_elem_label(comp,
		edge{ label => comp,
		      target => Comp::node{ cat => comp } },
		Name::'+ARG_acomp_de',
		W,
		_
	      ) :-
	rule_weight(Name,W,W3),
	chain( Comp
	     >> subst @ 'N2'
	     >> N::node{ cat => nc }
	     >> subst @ det
	     >> node{ cat => det,
		      lemma => L,
		      form => F }
	     ),
	( F == du ->
	  W1=300
	;
	  domain(L,[un,du]),
	  F=de_form[des,'de la','de l'''],
	  W1=400
	),
	%% re-inforce if coord
	( chain( N
	       >> adj @ label['N2',coord]
	       >> node{ cat => coo }
	       >> subst @ coord3
	       >> node{}
	       >> subst @ det
	       >> node{ cat => det,
			form => F2
		      }
	       ),
	  F2=de_form[des,'de la','de l'''] ->
	W2=300
	;
	    W2  = 0
	),
	W3 is W1 + W2
	.


%% Penalize object to prep � in infinitive relative (une pomme � croquer)
edge_cost_elem_lc(object,v,prep,
	       edge{ label => object,
		     source => node{ cat => v},
		     target => node{ cat => prep },
		     type => lexical
		   },
	       '-object_to_prep',
	       W,
	       _
	      ) :-
	rule_weight(Name,W,-1000)
	.

%% Penalise long PP-args over a completive or an infinitive
%% to avoid bad attachement in sentence such as
%% ex: il dit (que Paul mange une tarte) (aux fruits).
edge_cost_elem_label(preparg,
		edge{ label => preparg,
		      source => V::node{ cat => v,
					 cluster => cluster{ right => V_Right}
				       },
		      target => node{ cluster => cluster{ left => PP_Left }}
		    },
		Name::'-preparg_over_comp',
		W,
		_
	      ) :-
	rule_weight(Name,W,-900),
	chain( V
	     >> ( subst @ xcomp )
	     >> node{ cluster => cluster{ left => S_Left,
					  right => S_Right } }
	     ),
	V_Right < S_Left,
	S_Right < PP_Left
	.

%% Penalize obj or acomp starting with de, des, ... after noun or adj
edge_cost_elem_label_type(det,subst,
		edge{ label => label[det],
		      type => subst,
		      target => node{ form => Form::de_form[],
				      cluster => cluster{ left => Left }
				    }
		    },
		Name::'-de_after_noun',
		W,
		_
	      ) :-
%	rule_weight(Name,W,-700),
	rule_weight(Name,W,-500),
%%	domain(Form,[de,des,du,'de la','de l''']),
	node{ cat => cat[nc,adj,np],
	      cluster => cluster{ right => Left }
	    }
	.


%% Penalty on comp when N2, (confusion with inverted subject or object or even past participle)
edge_cost_elem_label_tcat(Label,TCat,
		edge{ label => Label::label[comp],
		      source => node{ lemma => L,
				      cluster => CV::cluster{ right => RV } },
		      target => N::node{ cat => TCat::cat[comp] }
		    },
		Name::'-N2asComp',
		W,
		_
	      ) :-
	rule_weight(Name,W,-600),
	%% CALL EDGE
	source2edge(
	      edge{ source => N,
		    target => node{ cat => cat[nc,np],
				    cluster => C::cluster{ left => LN }},
		    label => 'N2',
		    type => subst
		  }
	     ),
	( targetcluster2edge(
		edge{ label => label[subject,object],
		      target => node{ cluster => C }
		    }
	       ),
	  \+ L=estre
	;
%	  sourcecluster2edge(
		edge{ source => node{ cluster => C },
		      label => label['Infl',aux],
		      target => node{ cat => aux }
		    }
%	       )
	)
	.

%% Favor comp to adj (rather than N2)
edge_cost_elem_lc(Label,SCat,TCat,
		edge{ label => Label::label[comp],
		      source => V::node{ cat => SCat::v },
		      target => node{ cat => TCat::comp },
		      type => subst
		    },
		Name::'-comp_as_N2',
		W,
		_
	      ) :- rule_weight(Name,W,-1000),
	%% CALL EDGE
	source2edge(
	      edge{ label => label[comp],
		    source => V,
		    target => node{ cat => adj },
		    type => subst
		  }
	     )
	.

edge_cost_elem_lc(Label,SCat,TCat,
	       edge{ label => Label::label[comp],
		     source => node{ cat => SCat::v, lemma => V_Lemma },
		     target => Prep::node{ cat => TCat::prep,
					   lemma => Prep_Lemma,
					   cluster => cluster{ left => L,
							       right => R
							     }
					 },
		     type => subst
		   },
%	       Name::'-comp_as_PP',
		  Name,
		  XW,
		  _
	      ) :-
	rule_weight(_Name:: '-comp_as_PP',W1,-600),
	( domain(V_Lemma,[estre,sembler]) -> W2 = 0 ; W2 = -600),
	( domain(Prep_Lemma,[en,sans,pour,contre,comme]) -> W3 = 0 ;
	  domain(Prep_Lemma,[de,�]) -> W3 = -300 ;
	  W3 = -400
	),
	( node{ cat => cat[adv,prep],
		cluster => cluster{ left => L2, right => R2 }
	      },
	  R2-L2 > 1,
	  L2 =< L,
	  R2 >= R,
	  (L2 < L xor R2 > R) ->
	  W4 = -800
	;
	  W4 = 0
	),
	( chain(Prep >> subst >> node{ cat => XCat, lemma => XLemma }),
	  (XCat = cat[np,v,pro]
	   xor XLemma = entities[]
	   xor XLemma = date[]
	  ) ->
	  W5 = -500
	;
	  W5 = 0,
	  XLemma = 'void',
	  XCat = 'voidcat'
	),
	W is W1 + W2 + W3 + W4 + W5,
	XW = W,
	Name = _Name,
	true
	.

/*
%% penalize date as args
%% 25/05/08: seems redundant with +ARG
edge_cost_elem( 
		edge{ label => label[object,preparg,comp,xcomp],
		      target => node{ lemma => Lemma, deriv => Derivs }},
		'-date_as_ARG',
		-1000,
                _ ) :-
	( domain(Lemma,date[])
	xor 
	domain(D,Derivs),
	  deriv(D,EId,_,OId,_),
	  check_op_top_feature(OId,time,true_time[])
	)
	.
*/

%% Favor subject, except for gerundive and particiales
edge_cost_elem_label(Label,
		edge{ id => EId, label => Label::label[subject] },
		Name::'+SUBJ',
		W,
		_
	      ) :-
    %rule_weight(Name,W,1100),
    rule_weight(Name,W,100),
	\+ ( edge2sop(EId,OId),
	     check_op_top_feature(OId,mode,gerundive),
	     %%	     format('found gerundive ~w\n',[Op]),
	     true
	   )
	.

%% Favor impersonal subject over normal subjects
edge_cost_elem_label(Label,
		edge{ id => EId,
		      label => Label::label[impsubj],
		      source => N,
		      target => node{ cluster => cluster{ left => ImpLeft }}
		    },
		Name::'+IMPSUBJ',
		W,
		_
	      ) :-
	rule_weight(Name,W,1400),
	\+ ( node!older_ancestor(N,cat[v,aux],P::node{},['Infl','V',modal,aux]),
	     %% CALL EDGE
	     source2edge(
		   edge{ source => P,
			 label => 'subject',
			 target => node{ cluster => cluster{ right => SubjRight }}
		       }
		  ),
	     SubjRight < ImpLeft
	   )
	.

%% Penalize empty subject as strace
edge_cost_elem_label(strace,
		edge{ label => strace, source => S::node{} },
		Name::'-strace',
		W,
		_
	      ) :-
	rule_weight(Name,[W1,W2],[-100,-5000]),
	( chain( S << subst @ label[coord2,coord3] << node{ cat => coo } ) ->
	  W = W1
	;
	  W = W2
	)
	.

%% Favor verb at infinitive, against unsaturaed noun
edge_cost_elem_tcat(v,
		edge{ id => EId, target=> node{ cat => v, cluster => C } },
		Name::'+inf_verb_vs_noun',
		W,
		_) :-
	rule_weight(Name,W,1000),
	N::node{ cluster => C, cat => nc },
	edge2top(EId,TId),
	check_op_top_feature(TId,mode,infinitive)
	.

/*
%% Sligtly Favor post-verbal clitics
edge_cost_elem( 
		edge{ label => subject,
		      source => node{ cluster => cluster{ right => R } },
		      target => node{ cat => cln, cluster => cluster{ left => L } }
		    },
		'+post_verb_clitic',
		1000,
                _
	      ) :-
	R =< L
	.
	*/

%% Favor qui as subject when possible
edge_cost_elem_label(subject,
		edge{ label => subject,
		      target => node{ form => qui,
				      cat => cat[pri,prel] }},
		Name::'+qui_as_subj',
		W,
		_
	      ) :- rule_weight(Name,W,200).

/*
%% Penalize subject for gerundive and particiales
%% 25/05/08: seems redundant with +SUBJ
edge_cost_elem( edge{ id => EId, label => label[subject], deriv => Derivs },
		'-SUBJ',
		-300,
                _ ) :-
	domain(D,Derivs),
	deriv(D,EId,_,OId,_),
	check_op_top_feature(OId,mode,gerundive)
	.
*/

%% Penalize sentential subject
edge_cost_elem_label_tcat(subject,v,
		edge{ id => EId,
		      label => label[subject],
		      target => node{ cat => Cat::cat[v] },
		      source => node{ cat => v }
		    },
		Name::'-S_as_Subj',
		W,
		_) :- rule_weight(Name,W,-800).

edge_cost_elem_label_tcat(subject,TCat,
		edge{ id => EId,
		      label => label[subject],
		      target => node{ cat => TCat::cat[prep] },
		      source => N::node{ cat => v }
		    },
		Name::'-PrepS_as_Subj',
		W,
		_) :-
	rule_weight(Name,W,-1200),
	%% inverted subject because of impersonal subjects
	%% are more easily introduced by 'de'
	\+ chain( N >> (lexical @ impsubj) >> node{} )
	.


%% Favor xcomp over alternate construction with adjoining
%% but penalize (for citations) "participiale" xcomp
edge_cost_elem_lc(Label,SCat,TCat,
		edge{ label => Label::label[xcomp],
		      id => EId,
		      source => V1::node{ cat => SCat::cat[v,adj] },
		      target => V2::node{ cat => TCat::cat[v,'S'] }
		    },
		Name::'+ARG_XCOMP',
		W,
		_)
	:- rule_weight(Name,W1,800),
		(   edge2top(EId,TId),
		    check_op_top_feature(TId,mode,Mode),
		    domain(Mode,[gerundive,participle]) -> W is W1 - 300
		;
		    W = W1
		)
		.

edge_cost_elem_lc(Label,SCat,TCat,
		edge{ label => Label::label[xcomp],
		      source => V1::node{ cat => SCat::cat[nc] },
		      target => V2::node{ cat => TCat::cat[v,'S']}
		    },
		Name::'+NARG_XCOMP',
		W,
		_)
	:- rule_weight(Name,W,2000).

edge_cost_elem_lc(Label,SCat,TCat,
		edge{ label => Label::label[xcomp],
		      source => V1::node{ cat => SCat::cat[adv,advneg] },
		      target => V2::node{ cat => TCat::cat[v,'S']}
		    },
		Name::'+ADVARG_XCOMP',
		W,
		_)
	:- rule_weight(Name,W,2000).

edge_cost_elem_lc(Label,SCat,TCat,
		edge{ label => Label::label[xcomp],
		      source => V1::node{ cat => SCat::cat[adj] },
		      target => V2::node{ cat => TCat::cat[v,'S']}
		    },
		Name::'+ADJARG_XCOMP',
		W,
		_)
	:- rule_weight(Name,W,1500),
		\+ chain( V1 >> (lexical @ 'prep') >> node{ cat => prep } ),
		\+ ( chain( V1
			  << (subst @ comp) << node{ cat => v}
			  >> (subst @ subject) >> Subj::node{}
			  ),
		       precedes(V1,Subj)
		   )
		.

edge_cost_elem_label_scat(Label,SCat,
		edge{ label => Label::label[subject],
		      source => V1::node{ cat => SCat::cat[adj] },
		      target => node{}
		    },
		Name::'+ADJARG_SUBJ',
		W,
		_) :- rule_weight(Name,W,1300),
	chain( V1 >> (lexical @ impsubj) >> node{} )
	.

%% Favor xcomp built by adjoining
edge_cost_elem_lc(L,v,v,
		edge{ label => label['S','mod.xcomp'],
		      source => node{ cat => v },
		      target => node{ cat => v, tree => Tree },
		      type => adj
		    },
		Name::'+ARG_XCOMP_ADJ',
		W,
		_
	      ) :- rule_weight(Name,W,1000),
	domain('arg1:real_arg_xcomp_by_adj',Tree)
	.

edge_cost_elem_label_type(csu,lexical,
		edge{ type => lexical,
		      source => V1::node{ cat => cat[v,adj] },
		      target => node{ lemma => que, cluster => C },
		      label => csu
		    },
		Name::'+que_lexical',
		W,
		_
	      ) :-
	rule_weight(Name,W,700),
	targetcluster2edge(
	    edge{ type => subst,
		  source => V2::node{ cat => cat[v,adj] },
		  target => node{ cat => csu, cluster => C }
		}
	),
	precedes(V2,V1)
	.

%% Favor cleft constructions
edge_cost_elem_label(Label,
	       edge{ label => Label::'CleftQue' },
	       Name::'+CLEFT',
	       W,
	       _) :-
	rule_weight(Name,W,1000).

%% and peuso-cleft
edge_cost_elem_lc(csu,aux,que,
		edge{ label => 'csu',
		      target => node{ cat => que},
		      source => node{ cat => aux, lemma => estre }
		    },
		Name::'+PSEUDOCLEFT',
		W,
		_) :-
	rule_weight(Name,W,800).


:-light_tabular name_long_rule/2.
:-mode(name_long_rule/2,+(+,-)).

name_long_rule(N,Name) :-
    name_builder('-LONG~w',[N],Name).

%% Penalty for long distance dependencies, except for root node
edge_cost_elem( %none,
		Edge::edge{ source => node{ id => IdA, cat => SCat, cluster => cluster{ left => LA, right => RA }},
			    target => node{ cat => TCat, cluster => cluster{ left => LB, right => RB }},
			    type => Type::edge_kind[~virtual],
			    label => Label
			  },
		Name,		% -LONG%n
		W,
		_
	      ) :-
	SCat \== incise,
	verbose('Try length penalty on ~E\n',[Edge]),
	( RA =< LB -> 
	    D1 is  (LB - RA)
	;   
	    D1 is (LA - RB)
	),
%%	D1 > 3,
	(  D1 > 15 ->
	       Name = '-LONG15',
	       Default is 1400-200*15
	 ; D1 > 12 ->
	       Name = '-LONG12',
	       Default is 1400-200*D1
	 ; D1 > 9 ->
	       Name = '-LONG9',
	       Default is 1400-200*D1
	 ; D1 > 6 ->
	       %	  Name = '-LONG6',
	       name_long_rule(D1,Name),
	       Default is -100*(D1-6)
	; D1 > 3 ->
%	      Name = '-LONG3',
	      name_long_rule(D1,Name),
	      Default is 15-10*D1
	;
	%	  Name = '-LONG0',	%'0
	name_long_rule(D1,Name),
	  Default is -5*D1
	),
	( Label == subject ->
	  ( RA =< LB ->
	    %% inverted subject are closer => increase penalties
	    Default2 is Default * 4
	  ;
	    Default2 is Default * 3 / 2
	  )
	; Label == label['Infl',aux] ->
	  Default2 is Default * 30
	; SCat=v, TCat = adv, LB = 0 ->
	  %% almost no penalties for adv at the beginning of a sentence
	  Default2 is -5 * D1
	; Label = coord3, TCat = nominal[] ->
	  Default2 is Default * 2
	; Type = adj, Label = label[prep,csu,'mod.quantity'] -> % quantity mod
	  Default2 is Default * 10
	;
	  Default2 = Default
	),
	rule_weight(Name,W,Default2),
	W < 0
	.

%% Favour adj attribute rather than noun
edge_cost_elem_label_tcat(comp,adj,
		edge{ label => comp, target => node{ cat => adj } },
		Name::'+ATTR',
		W,
		_) :-
	rule_weight(Name,W,100).

%% Favour det rather than adj or noun or verb
%% edge_cost_elem( edge{ target => node{ cat => det } }, 40, _ ).


%% Favour noun over verb in first position, overcoming penalty for virtual edges
%% SHOULD ADD SOMETHING SIMILAR FOR 'start'
edge_cost_elem_tcat(TCat,
		E::edge{ target => node{ cat => TCat::cat[nc,np],
					 cluster => C::cluster{ left => 0 } } },
		Name::'+NOUN/VERB',
		W,
		_) :-
	rule_weight(Name,W,1700),
%%	format('Try Activation ~w\n',[E]),
	%% CALL EDGE
	targetcluster2edge(
			   edge{ target => V::node{ cat => v, cluster => C } }
			  ),
	\+ chain( V << subst << node{} << adj << node{ cat => cat[v,adj] } ),
%%	format('Activation\n',[]),
	true
	.

%% Penalty for raw pronon 'ce'
edge_cost_elem_tcat(TCat,
		edge{ target => N::node{ cluster => cluster{ lex => Lex }, cat => TCat::pro } },
		Name::'-CE',
		W,
		_
	      ) :-
	rule_weight(Name,W,-50),
	%% CALL EDGE
	\+ source2edge( edge{ source => N } ),
	'T'(Lex,Ce),
	domain(Ce,[ce,'Ce'])
	.

%% Penalty for raw 'ce' as subject
edge_cost_elem_label(subject,
		edge{ target => N::node{ cluster => cluster{ lex => Lex }},
		      source => V::node{ cat => v, lemma => L },
		      label => 'subject'		      
		    },
		Name::'-CE_as_subject',
		W,
		_
	      ) :-
	rule_weight(Name,W,-3250),
	%% CALL EDGE
        'T'(Lex,Ce),
	domain(Ce,[ce,'Ce']),
	\+ source2edge( edge{ source => N } ),
	\+ domain(L,['estre'])
	.



%% Favour coordinations
edge_cost_elem_label(Label,
		edge{ label => Label::label[coo,coo2,coord2,coord3],
		      type => Type::edge_kind[subst,lexical]
		    },
		Name::'+COORD',
		W,
		_) :-
	rule_weight(Name,W,20).

%% Favor coord built on same prep
%% specially when potential confusion with an article
edge_cost_elem_cats(prep,coo,
		edge{ source => node{ cat => prep,
				      form => Form1,
				      lemma => Lemma },
		      target => COO::node{ cat => coo }
		    },
		Name::'+coord_same_prep',
		W,
		_
	      ) :-
	rule_weight(Name,W,500),
	%% CALL EDGE
	source2edge( edge{ source => COO,
		     target => node{ cat => prep, lemma => Lemma },
		     label => coord3
		   }
	     )
	.

%% favour coords on 'de' as det
edge_cost_elem_tcat(coo,
		edge{ source => N1::node{ cat => xnominal[] },
		      target => COO::node{ cat => coo },
		      type => adj,
		      id => EId1
		    },
		Name::'+coord_det_de',
		W,
		_
	      ) :-
	chain( COO
	     >> (subst @ coord3) >> node{ cat => xnominal[] }
	     >> (subst @ det) >> node{ cat => det, form => Form::de_form[] }
	     ),
%%	domain(Form,[de,des,du,'de la','de l''']),
	source2edge(
	    edge{ source => N1,
		  target => node{ cat => det },
		  type => subst,
		  label => det,
		  id => EId2
		}
	),
	have_shared_derivs(EId1,EId2),
	rule_weight(Name,W,600)
	.

%% But penalize some coords
edge_cost_elem_lc(L,prep,coo,
		edge{ label => L::label[prep,coord],
		      type => adj,
		      source => node{ cat => prep },
		      target => node{ cat => coo, lemma => Lemma }
		    },
		Name::'-COORD_ON_prep',
		W,
		_) :-
	\+ domain(Lemma,[et,ou]),
	rule_weight(Name,W,-1000).

edge_cost_elem_lc(L,det,coo,
		edge{ label => L::label[det,coord],
		      type => adj,
		      source => node{ cat => det },
		      target => node{ cat => coo, lemma => Lemma }
		    },
		Name::'-COORD_ON_det',
		W,
		_) :-
	\+ domain(Lemma,[et,ou]),
	rule_weight(Name,W,-1000).


%% Penalize coords on S, with coord3 not a S
edge_cost_elem_label(coord3,
		edge{ source => COO::node{ cat => coo },
		      target => node{ cat => TCat },
		      label => coord3,
		      type => edge_kind[lexical,subst]
		    },
		Name::'-COORD_special_S',
		W,
		_
	      ) :-
	rule_weight(Name,W,-500),
	chain( COO << adj << node{ cat => v } ),
	\+ TCat = v
	.

%% Penalize adjoining edges on coo
edge_cost_elem_label_type(coo,adj,
		edge{ label => coo,
		      type => adj,
		      target => node{ cat => Cat },
		      source => COO::node{ cat => SCat::coo }
		    },
		Name::'-adj_on_coo',
		W,
		_
	      ) :-
	rule_weight(Name,[W1,W2],[-50,-500]),
	\+ chain( COO << adj << node{ cat => adv }),
	( Cat = cat[adv,prep] ->
	  W = W1
	;
	  W = W2
	)
	.

%% sligtly favor coord on adj
edge_cost_elem_lc(L,adj,coo,
		edge{ source => node{ cat => adj },
		      target => node{ cat => coo },
		      label => L::label[adj,coord],
		      type => adj
		    },
		Name::'+coord_on_ante_adj',
		W,
		_
	       ) :-
	rule_weight(Name,W,100)
	.

%% penalize verb ellipsis
edge_cost_elem_lc(coord3,coo,v,
		  edge{ label => coord3,
			type => subst,
			source => node{ cat => coo },
			target => node{ cat => v, cluster => cluster{ left => L, right => L } }
		      },
		  Name::'-Coord_with_verb_ellipse',
		  W,
		  _
		 ) :-
    \+ recorded(exotic_on),
	rule_weight(Name,W,-6000)
	.


%% in exotic mode, slightly penalize sharing as args of ellispses
edge_cost_elem(edge{ label => L,
		     type => edge_kind[subst,lexical],
		     source => S::node{ cluster => cluster{ left => SLeft, right => SLeft }},
		     target => node{ id => TId, cluster => cluster{ right => TRight }}
		   },
	       Name::'-sharing_on_ellipse',
	       W,
	       _
	      ) :-
    recorded(exotic_on),
    rule_weight(Name,W,-400),
    chain( S << (subst @ coord3) << node{ cat => coo, cluster => cluster{ left => COO_Left} }),
    TRight =< COO_Left
    	.

edge_cost_elem_label(L,
		edge{ label => L::label[coord],
		      source => N::node{},
		      target => T1::node{},
		      id => EId1
		    },
		Name::'+ENUM',
		W,
		_) :-
	rule_weight(Name,W,40),
	%% CALL EDGE
	source2edge(
	      edge{ label => coord,
		    source => N,
		    target => T2,
		    id => EId2
		  }
	     ),
	T1 \== T2,
	have_shared_derivs(EId1,EId2)
	.


/*
%% favour cords of number on midi and minuit (hours)
%% exemple: entre midi et deux
edge_cost_elem(
	       edge{ source => node{ cat => nc, lemma => Lemma },
		     target => COO::node{ cat => coo, lemma => et },
		     type => adj
		   },
	       Name::'+coo_hours',
	       W,
               _
	      ) :-
	rule_weight(Name,W,200),
	domain(Lemma,[midi,minuit]),
	chain(COO >> (subst @ coord3) >> node{ lemma => '_NUMBER' })
	.
*/

%% Favor noun apposition, specially when an entity is involved
%% and when a "function" is involved
edge_cost_elem_label_scat('N2app','N2',
			  edge{ id => EId,
				source => N2::node{ cat => 'N2',
						    tree => Tree2
						  },
				target =>  node{ cat => Cat3, lemma => Lemma3, cluster => cluster{ token => TForm, lex => TLex }},
				label => 'N2app',
				type => subst
			      },
			  Name,
			  %			  _W,
			  W,
			  _EId
			 ) :-
	rule_weight(_Name:: '+apposition',_W,5),
	target2edge(
	    edge{ id => _EId,
		  target => N2,
		  type => adj,
		  label => 'N2',
		  source => N1::node{ lemma => Lemma1, cat => Cat1, cluster => cluster{ token => SSForm, lex => SSLex } }
		}
	),
	( ( Cat1 = np ; Cat3 = np ),
	  ( Cat1 = nc ; Cat3 = nc ) ->
	  _W1 = 200
	;
	  _W1 = 0
	),
	\+ Lemma1 = date[],
	\+ Lemma3 = date[],
	( (appos_function(Lemma1) xor appos_function(Lemma3)) ->
	  _W2 is _W1+200
	; Lemma1 = number[], Lemma2 = number[] ->
	  _W2 is _W1+200
	;
	  _W2 = _W1
	),
	( acronym_pairing(SSForm,TLex) ->
	  W is _W2 + 200
	;
	  W = _W2
	),
	( Name = _Name
%	; name_builder('+apposition_~w_~w',[Cat1,Cat3],Name)
%	; name_builder('+apposition_~w',[Lemma3],Name)
	),
%	record_without_doublon(extra_indirect_cost_elem(EId,Name,_EId,W)),
	true
	.

:-std_prolog acronym_pairing/2.

acronym_pairing(Form1,TLex) :-
	recorded('T'(TLex,Form2)),
	verbose('acronym_pairing check ~w ~w\n',[Form1,Form2]),
	( is_acronym(Form2,L::[C|_]) ->
%	  format('\tis acronym ~w => ~w\n',[Form2,L]),
	  name(Form1,[C|_])
	;
	  fail
	),
	verbose('=> yes\n',[]),
	true
	.

%% Penalize initial binary coords built on "Ou" that maybe interpreted as
%% a wh-pronoun
edge_cost_elem_label(starter,
		edge{ label => 'starter',
		      target => node{ cat => coo,
				      cluster => C}
		    },
		Name::'-InitOuAsCoord',
		W,
		_
	      ) :-
	rule_weight(Name,W,-200),
	%% CALL EDGE
	targetcluster2edge(
			   edge{ target => node{ cat => pri,
						 cluster => C }
			       }
			  )
	.

%% Favor initial coord built on et (confused with aux etre)
edge_cost_elem_label(starter,
		     edge{ label => 'starter',
			   target => node{ cat => coo,
					   lemma => et,
					   cluster => C}
			 },
		     Name::'+InitEtsCoord',
		W,
		_
	      ) :-
	rule_weight(Name,W,+3000)
	.

%% Penaltie on transcategorization from adj to nc
edge_cost_elem_tcat(adj,
		edge{ target => node{ cat => adj, xcat => 'N2', tree => Tree } },
		Name::'-ADJ',
		W,
		_) :-
	rule_weight(Name,W,-1000),
	domain('adj_as_cnoun',Tree).

%% but favorize adj (as nc) to adv in superlative ('le plus grand')
edge_cost_elem_cats(adj,adv,
	       edge{ source => node{ cat => adj },
		     target => node{ cat => adv, lemma => Lemma } },
	       Name::'+superlative_on_adj',
	       W,
	       _
	      ) :-
	rule_weight(Name,W,500),
	domain(Lemma,[plus,moins,mieux])
	.

%% penalize vmod on adv (in comparative)
edge_cost_elem_label_scat(L,adv,
	       edge{ source => node{ cat => adv },
		     type => adj,
		     label => L::label['vmod',mod]
		   },
	       Name::'-vmod_on_supermod_adv',
	       W,
	       _
	      ) :-
	rule_weight(Name,W,-100)
	.

%% Favour det preceding numbers
edge_cost_elem_cats(number,det,
	       edge{ source => node{ cat => number }, target => node{ cat => det } },
	       Name::'+NUM.DET',
	       W,
	       _) :-
	rule_weight(Name,W,20).

%% Favour number range construction

edge_cost_elem_label(number2,
	       edge{ source => node{ cat => number },
		      target => node{ cat => number },
%%		      type => lexical,
		      label => number2
		    },
	       Name::'+number_range_as_det',
	       W,
	       _
	      ) :-
	%% should counter-balence alternate constructions with two det
	%% [(de dix � quinze) gar�ons] vs [(de dix) � (quinze garcons)]
	%%   1 det vs 2 det
	%% or should we add a rule favoring the longest det interpretation ?
	rule_weight(Name,W,800).


%% Favour some PP range constructions
edge_cost_elem_label_tcat(Label,prep,
			  edge{ source => S::node{ cat => prep },
				target => T::node{ cat => prep },
				label => Label::label['PP',range],
				type => edge_kind[adj]
			      },
			  Name,
			  W,
			  _
			 ) :-
    source2edge(
	edge{ source => S,
	      label => L,
	      type => _Type::edge_kind[subst,lexical],
	      target => node{ lemma => STLemma, cat => STCat }
	    }
    ),
    source2edge(
	edge{ source => T,
	      label => L,
	      type => _Type,
	      target => node{ lemma => TTLemma, cat => TTCat }
	    }
    ),
    ( TTLemma == STLemma ->
	  W = 4000,
	  Name = '+PP_range_sl'
      ;TTLemma = 'l''un', STLemma = 'l''autre' ->
	   W = 4000,
	   Name = '+PP_range_pro'
      ; TTCat = STCat ->
	    W = 100,
	    Name = '+PP_range_sc'
      ; W = 10,
	Name = '+PP_range'
    )
.


%% Favour predet constructions
edge_cost_elem_label_tcat(det,predet,
		edge{ source => node{ cat => cat[nc,adj,pro,det] },
		      target => node{ cat => predet },
		      label => det,
		      type => edge_kind[subst,adj]
		    },
		Name::'+PREDET',
		W,
		_) :-
	rule_weight(Name,W,100).

%% Favour det modifier
edge_cost_elem_lc(det,det,adj,
		edge{ source => node{ cat => det },
		      target => node{ cat => adj },
		      label => det,
		      type => adj
		    },
		Name::'+det_mod',
		W,
		_
	      ) :-
	rule_weight(Name,W,300)
	.

%% Favour preddet on pro
edge_cost_elem_lc(Label,pro,adj,
		edge{ source => node{ cat => pro },
		      target => node{ cat => adj },
		      label => Label::label[predet_ante,predet_post],
		      type => lexical
		    },
		Name::'+predet_on_pro',
		W,
		_
	      ) :-
	rule_weight(Name,W,500).


%% Favour aux-v rather rather than v-acomp
%% but strong penalty on distance
edge_cost_elem_cats(SCat,TCat,
		edge{ source => node{ cat => SCat::cat[v,aux] }, target => node{ cat => TCat::aux } },
		Name::'+AUX-V',
		W,
		_) :-
	rule_weight(Name,W,3000).

%% Penalty on person constructions (vs e.g participiales): Jean, viens manger !
edge_cost_elem_tcat('S',
		edge{ target => node{ cat => 'S', tree => Tree } },
		Name::'-PERS',
		W,
		_) :-
	rule_weight(Name,W,-20),
	domain(person_on_s,Tree)
	.

%% Favorize time mode, but on nouns
edge_cost_elem_label_type(time_mod,subst,
		edge{ type => subst,
		      label => time_mod,
		      target => N::node{ form => Form },
		      source => node{ cat => SCat }
		    },
		Name::'+time_mod',
		W,
		_
	      ) :-
	rule_weight(Name,W,50),
	( Form == '�t�' ->
	  chain( N >> (subst @ det) >> node{ cat => det } )
	;
	  true
	)
	.

%% Penalize attachement of time, person, audience amd reference incises
%% on subbordonate sentences
edge_cost_elem_label_type(Label,adj,
		edge{ type => adj,
		      source => V::node{ cluster => cluster{ right => R_V }},
		      target => S::node{ cat => cat['S','VMod'] },
		      label => Label::label['S','S2',vmod,mod],
		      id => EId
		    },
		Name::'-incise_on_subS',
		W,
		_
	      ) :-
	rule_weight(Name,W1,-50),
	%% CALL EDGE
	source2edge(
	      edge{ source => S,
		    type => subst,
		    label => label[person_mod,audience,reference,time_mod,'S_incise',position,ce_rel],
		    target => N::node{ cluster => cluster{ left => L_Incise },
				       lemma => Lemma,
				       cat => Cat
				     }
		  }
	     ),
	chain( V << edge_kind[~ virtual] << R::node{ tree => RTree } ),
	\+ chain( R >> (subst @ start) >> node{} ),
	\+ domain( sep_sentence_punct, RTree ),
	( %% fail,
	  chain( N >> adj >> node{} >> subst >> V) ->
	  %% to compensate participiale 
	  W2 = -600
	;
	  W2 = 0
	),
	( R_V =< L_Incise,
	  \+ chain( S >> (adj @ incise) >> node{ cat => incise } ) ->
	  W3 = 50
	;
	  W3 = 0
	),
	%% penalize such mod on participiales, ...
	( edge2sop(EId,OId),
	  check_op_top_feature(OId,mode,Mode),
	  domain(Mode,[participle,gerundive]) ->
	  W4 = -800
	;
	  W4 = 0
	),
	W is W1 + W2 + W3 + W4
	.

%% Penalize some attachement of person, audience amd reference incises
edge_cost_elem_label_type(Label,subst,
		edge{ source => node{ cat => cat['S','VMod'] },
		      type => subst,
		      label => Label::label[person_mod,audience,reference,ce_rel],
		      target => N::node{ cat => TCat, lemma => TLemma, cluster=> TC::cluster{ left => TLeft, right => TRight }}
		    },
%		Name::'-incises',
			  Name,
			  W3,
			  _
			 ) :-
	rule_weight(Name,_W,-800),
	( Label = label[person_mod,audience],
	  TCat = np,
	  TLemma = entities['_PERSON','_PERSON_m','_PERSON_f']
	->
	  W1 is _W + 150
	; TLemma = ce,
	  Label = label[~ ce_rel]
	->
	  W1 = -5000 
	;
	  W1 = _W
	),
	(   AltN::node{ cat => adv, cluster => _TC::cluster{ left => _TLeft, right => _TRight }},
	    ( TC = _TC ;
		(_TLeft =< TLeft, TRight =< _TRight )
	    )
	->
	    %% node_weight(AltN,WAlt),
	    WAlt = 0,
	    W2 is W1-WAlt
	;   
	    W2 = W1
	),
	( Label = label[audience] ->
	  ( recorded(audience(TLemma)) ->
	    W3 is W2 + 300,
	    Name = '-incises_audience_class'
	  ; chain( N >> (lexical @ 'Monsieur') >> node{} ) ->
	    W3 is W2 + 400,
	    Name = '-incises_audience_Monsieur'
	  ; chain( N >> (adj @ label['N',mod]) >> node{ cat => adj, lemma => AdjLemma } ),
	    domain(AdjLemma,[cher,honor�,estim�]) ->
	    W3 is W3 + 300,
	    Name = '-incises_audience_adjective'
	  ; domain(TCat,[pri,pro]) ->
	    W3 is W2 - 5000,
	    Name = '-incises_audience_pro'
	  ;
	    W3 is W2 - 500,
	    Name = '-incises_audience'
	  )
	; Label = label[ce_rel] ->
	  Name = '-incise_ce_rel',
	  W3 = 200
	;
	  W3 = W2,
	  Name = '-incises'
	),
	true
	.

audience(pr�sident).
audience('Monsieur').
audience(commissaire).
audience(coll�gue).
audience(professeur).
audience(capitaine).
audience(homme).
audience(ami).
audience(soeur).
audience(fr�re).
audience(p�re).
audience(m�re).
audience(tante).
audience(oncle).
audience(ministre).
audience('Premier ministre').
audience(d�put�).
audience(s�nateur).
audience(d�l�gu�).
audience(partlementaire).
audience(dieu).
audience(rapporteur).
audience(compagnon).
audience(confr�re).
audience(consoeur).
audience(chef).
audience(fille).
audience(fils).
audience(roi).
audience(reine).
audience(prince).
audience(princesse).
audience(militant).
audience(auteur).
audience(commandant).
audience(entities['_PERSON','_PERSON_m','_PERSON_f']).

%% Penalize s_modifiers on N2
edge_cost_elem_label_type( Label,subst,
			   edge{ source => node{ cat => 'N2' },
				 type => subst,
				 label => Label::label[person_mod,audience,reference,time_mod,position,ce_rel]
			       },
			   Name:: '-s_modifier_on_N2',
			   W,
			   _
			 ) :-
	rule_weight(Name,W,-200)
	.

edge_cost_elem( %none,
		edge{ target => N::node{ cat => Cat,
					 cluster => cluster{ left => Left, right => Right } } },
		Name::'+LONGLEX3',
		W,
		_
	      ) :-
	Right - Left > 1,
	( Cat = cat[det] ->
	    W = -300
	;
	    node_weight(N,W)
	)
	.

:-light_tabular node_weight/2.
:-mode(node_weight/2,+(+,-)).

%% to compensate closed words in locutions
node_weight( node{ lemma => Lemma,
		   cluster => cluster{ left => Left, right => Right } }, W) :-
	mutable(WM,0,true),
%%	format('try node add left=~w right=~w lemma=~w\n',[Left,Right,Lemma]),
	every(( node{ cat => Cat::cat[det,prep,csu,coo],
		      cluster => cluster{ left => _Left, right => _Right },
		      lemma => _Lemma
		    },
		Left =< _Left,
%%		format('*** node add ~w ~w l=~w r=~w\n',[_Lemma,Cat,_Left,_Right]),
		_Right =< Right,
		\+ (Left = Left, Right = _Right ),
		mutable_add(WM,1300)
	      )),
	mutable_read(WM,W),
	W > 0
	.


%% penalize some incise missing coma
%% ? use constraints
edge_cost_elem_lc(Label,SCat,TCat,
		edge{ source => node{ cat => SCat::cat[v,adj], cluster => cluster{ left => L} },
		      label => Label::label['S','S2',mod],
		      target => S::node{ cat => TCat::'S', cluster => cluster{ right => R }},
		      type => adj
		    },
		Name::'-pseudo_incise',
		W,
		_
	      ) :-
	R =< L,
	chain( S >> subst >> node{ cat => prep } ),
	\+ chain( S >> (adj @ incise) >> node{ cat => incise } ), 
	rule_weight(Name,W,-100)
	.

%% slightly favorize vmod->pro
edge_cost_elem_lc(Label,v,pro,
		edge{  %source => v,
		       %target => pro,
		       type => adj,
		       label => Label::label['S','S2',vmod,mod]
		    },
		Name::'+pro_as_mod',
		W,
		_
	      ) :- rule_weight(Name,W,100).

%% penalize default incise as paren
edge_cost_elem_label_type(Label,subst,
		edge{ source => node{ cat => cat['S','VMod']},
		      type => subst,
		      label => Label::label['S_incise']
		    },
		Name::'-paren_incise',
		W,
		_
	      ) :-
	rule_weight(Name,W,-1000)
	.


%% but favor S_incise for citations
edge_cost_elem_label_type(Label,subst,
		edge{ source => node{ cat => cat['S','VMod']},
		      target => V::node{ lemma => Lemma,
					 cat => cat[v],
					 cluster => cluster{ right => Right} },
		      type => subst,
		      label => Label::label['S_incise']
		    },
		Name::'+citation_incise',
		W,
		_
	      ) :-
	rule_weight(Name,W,3500),
	%format('here1 lemma=~w\n',[Lemma]),
	(   chain( V >> edge_kind[subst,lexical] @ subject >> Subj::node{ cluster => cluster{ left => Left } } ) ->
	    Right =< Left
	;
	    chain( V >> adj @ label['Infl','V',aux,modal] >> V1::node{ cat => cat[v,aux] } >> lexical @ subject >> Subj)
	),
	%format('here2 lemma=~w\n',[Lemma]),
	\+ ( chain( V >> edge_kind[subst,lexical] @ label[object] >> node{ cluster => cluster{ left => _Left}}),
	       _Left \== Left
	   ),
	%format('here2.1 lemma=~w\n',[Lemma]),
	\+ chain( V >> lexical @ label[prep,csu] >> node{} ),
	%format('here3 lemma=~w\n',[Lemma]),
	%% tmp hack: use list of citation verbs
	%% but better  to add a specific mechanism into FRMG
	domain(Lemma,[dire,indiquer,mentionner,poursuivre,continuer,affirmer,
		      confirmer,accuser,assurer,avancer,conclure,interroger,lancer,
		      noter,raconter,pr�venir,regretter,souligner,admettre,r�sumer,insister,
		      souligner,remarquer,annoncer,protester
		     ]
	      )
	.

/*
%% favor balanced incise marks
edge_cost_elem( 
		edge{ target => node{ cat => incise, tree => Tree },
		      label => incise,
		      type => adj
		    },
		Name::'+balance_incise_marks',
		W,
                _
	      ) :- rule_weight(Name,W,300),
	domain('incise_strict',Tree)
	.
*/

%% favour genitive over locative
edge_cost_elem_label(clg,
		edge{ target => node{ cat => clg }, label => clg },
		Name::'+CLG',
		W,
		_) :- rule_weight(Name,W,50).


%% favour active voice over passive
edge_cost_elem(edge{ source => V::node{ cat => v, id => NId },
		     target => node{ cat => aux, lemma => estre },
		     type => adj,
		     label => 'Infl',
		     deriv => Derivs
		   },
	       Name::'+active_voice',
	       W,
	       _
	      ) :-
    \+ chain( V >> _ @ preparg >> node{ cat => prep, lemma => par }),
    once(( domain(DId,Derivs),
	   deriv2htid(DId,HTId),
	   check_ht_feature( HTId, diathesis, active)
	 )),
    rule_weight(Name,W,50)
.

/*
%% Favour auxiliary over verbs
edge_cost_elem( 
		edge{ target => node{ cat => aux }},
		Name::'+AUX/V',
		W,
                _ ) :- rule_weight(Name,W,2000).
*/

%% Favour 'est' as verb
%% edge_cost_elem( edge{ target => node{ cluster => cluster{ }}, 1000, _ ).

%% Penalties on filler in robust parsing
edge_cost_elem_tcat(unknown,
		edge{ target => node{ cat => unknown } },
		Name::'-UNK',
		W,
		_) :- rule_weight(Name,W,-3000).

%% Extra penalties on unknown trees in robust parsing
edge_cost_elem_scat(unknown,
		edge{ source => node{ cat => unknown,
				      tree => [unknown]
				    }
		    },
		Name::'-UNK2',
		W,
		_
	      ) :- rule_weight(Name,W,-10000)
	.

%% Extra penalties on unknown v fillers
edge_cost_elem_cats(unknown,v,
		edge{ source => node{ cat => unknown },
		      target => node{ cat => cat[v] }
		    },
		Name::'-UNK3',
		W,
		_
	      ) :- rule_weight(Name,W,-1000)
	.

%% Penalties on virtual edges
edge_cost_elem_type(virtual,
		edge{ type => virtual },
		Name::'-VIRTUAL',
		W,
		_) :-
	rule_weight(Name,W,-3000).

%% But less penalties on virtual edges leading to verbs, not in imperative
edge_cost_elem_type(virtual,
		edge{ id => EId,
		      type => virtual,
		      target => node{ cat => v }
		    },
		Name::'+VIR->V',
		W,
		_) :- rule_weight(Name,W,500).


%% Penalties on imperative verbs when in robust mode
edge_cost_elem_scat(v,
		edge{ id => EId, source=> node{ cat => v, cluster => C }},
		Name::'-Imp_Verbs_when_Robust',
		W,
		_) :-
	rule_weight(Name,W,-4000),
	recorded( mode(robust) ),
	edge2sop(EId,OId),
	check_op_top_feature(OId,mode,imperative)
	.

%% Penalize clitics on infinitive and participial
%% when possible interpretation as det
edge_cost_elem_tcat(cla,
		edge{ source => V::node{ cat => v },
		      target => Cl::node{ cat => cat[cla], cluster => C},
		      type => lexical
		    },
		Name::'-cl_vs_det',
		W,
		_
	      ) :-
	rule_weight(Name,W,-1500),
	%% CALL EDGE
	\+ source2edge( edge{ source => V, label => subject } ),
	targetcluster2edge( edge{ target => node{ cluster => C, cat => det } } )
	.


%% Penalties on virtual edges leading to empty short sentence
edge_cost_elem_type(virtual,
		edge{ type => virtual,
		      target => node{ cat => 'S',
				      cluster => cluster{ lex => '' }}
		    },
		Name::'-SHORTVIRT',
		W,
		_) :-
	rule_weight(Name,W,-500),
	recorded(mode(robust)).


%% Favour closed categories interpretations over others
edge_cost_elem_tcat(Cat,
		    edge{ source => node{ cluster => cluster{ left => SLeft, right => SRight }},
			    target => node{ lemma => L,
				      cat => Cat::cat[csu,prep,coo,det,pri,pro,prel,coo,cln,cla,clr,clg,cll,cld,ilimp,caimp,predet,title,que,clneg],
				      form => F,
				      cluster => C::cluster{ left => Left, right => Right } } },
		Name,		% +CLOSED_%type
		W,
		_) :-
%%	rule_weight(Name,[W1,W2,W3,W4],[1700,1500,1200,600]),
	L \== unknown[],
	\+ (L  = number[]),
	\+ (L  = tout ),	% tout maybe too many things (det, adv, pro)
	( Cat = cat[predet] ->
	  Name = '+CLOSED_predet',
	  rule_weight(Name,W,1700),
	  \+ ( domain(L,[nombre]),
	       edge{ source => node{ cat => nc, cluster => C}, % *** WARNING: may be inefficient
		     target => node{ cat => cat[adj,det,predet] },
		     type => edge_kind[adj,subst]
		   }
	     ),
	  true
	; Cat = cat[det] ->
	  Name = '+CLOSED_det',
	  ( ( C1::cluster{ left => Left, right => Middle },
	      node{ cluster => C1, cat => prep },
	      cluster{ left => Middle, right => Right }
	    ) ->
	    %% for cases like 'il mange [de la] viande'	 
	    rule_weight(Name,W,2900)
	  ;
	    rule_weight(Name,W,1700)
	  )
	; Cat = clneg ->
	  Name = '+CLOSED_clneg',
	  rule_weight(Name,W,2000)
	; Cat = cat[cln,cla,clr,clg,cll,cld,ilimp,caimp],
	  (\+ domain(F,[en])) ->
	      Name = '+CLOSED_cl',
	      SLeft \== SRight,	% for the exotic mode and ellipsed verb
	  rule_weight(Name,W,1500)
	; Cat = cat[que] -> 
	  Name = '+CLOSED_que',
	  rule_weight(Name,W,1200)
	; Cat = cat[prep,csu,coo] ->
	  Name = '+CLOSED_conj_or_prep',
	  rule_weight(Name,W,1200)
% 	; Cat = cat[aux] ->
% 	  Name = '+CLOSED_aux',
% 	  rule_weight(Name,W,500)
	;
	  Name = '+CLOSED_misc',
%%	  \+ domain(L,[tout,aucun,autre]),
	  rule_weight(Name,W,600)
	)
	.
%%	edge{ target => node{ cat => cat[nc,np,adj,adv,v], cluster => C } }.

%% Strongly penalize det as noun or adj

%% Strongly penalize que or qu' as pri after verb
edge_cost_elem_label_tcat(object,pri,
		edge{ target => node{ lemma => quepro[],
				      cat => pri,
				      cluster => cluster{ left => L} },
		      source => node{ cluster => cluster{ right => R }},
		      type => subst,
		      label => object
		    },
		Name::'-queAsObject',
		W,
		_
	      ) :-
	rule_weight(Name,W,-10000),
	R =< L
	.

edge_cost_elem_tlemma(TLemma,
		edge{ source => N::node{ cat => comp},
		      target =>  node{ lemma => TLemma::quepro[],
				       cat => pri,
				       cluster => cluster{ left => L} },
		      type => subst,
		      label => 'N2'
		    },
		Name::'-queAsComp',
		W,
		_
	      ) :-
	rule_weight(Name,W,-20000),
	%% CALL EDGE
	target2edge(
		    edge{ target => N,
			  source => node{ cluster => cluster{ right => R }},
			  label => comp,
			  type => subst
			}
		   ),
	R =< L
	.


%% penalize que as pri in xcomp constructions
edge_cost_elem_tlemma(TLemma,
		edge{ source => V::node{ cat => v },
		      target => node{ cat => TCat::cat[pri],
				      lemma => TLemma::quepro[]
				    },
		      type => subst,
		      label => Label::label[comp,object]
		    },
		Name::'-que_as_pri',
		W,
		_) :-
	rule_weight(Name,W,-2000),
	%% CALL EDGE
	target2edge(
		    edge{ target => V,
			  source => VS,
			  type => subst,
			  label => xcomp
			}
		   )
	.

%% favor relative sentence over other constructions
%% weaker if the relative sentence is preceded by a comma
edge_cost_elem_label_type(Label,adj,
		edge{ source => N1::node{},
		      target => N2::node{},
		      type => adj,
		      label => Label::label['N2','dep.relative']
		    },
		Name::'+relative',
		W,
		_
	      ) :-
	rule_weight(Name,[W1,W2],[1100,1200]),
	node!empty(N2),
	%% CALL EDGE
	source2edge(
	      edge{ source => N2,
		    target => V::node{ cat => v, cluster => cluster{ left => Left} },
		    label => 'SRel',
		    type => subst
		  }
	     ),
	( chain( N2 >> adj >> node{ cat => incise } >> lexical 
	       >> node{ lemma => ',',
			cluster => cluster{ right => InciseRight }}
	       ),
	  InciseRight =< Left ->
	  W=W1
	;
	  W=W2
	)
	.

%% Strongly penalize relative on non-saturated nouns
%% except if in PP or if an NP or at the beginning of a sentence
edge_cost_elem_label_type(Label,adj,
		edge{ source => N1::node{ cat => N1Cat::xnominal[nc] },
		      target => N2::node{},
		      type => adj,
		      label => Label::label['N2','dep.relative']
		    },
		Name::'-relative_on_unsat_noun',
		W,
		_
	      ) :-
	rule_weight(Name,W,-4000),
	node!empty(N2),
	chain( N2 >> (subst @ 'SRel') >> node{} ),
	\+ chain( N1 >> (subst @ det) >> node{ cat => det } ),
	\+ chain( N1 << (subst @ 'N2') << node{ cat => prep } ),
	\+ chain( N1 << (subst @ 'N2') << node{ cat => comp } << subst @ comp << node{ cat => 'S' } >> subst @ start >> node{} )
	.
		
%% favor qui as relative
edge_cost_elem_label_type(Label,adj,
		edge{ source => N1,
		      target => N2::node{ cat => 'N2',
					  cluster => cluster{ left => L,
							      right =>L }},
		      type => adj,
		      label => Label::label['N2','dep.relative']
		    },
		Name::'+quiAsRel',
		W,
		_
	      ) :-
	rule_weight(Name,W,100),
	chain( N2 >>  (subst @ 'SRel')
	     >> V::node{ cat => v } >> (lexical @ subject)
	     >> node{ cat => prel }
	     )
	.

%% Penalize qui as rel pron without antecedent
edge_cost_elem_label_type('SRel',subst,
		edge{ source => N1::node{ cat => 'N2' },
		      label => 'SRel',
		      type => subst
		    },
		Name::'-quiAsRelNoAntecedent',
		W,
		_
	      ) :-
	rule_weight(Name,W,-3000),
	node!empty(N1),
	%% CALL EDGE
	\+ target2edge(
		       edge{ label => label['N2','dep.relative'],
			     type => adj,
			     target => N1
			   }
		       )
	.

%% Penalties on _uw and _Uw words
edge_cost_elem_tlemma(uw,
		edge{ target => node{ lemma => 'uw' } },
		Name::'-UW1',
		-1100,
		_
	      ) :- rule_weight(Name,W,-1100).

edge_cost_elem_tlemma(uw,
		edge{ target => node{ cat => nc, lemma => uw } },
		Name::'+UWasNc',
		W,
		_
	      ) :- rule_weight(Name,W,100).

edge_cost_elem_tlemma(uw,
		edge{ target => node{ cat => cat[adj,adv], lemma => uw } },
		Name::'+UWasAdjorAdv',
		W,
		_
	      ) :- rule_weight(Name,W,50)
	      .

edge_cost_elem_tlemma('_Uw',
		edge{ target => node{ lemma => '_Uw' } },
		Name::'-UW2',
		W,
		_
	      ) :- rule_weight(Name,W,-1000)
	      .

%% Penalize unknown words as verbs at beginning of sentence
edge_cost_elem_tlemma(uw,
		edge{ target => node{ lemma => uw,
				      cat => cat[v,aux],
				      cluster => cluster{ left => 0 }
				    } },
		Name::'-uw_as_v_on_start',
		W,
		_) :- rule_weight(Name,W,-1000).


%% Penalize capitalization when there is a non-capitalized word
%% except if there is not det for the word
edge_cost_elem_tcat(np,
		edge{ target => N::node{ cat => np, lemma => L, cluster => C } },
		Name::'-Capitalize',
		W,
		_
	      ) :-
	rule_weight(Name,W,-100),
%	L \== unknown[],
	node{ cat => cat[nc,adj], cluster => C },
	( chain( N >> (subst @ det ) >> node{ cat => det } )
	 ; chain( N << subst << node{ cat => prep } )
	)
	.

edge_cost_elem(edge{ target => node{ cat => Cat, cluster => cluster{ left => Left, right => Right } } },
	       Name::'-pseudo_lex',
	       W,
	       _) :-
           recorded( pseudo_lex(Left,lemma{ cat => Cat },Right) ),
	   rule_weight(Name,W,-200)
	   .

%% Penalties on sequence of Nc
edge_cost_elem_label('Nc2',
		Edge::edge{ source => node{ cat => nc },
		      target => node{ cat => nc, lemma => Lemma },
		      label => 'Nc2' },
		Name::'-NcSeq',
		W,
		_) :-
	rule_weight(Name,W,-1000),
	Lemma \== number[],
	verbose('I am here ~E\n',[Edge])
	.

%% Favor sequence of Nc leading to numbers
edge_cost_elem_label('Nc2',
		Edge::edge{ source => node{ cat => nc },
		      target => node{ cat => nc, lemma => number[] },
		      label => 'Nc2' },
		Name::'+NcSeq_Num',
		W,
		_) :-
	rule_weight(Name,W,+1050)
	.

%% Penalize nc sequence
edge_cost_elem_label('N',
		     edge{ label => 'N', type => adj, target => node{ cat => nc } },
		     Name:: '-NcSeq',
		     W,
		     _
		    ) :-
	rule_weight(Name,W,-200)
	.

%% Penalties on use of comma as sentence separator
%% Extremely strong penalties
%% Should only be used as very last option
edge_cost_elem_tcat('S',
		Edge::edge{ type => adj, target => N::node{ cat => 'S', tree => Tree }, source => Source::node{ cat => SCat }},
		Name::'-COMASEP',
		W,
		_) :-
	rule_weight(Name,[W1,W2],[-6000,-10000]),
%	format('test comasep ~w\n',[Edge]),
	domain(sep_sentence_punct_coma,Tree),
	%	format('success comasep ~w\n',[Edge]),
	( SCat = v,
	  chain( N >> subst >> node{ cat => v } ) ->
	      W =W1
	 ;
	      W = W2
	),
	true
	.

%% Penalties on sentence separator applied on subordonnate sentences
edge_cost_elem_tcat('S',
	       Edge::edge{ type => adj,
			   target => N::node{ cat => 'S', tree => Tree },
			   source => V::node{}
			 },
	       Name::'-sentence_sep_on_subs',
	       W,
	       _
	      ) :-
	fail,
	rule_weight(Name,W,-5000),
	node!empty(N),
	chain( V << _ << node{} ),
	chain( N >> lexical >> node{ lemma => Lemma} ),
	domain(Lemma,[(?),('...'),';',',','!','?','!?','?!','!!!','_SENT_BOUND','(...)'])
	.

%% Favor main verbal node (for verbal sentences) for full sentences
edge_cost_elem_type(virtual,
		edge{ id => root(NId),
		      target => node{ id=> NId, cat => v },
		      type => virtual
		    },
		Name::'+verbal_sentence',
		W,
		_
	      ) :-
	recorded( mode(parse_mode[full,corrected]) ),
	rule_weight(Name,W,300),
	%%	format('verbal_sentence\n',[]),
	true
	.

edge_cost_elem_tcat('S',
		Edge::edge{ type => adj, target => N::node{ cat => 'S' } },
		Name::'+QUOTED_SENT_MOD',
		W,
		_
	      ) :-
	rule_weight(Name,W,-1000), % strange pb => prefer penalize
	%% CALL EDGE
	source2edge(
	      edge{ source => N,
		    type => subst,
		    label => quoted_S
		  }
	     )
	.

%% penalize quoted_S in N2 context
edge_cost_elem_label_type(quoted_S,subst,
	       edge{ source => node{ cat => 'N2' },
		     label => quoted_S,
		     type => subst
		   },
	       Name::'-quoted_S_as_N2',
	       W,
	       _
	       ) :-
	rule_weight(Name,W,-1000)
	.

/*
%% Bonuses on use of comma in enumerations
%% depends on presence of ending ... and number of ','
edge_cost_elem( 
		Edge::edge{ type => adj, target => N::node{ tree => Tree }},
		Name::'+COMAENUM',
		K,
                _
	      ) :-
	rule_weight(Name,W,300),
	domain('N2_enum',Tree),
	mutable(M,W,true),
	%% CALL EDGE
	every(( source2edge(
		      edge{ source => N,
			    type => lexical,
			    target => node{ cluster => cluster{ lex => Lex }}}
		     ),
		label2lex(Lex,[X],_),
		mutable_read(M,_K),
		( X == (',') ->
		    New_K is 2*_K
		;   X == '...' ->
		    New_K is 3*_K
		;
		    New_K is _K
		),
		mutable(M,New_K)
	      )),
	mutable_read(M,K)
	.
*/

%% Favour adj->adv edges
edge_cost_elem_cats(adj,adv,
		edge{ source => node{ cat => cat[adj] },
		      target => node{ cat => adv },
		      type => adj
		    },
		Name::'+ADVMODADJ',
		W,
		_) :- rule_weight(Name,W,150).

%% Favour adv->adv edges
edge_cost_elem_cats(SCat,TCat,
		edge{ source => node{ cat => SCat::cat[adv,advneg] },
		      target => node{ cat => TCat::adv },
		      type => adj
		    },
		Name::'+ADVMODADV',
		W,
		_) :- rule_weight(Name,W,170).

%% Favour adj->pro edges
edge_cost_elem_cats(pro,adv,
		edge{ source => node{ cat => cat[pro] },
		      target => node{ cat => adv },
		      type => adj
		    },
		Name::'+ADVMODPRO',
		W,
		_) :- rule_weight(Name,W,200).


%% Penalize mutiple adv edges on adj or Prep on same side
edge_cost_elem_cats(SCat,TCat,
		edge{ source => N::node{ cat => SCat::cat[adj,prep], cluster => cluster{ left => NLeft, right => NRright } },
		      target => node{ cat => TCat::adv, cluster => cluster{ right => R }},
		      type => adj
		    },
		Name::'-multipleAdvOnAdjorPrep',
		W,
		_
	      ) :-
	rule_weight(Name,W,-1000),
	%% CALL EDGE
	source2edge(
	      edge{ source => N,
		    target => node{ cat => adv, cluster => cluster{ left => L } },
		    type => adj
		  }
	     ),
	R =< L,
	(L =< NLeft xor NRight =< R)
	.

%% Penalize adv on prep
edge_cost_elem_cats(SCat,TCat,
		edge{ source => node{ cat => SCat::cat[prep] },
		      target => node{ cat => TCat::adv },
		      type => adj
		    },
		Name::'-ADVONPREP',
		W,
		_) :- rule_weight(Name,W,-500).

/*
%% Penalties on edge coming from empty cluster
edge_cost_elem( 
		Edge::edge{ source => node{ cluster => cluster{ lex => '' }}},
		Name::'-EMPTY',
		W,
                _ ) :-
	rule_weight(Name,W,-20),
	verbose('Empty cluster penalty ~E\n',[Edge])
	.
*/

%% Favour long clusters, especially for closed cats
edge_cost_elem( %none,
		Edge::edge{ target => node{ cat => Cat,
					    lemma => Lemma,
					    form => Form,
					    cluster => cluster{ lex => Lex,
								left => Left,
								right => Right }}},
		Name::'+LONGLEX',
		K,
		_
	      ) :-
	rule_weight(Name,[F1,F2],[500,100]),
	Lex \== '',
	Right > Left + 1,
%%	format('LONGLEX ~w ~w lex=~w form=~w\n',[Left,Right,Lex,Form]),
%%	\+ agglutinate(_,_,Form),
	\+ Form = de_form['de la','du','des','de l'''],
	\+ ( Cat = np,
	     node{ cat => X::cat[adj,nc,prep,det], cluster => cluster{ left => LX, right => RX } },
	     LX >= Left,
	     RX =< Right,
%%	     ( LX \== Left xor RX \== Right),
	     \+ ( node{ cat => np,
			form=> Lemma,
			cluster => cluster{ left => LX, right => RX } },
		  Lemma \== '_Uw'
		),
	     true
	   ),
	( %% Cat = cat[det,pri,prel,adv,advneg,prep,csu,que] -> Factor = F1
	  %% *** WARNING *** not sure it is the best choice for prep
	  Cat = cat[det,pri,prel,csu,que,prep] -> Factor = F1
	; Factor = F2
	),
	K is (Right - Left) * (Right - Left) * Factor,
	K > 0,
%%	format('=> LONGLEX w=~w\n',[K]),
	true
	.

%% Favour long clusters case 2
edge_cost_elem( %none,
		Edge::edge{ target => node{ cat => Cat,
					    cluster => cluster{ lex => Lex,
								left => Left,
								right => Right
							      }}},
		Name::'+LONGLEX2',
		K,
		_
	      ) :-
	rule_weight(Name,[F1,F2],[500,100]),
	Lex \== '',
	( Lex = [_,_|_] ->
	  Right is Left+1,
	  length(L,N)
	;
	  Right > Left+1,
	  Cat = cat[adv,advneg,prep],
	  N is (Right - Left)
	),
	\+ ( Cat = np,
	     node{ cat => X::cat[adj,nc,prep,det], cluster => cluster{ left => LX, right => RX } },
	     LX >= Left,
	     RX =< Right,
	     \+ node{ cat => np, cluster => cluster{ left => LX, right => RX } }
	   ),
%%	format('Trying LONGLEX2 ~E\n',[Edge]),
%%	format('Trying2 LONGLEX2 ~E\n',[Edge]),
	( Cat = cat[det,pri,prel,prep,adv,advneg,prep,csu,que] -> Factor = F1
	; Factor = F2
	),
	K is N*N*Factor,
%%	format('Trying3 LONGLEX2 L=~w ~E\n',[L,Edge]),
	K > 0
	.

%% Favor long prep but in a decomposed way !
edge_cost_elem_tcat(prep,
		edge{ %% source => node{ cat => nc },
		      target => Last::node{ cat => prep,
					    cluster => cluster{ left => L,
								right => R,
								lex => Lex
							      }
					  }
		      %% label => 'N2',
		      %% type => adj
		    },
		Name::'+long_prep',
		W,
		_
	      ) :-
	recorded( opt( nocompound )),
	rule_weight(Name,W,500),
	%% CALL EDGE
	targetcluster2edge(
	      edge{ target => node{ cat => prep,
				    cluster => cluster{ left => L2,
							right => R,
							lex => Lex2,
							token => Token2
						      }
				  }}
	     ),
%%	format('try lex=~w lex2=~w token2=~w\n',[Lex,Lex2,Token2]),
	(\+ passage_compound(Token2)),
	Lex \== Lex2,		% not an explicit composed form for Easy
	L2 < L
	.

%% penalize adjonction on potential adv composed forms
%% these forms are decomposed for Passage but should preserve
%% their composed properties as much as possible
edge_cost_elem_type(adj,
		edge{ source => node{ cluster => cluster{ left => L, right => R }},
		      type => adj
		    },
		Name::'-Postmod_on_compound_adv',
		W,
		_
	      ) :-
	rule_weight(Name,W,-1000),
	%% CALL EDGE
	targetcluster2edge(
			   edge{ target => node{ cat => adv,
						 cluster => cluster{ left => L1, right => R }
					       }
			       }
			  ),
	L1 < L,
	true
	.

edge_cost_elem_cats(TCat,prep,
		edge{ source => N2::node{ cat => TCat::cat[nc,np,v],
					  lemma => Target
					},
		      target => Prep::node{ cat => prep },
		      type => adj
		    },
		Name::'-RESTR_mod_on_term',
		W,
		_
	      ) :-
	chain( N2
	     << subst
	     << node{ cat => prep, lemma => Rel }
	     << adj
	     << node{ cat => SCat, lemma => Source }
	     >> adj
	     >> Prep
	     ),
	check_term(Source,SCat,Target,TCat,Rel,W)
	.


%% penalize non term reading
edge_cost_elem(
	edge{ source => node{ cluster => cluster{ left => SLeft, right => SRight }},
	      target => node{ cluster => cluster{ left => TLeft, right => TRight }}
	    },
	Name::'-break_term',
	W,
	_
    ) :-
    recorded(term(Start,End,Term)),
    ( ( SLeft > Start, 
	SRight =< End,
	(TLeft < Start xor TRight > End)
      )
	  xor
	  (TLeft > Start,
	   TRight =< End,
	   (SLeft < Start xor SRight > End)
	  )
    ),
    W = -500
	.

%%% favorize passage compound
edge_cost_elem( %none,
		edge{ target => node{ cluster => cluster{ token => Token, left => L, right => R }}},
		Name::'+passage_compound',
		W,
		_
	      ) :-
	rule_weight(Name,W,500),
	L + 1 < R,
	passage_compound(Token)
	.

%% Penalize long lex when NP and reading with some non np components
edge_cost_elem_tcat(np,
		Edge::edge{ target => node{ cat => np,
					    cluster => cluster{ lex => Lex, left => Left, right => Right }}},
		Name::'-NP_as_LONGLEX',
		W,
		_) :-
	rule_weight(Name,W,-20),
	Right > Left + 1,
	node{ cat => X::cat[adj,nc,prep,det], cluster => cluster{ left => LX, right => RX } },
	LX >= Left,
	RX =< Right,
	\+ ( node{ cat => np, lemma => Lemma, cluster => cluster{ left => LX, right => RX } },
	     Lemma \== '_Uw'
	   ),
	true
	.

%% Favour prep->X edges
edge_cost_elem_scat(prep,
		Edge::edge{ source => node{ cat => prep },
			    target => node{ cat => cat[~ [adj,adv]] },
			    type => edge_kind[subst,lexical] },
		Name::'+PREP->X',
		W,
		_
	      ) :- rule_weight(Name,W,400).

%% Favour prep->nc|np edges
edge_cost_elem_cats(prep,TCat,
		Edge::edge{ source => node{ cat => prep },
			    type => edge_kind[subst,lexical],
			    target => node{ cat => TCat::cat[nc,np] }
			  },
		Name::'+PREP->n',
		W,
		_) :- rule_weight(Name,W,200).

%% Favour lexical v->prep edges
edge_cost_elem_tc(lexical,v,prep,
		Edge::edge{ source => node{ cat => v },
			    target => node{ cat => prep },
			    type => edge_kind[lexical] },
		Name::'+V->PREP',
		W,
		_
	      ) :- rule_weight(Name,W,800).

%% Penalize PP attachement of +time on nouns and adj
edge_cost_elem_tc(adj,SCat,TCat,
		Edge::edge{
			   source => node{ cat => SCat::cat[nc,np,adj] },
			   type => adj,
			   target => Prep::node{ cat => TCat::prep }
			  },
		Name::'-N2->prep->+time',
		W,
		_
	      ) :-
	rule_weight(Name,W,-200),
	%% CALL EDGE
	source2edge(
	      edge{ source => Prep,
		    type => edge_kind[subst,lexical],
		    %% label => 'N2',
		    target => N2::node{ id => NId2,
					cat => N2Cat,
					lemma => Lemma
				      }
		  }
	     ),
	( domain(Lemma,date[])
	xor N2Cat=adv
	xor check_node_top_feature(NId2,time,true_time[])
	)
	.

%% very low bonus for prep->ENTITY on verbs
%% mostly used to anchor cost-based tuning
%% ? use constraints
edge_cost_elem_tc(adj,SCat,TCat,
		  Edge::edge{
			     source => Source::node{ cat => SCat::cat['VMod',nc,np,adj], lemma => _SLemma },
			     type => Type,
			     target => Prep::node{ cat => TCat::prep, lemma => PLemma }
			    },
		  Name,
		  W,
		  _
		 ) :-
	%% CALL EDGE
%	format('test0 entity ~w\n',[Edge]),
	source2edge(
	      edge{ source => Prep,
		    type => edge_kind[subst,lexical],
		    target => node{ id => NId2,
				    cat => Cat,
				    lemma => Lemma::entities[]
				  }
		  }
	     ),
%	format('test1 entity ~w\n',[Edge]),
	( SCat = 'VMod' ->
	  Type = subst,
	  chain( Source << adj << node{ cat => v, lemma => SLemma } )
	;
	  SLemma = _SLemma,
	  Type = adj
	),
	( Name = '+prep->ENTITY'
	; name_builder( '+prep->ENTITY_~w_~w',[PLemma,Lemma], Name )
	; name_builder( '+prep->ENTITY_~w_~w_~w',[SLemma,PLemma,Lemma], Name )
	; %recorded(vector(PLemma,prep,PVec)),
	  recorded(vector(SLemma,SCat,SVec)),
	  name_builder( '+prep->ENTITY_vec_~w_~w',[SVec,PLemma], Name )
	),
	rule_weight(Name,W,+2)
	.

%% favor PP[pour]+inf v
edge_cost_elem_lc('PP',SCat,TCat,
		Edge::edge{
			   source => node{ cat => SCat::cat['VMod','S'] },
			   type => subst,
			   label => 'PP',
			   target => Prep::node{ cat => TCat::prep,
						 lemma => L
					       }
			  },
		Name::'+v->pour->Sinf',
		W,
		_
	      ) :-
	rule_weight(Name,_W,130),
	domain(L : W2 ,[pour : 0 ,
			'afin de' : 100,
			'sans' : 0,
			'quand �' : 100,
			'avant de' : 100,
			'en dehors de' : 100,
			'en d�pit de' : 100,
			'en fait de' : 100,
			'en plus de' : 100,
			'en train de' : 100,
			'en voie de' : 100,
			'en vue de' : 100,
			'entre' : 0,
			'hormis' : 0,
			'non sans' : 100,
			'plut�t que de' : 100,
			'sauf �' : 100,
			'si pr�s de' : 0,
			'� force de' : 100
		       ]),
	W is _W + W2,
	chain( Prep >> (subst @ 'S') >> node{ cat => v } )
	.

%% Favour participial on subjects
%% ? use constraints
edge_cost_elem_lc(Label,v,NCat,
		edge{ source => V::node{ cat => v },
		      target => N::node{ cat => NCat::cat['S','VMod'], tree => Tree },
		      type => adj,
		      label => Label::label['S','S2',vmod,mod] },
		Name::'+Participiale_on_Subject',
		W3,
		_) :-
	rule_weight(Name,W,200),
	%% CALL EDGE
	source2edge(
	      edge{ source => N,
		    target => Part::node{ cat => v },
		    label => 'SubS',
		    type => 'subst',
		    id => EId
		  }
	     ),
	edge2top(EId,OId),
	check_op_top_feature(OId,mode,Mode),
	domain(Mode,[participle,gerundive]),
	%% CALL EDGE
	( source2edge(edge{ source => N,
		      label => incise,
		      type => adj
		    }) ->
	  W1 is W+150
	;
	  W1 is W
	),
	( source2edge(
		edge{ source => V,
		      label => subject
		    }
	       )->
	  W2 is W1+ 300
	;
	  W2 is W1
	),
	( chain( Part
	       << subst
	       << node{}
	       << adj
	       << N2::node{ cat => xnominal[] }
	       ) ->
	    %% but favour attachement on nouns when possible
	    fail
	;
	    W3 = W2
	)
	.

%% Favour participle attachement on subjects
%% ? use constraints
edge_cost_elem_label_tcat('N2',TCat,
		edge{ source => N1::node{ cat => SCat::xnominal[] },
		      target => N2::node{ cat => TCat::'N2' },
		      label => 'N2',
		      type => adj
		    },
%%		Name::'+Participiale_on_Subject2',
			  Name,
			  W,
			  _
	      ) :-
	rule_weight(Name,W,50),
	chain(N1 << (subst @ subject) << node{ cat => v }),
	chain(N2 >> (adj @ incise) >> node{ cat => incise }),
	edge{ source => N2,
	      target => node{ cat => v},
	      label => 'SubS',
	      type => subst,
	      id => EId
	    },
	edge2top(EId,OId),
	check_op_top_feature(OId,mode,Mode),
	domain(Mode,[participle,gerundive]),
	( Mode = participle ->
	  Name = '+Participiale_on_Subject2_participle'
	;
	   Name = '+Participiale_on_Subject2_gerundive'
	)
	.

%% Penalize subject on participles
edge_cost_elem_label_scat(subject,v,
		edge{ source => V::node{ cat => v },
		      label => subject
		    },
		Name::'-subject_on_participle',
		W,
		_
	      ) :-
	edge{ target => V,
	      label => 'SubS',
	      type => subst,
	      id => EId
	    },
	rule_weight(Name,W,-500),
	edge2top(EId,OId),
	check_op_top_feature(OId,mode,Mode),
	domain(Mode,[participle])
	.
	      

%% Penalize SubS on SubS
%% ? use constraints
edge_cost_elem_cats(v,TCat,
		edge{ source => V::node{ cat => v },
		      target => S::node{ cat => TCat::cat['S','VMod'] },
		      type => adj
		    },
		Name::'-participiale_on_participiale',
		W,
		_
	      ) :-
	chain( S >> (subst @ 'SubS') >> node{ cat => v } ),
	chain( V << (subst @ 'SubS') << node{ cat => 'N2' } ),
	rule_weight(Name,W,-1000)
	.

%% Favour v->VMOd->prep edges for participials
%% ? use constraints (9893)
edge_cost_elem_cats(v,'VMod',
		Edge::edge{ source => V::node{ cat => v },
			    target => VMod::node{ cat => 'VMod' },
			    type => adj
			  },
		Name,
		W,
		_
	      ) :-
	rule_weight(Name:: '+V->VMOD->PREP',W,400),
	%% CALL EDGE
	source2edge(
	      edge{ source => VMod,
		    target => node{ cat => prep, lemma => PLemma },
		    type => subst,
		    label => 'PP'
		  }
	     ),
	target2edge(
		    edge{ source => node{ cat => 'N2' },
			  target => V,
			  type => subst,
			  label => 'SubS'
			}
		   ),
	true
	.

%% rule v->prep to be used as a feature in the cost mechanism
%% ? use constraints (206371)
%/*
edge_cost_elem_cats(v,'VMod',
		    E::edge{ source => node{ cat => v, lemma => VLemma },
			     target => VMod::node{ cat => cat['S','VMod'] },
			     type => adj
			   },
		    Name,
		    W,
		    _
		   ) :-
	chain( VMod >> (subst @ 'PP') >> node{ cat => prep, lemma => PrepLemma } ),
	W is 5,
	name_builder('+PP_V_~w_~w',[VLemma,PrepLemma],Name)
	.
%*/
/*
edge_cost_elem(edge{ type => subst,
		     label => 'PP',
		     source => VMod,
		     target => node{ cat => prep, lemma => PrepLemma}
		   },
	       Name,
	       W,
	       SEId		%parent edge
	      ) :-
    edge{ id => SEId,
	  type => adj,
	  source => node{ cat => v, lemma => VLemma },
	  target => VMod::node{ cat => cat['S','VMod'] }
	},
    W is 5,
    name_builder('+PP_V_~w_~w',[VLemma,PrepLemma],Name)
	.
*/

% edge_cost_elem( edge{ source => node{ lemma => Source },
% 		      target => N::node{},
% 		      type => adj
% 		    },
% 		Name,
% 		W,
%               _
% 	      ) :-
% 	( node!empty(N),
% 	  chain( N >> subst >> T::node{ lemma => Target } ),
% 	  \+ node!empty(T),
% 	  W is 2,
% 	  name_builder('indirect_~w',[Target],Name)
% 	xor fail
% 	)
% 	.

%% Favor v->prep if prep modified by adv
edge_cost_elem_cats(SCat,prep,
		edge{ source => node{ cat => SCat::cat['S','VMod',v] },
		      target => P::node{ cat => prep },
		      type => subst
		    },
%%		Name::'+V->prep+adv',
		    Name,
		    W,
		    _
	      ) :-
	rule_weight(Name:: '+V->prep+adv',W,250),
	%% CALL EDGE
	source2edge(
	      edge{ source => P,
		    target => node{ cat => adv, lemma => AdvLemma },
		    type => adj,
		    label => label['PP',mod]
		  }
	     )
	.

%% Penalty on v->prep[de,d',des] edges going to verbs
edge_cost_elem_cats(SCat,prep,
		Edge::edge{ source => node{ cat => SCat::cat['S','VMod','N2'] },
			    target => N::node{ cat => prep,
					       lemma => 'de'
					     }
			  },
		Name::'-V->de',
		W,
		_) :-
	rule_weight(Name,W,-100),
	%% CALL EDGE
	\+ source2edge(edge{ source => N, target => node{ cat => v } })
	.

%% Penalty on aux->VMod->des
edge_cost_elem_cats(aux,'VMod',
		    edge{ source => node{ cat => aux},
			  label => vmod,
			  target => N::node{ cat => 'VMod' }
			},
		    Name::'-aux->des',
		    W,
		    _
		   ) :-
    rule_weight(Name,W,-2200),
    source2edge(edge{ source => N,
		      label => 'PP',
		      target => node{ cat => prep,
				      lemma => de,
				      cluster => cluster{ token => Form}
				    }
		    }),
    domain(Form,[des,du,'de la'])
	.

%% Favor [adj,nc,np]->de edges instead of 'de X' as GN
edge_cost_elem_cats(SCat,TCat,
		Edge::edge{ source => N::node{ cat => SCat::cat[nc,np,adj,pro] },
			    type => edge_kind[~ lexical],
			    target => Prep::node{ cat => TCat::prep, lemma => 'de', cluster => cluster{ left => Left } } },
		Name::'+->de',
		W,
		_
	      ) :-
	rule_weight(Name,W,500),
	\+ chain( Prep >> (subst @ 'N2')
		>> node{} << (subst @ object) << node{}
		),
	\+ ( chain( N >> (subst @ xcomp) >> node{ cluster => cluster{ right => Right }} ),
	       Right =< Left
	   )
	.

%% penalize 'de X' as partitive
edge_cost_elem_cats(SCat,TCat,
		Edge::edge{ source => N::node{ cat => SCat::cat[nc] },
			    type => lexical,
			    target => Prep::node{ cat => TCat::prep, lemma => 'de',
						  cluster => cluster{ left => Left, token => Form } } },
		Name::'-de_partitive',
		W,
		_
	      ) :-	
	rule_weight(Name,_W,-500),
	( domain(Form,[des]) -> W is _W - 1000 ; W = _W )
	.

%% Favor de->np as prep (not sure it is useful)
edge_cost_elem_lc('N2',prep,np,
		edge{ source => node{ cat => prep, lemma => 'de' },
		      target => node{ cat => np },
		      type => subst,
		      label => 'N2'
		    },
		Name::'+de->np',
		W,
		_
	      ) :- rule_weight(Name,W,100).


%% Favor nc1->prep->nc2 if nc2 is not saturated
edge_cost_elem_lc('N2',prep,nc,
		edge{ source => node{ cat => prep },
		      target => N2::node{ cat => nc },
		      type => subst,
		      label => 'N2'
		    },
		Name::'+nc->prep->unsat_nc',
		W,
		_
	      ) :-
	rule_weight(Name,W,100),
	%% CALL EDGE
	\+ source2edge(
		 edge{ source => N2,
		       target => node{ cat => cat[det,predet] },
		       type => subst
		     }
		)
	.


%% Penalize adjoint on unsaturated nc preceded by a prep
edge_cost_elem_scat(nc,
		edge{ source => N1::node{ cat => nc },
		      target => node{ cat => cat[~prep] },
		      type => adj
		    },
		Name::'+-adj_on_unsat_nc_with_prep',
		W,
		_
	      ) :-
	rule_weight(Name,W,-10),
	%% CALL EDGE
	target2edge(
		    edge{ source => node{ cat => prep, lemma => Prep },
			  target => N1,
			  id => NId1
			}
		   ),
	domain(Prep,[de]),
	\+ ( chain( N1 << subst @ prep << node{ cat => prep, id => NId2 } ),
	     NId1 \== NId2
	   ),
	\+ source2edge(
		 edge{ source => N1,
		       target => node{ cat => cat[det,predet] },
		       type => subst
		     }
		)
	.


%% Penalize N2 adj after a relative, but for coordinations
edge_cost_elem_label(Label,
		edge{ source => N::node{ xcat => 'N2'},
		      label => Label::label['N2',mod,dep,'dep.relative'],
		      type => adj,
		      target => M :: node{ cat => cat[~ [coo]],
					   cluster => cluster{ left => LM }}
		    },
		Name::'-N2adj_after_rel',
		W,
		_
	      ) :-
	rule_weight(Name,W,-600),
	chain( N >> (adj @ label['N2','dep.relative'])
	     >> node{} >> (subst @ 'SRel')
	     >> node{ cluster => cluster{ left => LM1 } }
	     ),
	LM1 < LM
	.

%% Penalty based on rank
edge_cost_elem( %none,
		Edge::edge{ id => EId },
		Name,		% -RANK%n
		W,
		_
	      ) :-
	edge_rank(EId,Rank,_),
	build_rule_rank_name(Rank,Name),
	Default is -5*Rank,
	rule_weight(Name,W,Default).

:-light_tabular build_rule_rank_name/2.
:-mode(build_rule_rank_name/2,+(+,-)).

build_rule_rank_name(Rank,Name) :-
	name_builder('-RANK~w',[Rank],Name)
	.

%% Favor 'est' as verb or aux
edge_cost_elem_tcat(TCat,
		edge{ target => node{ cat => TCat::cat[v,aux],
				      form => 'est'
				      }
		    },
		Name::'+est/v_or_aux',
		W,
		_ ) :- rule_weight(Name,W,1000).


rule_no_cost('+RESTR_PP_V').

%% Use of restriction database: PP attachment on verbs
%% ? use constraints
edge_cost_elem_lc(Label,v,TCat1,
		edge{ source => node{ lemma => Source, cat => v },
		      target => VMod::node{ cat => TCat1::cat['S','VMod'] },
		      label => Label::label[vmod,'S','S2',mod],
		      type => adj
		    },
		  %%		Name::'+RESTR_PP_V',
		  Name,
		  W,
		 _) :-
	%%	rule_weight(Name,W,300),
	recorded(opt(restrictions,DB)),
	%% CALL EDGE
	( chain( VMod >> edge{}
	       >> node{ cat => prep } >> subst
	       >> node{ cat => cat[nc,np,v,adj] }
	       )
	xor fail
	),
	mutable(MW,0,true),
	mutable(WL,none,true),
	mutable(WP,none,true),
	mutable(WCat,none, true),
	every((
		     chain( VMod >> edge{}
			    >> node{ cat => prep, lemma => _Prep } >> subst
			    >> _T::node{ lemma => _Target, cat => TCat::cat[nc,np,v,adj] }
			  ),
		     ( check_restriction(Source,v,_Target,TCat,_Prep,_W1),
		       _W1 > 0
				 xor _W1 = 0
		     ),
		     ( chain( _T >> adj >> node{ cat => coo }
			      >> (subst @ coord3 ) >> node{ lemma => Target3, cat => TCat3 }
			    ),
		       check_restriction(Source,v,Target3,TCat3,_Prep,_W3) ->
			   _W2 is _W1 + _W3
		      ;
		      _W2 = _W1
		     ),
		     mutable_max(MW,_W2),
		     mutable(WL,_Target),
		     mutable(WCat,TCat),
		     mutable(WP,_Prep)
		 )),
	mutable_read(MW,XW),
	XW2 is XW / 2,
	mutable_read(WP,Prep),
	(
	    XW \== 0,
	    W=XW2,
	    mutable_read(WL,Target),
	    name_builder('+RESTR_PP_V_~w_~w_~w',[Source,Prep,Target],Name)
	 ;
	 XW \== 0,
	 W=XW2,
	 name_builder('+RESTR_PP_V_~w_~w',[Source,Prep],Name)
	 ;
	 W=2,
	 recorded(vector(Source,v,SVec)),
%	 recorded(vector(Prep,prep,PVec)),
%	 recorded(vector(Target,TCat,TVec)),
	 ( name_builder('+RESTR_PP_V_vec_~w_~w',[SVec,Prep],Name)
	  ;
	  mutable_read(WL,Target),
	  mutable_read(WCat,TCat),
	  recorded(vector(Target,TCat,TVec)),
	  name_builder('+RESTR_PP_V_vec_~w_~w_~w',[SVec,Prep,TVec],Name)
	 )
	)
.

rule_no_cost('+RESTR_PP_V_prel').

%% ? use constraints
edge_cost_elem_label_type(Label,adj,
		edge{ source => node{ lemma => Target, cat => TCat::cat[nc,np,adj] },
		      target => N::node{},
		      type => adj,
		      label => Label::label['N2',dep,'dep.relative']
		    },
%		Name::'+RESTR_PP_V_prel',
			  Name,
			  W,
			 _) :-
	%%	rule_weight(Name,W,300),
	recorded(opt(restrictions,DB)),
	chain( N >> (subst @ 'SRel') >> node{ cat => v, lemma => Source }
	     >> ( adj @ label['S',mod] ) >> S::node{}
	     >> (lexical @ prel ) >> node{ cat => prel }
	     ),
	chain( S >> (lexical @ prep) >> node{ cat => prep, lemma => Prep } ),
	check_restriction(Source,v,Target,TCat,Prep,XW),
	W is XW / 2,
	(
	 name_builder('+RESTR_PP_V_~w_~w_~w',[Source,Prep,Target],Name)
	;
	 name_builder('+RESTR_PP_V_~w_~w',[Source,Prep],Name)
	)

	.

rule_no_cost('+RESTR_ARG').

edge_cost_elem_scat(v,
		edge{ source => node{ lemma => Source, cat => v },
		      target => N::node{ lemma => Target, cat => TCat },
		      type => subst,
		      label => Label
		    },
%		Name::'+RESTR_ARG',
		    Name,
		    W,
		    _
	      ) :-
	recorded(opt(restrictions,DB)),
	( Label = subject ->
	  check_restriction(Source,v,Target,TCat,sujet,W),
	  name_builder('+RESTR_ARG_subject_~w_~w',[Source,Target],Name)
	; Label = object ->
	  check_restriction(Source,v,Target,TCat,cod,W),
	  name_builder('+RESTR_ARG_object_~w_~w',[Source,Target],Name)
	; Label = xcomp ->
	  check_restriction(Source,v,Target,TCat,cod,W),
	  Name = '+RESTR_ARG_xcomp'
	; Label = preparg ->
	  mutable(MW,0,true),
	  mutable(WL,none,true),
	  mutable(WCat,none,true),
	  every((
		 chain( N >> (subst @ 'N2') >> node{ lemma => _Target2,
						     cat => TCat2 }
		      ),
		 check_restriction(Source,v,_Target2,TCat2,Target,_W),
		 _W > 0,
		 mutable_max(MW,_W),
		 mutable(WL,_Target2),
		 mutable(WCat,TCat2)
		)),
	  mutable_read(MW,XW),
	  XW2 is XW / 2,
	  (XW \== 0,
	   W = XW2,
	   mutable_read(WL,Target2),
	   name_builder('+RESTR_ARG_preparg_~w_~w_~w',[Source,Target,Target2],Name)
	   ;
	   XW \== 0,
	   W =XW2,
	   name_builder('+RESTR_ARG_preparg_~w_~w',[Source,Target],Name)
	   ;
	   W = 2,
	   recorded(vector(Source,v,SVec)),
	   recorded(vector(Target,TCat,TVec)),
%	   recorded(vector(Target2,TCat2,T2Vec)),
	   ( name_builder('+RESTR_ARG_preparg_vec_~w_~w',[SVec,TVec],Name)
	    ;
	    mutable_read(WL,Target2),
	    mutable_read(WCat,TCat2),
	    recorded(vector(Target2,TCat2,T2Vec)),
	    name_builder('+RESTR_ARG_preparg_vec_~w_~w_~w',[SVec,TVec,T2Vec],Name)
	   )
	  )
	;
	  fail
	)
	.

rule_no_cost('+RESTR_ARG_prel').

%% ? use constraints
edge_cost_elem_label_type(_L,adj,
		edge{ source => node{ lemma => Target, cat => TCat::cat[nc,np,adj] },
		      target => N::node{},
		      type => adj,
		      label => _L::label['N2','dep.relative']
		    },
%		Name::'+RESTR_ARG_prel',
			  Name,
			  W,
			  _
	      ) :-
	recorded(opt(restrictions,DB)),
	chain( N >> (subst @ 'SRel') >> V::node{ cat => v, lemma => Source }
	     >> (lexical @ Label ) >> node{}
	     ),
	( Label = subject ->
	  check_restriction(Source,v,Target,TCat,sujet,W),
	  name_builder('+RESTR_ARG_subject_~w_~w',[Source,Target],Name)
	; Label = object ->
	  check_restriction(Source,v,Target,TCat,cod,W),
	  name_builder('+RESTR_ARG_object_~w_~w',[Source,Target],Name)
	; Label = preparg ->
	  chain( V >> (lexical @ prep) >> node{ lemma => Target2,
						cat => TCat2 }
	       ),
	  check_restriction(Source,v,Target2,TCat2,Target,W),
	  name_builder('+RESTR_ARG_preparg_~w_~w_~w',[Source,Target,Target2],Name)
	;
	  fail
	)
	.

rule_no_cost('+RESTR_ADJ').

%% ? use constraints
edge_cost_elem_tc(adj,SCat,TCat,
		edge{ source => node{ lemma => Source,
				      cat => SCat::cat[nc,np],
				      cluster => cluster{ left => L, right => R }
				    },
		      target => N::node{ lemma => Target, cat => TCat::adj,
					 cluster => cluster{ left => LAdj, right => RAdj }
				       },
		      type => adj
		    },
		Name::'+RESTR_ADJ',
		W,
		_
	      ) :-
	recorded(opt(restrictions,DB)),
	( R =<  LAdj ->
	  check_restriction(Source,SCat,Target,TCat,modifieur,W)
	; 
	  check_restriction(Source,SCat,Target,TCat,antemodifieur,W)
	)
	.

rule_no_cost('+RESTR_ADV').

%% ? use constraints (96065)
edge_cost_elem_tc(adj,SCat,TCat,
		edge{ source => node{ lemma => Source, cat => SCat::cat[adj,adv,nc,v] },
		      target => N::node{ lemma => Target, cat => TCat::cat[adv,advneg] },
		      type => adj
		    },
		Name::'+RESTR_ADV',
		W,
		_
	      ) :-
	recorded(opt(restrictions,DB)),
	check_restriction(Source,SCat,Target,TCat,modifieur,W)
	.

rule_no_cost('+RESTR_TIME').

edge_cost_elem_scat(SCat,
		edge{ source => node{ lemma => Source, cat => SCat::cat[v,aux] },
		      target => N::node{},
		      type => adj
		    },
%		Name::'+RESTR_TIME',
		    Name,
		    W,
		    _
	      ) :-
	chain( N >> (subst @ time_mod) >> node{ lemma => Target, cat => TCat }),
	recorded(opt(restrictions,DB)),
	check_restriction(Source,SCat,Target,TCat,modifieur,W1),
	W is W1*3,
	name_builder('+RESTR_TIME_~w',[Target],Name)
	.

%rule_no_cost('+RESTR_COORD').

edge_cost_elem_cats(Cat1,coo,
		edge{ source => node{ lemma => L1, cat => Cat1::cat[nc,adj,v] },
		      target => Coo::node{ cat => coo },
		      type => adj
		    },
		XName::'+RESTR_COORD',
%		    Name,
		W,
		_
	      ) :-
	chain( Coo >> (subst @ coord3) >> node{ lemma => L2, cat => Cat2::cat[nc,adj,v] } ),
%%	recorded(opt(restrictions,DB)),
	( L1=L2 ->
	  W = 100,
	  Name = '+RESTR_COORD'
	;
	  check_sim(L1,Cat1,L2,Cat2,W) ->
	      name_builder('+RESTR_COORD_~w_~w',[L1,L2],Name)
	;
	recorded(vector(L1,Cat1,Vec)),
	recorded(vector(L2,Cat2,Vec)),
	W =  2,
	name_builder('+RESTR_COORD_vec_~w',[Vec],Name)
	)
	.

edge_cost_elem_cats(prep,coo,
		edge{ source => P1::node{ cat => prep },
		      target => Coo::node{ cat => coo },
		      type => adj
		    },
		XName::'+RESTR_COORD_prep',
%		    Name,
		W,
		_
	      ) :-
	chain( P1 >> subst >> node{ lemma => L1, cat => Cat1::cat[nc,v] } ),
	chain( Coo >> (subst @ coord3) >> node{ cat => prep } >> subst >> node{ lemma => L2, cat => Cat2::cat[nc,v] } ),
%%	recorded(opt(restrictions,DB)),
	(L1=L2 ->
	 W = 100,
	 Name = '+RESTR_COORD_prep'
	;
	 check_sim(L1,Cat1,L2,Cat2,W),
	 name_builder('+RESTR_COORD_prep_~w_~w',[L1,L2],Name)
	)
	.

%% Favor 'en' for gerundives (participiale)
edge_cost_elem_type(lexical,
		edge{ source => N::node{},
		      target => N2::node{ lemma => Lemma, cat => prep},
		      type => lexical,
		      id=> _EId
		    },
		Name::'+en_as_GERUND',
		W,		% need to compensate penalty for empty source
		_
	      ) :-
	domain(Lemma,[en,'tout en']),
	rule_weight(Name,W,2000),
	%% CALL EDGE
	source2edge(
	      edge{ source => N,
		    target => node{ cat => v },
		    type => subst,
		    id =>EId
		  }
	     ),
	edge2top(EId,OId),
	verbose('Try gerundive ~w\n',[OId]),
	check_op_top_feature(OId,mode,gerundive),
	have_shared_derivs(_EId,EId),
	verbose('Found gerundive ~E\n',[N2]),
	true
	.

/*
edge_cost_elem( 
		edge{ label => 'SubS',
		      id => EId,
		      target => node{ cat => v },
		      source => N::node{ cat => 'S' },
		      type => subst
		    },
		Name::'+GERUNDIV_WITH_SUBJ',
		W,
                _
	      ) :-
	rule_weight(Name,W,1000),
	edge{ source => N,
	      label => incise,
	      type => adj
	    },
	edge2top(EId,OId),
	check_op_top_feature(OId,mode,Mode),
	domain(Mode,[gerundive,participle]),
	edge{ source => V::node{ cat => v},
	      target => N,
	      type => adj
	    },
	edge{ source => V,
	      label => subject
	    }
	.
*/


rule_no_cost('+RESTR_PP_N').

%% Use of restriction database: PP attachment on nouns
edge_cost_elem_lc(Label,SCat,prep,
		edge{ source => node{ lemma => Source, cat => SCat::cat[nc,adj] },
		      target => N::node{ cat => prep, lemma => Prep },
		      label => Label::label['N2',adjP,dep,mod],
		      type => adj,
		      id => EId
		    },
%%		Name::'+RESTR_PP_N',
		  Name,
		  W,
		 _) :-
	%%	rule_weight(Name,W,300),
	recorded(opt(restrictions,DB)),
	( source2edge(
		      edge{ source => N,
			    type => subst,
			    target => node{ cat => cat[nc,np,v,adj] }
			  }
	      ) xor fail
	),
	mutable(MW,0,true),
	mutable(WL,none,true),
	mutable(WCat,none,true),
	every((
		     source2edge(
			     edge{ source => N,
				   type => subst,
				   target => T::node{ lemma => _Target, cat => TCat::cat[nc,np,v,adj] }
				 }
			 ),
		     ( check_restriction(Source,SCat,_Target,TCat,Prep,_W1),
		       _W1 > 0
				 xor _W1 = 0
		     ),
		     ( chain( T >> adj >> node{ cat => coo }
			      >> (subst @ coord3 ) >> node{ lemma => Target3, cat => TCat3 }
			    ),
		       check_restriction(Source,SCat,Target3,TCat3,Prep,_W3), _W3 > 0 ->
			   _W2 is _W1 + _W3
		      ;
		      _W2 = _W1
		     ),
		     mutable_max(MW,_W2),
		     mutable(WL,_Target),
		     mutable(WCat,TCat)
		 )),
	mutable_read(MW,W1),
	W1 > 0,
	%%	format('Test rank for ~w source=~w target=~w\n',[EId,Source,Target]),
	edge_rank(EId,Rank,Dir),
	XW is 2*(W1-(Rank-1)*10),
	%%	format('Rank is ~w ~w\n',[Rank,Dir]),
	XW > 0,
	XW2 is XW / 2,
	(
	    W = XW2,
	    mutable_read(WL,Target),
	    name_builder('+RESTR_PP_N_~w_~w_~w',[Source,Prep,Target],Name)
	 ;
	 W = XW2,
	 name_builder('+RESTR_PP_N_~w_~w',[Source,Prep],Name)
	 ;
	 W = 2,
	 recorded(vector(Source,SCat,SVec)),
%	 recorded(vector(Prep,prep,PVec)),
%	 recorded(vector(Target,TCat,TVec)),
	 ( name_builder('+RESTR_PP_N_vec_~w_~w',[SVec,Prep],Name)
	  ;
	  mutable_read(WL,Target),
	  mutable_read(WCat,TCat),
	  recorded(vector(Target,TCat,TVec)),
	  name_builder('+RESTR_PP_N_vec_~w_~w_~w',[SVec,Prep,TVec],Name)
	 )
	),
	true
.

%% ? use constraints
edge_cost_elem_lc(Label,v,'VMod',
		edge{ source => node{ cat => v },
		      target => N::node{ cat => 'VMod'},
		      label => Label::label['vmod',mod],
		      type => adj
		    },
		Name::'+MOD_PP_V',
		W,
		_) :-
	rule_weight(Name,W,400),
	%% CALL EDGE
	source2edge(
	      edge{ source => N,
		    target => node{ cat => prep, lemma => Lemma },
		    type => subst,
		    label => 'PP'
		  }
	     ),
	domain(Lemma,[pendant,durant])
	.

%% Penalize PP attachments on adjP, except on attribute position
%% (18788)
edge_cost_elem_lc(Label,adj,prep,
		edge{ source => Adj::node{ cat => adj, cluster => C },
		      target => node{ cat => prep, cluster => CPrep },
		      type => adj,
		      label => Label::label['adjP',mod]
		    },
		Name::'-PP_ON_ADJP',
		W,
		_
	      ) :-
	rule_weight(Name,W,-400),
	%% CALL EDGE
	\+ target2edge(
		       edge{ target => Adj,
			     label => comp,
			     type => subst
			   }
		      ),
	\+ targetcluster2edge(
			      edge{ target => node{ cat => v,
						    cluster => C } }
			     ),
%	targetcluster2edge(
			   edge{ source => NC::node{ cat => cat[nc,v], cluster => C1 },
				 target => Prep::node{ cat => prep, cluster => CPrep }
			       },
%			  ),
	\+ chain( Adj >> adj >> node{ cat => adv, cluster => C1 } ),
%	format('penalize PP adj=~w nc=~w prep=~w\n',[Adj,NC,Prep]),
	true
	.

edge_cost_elem_lc(Label,adj,prep,
		edge{ source => Adj::node{ cat => adj, id => SNId },
		      target => node{ cat => prep, lemma => TLemma },
		      type => adj,
		      label => Label::label['adjP',mod]
		    },
		Name::'-PP_ON_COMP',
		W,
		_
	      ) :-
	rule_weight(Name,W,-200),
	%% CALL EDGE
	target2edge(
		     edge{ target => Adj,
			   label => comp,
			   type => subst
			 }
	    )
	.

edge_cost_elem_lc(Label,adj,prep,
		  E::edge{ id => EId,
			   source => Adj::node{ cat => adj, id => SNId },
			   target => node{ cat => prep, lemma => TLemma },
			   type => adj,
			   label => Label::label['adjP',mod],
			   deriv => DIds
			 },
		  Name::'+preparg_on_adj',
		  W,
		  _
	      ) :-
    rule_weight(Name,W,100),
%    format('test0 preparg_on_adj e=~w\n',[E]),
    domain(DId,DIds),
    deriv2htid(DId,HTId),
%    format('test preparg_on_adj htid=~w e=~w\n',[HTId,E]),
    check_xarg_feature(HTId,args[arg1],Fun::function[],_,_),
    (TLemma = de, Fun = objde xor TLemma = �, Fun = obj�)
	.

%% Penalize PP attachments on np and pro
%% but for demonstrative pronouns
edge_cost_elem_lc(Label,SCat,prep,
		edge{ source => node{ cat => SCat::cat[np,pro], lemma => Lemma },
		      target => node{ cat => prep },
		      type => adj,
		      label => Label::label['N2',dep]
		    },
		Name::'-PP_ON_np',
		W,
		_
	      ) :-
	rule_weight(Name,W,-400),
	\+ domain(Lemma,[celui])
	.

%% Penalize PP built upon an adj
edge_cost_elem_cats(prep,adj,
		edge{ source => node{ cat =>  prep },
		      target => node{ cat =>  adj }
		    },
		Name::'-PP_UPON_ADJ' ,
		W,
		_
	      ) :- rule_weight(Name,W,-500).


%% Penalize PP build upon entitie over dates
edge_cost_elem_label_tcat(Label,prep,
		edge{ source => N::node{ id => NId },
		      target => P::node{ cat => prep },
		      type => adj,
		      label => Label::label['N2',dep]
		    },
		Name::'-PPentity_UPON_date',
		W,
		_
	      ) :-
	rule_weight(Name,[W1,W2],[-300,-200]),
	chain( P >> subst >> T::node{ cat => TCat, lemma => TLemma, id => TId } ),
	node2op(NId,OId),
	check_op_top_feature(OId,time,true_time[]),
	((TCat = cat[v,adj] ; TLemma = entities[]) ->
	 W = W1
	; TCat=nc ->
	 node2op(TId,TOId),
	 ( check_op_top_feature(TOId,time,TTime) ->
	   TTime = (-)
	 ;
	   true
	 ),
	 W=W2
	;
	 W=W2
	)
	.

edge_cost_elem_tlemma(�,
		edge{ source => N::node{ id => NId },
		      target => P::node{ cat => prep, lemma => � },
		      type => adj,
		      label => label['N2',dep]
		    },
		Name::'+date_a_date',
		W,
		_
	      ) :-
	rule_weight(Name,W,200),
	node2op(NId,OId),
	check_op_top_feature(OId,time,true_time[]),
	chain( P >> subst >> node{ cat => nc, id => TId } ),
	node2op(TId,TOId),
	check_op_top_feature(TOId,time,true_time[])
	.

%% Strongly penalize nc/adj/... over det (for words such as un, le, la, une , ...)
edge_cost_elem_tcat(TCat,
		edge{ target => node{ cat => TCat::cat[nc,pro,cla,adj],
				      lemma => L,
				      cluster => C } },
		Name::'-N2_VS_DET',
		W,
		_
	      ) :-
	rule_weight(Name,W,-2000),
	%% CALL EDGE
	\+ domain(L,[tout]),
	targetcluster2edge(
			   edge{ target => node{ cat => cat[det], cluster => C } }
			  )
	.

%% Strongly penalize adv/... over prep (for words such as avec, ...)
edge_cost_elem_tcat(adv,
		edge{ target => node{ cat => cat[adv], cluster => C } },
		Name::'-ADV_VS_PREP',
		W,
		_
	      ) :-
	rule_weight(Name,W,-3000),
	%% CALL EDGE
	targetcluster2edge( edge{ target => node{ cat => cat[prep], cluster => C }} )
	.

%% Strongly penalize adv/... over csu (for words such as si, ...)
edge_cost_elem_tcat(adv,
		edge{ target => node{ cat => cat[adv], cluster => C } },
		Name::'-ADV_VS_CSU',
		W,
		_
	      ) :-
	rule_weight(Name,W,-500),
	%% CALL EDGE
	targetcluster2edge( edge{ target => node{ cat => cat[csu], cluster => C }} )
	.


%% Favor comme-csu on N2 and Adj
edge_cost_elem_tcat(csu,
		edge{ source => node{ cat => xnominal[] },
		      target => node{ cat => csu },
		      type => adj
		    },
		Name::'+comme_csu',
		W,
		_) :-
	rule_weight(Name,W,500)
	.

%% Favor csu attached on tensed verbs
edge_cost_elem_tcat(csu,
		edge{ source => node{ id => NId, cat => cat[v,adj] },
		      target => node{ cat => csu },
		      type => adj
		    },
		Name::'csu_on_tense_verb',
		W,
		_) :-
	rule_weight(Name,W,100),
	\+ ( check_node_top_feature(NId,mode,Mode),
	     domain(Mode,[infinitive,gerundive,participle])
	   )
	.

%% Penalize que as csu
edge_cost_elem_tcat(csu,
		    edge{ source => node{ id => NId, cat => cat[v,adj] },
			  target => node{ cat => csu, lemma => que },
			  type => adj
			},
		    Name::'-que_as_csu',
		    W,
		   _) :-
    rule_weight(Name,W,-1000)
	.

%% Favor adv on v after rather than on preceding aux,v when in the middle (but for negation)
edge_cost_elem_cats(v,adv,
		edge{ source => V::node{ cat => v,
				      cluster => cluster{ right => R1 }
				    },
		      target => node{ cat => adv,
				      cluster => cluster{ left => L, right => R }
				    }
		    },
		Name::'+ADV_ON_PREC_V',
		W,
		_) :-
	rule_weight(Name,W,200),
	R =< R1,
	chain( V >> (adj @ label['Infl',aux]) >> node{ cat => aux,
					    cluster => cluster{ right => R_Aux}
					  }
	     ),
	R_Aux =< L
	.


%% But favor advneg over adv (and also over comp) when possible
%% if a negation is possible
edge_cost_elem_tcat(advneg,
		edge{ source => V::node{ id => NId, cat => cat[v,aux], cluster => cluster{ right => V_Right } },
		      target => node{ cat => advneg, lemma => Lemma, cluster => cluster{ left => Adv_Left} }
		    },
		Name::'+AdvNeg',
		W,
		_
	      ) :-
	rule_weight(Name,[W1,W2],[2100,2000]),
	node2op(NId,OId),
	(   chain( V >> lexical >> node{ cat => clneg }) ->
	    W = W1
	;   check_op_top_feature(OId,neg,+),
	    W = W2
	;   V_Right =< Adv_Left, domain(Lemma,[pas,point]) ->
	    W = W1
	)
	.

%% favor ending URL as references
edge_cost_elem(
    edge{ type => subst,
	  label => reference,
	  target => node{ cat => np, lemma => '_URL' }
	},
    Name::'+URL_as_reference',
    W,
    _
) :-
    rule_weight(Name,W,900)
	.


:-std_prolog has_post_cl_subject/1.

has_post_cl_subject(N::node{ cat => cat[v,aux]}) :-
	( source2edge(
		      edge{ source => N,
			    target => Modal::node{ cat => cat[v] },
			    type => adj,
			    label => label['V',modal]
			  }
		     )->
	  ( source2edge(
			edge{ source => Modal,
			      target => node{ cat => cln },
			      label => subject
			    }
		       )
	  xor has_post_cl_subject(Modal)
	  )
	; source2edge(
		      edge{ source => N,
			    target => Aux::node{ cat => cat[v,aux] },
			    type => adj,
			    label => label['Infl',aux]
			  }
		     ) ->
	  ( source2edge(
			edge{ source => Aux,
			      target => node{ cat => cln },
			      label => subject
			    }
		       )
	  xor has_post_cl_subject(Aux)
	  )
	;
	  fail
	)
	.

%% Slightly penalize subject when post-clitic subject
edge_cost_elem_label_scat(subject,SCat,
		edge{ label => subject,
		      source => V::node{ cat => SCat::cat[v,aux] }
		    },
		Name::'-double_subj',
		W,
		_
	      ) :-
	rule_weight(Name,W,-150),
	has_post_cl_subject(V)
	.

%% Penalize inverted subjects (especially for robust mode)
%% when there exists some way to have a canonical subj
edge_cost_elem_label(subject,
		E::edge{ label => subject,
		      source => V::node{ cat => cat[~ [adj,aux]],
					 cluster => VCluster::cluster{ right => R } },
		      target => Subj::node{ cat => cat[~ cln],
					    cluster => cluster{ left => L } }
		    },
		Name::'-INVERTED_SUBJ',
		W,
		_
	      ) :-
	rule_weight(Name,W,10),
	\+ chain( V >> (lexical @ impsubj) >> node{}),
	once(( chain( node{ cluster=> VCluster} >> subject >> node{ cluster => cluster{ left => _L } } ),
	       _L =< R
	     ;
	        chain( Subj << (subst @ object) << node{ cluster => VCluster })
	     )),
	R =< L
	.

%% Favor inverted_subj for adj and for impersonal constructions
edge_cost_elem_label_type(subject,subst,
		edge{ label => subject,
		      source => Adj::node{ cat => adj },
		      target => node{ cat => cat[~cln] },
		      type => subst
		    },
		Name::'-INVSUBJ_ON_ADJ',
		W,
		_
	      ) :-
	rule_weight(Name,W,1500),
	chain( Adj >> (lexical @ impsubj) >> node{ cat => cat[ilimp,caimp] }),
	true
	.

%% But compensate inversion when topic xcomp
%% furthemore, the xcomp head and the main verb may be far away
edge_cost_elem_label_type(xcomp,subst,
		Edge::edge{ label => xcomp,
			    type => subst,
			    source => V::node{ lemma => Lemma, cluster => cluster{ left => L } },
			    target => Topic::node{ cat => Topic_Cat, cluster => cluster{ right => R }}
			  },
		Name::'+XCOMP_TOPIC',
		W,
		_
	      ) :-
	\+ domain(Lemma,[faire,estre]), %some verb have xcomp function but are not citation verbs 
	rule_weight(Name,W1,+2000),
	R =< L,
	%% CALL EDGE
	\+ ( chain( V >> adj
		  >> node{ cat => incise } >> (lexical @ void)
		  >> node{ lemma => ',',
			   cluster => cluster{ left => L_Incise }
			 }
		  ),
	     L_Incise < R
	   ),
%%	edge_cost_elem('-LONG',Edge,W2),
%%	W is  W1 - W2,
	%% favor real topic sentence rather than short sentences
	( Topic_Cat = cat[v] ->
	    W2 = 200
	; Topic_Cat = cat[adj],
	    chain(Topic >> (subst @ subject) >> node{}) ->
	    W2 = 200
	; W2 = 0
	),
	W is W1+W2
	.

%% But compensate inversion when causative
edge_cost_elem_label_type(subject,subst,
		edge{ label => subject,
		      type => subst,
		      source => V::node{ cat => v, lemma => Lemma },
		      id => EId1
		    },
		Name::'+CAUSATIVE',
		W,
		_
	      ) :-
	\+ Lemma = faire,
	source2edge(
	    Edge::edge{ label => causative_prep,
			type => lexical,
			source => V,
			target => node{ cat => prep, cluster => C },
			id => EId2
		      }
	),
	\+ source2edge(edge{ source => V, target => node{ cluster => C }, label => preparg, type => subst }),
	%% check that V has a faire aux (seems to be wrong in some case !!)
	rule_weight(Name,[W1,W2],[+1100,-2000]),
	 ( ( chain( V << subst @ xcomp << node{ lemma => faire })
	    ; chain( V >> adj @ label['S','mod.xcomp'] >> node{ lemma => faire } )
	   ),
	   have_shared_derivs(EId1,EId2) ->
	       W = W1
	  ;
	  W = W2
	 )
	.


%% Favor existence of an object if there is a causative_prep
edge_cost_elem_label(object,
		edge{ label => object,
		      source => V,
		      id => EId1
		    },
		Name::'+CAUSATIVE_OBJ',
		W,
		_
	      ) :-
	%% CALL EDGE
	source2edge(
	      edge{ label => causative_prep,
		    type => lexical,
		    source => V::node{ cat => v,
				       lemma => Lemma },
		    target => node{ cat => prep, cluster => C },
		    id => EId2
		  }
	     ),
	\+ Lemma = faire,
	rule_weight(Name,W,+4000),
	\+ source2edge(edge{ source => V, target => node{ cluster => C }, label => preparg, type => subst }),
	%% check that V has a faire aux (seems to be wrong in some case !!)
	( chain( V << subst @ xcomp << node{ lemma => faire })
	; chain( V >> adj @ label['S','mod.xcomp'] >> node{ lemma => faire } )
	),
	have_shared_derivs(EId1,EId2)
	.


%% Pb in FRMG: a causative may arise, even if 'faire' is not present
%% until the pb is fixed, we use a very strong penalty !
edge_cost_elem_label_type(causative_prep,lexical,
		edge{ label => causative_prep,
		      type => lexical,
		      source => V::node{ cat => v }
		    },
		Name::'-wrong_causative',
		W,
		_
	      ) :-
	rule_weight(Name,W,-10000),
	chain( V << subst @ xcomp << node{ lemma => Lemma } ),
	Lemma \== faire
	.

%% penalize non-saturated objects (possible with il y a)
edge_cost_elem_label_tcat(object,TCat,
		edge{ label => object,
		      target => N::node{ cat => TCat::cat[nc,adj] },
		      type => subst
		    },
		Name::'-unsat_obj',
		W,
		_
	      ) :-
	rule_weight(Name,W,-500),
	\+ chain( N >> ( subst @ det) >> node{ cat => det } )
	.

%/*
%% Penalties on object in presence of a clitic object or genitive,
%% specially if introduced by de
%% not sur it is useful !
edge_cost_elem_label_tcat(object,TCat,
		edge{ label => object,
		      source => V::node{},
		      target => Obj::node{ cat => TCat::cat[nc,np] },
		      type => subst
		    },
		Name::'-obj_plus_clitic',
		W,
		_
	      ) :-
	rule_weight(Name,W1,-200),
	chain( V >> (lexical @ label[object,clg]) >> node{} ),
	( chain( Obj >> (subst @ det) >> node{ cat => det, form => de_form[] } ) ->
	  W2 = -500
	;
	  W2 = 0
	),
	W is W1 + W2
	.
%*/

/*
%% Don't know exactly the role of next two rules !
%% Strongly penalize que? in N2
edge_cost_elem( 
		edge{ target => node{ lemma => 'que?', xcat => 'N2' } },
		Name::'-que?inN2',
		W,
                _
	      ) :- rule_weight(Name,W,-10000).

%% Penalize wh in N2 when in concurrence whith non subject wh-pronoun
edge_cost_elem( 
		edge{ label=> subject,
		      target => node{ cat => pri, xcat => 'N2' } },
		Name::'-whinN2Subj',
		W,
                _
	      ) :- rule_weight(Name,W,-2000).
*/

%% favor adjective reading for attributes over noun reading
edge_cost_elem_lc(comp,v,adj,
		edge{ label => comp,
		      source => node{ cat => v },
		      target => node{ cat => adj },
		      type => subst
		    },
		Name::'+adjAsComp',
		W,
		_
	      ) :- rule_weight(Name,W,400).

%% favor Adj on Nouns rather than comp
edge_cost_elem_label_type(comp,subst,
		edge{ target => node{ cat => cat[comp,adj],
				      cluster => C2
				    },
		      source => V::node{},
		      label => comp,
		      type => subst
		    },
		Name::'-adjAsComp1',
		W,
		_
	      ) :-
	rule_weight(Name,W,-1400),
	%% CALL EDGE
	targetcluster2edge(
			   edge{ source => N::node{ cat => nominal[~ [np]] },
				 target => node{ cat => adj, cluster => C2},
				 type => adj,
				 label => label['N2',mod]
			       }
			  ),
	\+ chain( N >> adj >> node{} >> subst @ 'SubS' >> V ),
	true
	.

%% favor Adj on Nouns rather than comp
edge_cost_elem_label_type(comp,subst,
		edge{ target => node{ cat => cat[comp,adj],
				      cluster => C1
				    },
		      label => comp,
		      type => subst
		    },
		Name::'-adjAsComp2',
		W,
		_
	      ) :-
	rule_weight(Name,W,-1400),
	%% CALL EDGE
%	sourcecluster2edge(
			   edge{ source => node{ cat => nominal[],
						 cluster => C1
					       },
				 target => node{ cat => adj,
						 cluster => C2},
				 type => adj,
				 label => label['N',mod]
			       },
%			  ),
	true
	.

%% slightly favor post adjectives
%% but strongly favor if superlative (to avoid confusion with a separate noun
edge_cost_elem_lc(Label,SCat,adj,
		edge{ source => node{ cat =>SCat::nominal[nc,np] },
		      target => Adj::node{ cat => adj },
		      type => adj,
		      label => Label::label['N2',mod]
		    },
		Name::'+post_adj',
		W,
		_
	      ) :-
	rule_weight(Name,[W1,W2],[100,500]),
	( chain( Adj >> (adj @ label[adj,mod]) >> node{ cat => adv } >> (subst @ det) >> node{ cat => det }) ->
	  W = W2
	;
	  W = W1
	)
	.

rule_no_cost('+CatPref').

%% Use preferences
edge_cost_elem( %none, 
		E::edge{ target => node{ form => F,
					 cat => _C,
					 lemma => _L
				       }},
		Name::'+CatPref',
		W,
		_
    ) :-
    F \== '',
    verbose('Test catpref F=~w C=~w L=~w\n',[F,_C,_L]),
    %% avoid catpref on closed cats and function words
    %%	\+ domain(_C,[det,prep,csu,coo]),
    ( _C = cat[ilimp,caimp] ->
	  C = cln,
	  L = cln
     ;   _C = C,
	 _L = L
    ),
    check_catpref(F,L,C,W)
.

%% Favor quote constructions
%% should give 1000, because used twice, one for each quote
%% Quote is inversely proportional to the length of the quoted part
%% but should not include subquotes of same kind
edge_cost_elem_label(void,
		edge{ source => N1::node{ tree => Tree },
		      target => node{ cluster => C1::cluster{ left => Left }},
		      type => lexical,
		      label => void
		    },
		Name::'+Quoted',
		W,
		_
	      ) :-
%	fail,
	rule_weight(Name,W,900),
%	rule_weight(Name,W,1500),
	domain(K,quoted[]),
	domain(K,Tree)
	.

%% Penalize spelling correction done by SxPipe
edge_cost_elem( %none,
	edge{ target => N::node{ form => Form,
				 lemma => Lemma,
				 cat => Cat,
				 cluster => cluster{ left => Left, token => Token, right => Right }
			       }
		    },
		Name::'-Spelling',
		W,
		_
    ) :-
        'C'(Left,lemma{ cat => Cat, lex => Form, lemma => Lemma, truelex => TLex },Right),
        \+ recorded(no_spelling_penalty(TLex,Form)),
	\+ (Form = entities[]),
	\+ (Form = date[]),
	\+ part_of_agglutinate(Form,Token),
	\+ domain(Form,['l''un','l''une']),
	\+ ( Form == 'que', domain(Token,['qu''','Qu''']) ),
	Form  \== '_EPSILON',
%%	Form \== Token,
        \+ '$interface'(lowercase(Form:string),[return(Token:string)]),
%	format('Spelling form=~w token=~w node=~w\n',[Form,Token,N]),
	rule_weight(Name,W,-500)
	.
							 
%% Favour _PERSON , _ORGANIZATION in subject or object position
edge_cost_elem_tlemma(Lemma,
		edge{ target => node{ lemma => Lemma::entities['_PERSON',
							       '_PERSON_m',
							       '_PERSON_f',
							       '_ORGANIZATION',
							       '_COMPANY'
							      ]
				    },
		      type => subst,
		      label => label[subject]
		    },
		Name::'+np_as_subj',
		W,
		_
	      ) :- rule_weight(Name,W1,20),
	( Lemma = entities['_ORGANIZATION','_COMPANY'] ->
	  W is W1 + 5
	;
	  W = W1
	).

edge_cost_elem_tlemma(Lemma,
		edge{ target => node{ lemma => Lemma::entities['_PERSON',
							       '_PERSON_m',
							       '_PERSON_f',
							       '_ORGANIZATION',
							       '_PRODUCT',
							       '_LOCATION',
							       '_COMPANY'
							      ]
				    },
		      type => subst,
		      label => label[object]
		    },
		Name::'+np_as_obj',
		W,
		_
	      ) :- rule_weight(Name,W1,20),
	( Lemma = entities['_ORGANIZATION','_COMPANY','_PRODUCT'] ->
	  W is W1 + 5
	;
	  W = W1
	).



edge_cost_elem_scat(prep, 
		E::edge{ target => node{ form => Entity },
			 type => subst,
			 source => node{ cat => prep,
					 lemma => Prep
				       },
			 label => 'N2'
		       },
		Name::'+location_in_prep',
		W,
		_
	      ) :-
%%	format('edge ~q\n',[E]),
	rule_weight(Name,W,100),
	prep_entity(Prep,Entity),
	true
	.

%% favorize prep attachement given a preference
edge_cost_elem_tcat(prep,
		edge{ target => node{ lemma => Prep, cat => prep },
		      source => node{ lemma => Governor,
				      cat => Cat::cat[]
				    },
		      type => adj
		    },
		Name::'+prep_pref',
		W,
		_
	      ) :-
	prep_pref(Governor,Cat,Prep,W).

%% favorize prep attachement given a preference
edge_cost_elem_tcat(prep,
		edge{ target => N::node{ lemma => Prep, cat => prep },
		      source => node{ lemma => Governor,
				      cat => Cat::cat[]
				    },
		      type => adj
		    },
		Name::'+prep_pref_full',
		W,
		_
	      ) :-
	%% CALL EDGE
	source2edge(
	      edge{ source => N,
		    target => node{ lemma => Governee },
		    type => subst
		  }
	     ),
	rpref(Governor,Prep,Governee,W).

%% penalize attachement on non-head on "compond form" with rpref
edge_cost_elem_type(adj,
		edge{ source => Mod1,
		      target => Mod,
		      type => adj
		    },
		Name::'+prep_pref_head',
		W,
		_
	      ) :-
	chain( Mod::node{} << adj
	     << node{ lemma => Governor } >> edge_kind[adj,subst]
	     >> node{ cat => prep, lemma => Prep } >> subst
	     >> Mod1::node{ lemma => Governee }
	     ),
	rpref(Governor,Prep,Governee,W1),
	W is - W1
	.


%% Penalize short sentences
edge_cost_elem_label(start,
		E::edge{ target => node{ cat => start },
			 source => node{ cat => 'S' },
			 label => start,
			 type => subst
		       },
		Name::'-start',
		W,
		_
	      ) :-
	rule_weight(Name,W,-1000)
	.

%% but still prefer short nominal sentences over other kinds
%% specially if there is some PP attachement
edge_cost_elem_lc('N2',comp,TCat,
	       edge{ target => N2::node{ cat => TCat::cat[nc,np] },
		     source => N1::node{ cat => comp },
		     label => 'N2',
		     type => subst
		   },
	       Name::'+short_N2',
	       W,
	       _
	      ) :-
	chain( N1 << ( subst @ comp )
	     << node{ cat => 'S' } >> ( subst @ start )
	     >> node{ cat => start }
	     ),
	rule_weight(Name,W1,100),
	(   source2edge(
			edge{ source => N2,
			      target => node{ cat => prep },
			      type => adj
			    }
		       ) ->
	    W is 2*W1
	;   
	    W = W1
	)
	.

%% penalize vmod on start sentences
edge_cost_elem_label_type(Label,adj,
		edge{ source => S::node{ cat => 'S' },
		      label => Label::label[vmod,'S',mod],
		      type => adj
		    },
		Name::'-vmod_on_short_sentence',
		W,
		_
	      ) :-
	rule_weight(Name,W,-600),
	chain( S >> subst>> node{ cat => start } ),
	true
	.

%% Penalize incise after que without coma
edge_cost_elem_slemma(SLemma,
		edge{ source => node{ lemma => SLemma::quepro[] },
		      target => node{ cat => 'N2' },
		      label => 'N2',
		      type => adj
		     },
	       Name::'-incise_after_que',
	       W,
	       _
	      ) :-
	rule_weight(Name,W,-1000)
	.

%% Penalize wh-comp args starting with que
edge_cost_elem_tlemma(TLemma,
		edge{ source => V::node{ cat => v,
					 cluster => cluster{ left => L }
				       },
		      target => node{ cat => pri,
				      lemma => TLemma::quepro[],
				      cluster => cluster{ right => R }
				    },
		      type => subst,
		      label => label[object,comp]
		    },
		Name::'-que_whcomp',
		W,
		_
	      ) :-
	rule_weight(Name,W,-2000),
	R =< L,
	target2edge(
	    edge{ target => V,
		  label => xcomp,
		  type => subst
		}
	)
	.

%% Penalize 'que' as whpro in robust partial parses
edge_cost_elem_tlemma(TLemma,
		edge{ source => node{ cat => comp },
		      target => node{ lemma => TLemma::quepro,
				      cluster => cluster{ left => L }},
		      type => subst,
		      label => 'N2'
		    },
		Name::'-que_whpro_robust',
		W,
		_
	      ) :-
	L > 0,
	rule_weight(Name,W,-3000)
	.

%% favor que as que_restr in negative sentences
edge_cost_elem_lc(advneg,v,que_restr,
		edge{ source => V::node{ cat => cat[v] },
		      target => node{ cat => que_restr },
		      label => advneg,
		      type => lexical
		    },
		Name::'+que_restr',
		W,
		_
	      ) :-
	rule_weight(Name,W,2200),
	chain( V >> (lexical @ clneg) >> node{ cat => clneg } ),
	\+ chain( V >> _ >> node{ cat => advneg } )
	.

%% Strongly penalize pronoun in enum
edge_cost_elem_lc(coord,'N2',pro,
		edge{ source => N2::node{ cat => 'N2' },
		      target => node{ cat => pro },
		      type => subst,
		      label => coord
		    },
		Name::'-pro_in_enum',
		W,
		_
	      ) :-
	target2edge(
		    edge{ source => node{ cat => Cat },
			  target => N2,
			  type => adj,
			  label => label['N2',coord]
			}
		   ),
	Cat \== pro,
	rule_weight(Name,W,-3000).

%% Favour superlative construction with possible
edge_cost_elem_tlemma(possible,
		edge{ type => lexical,
		      label => 'Modifier',
		      source => node{ cat => supermod },
		      target => node{ lemma => possible, cat => adj }
		    },
		Name::'+superlative_possible',
		W,
		_
	      ) :-
	rule_weight(Name,W,2000).

%% Favor constructions with  supermod
edge_cost_elem_label_type(supermod,adj,
		edge{ label => supermod,
		      type => adj
		      },
		Name::'+superlative',
		W,
		_
	      ) :-
	rule_weight(Name,W,2000).


%% Favor constructions with  supermod
edge_cost_elem_label_type(quantity,adj,
		edge{ label => quantity,
		      type => adj,
		      target => N::node{ cat => nominal[] }
		      },
		Name::'+quantity_on_superlative',
		W,
		_
	      ) :-
	rule_weight(Name,W,2000),
	chain( N >> (subst @ det) >> node{ cat => det, lemma => Lemma } ),
	domain(Lemma,['_NUMBER',un,une])
	.

%% Favor antepos adj when specified
edge_cost_elem_lc(Label,SCat,TCat,
		edge{ label => Label::label['N',ncpred,mod],
		      source => N::node{ cat => SCat::cat[nc,np,ncpred],
					 cluster => cluster{ left => L }
				       },
		      target => Adj::node{ cat => TCat::adj,
					   lemma => Lemma,
					   cluster => cluster{ right => R }
					 },
		      type => adj
		    },
		Name::'+ante_adj',
		W,
		_
	      ) :-
	R =< L,
	ante_adj_pref(Lemma,W)
	.


%% Penalize postpos adj when antepos is specified
%% except if the adj is modified
edge_cost_elem_lc(Label,SCat,TCat,
		edge{ label => Label::label['N2',mod],
		      source => N::node{ cat => SCat::cat[nc,np],
					 cluster => cluster{ right => R }
				       },
		      target => Adj::node{ cat => TCat::adj,
					   lemma => Lemma,
					   cluster => cluster{ left => L }
					 },
		      type => adj
		    },
		Name::'-ante_adj_used_as_post',
		W,
		_
	      ) :-
	R =< L,
	ante_adj_pref(Lemma,W1),
	\+ source2edge( edge{ source => Adj, type => adj}),
	W is -W1
	.

%% Penalize gender_alternative constructions
edge_cost_elem_type(adj,
		edge{ type => adj,
		      target => node{ tree => Tree }
		    },
		Name::'-gender_alternative',
		W,
		_
	      ) :-
	rule_weight(Name,W,-1000),
	domain(K,[gender_alternative_pri,
		  gender_alternative_pro,
		  gender_alternative_cln
		 ]),
	domain(K,Tree)
	.


%% Favour date number on year (should also try to cover things like "les ann�es Mitterand")
edge_cost_elem_tlemma('_NUMBER',
		      edge{ type => adj,
			    label => 'N',
			    source => node{ lemma => SLemma },
			    target => node{ lemma => '_NUMBER', cluster => cluster{ token => TForm } }
			  },
		      Name::'+number_on_year',
		      W,
		      _
		     ) :- rule_weight(Name,W,260),
			  %% check either slemma or format of the target
			  %% we have things like �t� 2013 but also 'budget 2013'
			  ( temporal_period(SLemma) xor is_year(TForm) ),
			  %%		  format('temporal form ~w ~w\n',[SLemma,TForm]),
			  true
	.

:-extensional temporal_period/1.

temporal_period(ann�e).
temporal_period(an).
temporal_period(printemps).
temporal_period(�t�).
temporal_period(automne).
temporal_period(hiver).
temporal_period(trimestre).
temporal_period(semestre).
temporal_period(d�cennie).


%% Favour modifier factorization when coordinnation
%% but avoid too much climbing to find a coordination
edge_cost_elem_cats(SCat,TCat,
		edge{ source => N1::node{ cat => SCat::cat[nc,adj] },
		      target => N2::node{ cat => TCat::cat[prep,adj],
					  cluster => cluster{ left => N2_Left }},
		      type => adj
		    },
		Name::'+mod_fact_coord',
		W,
		_
	      ) :-
	source2edge(
		    edge{ source => N1,
			  target => COO::node{ cat => coo,
					       lemma => COO_Lemma,
					       cluster => cluster{ right => COO_Right,
								   left => COO_Left
								 }
					     },
			  type => adj
			}
		   ),
	domain(COO_Lemma,[et,ou]),
	source2edge(
		    edge{ source => COO,
			  target => Last::node{ cluster => cluster{ right => Last_Right
								  }
					      },
			  label => coord3
			}
		   ),
	COO_Right < N2_Left,
	\+ ( source2edge(
			 edge{ source => N1,
			       target => N3::node{ cat => cat[prep,adj],
						   cluster => cluster{ right => N3_Right }
						 },
			       type => adj
			     }
			),
	     N3_Right =< COO_Left
	   ),
	\+ ( source2edge(
			 edge{ source => Last,
			       target => LastMod::node{ cat => cat[prep,adj],
							cluster => cluster{ left => LastMod_Left,
									    right => LastMod_Right
									  }
						      },
			       type => adj
			     }
			),
	     Last_Right =< LastMod_Left,
	     LastMod_Right =< N2_Left
	   ),
	rule_weight(Name,W,50)
	.
		      
edge_cost_elem_cats(SCat,TCat,
		edge{ source => node{ cat => SCat::cat[prep,csu],
				      lemma => SLemma,
				      cluster => cluster{ left => Left } },
		     target => N::node{ cat => TCat::nominal[],
					cluster => cluster{ right => Right } },
		     type => adj
		   },
	       Name::'-quantity_as_prep_mod',
	       W,
	       _
	      ) :-
	rule_weight(Name,W,-3000),
	chain( N >> (subst @ det) >> node{ cat => det, lemma => Lemma } ),
	\+ ( domain(Lemma,['_NUMBER',un,une]),
	       domain(SLemma,[avant,apr�s,devant,derri�re,'au-del�','au-dessus','au-dessous','au-devant',
			      dessus,dessous,
			      'avant que','apr�s que'
			     ])
	   )
	.

:-extensional prep_entity/2.

prep_entity(avec,entities['_PERSON','_PERSON_m','_PERSON_f','_ORGANIZATION','_COMPANY','_PRODUCT']).
prep_entity(dans,entities['_LOCATION','_ORGANIZATION','_COMPANY']).
prep_entity(contre,entities['_PERSON','_PERSON_m','_PERSON_f','_ORGANIZATION','_COMPANY']).
prep_entity(vers,entities['_PERSON','_PERSON_m','_PERSON_f','_LOCATION']).
prep_entity(pour,entities['_PERSON','_PERSON_m','_PERSON_f','_ORGANIZATION','_COMPANY']).
prep_entity(dans,entities['_LOCATION','_ORGANIZATION','_COMPANY']).
prep_entity('jusqu''�',entities['_LOCATION']).
prep_entity(chez,entities['_PERSON','_PERSON_m','_PERSON_f','_ORGANIZATION','_COMPANY']).
prep_entity(contre,entities['_PERSON','_PERSON_m','_PERSON_f','_ORGANIZATION','_COMPANY']).
prep_entity(comme,entities['_PERSON','_PERSON_m','_PERSON_f','_ORGANIZATION','_COMPANY']).

/*
%% Favor dependencies whose source and targets are in the same EASy F
edge_cost_elem( 
		edge{ source => node{ cluster => cluster{ lex => Lex }},
		      target => node{ cluster => cluster{ lex => Lex }}
		    },
		'+Dep_In_Same_F',
		1000,
                _
	      ) :-
	label2lex(Lex,_,[_|_])
	.
*/


edge_cost_elem_label_tcat(object,prep,
	       edge{ type => lexical,
		     label => object,
		     target => node{ cat => prep, lemma => � }
		   },
	       Name::'-�_as_obj',
	       W,
	       _
	      ) :-
	rule_weight(Name,W,-2000)
	.

%% penalize partitives on numbers
edge_cost_elem_lc(prep,SCat,prep,
	       edge{ label => prep,
		     type => lexical,
		     source => node{ cat => SCat::cat[pro,number],
				     lemma => '_NUMBER'
				   },
		     target => node{ cat => prep, lemma => de }
		   },
	       Name::'-partitive_on_number',
	       W,
	       _
	      ) :- rule_weight(Name,W,-3000)
	.

%% penalize extra ponctuation
edge_cost_elem_label(Label::label['ExtraWPunct','ExtraSPunct'],
		     edge{ label => Label, target => node{ cat => cat[poncts,ponctw] } },
		     Name::'-extra_punct',
		     W,
		     _
		    ) :- rule_weight(Name,W,-400).

%% penalize post adv on adj
edge_cost_elem_cats(SCat,adv,
	       edge{ type => adj,
		     source => node{ cat => SCat::cat[nc,adj], cluster => cluster{ right => Right }},
		     target => node{ cat => adv, cluster => cluster{ left => Left }}
		   },
	       Name::'-post_adv_on_adj',
	       W,
	       _
	      ) :-
	Right =< Left,
	rule_weight(Name,W,-100)
	.
	      
%% penalize vmod adj on comparative adv without supermod adj
%% should be blocked by FRMG
edge_cost_elem_label_scat(Label,adv,
		edge{ type => adj,
		      label => Label::label[vmod,mod],
		      source => Adv::node{ cat => adv }
		    },
		Name::'-bad_adv_vmod',
		W,
		_
	      ) :-
	rule_weight(Name,W,-5000),
	\+ chain( Adv >> adv @ supermod >> node{} ) 
	.

%Favor deep modifier extractions in xcomp args
edge_cost_elem_lc( Label,v,v,
		   edge{ type => adj,
			 source => V::node{ cat => v, cluster => C::cluster{} },
			 target => V1::node{ cat => v, cluster => C1::cluster{} },
			 label => Label::label['S',mod]
		      },
		   Name::'+mod_extraction_in_xcomp',
		   W,
		   _
		 ) :-
	rule_weight(Name,W,1000),
	chain( V1alt::node{ cat => v, cluster => C1 } >> subst @ xcomp >> node{ cat => v, cluster => C }),
	chain( V1alt >> adj >> Mod::node{} << adj << V )
	.

edge_cost_elem_label_scat(starter,'S',
			  edge{ type => subst,
				source => node{ tree => Tree }
			      },
			  Name:: '-unsatN2_starter',
			  W,
			  _
			 ) :-
	rule_weight(Name,W,-2000),
	domain('unsatN2_starter',Tree)
	.

%% defined in conll.pl (could also be a feature, or renamed)
:-std_prolog conll_is_adj/2.

edge_cost_elem(E::edge{ target => T::node{ cluster => cluster{ lex => TLex } },
			source => S::node{ cluster => cluster{ lex => SLex} }
		      },
	       Name,
	       W,
	       _
	      ) :-
    \+ node!empty(T),
    (TLex = [_|_] ->
	 domain(_TLex,TLex)
     ;
     _TLex = TLex
    ),
    'DEP'(_TLex,DepRel,DepDelta),
    \+ DepDelta == 0,
    _SLex is _TLex + DepDelta,
    (\+ node!empty(S) ->
	 (     (_SLex = SLex ; domain(_SLex,SLex)) ->
		   Name = '+depguide',
		   rule_weight(Name,W,100)
	  ; SLex is _SLex+1 ->
		Name = '+depguideP1',
		rule_weight(Name,W,5)
	  ; SLex is _SLex-1,
	    Name = '-depguideM1',
	    rule_weight(Name,W,5)
	 )
     ; conll_is_adj(E,A::node{ cluster => cluster{ lex => SSLex }}),
       (SSLex = _SLex xor domain(_SLex,SSLex)) ->
	   Name = '+depguideS',
	   rule_weight(Name,W,100),
	   true
    )
	.

%Penalize CleftQue when xcomp arg is possible
% example: c'est cette pomme que Paul veut que Pierre mange.
% we may need to compensate for an object in (que Paul veut)
% edge_cost_elem_label(Label,
% 		     edge{ type => lexical,
% 			   source => V::node{ cat => cat[v,aux,adj] },
% 			   target => node{ cat => prel, cluster => C::cluster{}},
% 			   label => Label::'CleftQue'
% 			 },
% 		     Name::'-cleft_vs_xcomp',
% 		     W,
%                    _
% 		    ) :-
% 	rule_weight(Name,W,-1700), 
% 	chain( V >> adj @ 'S' >> node{ lemma => XLemma, cat => cat[v,adj]} >> lexical @ csu >> node{ cat => que, cluster => C } ),
% 	\+ XLemma = estre
% 	.

%% penalize que_prel vs que_csu
edge_cost_elem(
	       edge{ type => lexical,
		     source => V::node{ cat => cat[v,aux,adj], cluster => C::cluster{} },
		     target => node{ cat => prel },
		     label => object
		   },
	       Name::'-ante_que_prel_vs_post_que_csu',
	       W,
	       _
	      ) :-
	rule_weight(Name,W,-1700),
	chain( node{ cluster => C } >> lexical @ csu >> node{ cat => que } )
	.


%% penalize use of a reserved lexeme
edge_cost_elem(
	edge{ target => node{ cat => Cat, cluster => cluster{ left => Left, right => Right } } },
	Name::'-reserved_lexeme',
	W,
	_
    ) :-
    (recorded( reserved('C'(Left,lemma{ cat => Cat },Right)) ) xor fail),
    rule_weight(Name,W,-500)
	.

%% penalize use of a non predicted supertag
edge_cost_elem(
	edge{ target => node{ cat => Cat, tree => Tree, cluster => cluster{ lex => TLex } } },
	Name:: '-unpredicted_stag',
	W,
	_
    ) :-
    recorded('SUPERTAG'(TLex,_,MD5)),
    recorded(tree2xtree(Tree,XTree)),
    \+ (XTree == MD5 xor tree2md5(XTree,MD5)),
    rule_weight(Name,W,-500)
	.

:-extensional xvector/3.
:-extensional xvector_rule_name/3.
      
%% exploiting word vector produced by glove
edge_cost_elem(
	edge{ source => node{ lemma => SLemma, cat => SCat },
	      target => node{ lemma => TLemma, cat => TCat }
	    },
	Name,
	W,
	_
    ) :-
    fail,
    xvector(SLemma,SCat,SVec),
    xvector(TLemma,TCat,TVec),
    domain(SD:SW,SVec),
    domain(TD:TW,TVec),
    ( xvector_rule_name(SD,TD,Name) ->
	  true
     ;
     name_builder('vector_~w_~w',[SD,TD],Name),
     record(xvector_rule_name(SD,TD,Name)),
     record(rule_multiply(Name))
    ),
    %    W is round(100 * SW * TW),
    '$interface'(vector_multiply(SW:int,TW:int),[return(W:int)]),
%    fail,
    % format('rule ~w ~w ~w => w=~w\n',[Name,SLemma,TLemma,W]),
    true
.

%% penalize ilya_as_mod_time when attached to a participle or gerundive
edge_cost_elem(
	edge{ label => 'S',
	      source => node{ id => SNId, cat => v, cluster => cluster{ left => SLeft } },
	      target => node{ cat => v, lemma => avoir, tree => TTree, cluster => cluster{ left => TLeft } }
	    },
	Name:: '-ilya_as_time_mod',
	W,
	_
    ) :-
    TLeft < SLeft,
    domain(verb_ilya_as_time_mod,TTree),
    rule_weight(Name,W,-40),
    node2op(SNId,OId),
    check_op_top_feature(OId,mode,Mode),
    domain(Mode,[participle,gerundive])
.

edge_cost_elem(
	edge{ label => 'S',
	      type => adj,
	      target => node{ tree => TTree}
	    },
	Name:: '-extraposed_rel',
	W
    ) :-
    domain(extraposed_rel_modifier,TTree),
    rule_weight(Name,W,-50)
.

%% END EDGE COST

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Assigning special costs for some configuration in robust mode

:-rec_prolog robust_re_weight/6.

robust_re_weight(NId,Edges,Deriv,Span,-50000,'RO_verb') :-
	recorded( mode(robust) ),
	N::node{ id => NId,
		 cat => v,
		 cluster => cluster{ left => L }},
	%% should check that the verb is a finite one
	verbose('Try Robust reweight ~w edges=~w deriv=~w span=~w\n',[NId,Edges,Deriv,Span]),
	( deriv(Deriv,_,_,OId,_) ->
	  check_op_top_feature(OId,mode,Mode),
	  \+ domain(Mode,[infinitive,gerundive,participle]),
	  true
	;
	  true
	),
	\+ ( edge_in_children( NId,
			       edge{ label => label[subject,impsubj],
				     source => N,
				     target => node{ cluster => cluster{ right => R } }
				   },
			       Edges
			     ),
	     R =< L ),
	verbose('Robust reweight ~w ~w\n',[NId,Edges]),
	true
	.

robust_re_weight(NId,Edges,Deriv,[Left,Right],W,'RO_ponct') :-
	recorded( mode(robust) ),
	( (Left = 0 xor node{ cluster => cluster{ right => Left }, cat => cat[poncts,ponctw] }) ->
	  WLeft = 0
	;
	  WLeft = -500
	),
	( ( node{ cluster => cluster{ left => Right }, cat => cat[poncts,ponctw] }
	  xor \+ cluster{ left => Right }
	  ) ->
	  WRight = 0
	;
	  WRight = -500
	),
	W is WLeft + WRight
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Assigning regional costs to nodes (and a set of edges)

:-xcompiler
sum_node_cost_regional(NId,Children,Deriv,InSpan,W) :-
    mutable(_WM,0,true),
    mutable(RM,[],true),
    mutable(LM,[],true),
    every((
		 \+ '$answers'( prepare_oracle(_) ),
%		 fail,
		 ( 
		     node_cost_regional(_Name,NId,Children,Deriv,InSpan,_W_Reg)
		 ; 
		 node{ id => NId, cat => Cat::cat[v,pro] },
		 deriv2htid(Deriv,HTId),
		 node_cost_regional_cat(Cat,_Name,NId,Children,Deriv,InSpan,HTId,_W_Reg)
		 ),
		 mutable_add(_WM,_W_Reg),
		 Next ::= _Name:_W_Reg,
		 mutable_list_extend(RM,Next),
		 %	       record(regional_cost(NId,Deriv,_Name,RFeatures,_W_Reg)),
		 verbose('Regional cost n=~w rule=~w w=~w\n',[NId,_Name,_W_Reg])
	     )),
    mutable_read(RM,Names),
    (Names = [] ->
	 true
    ;
%    fail,
     regional_features(NId,Children,InSpan,RFeatures) ->
     ( 
	 use_model,
	 \+ Deriv = root(_),
	 \+ Deriv = []
     ->
     length(Children,NChildren),
     rtemplates(RFeatures,_Name,_Values),
     %	  format('try regional add rules=~w values=~w\n',[Names,[rcost,rcost|_Values]]),
       every(( model!query([rcost,rcost|_Values],_ModelW,Names),
	       domain(_Name:_W_Reg,Names),
	       %		  _ModelW2 is 3 * _ModelW * (1+NChildren),
	       %		  _ModelW2 = _ModelW,
	       _ModelW2 is _ModelW * NChildren,
	       %		  format('regional model add rule=~w n=~w ~w ~w\n',[_Name,NChildren,_ModelW,_ModelW2]),
	       mutable_add(_WM,_ModelW2),
	       _W_Reg2 is _W_Reg + _ModelW2,
	       _Next ::= _Name:_W_Reg2,
	       mutable_list_extend(LM,_Next)
	     )),
       mutable_read(LM,_L),
       every((
		    domain(Name:_W,Names),
		    \+ domain(Name:_,_L),
		    _Next ::= Name:_W,
		    mutable_list_extend(LM,_Next)
		))
       ;
       mutable(LM,Names),
       true
     ),
     mutable_read(LM,AllNames),
     RFeatures = [_|RFeaturesTail],
     (recorded(regional_cost(NId,_,[_|RFeaturesTail],_,_)) xor record(regional_cost(NId,AllNames,RFeatures,Deriv,Children))),
     true
     ;
     format('*** pb computation of regional features ~w ~w ~w\n',[NId,Children,InSpan]),
     true
    ),
    mutable_read(_WM,W),
%    format('regional edge cost ~w ~w => ~w\n',[NId,Deriv,W]),
    true
	.


:-rec_prolog edge_weight_distrib/5.

%% not yet activated !
edge_weight_distrib(EId,NId,Children,W,WDistrib) :-
	fail,
	edge{ id => EId,
	      target => N::node{ id => NId, cat => cat[prep,nc,v,np,adj] }
	    },
	once((
	      edge_in_children( NId,
				edge{ type => adj,
				      id => _EId,
				      source => N,
				      target => COO::node{ cat => coo, id => COO_Id, deriv => COO_Derivs }
				    },
				Children
			      ),
%%	      format('weight distrib ~w\n',[NId]),
	      mutable(MW,0,true),
	      domain(dinfo(COO_OId,_EId,COO_Id,_Cst),Children),
	      '$answers'(best_parse(COO_Id,COO_OId,_,_Cst,
				    dstruct{ deriv => COO_Best_Deriv,
					     children => COO_Best_Children }
				   )
			),
	      every(( edge_in_children(COO_Id,
				       edge{ label => label[coord2,coord3] },
				       COO_Best_Children
				      ),
		      mutable_add(MW,W)
		    )),
	      mutable_read(MW,WDistrib),
%%	      format('distrib w ~w => ~w\n',[EId,WDistrib]),
	      true
	     ))
	.


:-rec_prolog
	node_cost_regional/6,
	node_cost_regional_cat/8.

/*
node_cost_regional(Name,NId,Edges,Deriv,Span,1) :-
    length(Edges,N),
    build_reg_dummy_rule(N,Name)
.
*/

:-light_tabular build_reg_dummy_rule/2.
:-mode(build_reg_dummy_rule/2,+(+,-)).

build_reg_dummy_rule(N,Name) :-
    name_builder('REG_dummy_~w',[N],Name)
.

node_cost_regional(Name::'+brother_features',NId,Edges,Deriv,Span,W) :-
	opt(bcost),
%	(use_feature_cost xor recorded(opt(cost))),
	(use_model xor recorded(opt(cost))),
	mutable(MW,0,true),
	every((
	       regional_brother_edges(NId,Edges,_W),
	       mutable_add(MW,_W)
	      )),
	mutable_read(MW,W),
	W \== 0,
%	format('brother weight ~w\n',[W]),
	true
	.

:-std_prolog regional_brother_edges/3.

regional_brother_edges(NId,Edges,W) :-
	edge_in_children(NId,
			 E1::edge{ id => EId1,
				   source => node{ id => NId, cluster => cluster{ right => R, left => L }},
				   target => node{ cluster => cluster{ right => R1, left => L1 }}
				 },
			 Edges
			),
	( R =< L1 -> 		% dir = right
	  Dir = right,
%	  format('BROTHER TRY dir=right ~E\n',[E1]),
	  ( edge_in_children(NId,
			     E2::edge{ id => EId2,
				       target => node{ cluster => cluster{ right => R2, left => L2 }}
				     },
			     Edges
			    ),
	    EId1 \== EId2,
	    R =< L2,
	    R2 =< L1,
	    \+ ( edge_in_children(NId,
				  _E2::edge{ id => _EId2,
					     target => node{ cluster => cluster{ right => _R2, left => _L2 }}
					   },
				  Children
				 ),
		 _R2 =< L1,
		 R2 =< _L2,
		 _EId2 \== EId2
	       ) ->
%	    format('=> BROTHER dir=right ~E\n',[E2]),
	    brother_edge_cost(EId1,EId2,Dir,W)
	  ;
	    brother_edge_cost(EId1,EId1,Dir,W)
	  )
	
	;			% dir = left
	  Dir = left,
	  ( edge_in_children(NId,E2,Children),
	    R2 =< L,
	    R1 =< L2,
	    EId1 \== EId2,
	    \+ ( edge_in_children(NId,_E2,Children),
		 _EId2 \== EId2,
		 R1 =< _L2,
		 _R2 =< L2
	       ) ->
	    brother_edge_cost(EId1,EId2,Dir,W)
	  ;
	    brother_edge_cost(EId1,EId1,Dir,W)
	  )
	),
	true
	.

/*
%% Strong penalty for verbs with subject after cod or xcomp (when cod after verb)
node_cost_regional_cat(v,Name::'-REG_v_obj_subj',NId,Edges::[_,_|_],Deriv,Span,_,-8000) :-
	once((
	      node{ id => NId,
		    cat => v,
		    cluster => cluster{ right => Pos}
		  },
	      edge_in_children(NId,
			       E2::edge{ id => EId2,
					 target => node{ cluster => cluster{ right => Right } },
					 label => label[object,xcomp]
				       },
			       Edges),
	      Pos =< Right,
	      edge_in_children(NId,
			       E1::edge{ id => EId1,
					 target => node{ cluster => cluster{ left => Left } },
					 label => label[subject,impsubj]
				       },
			       Edges),
	      Right =< Left,
	      verbose('Regional penalty cod before subj ~w ~w\n',[E1,E2]),
	      true
	     ))
	.
*/

/*
node_cost_regional(root,Edges,Deriv,Span::[L,R],W) :-
	recorded( mode(robust) ),
	Delta is R-L,
	W is 1000 * Delta,
	verbose('Regional penalty robust ~w ~w\n',[Span,W])
	.
*/

/* tmp deactivated
node_cost_regional(root,Edges,Deriv,Span,-50000) :-
	\+ recorded( mode(robust) ),
	verbose('Try Regional penalty on incomplete root node ~w\n',[CIds]),
	_C::cluster{ id => _CId, lex => _Lex },
	_Lex \== '',
	\+ ( domain(_CId,CIds)
	   xor
	   domain(__CId,CIds),
	     cluster_overlap(_C,__C::cluster{ id => __CId }),
	     verbose('Overlap ~E ~E\n',[_C,__C])
	   ),
	verbose('Pb with ~E\n',[_C]),
	verbose('Found Regional penalty on incomplete root node ~w\n',[CIds]),
	true
	.
*/
	     
node_cost_regional(Name::'+REG_LemmaFreq',NId,Edges,Deriv,Span,WFreq) :-
%	fail,
	once((opt( weights ),
	      node{ id =>NId, w => Ws },
	      domain(lemmaFreq:W,Ws),
	      WFreq is W*10,
	      true
	     ))
	.

node_cost_regional(Name::'+REG_LemmaFreq',NId,Edges,Deriv,Span,WTag) :-
%	fail,
	once((
	      opt( weights ),
	      node{ id =>NId, w => Ws },
	      domain(matchTagger:1,Ws),
	      WTag is 1000,
	      true
	     ))
	.

:-light_tabular potential_coord/1.
:-mode(potential_coord/1,+(-)).

potential_coord(NId) :-
	chain( node{ id => NId } >> adj >> node{ cat => coo }).
potential_coord(NId) :-
	chain( node{ id => NId } >> adj >> node{} >> (subst @ coord ) >> node{}).

%% Favor coordination with same type of coords
node_cost_regional(Name::'+REG_coord',NId,Edges::[_|_],Deriv,Span,W) :-
%	fail, %% ++
	potential_coord(NId),
	verbose('Regional cost nid=~w deriv=~w edges=~w\n',[NId,Deriv,Edges]),
	once((
	      edge_in_children(NId,
			       edge{
				    id => EId,
				    source => S::node{ id => NId,
						       cat => _Cat,
						       lemma => _Lemma
						     },
				    target => COO::node{ cat => COO_Cat::cat[coo,'N2'],
							 id => COO_Id,
							 deriv => COO_Derivs
						       }	      
				   },
			       Edges
			      ),
	      ( COO_Cat = 'N2' ->
		(chain(COO >> (subst @ coord) >> node{}) xor fail)
	      ;
		true
	      ),
	      domain(dinfo(COO_OId,EId,COO_Id,_Cst),Edges),
	      '$answers'(best_parse(COO_Id,COO_OId,_,__Cst,
				    dstruct{ deriv => COO_Best_Deriv,
					     children => COO_Best_Children }
				   )
			),
	      (node!empty(S) ->
	       edge_in_children(NId,
				edge{ source => S,
				      target => node{ cat => Cat, lemma => Lemma },
				      type => edge_kind[subst,lexical]
				    },
				Edges)
	      ; Cat = _Cat,
		Lemma = _Lemma
	      ),
% 	\+ ( edge_in_children(NId,
% 			      E::edge{ source => COO,
% 				       label => label[coord,coord2,coord3],
% 				       target => node{ cat => _Cat }
% 				     },
% 			      Edges ),
% 	     _Cat \== Cat
% 	   ),
	      mutable(M,0,true),
	      mutable(MF,1,true),
	      mutable(MCount,0,true),
%	      verbose('Hello ~w ~w\n',[COO_Derivs,COO_OId]),
	      every((
		     source2edge(
				 E::edge{ source => COO,
					  label => label[coord,coord2,coord3],
					  target => _N::node{ cat => Cat2,
							      id => XNId,
							      lemma => Lemma2
							    },
					  id => XEId
					}
			 ),
		     domain(dinfo(_,XEId,XNId,___Cst),COO_Best_Children),
		     %% domain(COO_Best_Deriv,XDerivs),
		     %%  deriv(_D,XEId,_,COO_OId,_),
		     ( Cat = Cat2
		     xor Cat=adj, Cat2=v % participles
		     xor Cat=v, Cat2 = adj
		     xor Cat=nc, Cat2 = np
		     xor Cat=np, Cat2=nc
		     xor Cat=cat[nc,np], Cat2=pro
		     xor Cat=pro, Cat2=cat[nc,np]
		     ),
		     ( Cat = Cat2 -> _W3 = 100 ; _W3 = 0),
%		     format('try coord similarity ~w_~w ~w_~w\n',[Lemma,Cat,Lemma2,Cat2]),
		     (check_sim(Lemma,Cat,Lemma2,Cat2,_W4),
		      %		      format('coord similarity ~w ~w => ~w\n',[Lemma,Lemma2,_W4]),
		      true
		      xor _W4=0),
%%		     verbose('In regional: ~w\n',[E]),
		     _W2 is 400+_W3+_W4,
		     mutable_add(M,_W2),
		     mutable_inc(MCount,_),
		     true
		    )),
	      mutable_read(MCount,Count),
%	      (COO_Cat == coo xor Count > 1),
	      mutable_read(M,XW),
	      mutable_read(MF,F),
	      ( (COO_Cat == coo xor Count > 1 xor chain( COO >> lexical >> node{ form => '...' } ) ) ->
		W is XW*F
	      ; % penalize enum with only 2 components and no final ...
		W is -3000
	      ),
	      verbose('Regional cost COORD w=~w: nid=~w coo=~E source=~E edges=~w\n',[W,NId,COO,S,Edges]),
%	      format('Regional cost COORD w=~w xw=~w count=~w F=~w: nid=~w coo=~E source=~E edges=~w\n',[W,XW,Count,F,NId,COO,S,Edges]),
	      true
	     ))
	.


:-light_tabular best_parse/5.
:-mode(best_parse/5,+(+,+,+,+,-)).

:-xcompiler
multiple_adj_subst_edges(NId,Label,Edges) :-
	edge_in_children(NId,
			 edge{ id => E1, type => adj, target => N1::node{ id => NId1} },
			 Edges
			),
	source2edge(
		    edge{ source => N1,
			  id => EId1,
			  type => subst,
			  label => Label
			}
		   ),
	edge_in_children(NId,
			 edge{ id => E2, type => adj, target => N2::node{ id => NId2 } },
			 Edges
			),
	E1 < E2,
	source2edge(
		    edge{ source => N2,
			  id => EId2,
			  type => subst,
			  label => Label
			}
		   ),

	domain(dinfo(OId1,E1,NId1,_Cst1),Edges),
	'$answers'(best_parse(NId1,OId1,_,__Cst1,dstruct{ children => Children1 })),
	domain(dinfo(_,EId1,_,_),Children1),

	domain(dinfo(OId2,E2,NId2,_Cst2),Edges),
	'$answers'(best_parse(NId2,OId2,_,__Cst2,dstruct{ children => Children2 })),
	domain(dinfo(_,EId2,_,_),Children2),

%%	format('multiple e1=~w e2=~w label=~w\n',[E1,E2,Label]),
	
	true
	
	.

%% strong penalties on multiple audience (or similar) on same node
node_cost_regional(Name::'-REG_multiple_audience',NId,Edges::[_,_|_],Deriv,Span,W) :-
    %% fail, %% ++
    once((
		multiple_adj_subst_edges(NId,Label::label['SRel',audience,reference,person_mod,time_mod,'N2app',position,ce_rel],Edges)
	    )),
	( Label = 'SRel' -> W = -10000 ;  W = -5000 ),
	true
	.

:-finite_set(cld,[cld,cld12,cld3]).

%% penalties on verbs with cld but no obj�-arg
node_cost_regional_cat(v,Name::'-REG_cld_without_obj�',NId,Edges::[_|_],Deriv,Span,HTId,-2000) :-
	once((
	      %% node{ id => NId, cat => v },
	      edge_in_children(NId,
			       edge{ target => node{ cat => cld },
				     label => preparg
				   },
			       Edges
			      ),
	      \+ check_xarg_feature(HTId,args[arg1,arg2],obj�,_,cld[])
	     )).

%% counter-balance bonus on args, for Oblique args
%% maybe could add loc args
node_cost_regional_cat(v,Name::'-REG_obl_args',NId,Edges::[_|_],Deriv,Span,HTId,W) :-
	%% fail, %+
	once((
	      check_xarg_feature(HTId,args[arg1,arg2],F::function[obl,obl2],prepobj,_),
	      (	F = obl ->
		  W = -800
	      ;	  
		%% F = obl2,
		W = -900
	      )
	     ))
	.

%% Penalize obj+comp subcat, that may be easily confused with a single obj
node_cost_regional_cat(v,Name::'-REG_obj_and_comp',NId,Edges::[_,_|_],Deriv,Span,HTId,W) :-
	once((
	      node{ id => NId, cat => v, lemma => Lemma },
	      edge_in_children(NId,
			       edge{ 
				     target => Att::node{ cat => AttCat::cat[adj,comp,v,prep],
						     cluster => cluster{ left => L_Att
								       }
						   },
				     label => comp
				   },
			       Edges
			      ),
%%	      format('here ~w ~E\n',[Name,V]),
	      edge_in_children(NId,
			       edge{ 
				     target => Obj::node{ cat => cat[nc,np,adj],
							  cluster => cluster{ right => R_Obj }
							},
				     label => object
				   },
			       Edges),
	      R_Obj =< L_Att,
	      ( obj_and_att(Lemma,W1) xor W1=0),
	      ( AttCat=prep -> W2 = -200 ; W2 = 0 ),
	      W is -150+W2+W1+(R_Obj-L_Att) * 200,
%	      format('here ~w w=~w ~E att=~E\n',[Name,W,V,Att]),
	      true
	     ))
	.

:-extensional obj_and_att/2.

obj_and_att(faire,-200).
obj_and_att(aimer,-200).
obj_and_att(appeler,-200).

%% Penalize demonstrative pronoun with no modifier
node_cost_regional_cat(pro,Name::'-REG_raw_dem_pro',NId,Edges::[_|_],Deriv,Span,HTId,-200) :-
	once((
	      node{ id => NId, cat => pro, lemma => Lemma::celui },
	      %%	     domain(Lemma,[celui]),
	      \+ edge_in_children(NId, edge{ type => adj }, Edges )
	    ))
	.

%% penalize comp+obj when obj may be confused with a de-PP
node_cost_regional_cat(v,Name::'-REG_comp_and_obj',NId,Edges::[_,_|_],Deriv,Span,HTId,W) :-
	once((
	      node{ id => NId, cat => v, lemma => Lemma },
	      edge_in_children(NId,
			       edge{
				     target => Att::node{ cat => AttCat::cat[adj,comp,v,prep],
							  cluster => cluster{ right => R_Att
									    }
							},
				     label => comp
				   },
			       Edges
			      ),
	      %%	      format('here ~w ~E\n',[Name,V]),
	      edge_in_children(NId,
			       edge{
				     target => Obj::node{ cat => cat[nc,np],
							  cluster => cluster{ left => L_Obj }
							},
				     label => object
				   },
			       Edges),
	      R_Att =< L_Obj,
%%	      format('here1 ~w w=~w ~E att=~E\n',[Name,W,V,Att]),
	      ( chain( Obj >> (subst @ det ) >> node{ form => de_form[] } ),
		%%	      format('here2 ~w w=~w ~E att=~E form=~w\n',[Name,W,V,Att,Form]),
%%		domain(Form,[de,des,du,'de la','de l''']),
		W = -1200
	      xor AttCat = prep,
		  W = -600
	      xor W = -300
	      ),
%%	      format('here ~w w=~w ~E att=~E\n',[Name,W,V,Att]),
	      true
	     ))
	     .



:-xcompiler
multiple_adj_edges(NId,Label,Edges) :-
	edge_in_children(NId,
			 edge{ id => EId1, type => adj, label => Label },
			 Edges
			),
	edge_in_children(NId,
			 edge{ id => EId2, type => adj, label => Label },
			 Edges
			),
	EId1 \== EId2
	.

%% strong penalties on some multiple adj (like quantity)
node_cost_regional(Name::'-REG_multiple_adj',NId,Edges::[_,_|_],Deriv,Span, -10000) :-
	%% fail,
	once(( multiple_adj_edges(NId,Label::label[quantity,supermod],Edges) )),
	%% format('multiple audience label=~w\n',[Label]),
	true
	.

%% favor causative derivations, whenever possible
node_cost_regional_cat(v,Name::'+REG_causative_deriv',NId,Edges::[_|_],Deriv,Span,HTId,1000) :-
	once((
	      node{ id => NId, cat => v, lemma => faire },
	      check_xarg_feature(HTId,arg1,_,fkind[vcompcaus],_),
	      edge_in_children(NId,
			       edge{ label => xcomp, type => subst },
			       Edges
			      )
	     ))
	.

%% User-provided info on chunks
%% *** to be checked and refined
node_cost_regional(Name::'REG_UserChunkCost',NId,Edges,Deriv,Span::[XLeft,XRight|_],W) :-
	%% fail,
	extra_chunk_cost(Left,Right,Type,W),
	(var(Left) xor XLeft =< Left),
	(var(Right) xor XRight >= Right),
	N::node{ id => NId, xcat => XCat, cat => Cat, cluster => cluster{ left => NLeft, right => NRight}},
	Left =< NLeft,
	NRight =< Right,
	( Type = 'GN' ->
	    (	Left = XLeft
	    xor %%fail,
	    edge_in_children(NId,
				 edge{
				       target => _N::node{ cat => _Cat, cluster => cluster{ right => _Right } } },
				 Edges ),
		( _Right =< Left
		xor 
		    _Cat = det,
		    source2edge(edge{ source => _N, 
				      target => node{ cluster => cluster{ right => __Right } } }),
		    __Right =< Left
		)
	    ),
	    XCat = 'N2',
	    Cat = nominal[]
	;   Type = 'GA' ->
	    (	Left = XLeft xor edge_in_children(NId,
						  edge{ 
							target => node{ cluster => cluster{ right => _Right } } },
						  Edges ),
		_Right =< Left
	    ),
	    XCat = cat['adjP',adj,'N',det],
	    Cat = cat[adj,v]
	;   Type = 'GP' ->
%	  fail,
	    Left = XLeft,
%	    XCat = cat['PP'],
	    % XCat = cat['PP','S','VMod'],
%	  XCat = cat['PP','S','VMod','N2','adjP'],
	    Cat = cat[prep],
	  true
	;   Type = 'PV' ->
	    (	%% PP with a verb
		%%	fail,
		Left = XLeft,
		XCat = cat['PP','S','VMod','N2','adjP'],
		Cat = cat[prep]
	    ;	%% xcomp verb argument introduced by a prep
		Cat = cat[v,adj],
		%%		format('here0 n=~w\n',[N]),
		edge_in_children(NId,
				 edge{ 
				       target => Prep::node{ cat => prep, cluster => cluster{ left => Left, right => Left2 } },
				       label => prep,
				       type => lexical
				     },
				 Edges
				),
%		format('here1 n=~w prep=~w\n',[N,Prep]),
		edge_in_children(NId,
				 edge{ 
				       target => XCOMP::node{ cat => v, cluster => cluster{ right => Right } },
				       label => xcomp,
				       type => subst
				     },
				 Edges
				),
%		format('here2 n=~w prep=~w xcomp=~w\n',[N,Prep,XCOMP]),
		true
	    )
	;   Type = 'GR' ->
	    ( Left = XLeft
	    xor edge_in_children(NId,
				 edge{ 
				       target => node{ cluster => cluster{ right => _Right } } },
				 Edges ),
		_Right =< Left
	    ),
	    XCat = cat['S','VMod',adv,advneg],
	    Cat = cat[adv,advneg]
	;   
	    Cat = cat[v],
	    XCat = cat['S']
	),
%	format('UserChunkCost ~w ~w ~w ~w\n',[N,Span,W,Edges]),
	true
	.

node_cost_regional(Name::'REG_dummy',NId,Edges,Deriv,Span,0).

%% penalties on surrounding material for preceding citations
%% eg: X S Y, affirme-t-il, with X or Y attached to 'affirmer'
node_cost_regional_cat(v,Name::'-REG_incise_with_surround',NId,Edges::[_,_|_],Deriv,Span,HTId,-2000) :-
%	fail,
	once((
	      node{ id => NId, cat => v, cluster => cluster{ left => VLeft } },
	      edge_in_children(NId,
			       edge{ 
				     label => xcomp,
				     type => subst,
				     target => node{ cluster => cluster{ left => TLeft, right => TRight } }
				   },
			       Edges
			      ),
	      TRight =< VLeft,
	      edge_in_children(NId,
			       edge{ 
				     label => label['S',vmod,mod],
				     type => adj,
				     target => node{ cluster => cluster{ left => ModLeft, right => ModRight } }
				   },
			       Edges
			      ),
				% (   ModRight =< TLeft xor TRight =< ModLeft, ModRight =< VLeft )
	      ModRight =< VLeft
	     ))
	.
				
%% END REGIONAL COST 
