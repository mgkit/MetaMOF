# MetaMOF

Metagrammar for Old and Middle French, adapted from FRMG by E. Villemonte de la Clergerie
[FRMG](http://alpage.inria.fr/frmgwiki/)

## License

This software is distributed under the LGPL-v3 license. For more information, please check the LICENSE file.

## Installing

run recompile.sh

## Processing sentences

### From the terminal

```
echo "Yseut plore" | ./metamof_lexer | ./metamof_parser - [-forest] [-robust]
```
	where:
	-forest : to output the shared derivation forest
	-robust : to do partial parsing


### From the shell

```
metamof_shell --local
```

## Contact

Mathilde Regnault : mathilde.regnault@inria.fr


# Files of the FRMG / MetaMOF chain

## Presentation
 - **LICENSE**
 - **readme.md**

## Basics
### Metagrammar
 - **\*.smg**: metagrammar following the Simple Metagrammar formalism

### Grammar
 - **\*.tag**: resulting Tree Adjoining Grammar (feature-based & lexicalised)
 - **\*.tag.xml**: Tree Adjoining Grammar with XML format
 - **addons.tag**: extra TAG trees
 - **preheader.tag**
 - **postheader.tag**
 - **main.tag**
 - **tag_generic.pl**
 - **metamof.xml** ?
 - **tagxml2tagxml.pl** ?

### Additional Files
 - **metamof.dump**: dump of information and constraints on trees
 - **metamof.xml.log**
 - **features_header.tag**: building TIG trees

### Fichiers d'interface avec le lexique
 - **metamof.map**

### Compilation
 - **compile**

### Utilisation
 - **metamof_shell**

## Autotools
 - macros: **aclocal.m4**
 - configure: **configure.ac, configure, config.guess, config.log, config.status, config.sub**
 - Makefile: **Makefile.am, Makefile.in, Makefile**
 - Files: **missing, libtool, metamof.pc.in, metamof.pc, install-sh, ltmain.sh, autogen.sh**
 - **m4, autom4te.cache**: additional repositories
 - **recompile.sh**: install/reinstall the package

## Segmentation
 - **metamof_lexer.in, metamof_lexer**: selects lexicon entries for each sentence
 	- exclude function: filter features
 	- transfer functions: rename categories, features, values, prepositions...
 - **frmg_lexer**: additional file for immediate comparison

## Analysis
 - **rx.pl, rx_c.c**: files for DyALog
 - **cost.pl**: weight on edges
 - **extract.pl**: extraction of a shared dependency forest
 - **features.pl, features.conf** (empty for this metagrammar): handling features
 - Disambiguation: **disamb.pl** (handling disambiguation and conversions), **best.pl** (extraction of the best dependency tree)
 - **data.pl, utils.pl**: additional scripts for conversions and disambiguation
 - **transform.pl**: edge transformation
 - Converting schemas/formats: **depxml.pl, passage.pl, conll.pl, depconll.pl, udep.pl**
 - **test.log**

## Handling the lexicon
 - **missing.lex**: if the lexical entry is not in OFrLex, write it here
 - **complete.lex**: if you want to add information about existing entries
 - **restrictions.txt**: if you want to forbid use of an entry
 - **MyRegExp.pm**

## Oracle
New script: conllu2oracle.py

## Not described yet
 - **depcomp**
 - **LefffDecoder.yp, LefffDecoder.pm, lefffdecoder**
 - **lexer_mod.map** (ignore?)
 - **templates.db** (ignore?)
