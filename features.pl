/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2015, 2016, 2017, 2021 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  features.pl -- Handling features and stat models
 *
 * ----------------------------------------------------------------
 * Description
 * - Navigating in the dependency trees
 * - Identifying specific linguistic configurations (main verb, ...)
 * - Defining and extracting features from nodes, edges, words, ...
 * - Using statistical models that exploit the features
 * ----------------------------------------------------------------
 */

:-import{ module=> sqlite,
	  file => 'libdyalogsqlite.pl'
	}.

:-require 'extract.pl'.
:-require 'data.pl'.

:-op( 700, fx, [template]).

:-finite_set(parse_mode,[full,corrected,robust]).

:-extensional 'F'/2.

:-extensional compound/4.

:-finite_set(pos,[left,right,xleft,xright,in,out,xin,xout]).

:-finite_set(label,[ subject,impsubj,
		     object,arg,preparg,scomp,comp,xcomp,
		     'Infl','V1','V',v,'vmod',
		     cla,cld,clg,cll,clr,clneg,
		     void,coo,coo2,coord2,coord3,coord,
		     det,det1, incise,en,
		     nc,np,number,number2,ncpred,
		     'CS','Modifier',
		     'N','N2','N2app','Nc2','Np2',
		     'PP',prep,csu,
		     prel,pri,pro,'Root','Punct',
		     'S','S2','SRel','N2Rel','SubS',varg,
		     start,wh,starter,advneg,aux,ce,
		     predet_ante,predet_post,
		     'Monsieur',adjP, person_mod, time_mod, audience, reference,
		     'S_incise', pas, adj,quoted_S,quoted_N2,quoted_PP,quantity,
		     supermod, 'ExtraWPunct','ExtraSPunct', position, ce_rel, pres,
		     mod, 
		     'mod.xcomp',
		     dep,
		     modal,
		     'mod.quantity',
		     range,
		     'dep.relative',
		     'juxt',
		     adv,
		     dcln,dcla,dcld,dclg,dcll
		     ]).

:-finite_set(quoted,[double_quoted,chevron_quoted,simple_quoted,plus_quoted,quoted_as_N2,
		     quoted_sentence_as_post_mod,
		     quoted_sentence_as_ante_mod
		    ]).


:-finite_set(fkind,[scomp,prepscomp,vcomp,prepvcomp,obj,whcomp,prepobj,acomp,vcompcaus,sadv,subj,
		    prepwhcomp]).
:-finite_set(args,[arg0,arg1,arg2]).

:-finite_set(binary,[+,-]).

:-extensional
      (template)/1,
  (stemplate)/1,
  (rtemplate)/1
.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generic accessors in Feature Structures

:-xcompiler
safe_inlined_feature_arg(Top,F,FV,V) :-
    inlined_feature_arg(Top,F,FV,V),
    \+ V = not_a_feature_value
		.

:-xcompiler
full_feature_arg(Top,LexTop,F,FV,V) :-
    safe_inlined_feature_arg(Top,F,FV,V),
    (inlined_feature_arg(LexTop,F,FV,V) xor true)
		.

:-xcompiler
edge_in_children(NId,E::edge{ id => EId },Children) :-
    (var(EId) ->
	 domain(dinfo(_,EId,_,_),Children),
	 source2edge(NId,E)
    ;
    source2edge(NId,E),
    domain(dinfo(_,EId,_,_),Children)
    ),
    true
.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compact notation to naviate in a dependency tree (or forest)

:-op(700,xfy, [>>,<<]).

/*
  We can write complex query expression with chain using a compact notation

  Exp := Node >> ForwardPathExp
       | Node << BackwardPathExp

  XExp := Node
       |  Exp

  ForwardPathExp := EdgeExp >> XExp
                  | ( ForwardPathExp & ForwardPathExp)
                  | ( ForwardPathExp ; ForwardPathExp)
                  | ( ForwardPathExp xor ForwardPathExp)
                  | ( \+ ForwardPathExp )

  BackwardPathExp := EdgeExp >> XExp
                  | ( BackwardPathExp & BackwardPathExp )
                  | ( BackwardPathExp ; BackwardPathExp )
                  | ( BackwardPathExp xor BackwardPathExp)
                  | ( \+ BackwardPathExp )

  EdgeExp :=  edge{}
           |  edge_kind
           |  edge_kind @ edge_label
           |  ind( EdgeExp )
           |  (EdgeExp @*)
           |  { EdgeExp ; EdgeExp ... }

  Node := node{}
  
*/

:-toplevel_clause
chain(In,Out) :-
    ( In = (A >> (E & Rest)) ->
	chain(A >> E,XE),
	chain(A >> Rest,XRest),
	Out = (XE,XRest)
    ;	In = (A >> (E ; Rest)) ->
	chain(A >> E,XE),
	chain(A >> Rest,XRest),
	Out = (XE ; XRest)
    ;	In = (A >> (E xor Rest)) ->
	chain(A >> E,XE),
	chain(A >> Rest,XRest),
	Out = (XE xor XRest)
    ;   In = (A >> (\+ E)) ->
	chain(A >> E,XE),
	Out = (\+ XE)
    ;  In = (A << (E & Rest)) ->
	chain(A << E,XE),
	chain(A << Rest,XRest),
	Out = (XE,XRest)
    ;	In = (A << (E ; Rest)) ->
	chain(A << E,XE),
	chain(A << Rest,XRest),
	Out = (XE ; XRest)
    ;	In = (A << (E xor Rest)) ->
	chain(A << E,XE),
	chain(A << Rest,XRest),
	Out = (XE xor XRest)
    ;	In = (A << (\+ E)) ->
	chain(A << E,XE),
	Out = (\+ XE)
    ;
	( In = (A >> E >> In2) 
	  ->
%	  format('xhere E=~w\n',[E]),
	    (
%	     \+ var(E),
	     E = '$head' 
	    ->
%	     format('*** chain expansion of head G=~w\n',[E]),
	     G = (get_head(A,B)),
	     true
	    ; 
%	     \+ var(E),
		E = {E1;Rest} ->
		chain( A >> E1 >> B, G1),
		chain( A >> { Rest } >> B, G2),
		G = (G1;G2)
	    ;	% \+ var(E),
		E = {E1} ->
		chain( A >> E1 >> B, G)
	    ;	% \+ var(E),
		E = ind(E1) ->
		chain( A >> adj >> node{} >> E1 >> B, G )
	    ;	% \+ var(E),
		E = (E1 @*) ->
		chain( _A::node{} >> E1 >> _B::node{}, G1),
		G = ( @*{ goal => G1,
			  collect_first => [A],
			  collect_last => [B],
			  collect_loop => [_A],
			  collect_next => [_B]
			}
		    )
	    ;
		(   E=edge{ id => EId,
			    type => Type,
			    label => Label
			  }
		xor E = (EId : Type @ Label)
		xor E = (EId : Type)
		xor E = Type @ Label
		xor E = Type
		),
		G=source2edge(
			      edge{ source => A::node{},
				    target => B::node{},
				    id => EId,
				    type => Type,
				    label => Label
				  }
			     )
	    )
	; In = (A << E << In2) ->
	    (	% \+ var(E),
		E = {E1;Rest} ->
		chain( A << E1 << B, G1),
		chain( A << {Rest} << B, G2),
		G = (G1;G2)
	    ;	% \+ var(E),
		E = {E1} ->
		chain( A << E1 << B, G)
	    ;	% \+ var(E),
		E = ind(E1) ->
		chain( A << E1 << node{} << adj << B, G )
	    ;	% \+ var(E),
		E = (E1 @*) ->
		chain( _A::node{} << E1 << _B::node{}, G1),
		G = ( @*{ goal => G1,
			  collect_first => [A],
			  collect_last => [B],
			  collect_loop => [_A],
			  collect_next => [_B]
			}
		    )
	    ;
		(   E=edge{ id => EId,
			    type => Type,
			    label => Label
			  }
		xor E = (EId : Type @ Label)
		xor E = (EId : Type)
		xor E= Type @ Label 
		xor E= Type
		),
		G=target2edge(
			      edge{ target => A,
				    source => B,
				    id => EId,
				    type => Type,
				    label => Label
				  }
			     )
	    )
	;
	  format('*** Chain Expansion problem: chain(~w)\n',[In]),
	  fail
	),
	( In2 = B ->
	    Out = G
	;   In2 = (B >> _) ->
	    chain(In2,Out2),
	    Out = (G,Out2)
	;   In2 = (B << _),
	    chain(In2,Out2),
	    Out = (G,Out2)
	)
        )
.

:-toplevel_clause
term_expand(chain(In),Out) :-
	chain(In,Out),
%	format('term expansion ~w\n',[Out]),
	true
.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Navigation primitives between edges and nodes

:-extensional source2edge/2.
:-extensional erased_source2edge/2.

:-xcompiler
source2edge(Edge::edge{ source => node{ id => NId} }) :- source2edge(NId,Edge).

:-xcompiler
all_source2edge(Edge::edge{ source => node{ id => NId} }) :-
    ( source2edge(NId,Edge) ; erased_source2edge(NId,Edge) ).

:-extensional target2edge/2.

:-xcompiler
target2edge(Edge::edge{ target => node{ id => NId }}) :- target2edge(NId,Edge).

:-extensional sourcecluster2edge/2.

:-xcompiler
sourcecluster2edge(Edge::edge{ source => node{ cluster => C} }) :- sourcecluster2edge(C,Edge).

:-extensional targetcluster2edge/2.

:-xcompiler
targetcluster2edge(Edge::edge{ target => node{ cluster => C} }) :- targetcluster2edge(C,Edge).

:-light_tabular node!neighbour/3.
:-mode(node!neighbour/3,+(+,+,-)).

node!neighbour(left, X, N::node{} ) :-
	(   X = node{ cluster => ClusterA::cluster{ left => P} } xor X = P ),
	ClusterB::cluster{ right => P, left => _P },
	_N::node{ cluster => ClusterB, form => _Form },
	( _Form = '_EPSILON' ->
	  node!neighbour(left,_N,N)
	; P=_P ->
	  node!neighbour(left,_N,N)
	;
	  N=_N
	)
	.

node!neighbour(right, X, N::node{}) :-
	(   X = node{ cluster => ClusterA::cluster{ right => P} } xor X = P ),
	ClusterB::cluster{ left => P },
	N::node{ cluster => ClusterB }
	.

node!neighbour(xleft, X, N::node{}) :-
	(   X = node{ cluster => ClusterA::cluster{ left => P} } xor X = P ),
	ClusterB::cluster{ right => Q }, Q =< P,
	N::node{ cluster => ClusterB }
	.

node!neighbour(xright, X, N::node{}) :-
	(   X = node{ cluster => ClusterA::cluster{ right => P } } xor X = P ),
	ClusterB::cluster{ left => Q }, P =< Q,
	N::node{ cluster => ClusterB }
	.

:-std_prolog node!all_neighbours/4.

node!all_neighbours(Mode,X,Model::node{},N::node{}) :-
	node!neighbour(Mode,X,Y),
	\+ \+ (Y=Model),
	( Y = N
	; node!all_neighbours(Mode,Y,Model,N)
	)
	.

:-std_prolog node!empty/1.

node!empty( node{ cluster => cluster{ lex => '' } } ).


:-light_tabular node!dependency/4.
:-mode(node!dependency/4,+(+,+,-,-)).

node!dependency(out,N1::node{},N2::node{},[Edge]) :-
	Edge::edge{ source => N1, target => N2 }
	.

node!dependency(in,N1::node{},N2::node{},[Edge]) :-
	Edge::edge{ target => N1, source => N2 }
	.

node!dependency(xout,N1::node{},N2::node{},L) :-
	node!dependency(out,N1,N3,[E]),
	N1 \== N3,
	(   N2 = N3,
	    L = [E]
	;   
	    node!dependency(xout,N3,N2,L1),
	    (\+ domain( edge{ target => N1 }, L1 )),
	    L = [E|L1]
	)
	.

node!dependency(xin,N1::node{},N2::node{},L) :-
	@*{ goal => ( _E::edge{ target => _N1, source => _N2 },
			\+ domain( edge{ target => _N2 },L),
			_N2 \== N1 % to avoid loop
		    ),
	    from => 1,
	    collect_first => [N1,[]],
	    collect_last  => [N2,L],
	    collect_loop => [ _N1::node{}, _L],
	    collect_next => [ _N2::node{}, [_E|_L] ]
	  }
	.

:-light_tabular node!safe_dependency/3.
:-mode(node!safe_dependency/4,+(+,+,-)).

node!safe_dependency(xout,N1::node{},N2::node{}) :-
	node!dependency(out,N1,N3,_),
	(   N2 = N3
	;   
	    node!safe_dependency(xout,N3,N2)
	),
	N1 \== N2
	.

node!safe_dependency(xin,N1::node{},N2::node{}) :-
	node!dependency(in,N1,N3,_),
	(   N2 = N3
	;   
	    node!safe_dependency(xin,N3,N2)
	),
	N1 \== N2
	.

:-xcompiler
node!collect(X^G,L) :-
	mutable(M,[],true),
	every((G,
	       mutable_read(M,L1),
	       (	 \+ domain(X,L1)),
	       X = node{ cluster => cluster{ lex => LexX}},
	       LexX \== '',		     
	       node!add(X,L1,L2),
	       mutable(M,L2)
	      )),
	mutable_read(M,L).

:-rec_prolog node!add/3.

node!add( N::node{},[],[N]).
node!add( N::node{ cluster => Cluster::cluster{ left => Left}},
	  L1::[N1::node{ cluster => Cluster1::cluster{ left => Left1}}|XL1],
	  L2
	) :-
	( Cluster == Cluster1 ->
	    L2 = L1
	;   
	    Left < Left1 ->
	    L2 = [N|L1]
	;   
	    node!add(N,XL1,XL2),
	    L2 = [N1|XL2]
	),
%%	format('NODE ADD ~w\n',[L2]),
	true
	.

:-rec_prolog node!add_fillers/2.

node!add_fillers( L::[N::node{}], L ).
node!add_fillers( L::[N1::node{ cluster => cluster{ left => Left1, right => Right1 }},
		      N2::node{ cluster => cluster{ left => Left2, right => Right2 }} | LL ],
		  XL::[N1|XL2]
		 ) :-
	(   N::node{ cluster => cluster{ lex => _Lex, left => Right1, right => _Left } },
	    _Lex \== '',
	    _Left =< Left2 ->
	    node!add_fillers([N,N2|LL],XL2)
	;
	    node!add_fillers([N2|LL],XL2)
	)
	.

:-std_prolog node!terminal/3.

node!terminal(N,Cat,Path) :-
	\+ ( node!dependency(out,N, node{ cat => Cat },[edge{ label => Label} ]),
	       domain(Label,Path)
	   )
	.

:-std_prolog node!parent/4.

node!parent(N,Cat,P,Path) :-
	node!dependency(in,N,P::node{ cat => XCat },[edge{ label => Label }]),
	domain(Label,Path),
	domain(XCat,Cat)
	.

:-std_prolog node!older_ancestor/4.

node!older_ancestor(N,Cat,P,Path) :-
	(   node!parent(N,Cat,P1,Path),
	    node!older_ancestor(P1,Cat,P,Path)
	xor P = N
	).

:-std_prolog node!first_v_ancestor/2.

node!first_v_ancestor(N::node{cat => Cat::cat[aux,v], tree => Tree}, N1) :-
	( Cat == cat[v] ->
	    N1 = N
	;   Cat == aux, domain(cleft_verb,Tree) ->
	    N1 = N
	;   
	    node!parent(N,cat[aux,v],N2,['Infl','V',aux,modal]),
	    node!first_v_ancestor(N2,N1)
	)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Special configurations

:-light_tabular have_shared_derivs/2.
:-mode(have_shared_derivs/2,+(+,+)).

%% 2 edges belong to at least one shared derivation
have_shared_derivs(EId1,EId2) :-
	once(( EId1 \== EId2,
	       %% CALL EDGE
	       (E1::edge{ id => EId1, deriv => Derivs1, source => N::node{ deriv => Derivs}} xor recorded(erased(E1))),
	       (E2::edge{ id => EId2, source => N } xor recorded(erased(E2))),
	       domain(D,Derivs1),
%	       deriv2edge(D,info(EId1,_,_,_,_)),
	       deriv2edge(D,info(EId2,_,_,_,_))
	     ))
	.

:-light_tabular single_past_participiale/1.

single_past_participiale(N::node{}) :-
	edge{ source => node{ cat => 'N2' },
	      target => N::node{ cat => v, cluster => cluster{ lex => Lex } },
	      type => subst,
	      label => 'SubS'
	    },
	\+ source2edge(edge{ source => N, label => label[object,arg,preparg,scomp,xcomp] }),
	\+ source2edge(edge{ source => N, target => node{ cat => aux } }),
	\+ ( source2edge(edge{ source => N, target => VMod::node{ cat => 'VMod' },
		   type => adj, label => label[vmod,mod] }),
	     source2edge(edge{ source => VMod, target => node{ cat => prep }, label => 'PP'})
	   ),
%%	format('Try1 participale2adj on ~w ~w\n',[Lex,TLex]),
	'T'(Lex,TLex),
	(   rx!match{ string => TLex, rx => rx{ pattern => 'ant$'} } ->
	    verbose('Match succeeded\n',[]),
	    fail
	;
	    verbose('Match failed\n',[]),
	    true
	)
	.

:-std_prolog coord_next/3.

%% search for the next component of a coordination
coord_next( LastCoord::node{ cat => LastCat, cluster => cluster{ left => LastLeft }},
	    Start::node{ cluster => cluster{ right => StartRight }},
	    Next::node{ id => NId,
			cluster => cluster{ left => NextLeft,
					    right => NextRight,
					    token => NextToken
					  }}
	  ) :-
	verbose('Search coord next: ~E ~E\n',[Start,LastCoord]),
	(   Next = LastCoord,
	    \+ node!empty(Next)
	;   source2edge(
		edge{ source => LastCoord,
		      target => Next,
		      label => label[coord2,coord3,coord,xcomp,'Modifier'],
		      type => edge_kind[~ [adj]]
		    }
	    )
	;  source2edge(
	       edge{ source => LastCoord,
		     target => Next,
		     label => void
		   }
	   ),
	    ( LastCat == coo -> NextRight < LastLeft ; true),
	    NextToken = (','),
%%	    format('last=~E start=~E next=~E\n',[LastCoord,Start,Next]),
	    true
	),
	NextLeft >= StartRight,
	verbose('Potential next=~w\n',[Next]),
	\+ ( ( source2edge(
		   Edge2::edge{ source => LastCoord,
				target => Next2::node{ id =>NId2,
						       cluster => cluster{ token => NextToken2,
									   left => NextLeft2,
									   right => NextRight2 }},
				label => Label2::label[void,coord2,coord3,coord] }
	       ),
	       NId2 \== NId,
	       NextLeft2 >= StartRight,
	       NextRight2 =< NextLeft,
	       ( Label2 = void ->
		 NextToken2 = (','),
		 %% LastCoord may be preceded by a coma
		 \+ ( NextRight2 = NextLeft,
		      Next = LastCoord
		    )
	       ;
		 true
	       ),
	       /*
	       \+ ( %% LastCoord may be preceded by a coma
		    Label2 = void,
		    NextRight2 = NextLeft,
		    Next = LastCoord
		  ),
	       */
	       verbose('coord discard case1 next=~w next2=~w\n',[Next,Next2]),
	       true
	     ;	
		 Next2 = LastCoord,
	       \+ node!empty(Next2), % not the enum case but a true coord
	       NextLeft2 >= StartRight,
	       NextRight2 =< NextLeft,
	       verbose('coord discard case2 next=~w next2=~w\n',[Next,Next2]),
	       true
	     )
	   ),
	verbose('\n\t ~E => ~E\n',[Start,Next]),
	true
	. 

:-light_tabular get_head/2.
:-mode(get_head/2,+(+,-)).

get_head(N1::node{},N3::node{}) :-
	( N1 = node{ cat => prep },
	  source2edge(
	      edge{ source => N1,
		    target => N2::node{},
		    type => subst
		  }
	  )
	xor N1 = node{ cat => csu },
	  source2edge(
	      edge{ source => N1,
		    target => N2,
		    type => subst
		  }
	  )
	xor  node!empty(N1),
	  source2edge(
	      edge{ source => N1, target => _N2::node{}, type => subst }
	  ),
	  get_head(_N2,N2)
	xor
	  N1 = N2
	),
	( node!first_main_verb(N2,N3)
	xor N3=N2
	)
	.

:-light_tabular get_head_no_climb/2.
:-mode(get_head_no_climb/2,+(+,-)).

get_head_no_climb(N1::node{},N3::node{}) :-
	( N1 = node{ cat => prep },
	  source2edge(
	      edge{ source => N1,
		    target => N2::node{},
		    type => subst
		  }
	  )
	xor N1 = node{ cat => csu },
	  source2edge(
	      edge{ source => N1,
		    target => N2,
		    type => subst
		  }
	  )
	xor  node!empty(N1),
	  source2edge(
	      edge{ source => N1, target => _N2::node{}, type => subst }
	  ),
	  get_head_no_climb(_N2,N2)
	xor
	  N1 = N2
	),
	N3=N2
	.

:-light_tabular node!first_main_verb/2.
:-mode(node!first_main_verb/2,+(+,-)).

node!first_main_verb( N1::node{cat => v},
		      N2::node{ cat => v }) :-
    ( source2edge(
	  edge{ source => N1,
		target => _N2,
		label => 'V',
		type => adj
	      }
      ),
	  node!first_main_verb(_N2,N2)
	xor
	  N2 = N1
	).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:-light_tabular span_max/1.

span_max(Span::[0,Max]) :- recorded('N'(Max)).

:-light_tabular cluster_overlap/2.

cluster_overlap( CId1, CId2 ) :-
	%% overlap should now work with empty clusters
	%% verbose('Checking overlap ~E ~E\n',[C1,C2]),
	C1::cluster{ lex => Lex1, id => CId1, left => Left1, right => Right1 },
	Lex1 \== '',
	C2::cluster{ lex => Lex2, id => CId2, left => Left2, right => Right2 },
	Lex2 \== '',
	(   CId1 == CId2
	xor Left1 =< Left2, Left2 < Right1
	xor Left2 =< Left1, Left1 < Right2
	xor Left1 == Left2, Right1 == Right2
	),
	verbose('Overlap ~E ~E\n',[C1,C2]),
	true
	.

:-xcompiler 
edge_features(EId,
	      Left,
	      Features
	     ) :-
	( Edge::edge{ id => EId, source => Source, target => Target }
	xor recorded(erased(Edge))
	),
	simple_edge_features(EId,Left,Data),
	feature_map(Data,_Features),
	xnode2conll_source(Source,SPos),
	xnode2conll_source(Target,TPos),
	Features = [tpos: TPos, spos: SPos | _Features ]
	.


:-light_tabular next_and_prev_forms/5.
:-mode(next_and_prev_forms/5,+(+,+,-,-,-)).

next_and_prev_forms(Left,Right,PForm,NForm,PPForm) :-
%%	format('try next_and_prev_forms left=~w right=~w\n',[Left,Right]),
	(prev_form(Left,PForm,_Left) ->
	 (prev_form(_Left,PPForm,_) xor PPForm = '****')
	; PForm = '****',
	 PPForm = '****'
	),
	(next_form(Right,NForm) xor NForm = '****'),
%%	format('=> next_and_prev_forms left=~w right=~w pform=~w nform=~w ppform=~w\n',[Left,Right,PForm,NForm,PPForm]),
	true
	.

:-light_tabular prev_form/3, next_form/2.
:-mode(prev_form/3,+(+,-,-)).
:-mode(next_form/2,+(+,-)).

prev_form(Left,PForm,_Left) :-
	( node{ form => PForm, cluster => cluster{ right => Left, left => _Left } },
	  _Left < Left
	xor
	node{ form => PForm, cluster => cluster{ right => XLeft, left => _Left } },
	  _Left < Left,
	  Left < XLeft
	xor
	fail
	).
prev_form(0,'^^^',-1).
prev_form(-1,'^^^^^^',-2).


next_form(N,'$$$') :- recorded('N'(N)).
next_form(Right,NForm) :-
	( node{ form => NForm, cluster => cluster{ left => Right, right => _Right } },
	  Right < _Right
	xor
	node{ form => NForm, cluster => cluster{ left => XRight, right => _Right} },
	  Right < _Right,
	  XRight < Right
	xor
	fail
	).

:-xcompiler
has_suffix_n(L,N,S) :-
	'$interface'('n_suffix'(L:string, N:int),[return(S:string)])
	.

:-light_tabular suffix3/2.
:-mode(suffix3/2,+(+,-)).

suffix3(Lex,Suff) :- 
	( Lex = '' -> Suff = '-'
	; Lex = entities[] -> Suff = '-'
	;  has_suffix_n(Lex,3,_Suff), 
	  (_Suff==Lex -> Suff= '-' ; Suff = _Suff)
	).

:-xcompiler
is_number(L) :-
	'$interface'('is_number'(L:string),[])
	.
:-xcompiler
is_year(L) :-
	'$interface'('is_year'(L:string),[])
	.

:-xcompiler
is_capitalized(L) :-
	'$interface'('is_capitalized'(L:string),[])
	.

%:-xcompiler

:-light_tabular is_acronym/2.
:-mode(is_acronym/2,+(+,-)).

is_acronym(Form,CharList) :-
	'$interface'('is_acronym'(Form:string, CharList: term),[])
	.

:-light_tabular capitalized_cluster/1.

capitalized_cluster(TokenId) :-
	( TokenId=[_|_] ->
	  once((
		domain(_TokenId,TokenId),
		'T'(_TokenId,_Lex),
		is_capitalized(_Lex)
	       ))
	;
	  'T'(TokenId,_Lex),
	  is_capitalized(_Lex)
	)
	.

:-light_tabular node_root_status/2.
:-mode(node_root_status/2,+(+,-)).

node_root_status(NId,Status) :-
	( N::node{ id => NId} ->
	  ( target2edge(E::edge{ type => edge_kind[~ virtual], target => N }) -> Status = 0 ; Status = 1 )
	; recorded( erased(N) ) ->
	  ( recorded( erased(E) ) -> Status = 0 ; Status = 1 )
	;
	  Status = 1
	)
	.

:-light_tabular edge_min_height/2.
:-mode(edge_min_height/2,+(+,-)).

edge_min_height(EId,H) :-
    ( E::edge{ id => EId,
	       target => node{ id => TNId }
	     }
      xor recorded(erased(E))
    ),
    mutable(MH,0),
    every((
		 ( source2edge(_E::edge{ id => _EId, source => node{ id => TNId}})
		  ; recorded( erased(_E))
		 ),
		 edge_min_height(_EId,_H),
		 _XH is min(_H+1,8),
		 mutable_max(MH,_XH)
	     )),
    mutable_read(MH,H)
.

:-light_tabular edge_min_depth/2.
:-mode(edge_min_depth/2,+(+,-)).

edge_min_depth(EId,D) :-
    (E::edge{ id => EId,
	      source => node{ id => SNId }
	    }
      xor recorded(erased(E))
    ),
    mutable(MD,0),
    every((
		 ( target2edge(_E::edge{ id => _EId, target => node{ id => SNId}})
		  ; recorded(erased(_E))
		 ),
		 edge_min_depth(_EId,_D),
		 _XD is min(_D+1,8),
		 mutable_max(MD,_XD)
	     )),
    mutable_read(MD,D)
.


:-light_tabular edge_left_sibling/2.
:-mode(edge_left_sibling/2,+(+,-)).

edge_left_sibling(EId,XEId) :-
    E::edge{ id => EId,
	     source => N::node{},
	     target => node{ cluster => cluster{ left => Left, right => Right } },
	     deriv => Derivs
	   },
    source2edge(
	edge{ id => XEId,
	      source => N,
	      target => node{ cluster => cluster{ right => XRight } },
	      deriv => XDerivs
	    }
    ),
    XRight =< Left,
    \+ XEId == EId,
    (domain(D,Derivs), domain(D,XDerivs) -> true ; fail),
    \+ ((source2edge(
	     edge{ id => _EId,
		   source => N,
		   target => node{ cluster => cluster{ left => _Left, right => _Right }},
		   deriv => _Derivs
		 }
	 ),
	 _Right =< Left,
	 XRight =< _Left,
         \+ EId == _EId,
         \+ XEId == _EId,
         domain(_D,Derivs),
	 domain(_D,_Derivs)
	)),
    true
.

:-light_tabular edge_rank/3.
:-mode(edge_rank/3,+(+,-,-)).

edge_rank( EId,
	   Rank,
	   Dir
	 ) :-
	E::edge{ id => EId,
		 source => N::node{},
		 target => T::node{ cluster => cluster{ left => Left, right => Right } },
		 deriv => Derivs
	       },
	(   edge_to_right(EId) ->
	    Dir = right,
	    (	source2edge(
			    E1::edge{ id => EId1,
				      source => N,
				      target => T1::node{ cluster => cluster{ left => Left1,
									      right => Right1
									    }
							},
				      deriv => Derivs1
				    }
			   ),
		EId1 \== EId,
		edge_to_right(EId1),
		Right1 < Left,
		\+ ( source2edge(
				 E2::edge{ id => EId2,
					   source => N,
					   deriv => Derivs2,
					   target => T2::node{ cluster => cluster{ left => Left2,
										   right => Right2
										 }
							     }}
				),
		     EId1 \== EId2,
		       Right2 < Left,
		       Right1 =< Left2,
		     % ( domain(_D,Derivs),
		     %   domain(_D,Derivs1),
		     %   domain(_D,Derivs2)
		     % xor fail
		     % ),
%%		      format('test ~E left=~w left1=~w right1=~w left2=~w right2=~w E=~E E1=~E E2=~E\n',[N,Left,Left1,Right1,Left2,Right2,E,E1,E2]),
		     true
		   )
	    ->	
		edge_rank(EId1,Rank1,right),
		Rank is Rank1 + 1
	    ;	
		Rank is 1
	    )
	;   
	    Dir = left,
	    (	source2edge(
			    E1::edge{ id => EId1,
				      source => N,
				      target => T1::node{ cluster => cluster{ left => Left1,
									      right => Right1
									    }
							}}
			   ),
		EId1 \== EId,
%		domain(_D,Derivs),
%		domain(_D,Derivs1),
		\+ edge_to_right(EId1),
		Right < Left1,
		\+ ( source2edge(
				 E2::edge{ source => N,
					   target => T2::node{ cluster => cluster{ left => Left2,
										   right => Right2
										 }
							     }}
				),
		     Right < Left2,
		     Right2 =< Left1,
		     EId1 \== EId2,
		     % ( domain(_D,Derivs),
		     %   domain(_D,Derivs1),
		     %   domain(_D,Derivs2) xor fail
		     % ),
		     true
		   ) ->  
		edge_rank(EId1,Rank1,left),
		Rank is Rank1 + 1
	    ;	
		Rank is 1
	    )
	),
	verbose('Edge rank ~w ~w ~E\n',[Rank,Dir,E]),
	true
	.

:-light_tabular edge_to_right/1.

edge_to_right( EId ) :-
	edge{ id => EId,
	      source => node{ cluster => cluster{ right => Right } },
	      target => node{ cluster => cluster{ left => Left } }
	    },
	Right =< Left
	.

%:-extensional use_feature_cost/0.
:-extensional use_model/0.
:-extensional templates/4.
:-extensional rtemplates/3.

%:-extensional use_cluster_feature/0.

:-light_tabular node_vmode/2.
:-mode(node_vmode/2,+(+,-)).

node_vmode( NId, VMode ) :-
	(N::node{ id => NId, cat => cat[v,aux] } xor recorded(erased(N))),
	( node2op(NId,OId),
	  check_op_top_feature(OId,mode,Mode) ->
	  true
	;
	  fail
	),
	( %chain(N::node{} >> (adj @ label['Infl',aux]) >> node{}) ->
	    source2edge(EVPP::edge{ source => N, type => adj, label => label['Infl',aux]}) xor recorded(erased(EVPP)) ->
	  VMode = 'VPP'
	 ;   %chain(N::node{} >> (adj @ label['V',modal]) >> node{ cat => v}) ->
	 source2edge(EVINF::edge{ source => N, type => adj, label => label['V',modal]}) xor recorded(erased(EVINF)) ->
	  VMode = 'VINF'
	; Mode = infinitive ->
	  VMode = 'VINF'
	;   Mode = participle ->
	  VMode = 'VPP'
	;   Mode = gerundive ->
	  VMode = 'VPR'
	;   Mode = imperative ->
	  VMode = 'VIMP'
	;   Mode = indicative ->
	  VMode ='V'
	;   Mode = subjonctive ->
	  VMode = 'VS'
	;  Mode = conditional ->
	  VMode = 'VCOND'
	;   
	  VMode = 'V'
	).

node_vmode( NId, SemType ) :-
	(N::node{ id => NId, cat => cat[nc] } xor recorded(erased(N))),
	node2op(NId,OId),
	( check_op_top_feature(OId,semtype,SemType),
	  ( SemType = event
	  xor SemType = bodypart
	  )
	xor
	check_op_top_feature(OId,time,SemType),
	  \+ SemType = (-)
	)
	.

node_vmode( NId, Real ) :-
	(N::node{ id => NId, cat => cat[prep] } xor recorded(erased(N))),
	node2op(NId,OId),
	check_op_top_feature(OId,real,Real)
	.

:-light_tabular node_subcat/3.
:-mode(node_subcat/3,+(+,-,-)).

node_subcat(NId,Subcat,XInfo) :-
%    format('node_subcat ~w\n',[NId]),
	(
	 (N::node{ id => NId, cat => v, deriv => DIds } xor recorded(erased(N))) ->
	     ( ( fail, alive_deriv(DIds,DId) -> true ; domain(DId,DIds) ),
	       deriv2htid(DId,HTId),
	       ( _HT::hypertag{ id => HTId, ht => ht{ arg0 => arg{ function => Fun0, kind => Kind0, real => Real0, pcas => PCas0, extracted => X0 },
						  arg1 => arg{ function => Fun1, kind => Kind1, real => Real1, pcas => PCas1, extracted => X1 },
						  arg2 => arg{ function => Fun2, kind => Kind2, real => Real2, pcas => PCas2, extracted => X2 },
						  imp => Imp,
						  refl => Refl,
						  distrib => Distrib
						}}
	   xor recorded(erased(_HT))
	   )
	 ->
%	     format('nid=~w did=~w htid=~w => fun0=~w fun1=~w fun2=~w\n',[NId,DId,HTId,Fun0,Fun1,Fun2]),
	   Fun0 ?= '-',
	   Fun1 ?= '-',
	   Fun2 ?= '-',
	   Kind0 ?= '-',
	   Kind1 ?= '-',
	   Kind2 ?= '-',
	   Real0 ?= '-',
	   Real1 ?= '-',
	   Real2 ?= '-',
	   Imp ?= '-',
	   Refl ?= '-',
	   Distrib ?= '-',
	   PCas0 ?= '-',
	   PCas1 ?= '-',
	   PCas2 ?= '-',
	   X0 ?= '-',
	   X1 ?= '-',
	   X2 ?= '-',
	   ( X0=extraction[rel,topic,wh,cleft] -> Extracted = 0, XType = X0
	   ; X1=extraction[rel,topic,wh,cleft] -> Extracted = 1, XType = X1
	   ; X2=extraction[rel,topic,wh,cleft] -> Extracted = 2, XType = X2
	   ; Extracted = (-), XType = (-)
	   ),
	   name_builder('~w_~w_~w_~w_~w_~w',
		       [Imp,
			Refl,
			Distrib,
			Fun0,
			Fun1,
			Fun2
		       ],
		       Subcat),
%	   format('subcat ~w\n',[Subcat]),
	   (Extracted = (-) ->
	    XInfo = 'none'
	   ;
	    name_builder('~w_~w',[Extracted,XType],XInfo)
	   )
	 ;
	   Subcat = 'none', XInfo = 'none'
	 )
	;
	 Subcat = 'none', XInfo = 'none'
	)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Feature templates (for statistical models)

:-extensional feature_map/2.
:-extensional sfeature_map/2.
:-extensional rfeature_map/2.

feature_map(
	    [type: Type,
	     label: Label,
	     rank: Rank,
	     dir: Dir,
	     delta: Delta,
	     inbetween: Between,
	     height: Height,
	     depth: Depth,
	     source:  [ cat: SCat,
			xcat: SXCat,
			form: SForm,
			lemma: SLemma,
			cap: SCap,
			(tree): STree,
			vmode: SVMode,
			root: SRoot,
			subcat: SSubcat,
			xinfo: SXInfo,
			pos: _,
			postag: SPosTag,
			deprel: SDepRel,
			depdelta: SDepDelta,
			form_features: [ form: SForm2,
					 cluster: SCluster,
					 suff: SSuff,
					 cats: SCats
				       ],
			position_features:  [ pfeatures: [
							  form: PSForm,
							  cluster: PSCluster,
							  suff: PSSuff,
							  cats: PSCats
							 ],
					      ppfeatures: [
							   form: PPSForm,
							   cluster: PPSCluster,
							   suff: PPSSuff,
							   cats: PPSCats
							  ],
					      nfeatures: [
							  form: NSForm,
							  cluster: NSCluster,
							  suff: NSSuff,
							  cats: NSCats
							 ]
					    ],
			msf: SMSF,
			vec: SVec
		      ],
	     target: [
		      cat: TCat,
		      xcat: TXCat,
		      form: TForm,
		      lemma: TLemma,
		      cap: TCap,
		      (tree): TTree,
		      vmode: TVMode,
		      root: TRoot,
		      subcat: TSubcat,
		      xinfo: TXInfo,
		      pos: Pos,
		      postag: TPosTag,
		      deprel: TDepRel,
		      depdelta: TDepDelta,
		      form_features: [ form: TForm2,
				       cluster: TCluster,
				       suff: TSuff,
				       cats: TCats
				     ],
		      position_features: [ pfeatures: [
						       form: PTForm,
						       cluster: PTCluster,
						       suff: PTSuff,
						       cats: PTCats
						      ],
					   ppfeatures: [
							form: PPTForm,
							cluster: PPTCluster,
							suff: PPTSuff,
							cats: PPTCats
						       ],
					   nfeatures: [
						       form: NTForm,
						       cluster: NTCluster,
						       suff: NTSuff,
						       cats: NTCats
						      ]
					 ],
		      msf: TMSF,
		      vec: TVec
	     ],

	     xbrothers: Brothers

	    ],

	    [
	     label: Label,
	     type: Type,
	     slemma: SLemma,
	     tlemma: TLemma,
	     sform2: SForm2,
	     sform: SForm,
	     tform2: TForm2,
	     tform: TForm,
	     scat: SCat,
	     tcat: TCat,
	     sxcat: SXCat,
	     txcat: TXCat,
	     tvmode: TVMode,
	     svmode: SVMode,
	     dir: Dir,
	     pos: Pos,
	     delta: Delta,
	     rank: Rank,
	     tcap: TCap,
	     scap: SCap,
	     psform: PSForm,
	     nsform: NSForm,
	     ptform: PTForm,
	     ntform: NTForm,
	     ppsform: PPSForm,
	     pptform: PPTForm,

	     spostag: SPosTag,
	     tpostag: TPosTag,
	     sdeprel: SDepRel,
	     sdepdelta: SDepDelta,
	     tdeprel: TDepRel,
	     tdepdelta: TDepDelta,
	     
	     pscats: PSCats,
	     nscats: NSCats,
	     ptcats: PTCats,
	     ntcats: NTCats,
	     ppscats: PPSCats,
	     pptcats: PPTCats,

	     scats: SCats,
	     tcats: TCats,
	     
	     ssuff: SSuff,
	     tsuff: TSuff,
	     ssubcat: SSubcat,
	     tsubcat: TSubcat,
	     sxinfo: SXInfo,
	     txinfo: TXInfo,
	     tcluster: TCluster,
	     scluster: SCluster,
	     pscluster: PSCluster,
	     nscluster: NSCluster,
	     ptcluster: PTCluster,
	     ntcluster: NTCluster,
	     ppscluster: PPSCluster,
	     pptcluster: PPTCluster,
	     ttree: TTree,
	     stree: STree,
	     inbetween: Between,
	     height: Height,
	     depth: Depth,
	     sroot: SRoot,

	     smsf: SMSF,
	     tmsf: TMSF,

	     svec: SVec,
	     tvec: TVec,

	     xbrothers: Brothers
	     
	     ]
	   ).

sfeature_map(
	     [ type: SType,
	       label: SLabel,
	       rank: SRank,
	       dir: SDir,
	       delta: SDelta,
	       inbetween: SBetween,
	       height: SHeight,
	       depth: SDepth,
	       source: [ cat: SSCat,
			 xcat: SSXCat,
			 form: SSForm,
			 lemma: SSLemma,
			 cap: SSCap,
			 (tree): SSTree,
			 vmode: SSVMode,
			 root: SSRoot,
			 subcat: SSSubcat,
			 xinfo: SSXInfo,
			 pos: _,
			 postag: SSPosTag,
			 deprel: SSDepRel,
			 depdelta: SSDepDelta,
			 form_features: [ form: SSForm2,
					  cluster: SSCluster,
					  suff: SSSuff,
					  cats: SSCats
					],
			 position_features: [ pfeatures: [ form: PSSForm,
							   cluster: PSSCluster,
							   suff: PSSSuff,
							   cats: PSSCats
							 ],
					      ppfeatures: [
							   form: PPSSForm,
							   cluster: PPSSCluster,
							   suff: PPSSSuff,
							   cats: PPSSCats
							  ],
					      nfeatures: [
							  form: NSSForm,
							  cluster: NSSCluster,
							  suff: NSSSuff,
							  cats: NSSCats
							 ]
					    ],
			 msf: SSMSF,
			 vec: SSVec
		       ],
	       target: _,
	       xbrothers: _
	     ],
	     [
	      stype: SType,
	      sslemma: SSLemma,
	      sscat: SSCat,
	      ssform: SSForm,
	      sssuff: SSSuff,
	      sssubcat: SSSubcat,
	      ssxinfo: SSXInfo,
	      sscluster: SSCluster,
	      sstree: SSTree,
	      sdelta: SDelta,
	      srank: SRank,
	      sdir: SDir,
	      slabel: SLabel,
	      ssroot: SSRoot,
	      pssform: PSSForm,
	      nssform: NSSForm,
	      ppssform: PPSSForm,

	      sspostag: SSPosTag,
	      ssdeprel: SSDepRel,
	      ssdepdelta: SSDepDelta,
	      
	      psscats: PSSCats,
	      nsscats: NSSCats,
	      ppsscats: PPSSCats,
	      sscats: SSCats,
	      
	      psscluster: PSSCluster,
	      nsscluster: NSSCluster,
	      ppsscluster: PPSSCluster,
	      ssvmode: SSVMode,
	      ssmsf: SSMSF,
	      ssvec: SSVec
	      ]
	    )
.

rfeature_map(
	[
	    children: Children,
	    nedges: R_NEdges,
	    nadj: R_NAdj,
	    hfeatures: [ cat: R_HCat,
			 xcat: R_HXCat,
			 form: R_HForm,
			 lemma: R_HLemma,
			 cap: _,
			 (tree): R_HTree,
			 vmode: R_HVMode,
			 root: R_HRoot,
			 subcat: R_HSubcat,
			 xinfo: R_HXInfo,
			 pos: R_HPos,
			 postag: R_HPosTag,
			 deprel: R_HDepRel,
			 depdelta: R_HDepDelta,
			 form_features:  [ form: R_HForm2,
					   cluster: R_HCluster,
					   suff: R_HSuff,
					   cats: R_HCats
					 ],
			 position_features: R_HPosition_Features,
			 msf: R_HMSFeats,
			 vec: R_HVec
		       ],
	    length: R_SpanLength,
	    cpattern: R_ChildPattern,
	    cpos: R_ChildPOS,
	    cclusters: R_ChildClustersPattern,
	    cadj: R_AdjPattern,
	    first: R_First,
	    last: R_Last,
	    firstpos: R_FirstPos,
	    lastpos: R_LastPos,
	    firstcluster: R_FirstCluster,
	    lastcluster: R_LastCluster,
	    nextlastcluster: R_NextLastCluster,
	    valency: R_Valency
	],
	[
	    nedges: R_NEdges,
	    nadj: R_NAdj,
	    length: R_SpanLength,
	    cpattern: R_ChildPattern,
	    cpos: R_ChildPOS,
	    cclusters: R_ChildClustersPattern,
	    hcat: R_HCat,
	    hxcat: R_HXCat,
	    hform2: R_HForm,
	    hlemma: R_HLemma,
	    htree: R_HTree,
	    hsubcat: R_HSubcat,
	    hform: R_HForm2,
	    hcluster: R_HCluster,
	    cadj: R_AdjPattern,
	    first: R_First,
	    last: R_Last,
	    firstpos: R_FirstPos,
	    lastpos: R_LastPos,
	    firstcluster: R_FirstCluster,
	    lastcluster: R_LastCluster,
	    nextlastcluster: R_NextLastCluster,
	    valency: R_Valency
	]
    ).


:-extensional features_persistent_table/1.
      
:-light_tabular form_features/3.
:-mode(form_features/3,+(+,+,-)).

form_features(Form2,
	      Form,
	      F::[ form: AbstractForm,
		   cluster: Cluster,
		   suff: Suff,
		   cats: Cats
		 ] ) :-
    features_persistent_table(Table),
%    format('get form features table=~w <~w> <~w>\n',[Table,Form2,Form]),
    (
	local_table!recorded(Table,form_features(Form2,Form,F)),
%	format('reusing persitsnte form features ~w ~w ~w\n',[Form2,Form,F]),
	true
    xor
    form2cluster_feature(Form2,Form,Cluster),
     suffix3(Form,Suff),
     ('F'(Form,Cats) xor Cats = 0),
     ( recorded(rare_form(Form)) ->
	   AbstractForm = '#UKV'
      ;
      AbstractForm = Form
     ),
     local_table!record(Table,form_features(Form2,Form,F)),
     %	format('==> ~w\n',[F]),
     true
    )
.

:-light_tabular position_features/3.
:-mode(position_features/3,+(+,+,-)).

position_features(Left,Right,
		  [ pfeatures: PFeatures,
		    ppfeatures: PPFeatures,
		    nfeatures: NFeatures
		  ]
		 ) :-
	next_and_prev_forms(Left,Right,PForm,NForm,PPForm),
	form_features(PForm,PForm,PFeatures),
	form_features(NForm,NForm,NFeatures),
	form_features(PPForm,PPForm,PPFeatures)
	.


:-light_tabular abstract_form/2.
:-mode(abstract_form/2,+(+,-)).

abstract_form(Cat,AForm) :-
    name_builder('#UKV_~w',[Cat],AForm)
.

:-light_tabular node_features/2.
:-mode(node_features/2,+(+,-)).

node_features( NId,
	       [ cat: Cat,
		 xcat: XCat,
		 form: AbstractForm2,
		 lemma: Lemma,
		 cap: Cap,
		 (tree) : Tree,
		 vmode: VMode,
		 root: Root,
		 subcat: Subcat,
		 xinfo: XInfo,
		 pos: Pos,
		 postag: PosTag,
		 deprel: DepRel,
		 depdelta: DepDelta,
		 form_features: Form_Features,
		 position_features: Position_Features,
		 msf: MSFeats,
		 vec: Vec
	       ]
	     ) :-
	( N::node{ id => NId,
		   cat => Cat,
		   lemma => Lemma,
		   form => Form,
		   xcat => XCat,
		   tree => XTree,
		   cluster => cluster{ left => Left,
				       right => Right,
				       token => Form2,
				       lex => Lex
				     }
		 }
	xor recorded(erased(N))
	),
	( recorded(rare_form(Form2)) ->
	      abstract_form(Cat,AbstractForm2)
	 ;
	 AbstractForm2 = Form2
	),
	XCat ?= none,
	( node_vmode(NId,VMode) xor true),
	VMode ?= none,
	node_subcat(NId,Subcat,XInfo),
	node_root_status(NId,Root),
	( Left == 0 -> Pos = start
	; span_max([0,Right]) -> Pos = end
	; Pos = middle
	),
	(recorded(tree2xtree(XTree,_Tree)),
	 tree2md5(_Tree,Tree)
        xor XTree=[Tree|_]
	xor XTree = Tree
	),
	form_features(Form2,Form,Form_Features),
	position_features(Left,Right,Position_Features),
	(capitalized_cluster(Lex) -> Cap = 1 ; Cap = 0),
	%% format('test ~w\n',[N]),
	('POSTAG'(Lex,PosTag) ->
	     %	     format('%% postag ~w:~w ~w\n',[Lex,Form2,PosTag]),
	     true
	 ; Lex = [_Lex1|_Lex2],
	   'POSTAG'(_Lex1,_PosTag1) ->
	       mutable(MPosTag,_PosTag1),
	       every(( domain(_Lex,_Lex2),
		       ('POSTAG'(_Lex,_PosTag) xor _PosTag='NONE'),
		       mutable_read(MPosTag,_OldPosTag),
		       name_builder('~w+~w',[_OldPosTag,_PosTag],_NewPosTag),
		       mutable(MPosTag,_NewPosTag)
		     )),
	       mutable_read(MPosTag,PosTag),
	       true
	 ;
	 PosTag = 'NONE'
	),
	('DEP'(Lex,DepRel,DepDelta) ->
	     true
	 ;
	 DepRel = unknown,
	 DepDelta = 0
	),
%	format('lemma ~w tokens=~w postag=~w\n',[Lemma,Lex,PosTag]),
	( 'C'(Left,lemma{ cat => Cat, lemma => Lemma, top => LexTop, truelex => Lex },Right) ->
	      mutable(MSFeatsM,[],true),
	      every((
			   features_persistent_table(FTable),
			   domain(F,[number,person,tense,gender,mode,time,hum,semtype,case,numberposs]),
			   inlined_feature_arg(LexTop,F,_,V),
			   ( node2op(NId,OId),
			     check_op_top_feature(OId,F,V)
 			   xor true
			   ),
                           atomic(V),
			   (local_table!recorded(FTable,msf2feature(F,V,K))
				       xor
		            name_builder('~w:~w',[F,V],K),
			    local_table!record(FTable,msf2feature(F,V,K))
			   ),
%%			   K ::= (F:V),
			   mutable_list_extend(MSFeatsM,K),
			   true
		       )),
	      mutable_read(MSFeatsM,_MSFeats),
	      (_MSFeats = [] ->
		   MSFeats=none
	       ;
	       %	       name_builder('~U',[['~w:~w','|'],_MSFeats],MSFeats)
	       MSFeats = _MSFeats
	      ),
	      true
	 ;
	 MSFeats = none
	),
	(recorded(vector(Lemma,Cat,Vec)) xor Vec=none),
	%	format('node_feature lemma=~w msfeats=~w\n',[Lemma,MSFeats]),
	true
	.

:-light_tabular simple_edge_features/3.
:-mode(simple_edge_features/3,+(+,+,-)).

simple_edge_features(EId,
		     Left,
		     [ type: Type,
		       label: Label,
		       rank: Rank,
		       dir: Dir,
		       delta: Delta,
		       inbetween: Between,
		       height: Height,
		       depth: Depth,
		       source: Source_Features,
		       target: Target_Features,
		       xbrothers: Brothers
		     ]
		    ) :-
	(E::edge{ id => EId,
		  type => Type,
		  label => Label,
		  source => Source::node{ id => SNId, cluster => cluster{ left => SLeft, right => SRight }},
		  target => Target::node{ id => TNId, cluster => cluster{ left => TLeft, right => TRight }}
		}
	xor recorded( erased(E) )
	),
	edge_abstract_rank(EId,Rank,Dir),
	edge_delta(SLeft,Left,Dir,Delta),
	edge_min_height(EId,Height),
	edge_min_depth(EId,Depth),
	node_features(SNId,Source_Features),
	node_features(TNId,Target_Features),
	( Dir = right ->
	  in_between_ponct(SRight,TLeft,Between)
	;
	  in_between_ponct(TRight,SLeft,Between)
	),
	(edge_brothers(EId,Brothers) xor Brothers=ukw),
	true
	.

:-light_tabular edge_brothers/2.
:-mode(edge_brothers/2,+(+,-)).

edge_brothers(EId,XBrothers) :-
    (E::edge{ id => EId,
	      type => Type,
	      label => Label,
	      deriv => Derivs,
	      source => Source::node{ id => SNId, cluster => cluster{ left => SLeft, right => SRight }},
	      target => Target::node{ id => TNId, cluster => cluster{ left => TLeft, right => TRight }}
	    }
      xor recorded( erased(E) )
    ),
    mutable(BM,[],true),	% potential brothers
    every((
		 all_source2edge(E2::edge{ id => EId2,
					   deriv => Derivs2,
					   label => _Label2,
					   source => Source,
					   target => N2::node{ cluster => cluster{ left => TLeft2, right => TRight2 }}
					 }
				),
		 EId \== EId2,
		 ( TRight =< SLeft ->
		       TRight2 =< SLeft,
		       TRight =< TLeft2
		  ;
		  SRight =< TLeft2,
		  TRight2 =< TLeft
		 ),
		 \+ (
		     all_source2edge(E3::edge{ id => EId3,
					       deriv => Derivs3,
					       label => Label3,
					       source => Source,
					       target => node{ cluster => cluster{ left => TLeft3, right => TRight3 }}
					     }
				    ),
		     EId2 \== EId3,
		     EId \== EId3,
		     ( TRight =< SLeft ->
			   TRight =< TLeft3,
			   TRight3 =< TLeft2
		      ;
		      TRight3 =< TLeft,
		      TRight2 =< TLeft3
		     ),
%		     have_shared_derivs(EId,EId3),
%		     have_shared_derivs(EId2,EId3),
%format('Try l1=~w l2=~w l3=~w d1=~w d2=~w d3=~w\n',[Label,_Label2,Label3,Derivs,Derivs2,Derivs3]),
		     \+ '$interface'('Deriv_At_Most_Two_Shared'(Derivs:term,Derivs2:term,Derivs3:term),[]),
		     true
		 ),
		 have_shared_derivs(EId,EId2),
		 (TLeft2 == TRight2,
		  all_source2edge(edge{ type => subst, source => N2, type => subst, label => Label2 }) ->
		      true
		  ;
		  Label2 = _Label2
		 ),
		 mutable_read(BM,_OldBrothers),
		 list_sorted_add(Label2,_OldBrothers,_NewBrothers),
		 mutable(BM,_NewBrothers)
	     )),
    mutable_read(BM,Brothers),
    (Brothers = [] -> XBrothers = none
     ; Brothers = [XBrothers] -> true
     ; name_builder('~L',[['~w','|'],Brothers],XBrothers)
    ),					
    %    format('edge brothers ~w => ~w\n',[E,XBrothers]),
    true
.

%:-light_tabular constrained_edge_features/5.
%:-mode(constrained_edge_features/5,+(+,+,+,-,-)).

:-xcompiler
constrained_edge_features(EId,
			  _Cst,
			  _Name,
			  Values
			 ) :-
	edge{ id => EId, target => node{ cluster => cluster{ left => Left }} },
	simple_edge_features(EId,Left,Features),
	( _Cst = [] ->
	      templates([],Features,_Name,Values),
%	      verbose_cost('cost features features=~w name=~w values=~w\n',[Features,_Name,Values]),
	      true
	;
	  simple_edge_features(_Cst,Left,SFeatures),
	  templates(SFeatures,Features,_Name,Values)
	)
	.

:-xcompiler
edge_delta_old(SLeft,TLeft,Dir,Delta) :-
	_Delta is abs(SLeft-TLeft),
	( _Delta > 15 -> Delta1 = 15
	;   _Delta > 6 -> Delta1 = 6
	 %%	;   _Delta > 3 -> Delta = 3
	;   Delta1 = _Delta
	),
	( Dir == left -> Delta is - Delta1 ; Delta = Delta1 ),
	true
	.

:-xcompiler
edge_delta(SLeft,TLeft,Dir,Delta) :-
    _Delta is abs(SLeft-TLeft),
    (_Delta > 30 -> Delta1 = 30
     ; _Delta > 20 -> Delta1 is (_Delta / 3) * 3
     ; _Delta > 15 -> Delta1 is (_Delta / 2) * 2
     ; Delta1 = _Delta
    ),
    ( Dir == left -> Delta is - Delta1 ; Delta = Delta1 ),
    true
	.


:-xcompiler
edge_abstract_rank(EId,Rank,Dir) :-
	edge_rank(EId,_Rank,Dir),
	( _Rank > 10 -> Rank = 10
	;   _Rank > 5 -> Rank = 5
	;   Rank = _Rank
	)
	.

:-light_tabular in_between_ponct/3.
:-mode(in_between_ponct/3,+(+,+,-)).

in_between_ponct(Left,Right,Between) :-
	in_between_ponct_aux(Left,Right,_Between),
	( _Between = [] -> Between = none
	;
	  name_builder('~L',[['~w','_'],_Between],Between)
	)
	.

:-std_prolog in_between_ponct_aux/3.

in_between_ponct_aux(Left,Right,Between) :-
	( Left >= Right -> Between = []
	; 'C'(Left,lemma{ lemma => Lemma, cat => cat[ponctw,poncts] },Left2) ->
	  in_between_ponct_aux(Left2,Right,Between2),
	  Between = [Lemma|Between2]
	;
	  Left2 is Left+1,
	  in_between_ponct_aux(Left2,Right,Between)
	)
	.

:-xcompiler
sqlite!inlined_reset_and_bind(PStmt,Values) :-
        '$interface'('DyALog_sqlite3_reset_and_bindvalues'(PStmt:ptr,Values:term),[])
        .

:-xcompiler
sqlite!inlined_tuple(PStmt,Row) :-
        '$interface'('DyALog_sqlite3_tuple'(PStmt:ptr,Row:term),[choice_size(0)])
        .


:-light_tabular cluster_feature_db/1.
:-mode(cluster_feature_db/1,+(-)).

cluster_feature_db(DB) :-
	recorded(cluster_feature_table(File)),
	sqlite!open_readonly(File,DB)
	.

:-light_tabular cluster_feature_stmt/1.
:-mode(cluster_feature_stmt/1,+(-)).

cluster_feature_stmt(PStmt) :-
	cluster_feature_db(DB),
	recorded(cluster_feature_prepare(Stmt)),
	sqlite!prepare(DB,Stmt,PStmt)
	.

:-light_tabular form2cluster_feature/3.
:-mode(form2cluster_feature/3,+(+,+,-)).

form2cluster_feature(Form,NodeForm,Cluster) :-
%	format('form2cluster <~w> <~w>\n',[Form,NodeForm]),
	( ( Form \== '',
	    cluster_feature_stmt(Stmt),
	    sqlite!inlined_reset_and_bind(Stmt,[Form]),
	    sqlite!inlined_tuple(Stmt,[Cluster])
	  ) ->
	  true
	;
	  ( NodeForm \== '',
	    cluster_feature_stmt(Stmt),
	    sqlite!inlined_reset_and_bind(Stmt,[NodeForm]),
	    sqlite!inlined_tuple(Stmt,[Cluster])
	  ) ->
	  true
	; simple_form2cluster_feature(NodeForm,Cluster) ->
	  true
	; recorded(cluster_feature_table(_)) ->
	  Cluster = -1		% unknown cluster for this word
	;
	  Cluster = -2		% not using cluster information
	),
%	format('=>~w\n',[Cluster]),
	true
	.

:-extensional simple_form2cluster_feature/2.

simple_form2cluster_feature(date[],-2).
simple_form2cluster_feature(entities['_NUMBER'],-3).
simple_form2cluster_feature(entities['_NUM','_ROMNUM'],-4).
simple_form2cluster_feature(entities['_PERSON','_PERSON_m','_PERSON_f'],-5).
simple_form2cluster_feature(entities['_LOCATION'],-6).
simple_form2cluster_feature(entities['_COMPANY','_ORGANIZATION'],-7).
simple_form2cluster_feature(entities['_PRODUCT'],-8).

:-light_tabular brother_edge_cost/4.
:-mode(brother_edge_cost/4,+(+,+,+,-)).

brother_edge_cost(EId,BEId,Dir,W) :-
%	format('try brother cost eid=~w beid=~w dir=~w\n',[EId,BEId,Dir]),
	edge{ id => EId, target => node{ cluster => cluster{ left => TLeft }}},
	simple_edge_features(EId,TLeft,
			     Features :: [type: Type,
					  label: Label,
					  rank: Rank,
					  dir: Dir,
					  delta: Delta,
					  inbetween: Between,
					  height: Height,
					  depth: Depth,
					  source:  [ cat: SCat,
						     xcat: SXCat,
						     form: SForm,
						     lemma: SLemma,
						     cap: SCap,
						     (tree): STree,
						     vmode: SVMode,
						     root: SRoot,
						     subcat: SSubcat,
						     xinfo: SXInfo,
						     pos: _,
						     form_features: [ form: SForm2,
								      cluster: SCluster,
								      suff: SSuff
								    ],
						     position_features:  [ pfeatures: [
										       form: PSForm,
										       cluster: PSCluster,
										       suff: PSSuff
										      ],
									   ppfeatures: [
											form: PPSForm,
											cluster: PPSCluster,
											suff: PPSSuff
										       ],
									   nfeatures: [
										       form: NSForm,
										       cluster: NSCluster,
										       suff: NSSuff
										      ]
									 ]
						   ],
					  
					  target: [
						   cat: TCat,
						   xcat: TXCat,
						   form: TForm,
						   lemma: TLemma,
						   cap: TCap,
						   (tree): TTree,
						   vmode: TVMode,
						   root: TRoot,
						   subcat: TSubcat,
						   xinfo: TXInfo,
						   pos: Pos,
						   form_features: [ form: TForm2,
								    cluster: TCluster,
								    suff: TSuff
								  ],
						   position_features: [ pfeatures: [
										    form: PTForm,
										    cluster: PTCluster,
										    suff: PTSuff
										   ],
									ppfeatures: [
										     form: PPTForm,
										     cluster: PPTCluster,
										     suff: PPTSuff
										    ],
									nfeatures: [
										    form: NTForm,
										    cluster: NTCluster,
										    suff: NTSuff
										   ]
								      ]
					  ],

					  xbrothers:  Brothers
					 
					 ]),
	( EId == BEId ->
	  BType = none
	;
	  edge{ id => BEId, target => node{ cluster => cluster{ left => BTLeft } } },
	  simple_edge_features(BEId,BTLeft,
			       BTFeatures :: [ type: BType,
					       label: BLabel,
					       rank: BRank,
					       dir: BDir,
					       delta: BDelta,
					       inbetween: BBetween,
					       height: BHeight,
					       depth: BDepth,
					       source:  _,
					  
					       target: [
							cat: BTCat,
							xcat: BTXCat,
							form: BTForm,
							lemma: BTLemma,
							cap: BTCap,
							(tree): BTTree,
							vmode: BTVMode,
							root: _,
							subcat: BTSubcat,
							xinfo: BTXInfo,
							pos: BPos,
							form_features: [ form: BTForm2,
									 cluster: BTCluster,
									 suff: BTSuff
								       ],
							position_features: [ pfeatures: [
											 form: BPTForm,
											 cluster: BPTCluster,
											 suff: BPTSuff
											],
									ppfeatures: [
										     form: BPPTForm,
										     cluster: BPPTCluster,
										     suff: BPPTSuff
										    ],
									nfeatures: [
										    form: BNTForm,
										    cluster: BNTCluster,
										    suff: BNTSuff
										   ]
									   ]
					       ],

					       xbrothers: _
					     
					     ]
			      )
	),
	mutable(MW,0,true),
	every((
%	       feature_cost_rlt(brother,Label,Type,RLT_Id),
	       ( Key = features_btype, Values = [BType]
	       ; ( BType \== none,
		   (
		    Key = features_blabel_btype_dir_btcat_scat_tcat, Values = [BLabel,BType,Dir,BTCat,SCat,TCat]
		   ; Key = features_blabel_btype_dir_btcat_tcat, Values = [BLabel,BType,Dir,BTCat,TCat]
		   ; Key = features_blabel_btype_dir_scat, Values = [BLabel,BType,Dir,SCat]
		   ; Key = features_blabel_btype_dir_scat_ssubcat, Values = [BLabel,BType,Dir,SCat,SSubcat]
		   ; Key = features_blabel_btype_dir_stree, Values = [BLabel,BType,Dir,STree]
		   ; Key = features_blabel_btype_dir_stree_bttree_ttree, Values = [BLabel,BType,Dir,STree,BTTree,TTree]
		   ; Key = features_blabel_btype_dir_scat_slemma_tcat_tlemma_btcat_btlemma,
		    Values = [BLabel,BType,Dir,SCat,SLemma,TCat,TLemma,BTCat,BTLemma]
		   ; Key = features_blabel_btype_dir_tcat_tlemma_btcat_btlemma,
		    Values = [BLabel,BType,Dir,TCat,TLemma,BTCat,BTLemma]
		   ; Key = features_blabel_btype_dir_scat_scluster_tcat_tcluster_btcat_btcluster,
		    Values = [BLabel,BType,Dir,SCat,SCluster,TCat,TCluster,BTCat,BTCluster]
		   ; Key = features_blabel_btype_dir_tcat_tcluster_btcat_btcluster,
		    Values = [BLabel,BType,Dir,TCat,TCluster,BTCat,BTCluster]
		   ; Key = features_blabel_btype_dir_scat_scluster,
		    Values = [BLabel,BType,Dir,TCat,TCluster,SCat,SCluster]
		   )
		 )
	       ),
	       fail,		% 2013/10/05 To be updated using models rather than SQLite
	       % feature_cost_check(Key,RLT_Id,Values,_W),
	       % format('\tbrother cost eid=~w beid=~w dir=~w key=~w => w=~w\n',[EId,BEId,Dir,Key,_W]),
	       mutable_add(MW,_W)
	      )),
	mutable_read(MW,W),
%	format('brother cost eid=~w beid=~w dir=~w tleft=~w:~w sleft=~w:~w bleft=~w label=~w => w=~w\n',[EId,BEId,Dir,TLeft,TForm,SLeft,SForm,BLeft,Label,W]),
	true
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Getting morpho-syntactic features (from op elements)
%% and hypertag features (from hypertag elements)

:-light_tabular check_node_top_feature/3.
:-mode(check_node_top_feature/3,+(+,+,-)).

check_node_top_feature(NId,F,V) :-
	( node2op(NId,OId),
	  check_op_top_feature(OId,F,V)
	; \+ node2op(NId,_),
	  recorded(node2top(NId,Top)),
	  inlined_feature_arg(Top,F,_,V)
	)
	.

:-light_tabular check_op_top_feature/3.
:-mode(check_op_top_feature/3,+(+,+,-)).

check_op_top_feature(OId,F,V) :-
	(Op::op{ id  => OId, top => Top }
	xor recorded(Op)
	),
%	format('check op f=~w v=~w oid=~w top=~w\n',[F,V,OId,Top]),
	inlined_feature_arg(Top,F,_,V),
%	format('success op f=~w v=~w oid=~w top=~w\n',[F,V,OId,Top]),
	true
	.

:-light_tabular check_ht_feature/3.
:-mode(check_ht_feature/3,+(+,+,-)).

check_ht_feature(HTId,F,V) :-
	HT::hypertag{ id => HTId, ht => _HT::ht{} },
	inlined_feature_arg(_HT,F,_,V)
	.

:-light_tabular check_arg_feature/4.
:-mode(check_arg_feature/4,+(+,+,+,-)).

check_arg_feature(HTId,Arg,F,V) :-
	HT::hypertag{ id => HTId, ht => _HT::ht{} },
	inlined_feature_arg(_HT,Arg,_,ArgFs),
	inlined_feature_arg(ArgFs,F,_,V)
	.

:-light_tabular check_xarg_feature/5.
:-mode(check_xarg_feature/5,+(+,+,-,-,-)).

check_xarg_feature(HTId,Arg,Function,Kind,Real) :-
    hypertag{ id => HTId, ht => _HT::ht{} },
%    format('hypertag ~w : ~w\n',[HTId,_HT]),
    inlined_feature_arg(_HT,Arg,I,ArgFs::arg{ function => Function, kind => Kind, real => Real}),
    Arg = args[],
%    format('test0 HId=~w i=~w arg=~w fs=~w fun=~w real=~w\n',[HTId,I,Arg,ArgFs,Function,Real]),
    true
.

:-light_tabular check_arg_allfeatures/3.
:-mode(check_arg_allfeatures/3,+(+,+,-)).

check_arg_allfeatures(HTId,Arg,ArgFs) :-
    hypertag{ id => HTId, ht => _HT::ht{} },
    inlined_feature_arg(_HT,Arg,_,ArgFs::arg{})
.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Restrictions

:-light_tabular prepare_restriction/1.

prepare_restriction([PStmt1,PStmt2,PStmt3,PStmt4,PStmt5,PStmt6,PStmt7]) :-
	recorded( opt(restrictions,DB) ),
	sqlite!prepare(DB,
		       'select dep.weight
    from lemma as source,
         lemma as rel,
         lemma as target,
         dep
    where source.lemma=? and source.pos=?
      and target.lemma=? and target.pos=?
      and rel.lemma=? and rel.pos=?
      and dep.source=source.rowid
      and dep.rel=rel.rowid
      and dep.target=target.rowid',PStmt1),
	
	sqlite!prepare(DB,
		       'select dep2.weight
    from lemma as source,
         lemma as rel,
         dep2
    where source.lemma=? and source.pos=?
      and rel.lemma=? and rel.pos=?
      and dep2.source=source.rowid
      and dep2.rel=rel.rowid',PStmt2),

	sqlite!prepare(DB,
		       'select dep3.weight
    from lemma as target,
         lemma as rel,
         dep3
    where target.lemma=? and target.pos=?
      and rel.lemma=? and rel.pos=?
      and dep3.target=target.rowid
      and dep3.rel=rel.rowid',PStmt3),

	sqlite!prepare(DB,
		       'select form2lemma.w
		      from form, lemma, form2lemma
		      where form.form=? and lemma.lemma=? and lemma.pos=?
		        and form2lemma.form=form.rowid
		        and form2lemma.lemma=lemma.rowid', PStmt4),

	sqlite!prepare(DB,
		       'select sim.weight
		      from lemma as l1, lemma as l2, sim
		      where l1.lemma=? and l1.pos=?
		      and l2.lemma=? and l2.pos=?
		      and l1.rowid=sim.l1 and l2.rowid=sim.l2', PStmt5),


	sqlite!prepare(DB,
		       'select term.weight
    from lemma as source,
         lemma as rel,
         lemma as target,
         term
    where source.lemma=? and source.pos=?
      and target.lemma=? and target.pos=?
      and rel.lemma=? and rel.pos=?
      and term.source=source.rowid
      and term.rel=rel.rowid
      and term.target=target.rowid',PStmt6),

	( recorded(opt(restrictions2,DB2)) ->
	      sqlite!prepare(DB2,
			     ' select dep.npmi/10
from lemma as source,
     lemma as rel,
     lemma as target,
     dep
where source.lemma=? and source.pos=?
  and target.lemma=? and target.pos=?
  and rel.lemma=? and rel.pos=?
  and dep.source=source.rowid
  and dep.rel=rel.rowid
  and dep.target=target.rowid', PStmt7)
	 ; PStmt7 = []
	)

	.

:-light_tabular check_restriction/6.
:-mode(check_restriction/6,+(+,+,+,+,+,-)).

check_restriction(Source,SCat,Target,TCat,Rel,W) :-
	'$answers'(prepare_restriction([PStmt1,
					PStmt2,
					PStmt3,
					PStmt4,
					PStmt5,
					PStmt6,
					PStmt7
				       |_])),
	sqlite!inlined_reset_and_bind(PStmt1,[Source,SCat,Target,TCat,Rel,prep]),
	sqlite!inlined_reset_and_bind(PStmt2,[Source,SCat,Rel,prep]),
	sqlite!inlined_reset_and_bind(PStmt3,[Target,TCat,Rel,prep]),
	sqlite!inlined_reset_and_bind(PStmt6,[Source,SCat,Target,TCat,Rel,prep]),
	(PStmt7 = [] xor sqlite!inlined_reset_and_bind(PStmt7,[Source,SCat,Target,TCat,Rel,rel])),
%%	format('Bind ~w\n',[L]),
	verbose('restr tried ~w_~w ~w_~w ~w_~w\n',[Source,SCat,Rel,prep,Target,TCat]),
%%	format('*** restr tried ~w_~w ~w_~w ~w_~w\n',[Source,SCat,Rel,prep,Target,TCat]),
	(   sqlite!inlined_tuple(PStmt2,[W2]) xor W2=0),
	(   sqlite!inlined_tuple(PStmt3,[W3]) xor W3=0),
	(   sqlite!inlined_tuple(PStmt1,[W1]) xor W1=0),
	(   sqlite!inlined_tuple(PStmt6,[W6]) xor W6=0),
	(   PStmt7 \== [], sqlite!inlined_tuple(PStmt7,[W7]) ->
		verbose('pstmt7 cost ~w ~w ~w => ~w\n',[Source,Target,Rel,W7]),
		true
	 ;
	 W7=0
	),
%	W is W1+W2+W3+2*W6,
	W is 2*(W1+W7)+W3+2*W6,
%	W >= 10,
	W > 0,
%%	format('Restriction found for source=~w target=~w rel=~w => ~w w1=~w w2=~w w3=~w w6=~w\n',[Source,Target,Rel,W,W1,W2,W3,W6]),
	verbose('Restriction found for source=~w target=~w rel=~w => ~w\n',[Source,Target,Rel,W])
	.

:-light_tabular check_term/6.

check_term(Source,SCat,Target,TCat,Rel,W) :-
	'$answers'(prepare_restriction([PStmt1,
					PStmt2,
					PStmt3,
					PStmt4,
					PStmt5,
					PStmt6
				       |_])),
	sqlite!inlined_reset_and_bind(PStmt6,[Source,SCat,Target,TCat,Rel,prep]),
	(   sqlite!inlined_tuple(PStmt6,[_W]) xor _W=0),
	_W >= 10,
	W is - _W,
	verbose('Term restriction found for source=~w target=~w rel=~w => ~w\n',[Source,Target,Rel,W]),
	true
	.

%:-extensional catpref/4.

:-light_tabular check_catpref/4.
:-mode(check_catpref/4,+(+,+,+,-)).

check_catpref(Form,Lemma,Cat,W) :-
    (
	catpref(Form,Lemma,Cat,W),
	true
	xor
%	format('check catpref restr form=~w lemma=~w cat=~w\n',[Form,Lemma,Cat]),
	'$answers'(prepare_restriction([_,_,_,Stmt|_])),
		  sqlite!inlined_reset_and_bind(Stmt,[Form,Lemma,Cat]),
		  once((sqlite!inlined_tuple(Stmt,[W1]))),
	    (	( domain(Form,[de,des,du,'d''',le,la,les,un,une])
		;   domain(Lemma,[tout]) 
		) 
	    -> 		% delicate frequent words
		W is W1 * 7 / 2
	    ;
		W is W1 * 7
	    ),
	    %%	    format('=> W=~w\n',[W]),
	    true
	)
	.

:-light_tabular check_sim/5.
:-mode(check_sim/5,+(+,+,+,+,-)).

check_sim(L1,Cat1,L2,Cat2,W) :-
	'$answers'(prepare_restriction([_,_,_,_,Stmt|_])),
	sqlite!inlined_reset_and_bind(Stmt,[L1,Cat1,L2,Cat2]),
	once(( sqlite!inlined_tuple(Stmt,[_W]) )),
	W is _W * 3
.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% handling regional features

:-light_tabular best_parse/5.
:-mode(best_parse/5,+(+,+,+,+,-)).

:-std_prolog regional_features/4.

regional_features(NId,
		  Children,
		  Span,
		  Features::[
			      children: Childs,
			      nedges: NEdges,
			      nadj: NAdj,
			      hfeatures: HeadFeatures,
			      length: SpanLength,
			      cpattern: ChildPattern,
			      cpos: ChildPOSPattern,
			      cclusters: ChildClustersPattern,
			      cadj: CAdjPattern,
			      first: First,
			      last: Last,
			      firstpos: FirstPOS,
			      lastpos: LastPOS,
			      firstcluster: FirstCluster,
			      lastcluster: LastCluster,
			      nextlastcluster: NextLastCluster,
			      valency: Valency
			  ]
		 ) :-
%    format('try regional features nid=~w span=~w children=~w\n',[NId,Span,Children]),
    length(Children,_NEdges),
    NEdges is min(_NEdges,10),
    mutable(MAdj,0),
    mutable(MPat,[base]),
    mutable(MChildPOS,[base]),
    mutable(MChildClusters,[base]),
    mutable(MChilds,[base]),
    mutable(MCAdj,[base]),
    mutable(MLast,none),
    mutable(MFirst,[base]),
    mutable(MLabels,[]),
    mutable(MNextLastCluster,-10),
    node_features(NId,HeadFeatures),
    Span = [Left,Right|_],
    _SpanLength is Right - Left,
    (_SpanLength > 30 -> SpanLength = 30
     ; _SpanLength > 20 -> SpanLength is (_SpanLength / 3) * 3
     ; _SpanLength > 15 -> SpanLength is (_SpanLength / 2) * 2
     ; SpanLength = _SpanLength
    ),
    term_range(Left,Right,SpanRange),
    mutable(MLeft,Left),
    mutable(MEdges,[]),
    every((
		 edge_in_children(NId,
				  _E::edge{},
				  Children),
			 %			 format('Edge: ~w\n',[_E]),
			 _E2 ::= _E,
			 mutable_list_extend(MEdges,_E2)
	     )),
    mutable_read(MEdges,Edges),
    every((
		 domain(TLeft,SpanRange),
		 mutable_read(MLeft,_Left),
		 TLeft >= _Left,
		 (TRight=TLeft,
		  domain(
		      E::edge{ type => Type,
			       id => _EId,
			       label => Label,
			       target => T::node{ id => TNId, cluster => cluster{ left => TLeft, right => TRight, lex => TLex }}
			     },
		      Edges)
		  xor
		  domain(E,Edges),
		  TLeft < TRight
		 ),
%		 format('check children nid=~w span=~w left=~w right=~w T=~w\n',[NId,Span,TLeft,TRight,T]),
		 mutable(MLeft,TRight),
		 mutable(MLast,Label),
		 mutable_read(MFirst,_First),
		 ( _First = [_,_,_|_] -> true
		 ; mutable(MFirst,[Label|_First])
		 ),
		 ( Type = adj ->
%		       mutable_list_extend(MPat,Type),
		       mutable_list_extend(MPat,Label),
		       mutable_list_extend(MCAdj,Label),
		       mutable_inc(MAdj,_)
		  ;
		  mutable_list_extend(MPat,Label),
		  true
		 ),
		 mutable_list_extend(MLabels,Label),

		 ( TLeft = TRight,
		   domain(dinfo(T_OId,_EId,TNId,_),Children),
		   '$answers'(best_parse(TNId,T_OId,_,_,
					 dstruct{ deriv => T_Best_Deriv,
						  children => T_Best_Children }
					)) ->
%		       format('\there1 children=~w\n',[T_Best_Children]),
		       ( edge_in_children(TNId,
					  edge{ type => subst,
						label => Label2,
						target => node{ id => TTNId, cluster => cluster{ right => TTRight }}
					      },
					  T_Best_Children
					 ) ->
			 mutable_list_extend(MPat,Label2),
			 mutable_list_extend(MLabels,Label2),
			 mutable(MLeft,TTRight)
			; edge_in_children(TNId,
					   edge{ type => lexical,
						 label => Label2,
						 target => node{ id => TTNId, cluster => cluster{ right => TTRight } }
					       },
					   T_Best_Children
					  ) ->
			  mutable_list_extend(MPat,Label2),
			  mutable_list_extend(MLabels,Label2),
			      mutable(MLeft,TTRight)
			;
			TTNId = TNId
		       )
		  ;
		  TTNId = TNId
		 ),
		 (node{ id => TTNId, cat => prep, lemma => TTLemma},
		  domain(TTLemma,[�,de,par,avec,pour,sans]) ->
		      mutable_list_extend(MPat,TTLemma),
		      mutable_list_extend(MLabels,TTLemma)
		  ;
		  true
		 ),
		 mutable_list_extend(MChilds,TTNId),
		 node_features(TTNId,Target_Features::[cat: TTCat|_]),
		 mutable_list_extend(MChildPOS,TTCat),
		 (
		     domain((form_features:Target_Form_Features),Target_Features),
		     domain((cluster:TTCluster),Target_Form_Features),
		     mutable_list_extend(MChildClusters,TTCluster)
		 ;
		 domain((position_features: [pfeatures: _,
					     ppfeatures: _,
					     nfeatures: [form: _, cluster: _NextLastCluster |_]
					    ]),Target_Features),
		 mutable(MNextLastCluster,_NextLastCluster)
		 ),
		 true
	     )),
    mutable_read(MAdj,_NAdj),
    NAdj is min(_NAdj,8),
    mutable_read(MChilds,Childs),
    mutable_read(MPat,_Pattern), feature_list2string(_Pattern,ChildPattern),
    mutable_read(MChildPOS,_ChildPOS), feature_list2string(_ChildPOS,ChildPOSPattern),
    mutable_read(MChildClusters, _ChildClusters), feature_list2string(_ChildClusters,ChildClustersPattern),

    mutable_read(MNextLastCluster,NextLastCluster),
    
    (recorded(tree2xtree(XTree,_Tree)),
     tree2md5(_Tree,HeadTree)
             xor XTree=[HeadTree|_]
			   xor XTree = HeadTree
    ),

    list!first(_ChildPOS,FirstPOS),
    list!last(_ChildPOS,LastPOS),

    list!first(_ChildClusters,FirstCluster),
    list!last(_ChildClusters,LastCluster),

    
    mutable_read(MLast,Last),
    mutable_read(MFirst,XFirst),
    name_builder('~L',[['~w','_'],XFirst],First),
    mutable_read(MCAdj,_CAdj), feature_list2string(_CAdj,CAdjPattern),

    mutable_read(MLabels,_Labels),
    '$interface'('Label_Count'(_Labels:term,Valency:term),[]),
%    format('regional pattern=~w valence=~w\n',[_Labels,Valency]),
    
    %    format('regional nid=~w children=~w => features=~w\n',[NId,Children,Features]),
    true
.

:-xcompiler
list_truncate(L,TL) :-
    (L=[X1,X2,X3,X4,X5,X6,X7|_] -> TL=[X1,X2,X3,X4,X5,X6,cut] ; TL = L)
.

:-std_prolog list!last/2.

list!last(L,Last) :-
    (L=[_] -> Last = none
    ; L=[Last,_] -> true
    ; L=[_|L2],
      list!last(L2,Last)
    )
.

:-xcompiler
list!first(L,First) :-
    (L=[First|_] -> true
    ; First = none
    )
    .

:-xcompiler
feature_list2string(List,String) :-
    features_persistent_table(FTable),
    ( local_table!recorded(FTable,list2string(List,String)) -> true
    ;
    list_truncate(List,TruncatedList),
    name_builder('~L',[['~w','_'],TruncatedList],String),
    local_table!record(FTable,list2string(List,String))
    ).
    

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% handling templates

:-std_prolog expand_to_symbol/2.

expand_to_symbol(FNames,Smb) :-
	( FNames = [Smb] -> true
	; FNames = [F|FNames2],
	  expand_to_symbol(FNames2,Smb2),
	  name_builder('~w_~w',[Smb2,F],Smb)
	)
	.

:-std_prolog templates2values_disj/4.

templates2values_disj(FNames,Map,Values,FSet) :-
	( FNames = [FNames1|FNames2] ->
	  templates2values(FNames1,Map,Values1,FSet),
	  templates2values_disj(FNames2,Map,Values2,FSet),
	  Values = [Values1|Values2]
	;
	  Values = []
	)
	.

:-std_prolog templates2values/4.

templates2values(FNames,Map,Values,FSet) :-
%%	format('templates2values fnames=~w map=~w\n',[FNames,Map]),
	( FNames = true ->
	  ( expand_to_symbol(FSet,Smb)
	  xor format('*** could not expand smb ~w\n',[FSet]), fail
	  ),
	  verbose('template=~w\n',[Smb]),
	  domain(rule:Rule,Map),
	  Values = [Smb,Rule]
	; FNames = [_|_] ->
	  ( templates2values_disj(FNames,Map,Values1,FSet) xor format('*** could not expand flist ~w\n',[FNames]), fail),
	  ( Values1 = [Values]
	  -> true
	  ;
	    Values =.. [disj|Values1]
	  )
	; FNames = (F:FNames2) ->
	  ( templates2values(FNames2,Map,Values2,[F|FSet]) xor format('*** could not expand flist2 ~w\n',[FNames2]), fail),
	  ( domain(F:V,Map) ->
	    Values = [V|Values2]
	  ;
	    format('*** warning: feature ~w not found in map\n',[F]),
	    fail
	  )
	;
	  format('*** bad format ~w\n',[FNames]),
	  fail
	)
	.

:-std_prolog template_add/4.

template_add(F,T,Templates,XTemplates)	:-
%	format('try add F=~w T=~w to ~w\n',[F,T,Templates]),
	( Templates = [] ->
	  XTemplates = [F:XT],
	  template_add(T,[],XT)
	; Templates = [true|Templates2] ->
	  template_add(F,T,Templates2,XTemplates2),
	  XTemplates = [true|XTemplates2]
	; Templates = [F1:T1|Templates2],
	  ( F = F1 ->
	    ( T = true ->
	      ( domain(T,T1) ->
		XTemplates = Templates
	      ;
		XTemplates = [F1:[true|T1]|Templates2]
	      )
	    ;
	      ( T=XF:XT ->
		template_add(XF,XT,T1,XT1)
	      ;
		template_add(T,true,T1,XT1)
	      ),
	      XTemplates=[F1:XT1|Templates2]
	    )
	  ;
	    template_add(F,T,Templates2,XTemplates2),
	    XTemplates = [F1:T1|XTemplates2]
	  )
	),
%	format('added F=~w T=~w to ~w => ~w\n',[F,T,Templates,XTemplates]),
	true
	.

:-std_prolog template_add/3.

template_add(T,XT1,XT2) :-
	( T=true ->
	  ( domain(true,XT1) -> XT2 = XT1
	  ; XT2 = [true|XT1]
	  )
	; T = F:T2 ->
	  template_add(F,T2,XT1,XT2)
	;
	  template_add(T,true,XT1,XT2)
	)
	.

:-rec_prolog generate_templates.

generate_templates :-
	feature_map(Features,Map),
	verbose('handling templates\n',[]),
	mutable(M,0),
	mutable(MT,[]),
	every((
	       template(Template),
%	       Template = (label:type:_Template),
	       mutable_inc(M,_),
	       mutable_read(MT,_T1),
%%	       format('try add ~w\n',[Template]),
	       template_add(Template,_T1,_T2), % build template tree
%%	       format('adding ~w => ~w\n',[Template,_T2]),
	       mutable(MT,_T2)
	      )),
	mutable_read(MT,Templates),
	mutable_read(M,NTemplates),
	NTemplates > 0,
%	format('expand tree template ~w ~w\n',[NTemplates,Templates]),
	%% get value tree corresponding to template tree
	templates2values(Templates,[rule:Rule|Map],_Values,[]),
	domain(type:Type,Map),
	domain(label:Label,Map),
	Values = [Label,Type|_Values],
%%	format('expand tree template ~w => values=~w\n',[Templates,Values]),
	record_without_doublon(templates([],Features,Rule,Values)),
	true
.

generate_templates :-
        feature_map(Features,Map),
        sfeature_map(SFeatures,SMap),
        verbose('handling stemplates\n',[]),
        mutable(M,0),
        mutable(MT,[]),
        every((
               ( template(Template) ; stemplate(Template) ),
%              Template = (label:type:_Template),
               mutable_inc(M,_),
               mutable_read(MT,_T1),
%%             format('try add ~w\n',[Template]),
               template_add(Template,_T1,_T2), % build template tree
%%             format('adding ~w => ~w\n',[Template,_T2]),
               mutable(MT,_T2)
              )),
        mutable_read(MT,Templates),
        mutable_read(M,NTemplates),
        NTemplates > 0,
%       format('expand tree template ~w ~w\n',[NTemplates,Templates]),
        %% get value tree corresponding to template tree
        append(SMap,Map,FullMap),
        templates2values(Templates,[rule:Rule|FullMap],_Values,[]),
        domain(type:Type,Map),
        domain(label:Label,Map),
        Values = [Label,Type|_Values],
%       format('expand tree stemplate ~w => values=~w\n',[Templates,Values]),
        record_without_doublon(templates(SFeatures,Features,Rule,Values)),
        true
        .

generate_templates :-
	rfeature_map(RFeatures,RMap),
	verbose('handling rtemplates\n',[]),
	mutable(M,0),
	mutable(MT,[]),
	every((
	       rtemplate(Template),
%	       Template = (label:type:_Template),
	       mutable_inc(M,_),
	       mutable_read(MT,_T1),
%%	       format('try add ~w\n',[Template]),
	       template_add(Template,_T1,_T2), % build template tree
%%	       format('adding ~w => ~w\n',[Template,_T2]),
	       mutable(MT,_T2)
	      )),
	mutable_read(MT,Templates),
	mutable_read(M,NTemplates),
	NTemplates > 0,
%	format('expand tree template ~w ~w\n',[NTemplates,Templates]),
	%% get value tree corresponding to template tree
	templates2values(Templates,[rule:Rule|RMap],Values,[]),
%	format('expand tree rtemplate ~w => values=~w\n',[Templates,Values]),
	record_without_doublon(rtemplates(RFeatures,Rule,Values)),
	true
	.

