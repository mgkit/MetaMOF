## My completion file for adding missing features to words
## beware of tabs as separator (not blanks)

# Evaluation de la couverture de la MG avec des pseudo phrases



# MetaMOF
dontre	100	adv	[pred="dontre___9999__1<Obj:(scompl)>",upos=ADV,cat=adv]	dontre___9999__1	Default		%default	adv
des	100	adv	[pred="des___9999__1<Obj:(scompl)>",upos=ADV,cat=adv]	des___9999__1	Default		%default	adv
testes	100	nc	[pred="teste___48366__1<Objde:(de-sinf|de-sn),Obj:(-sinf)>",upos=NOUN,cat=nc,@pl.fem]	teste___48366__1	Default		%default	noun-f
a	100	prep	[pred="a___56319__1<Obj:sa|sadv|sinf|sn|scompl>",upos=ADP,cat=prep]	a___56319__1	Default	apos	%default	inv
devant	100	prep	[pred="devant___56422__1<Obj:sa|sadv|sinf|sn|scompl>",upos=ADP,cat=prep]	devant___56422__1	Default	apos	%default	inv
puis	100	prep	[pred="puis___56544__1<Obj:sa|sadv|sinf|sn|scompl>",upos=ADP,cat=prep]	puis___56544__1	Default	apos	%default	inv
pur	100	prep	[pred="por___56532__1<Obj:sa|sadv|sinf|sn|scompl>",upos=ADP,cat=prep]	por___56532__1	Default		%default	inv
apres	100	prep	[pred="aprs___56334__1<Obj:sa|sadv|sinf|sn|scompl>",upos=ADP,cat=prep]	aprs___56334__1	Default		%default	inv
ainz	100	adv	[pred="ainz___9999__1<Obl:(scompl)>",upos=ADV,cat=adv]	ainz___9999__1	Default		%default	adv
ainsi	100	adv	[pred="ainsi___9999__1<Obl:(scompl)>",upos=ADV,cat=adv]	ainsi___9999__1	Default		%default	adv
afin	100	adv	[pred="afin___9999__1<Obl:(scompl)>",upos=ADV,cat=adv]	afin___9999__1	Default		%default	adv
bien	100	adv	[pred="bien___9999__1<Obl:(scompl)>",upos=ADV,cat=adv]	bien___9999__1	Default		%default	adv
cependant	100	adv	[pred="cependant___9999__1<Obl:(scompl)>",upos=ADV,cat=adv]	cependant___9999__1	Default		%default	adv
encore	100	adv	[pred="encore___9999__1<Obl:(scompl)>",upos=ADV,cat=adv]	encore___9999__1	Default		%default	adv
maintenant	100	adv	[pred="maintenant___9999__1<Obl:(scompl)>",upos=ADV,cat=adv]	maintenant___9999__1	Default		%default	adv
nonobstant	100	adv	[pred="nonobstant___9999__1<Obl:(scompl)>",upos=ADV,cat=adv]	nonobstant___9999__1	Default		%default	adv
mentres	100	adv	[pred="mentres___9999__1<Obl:(scompl)>",upos=ADV,cat=adv]	mentres___9999__1	Default		%default	adv
plus	100	adv	[pred="plus___9999__1<Obl:(scompl)>",upos=ADV,cat=adv]	plus___9999__1	Default		%default	adv
pendant	100	adv	[pred="pendant___9999__1<Obl:(scompl)>",upos=ADV,cat=adv]	pendant___9999__1	Default		%default	adv
tandis	100	adv	[pred="tandis___9999__1<Obl:(scompl)>",upos=ADV,cat=adv]	tandis___9999__1	Default		%default	adv
tant	100	adv	[pred="tant___9999__1<Obl:(scompl)>",upos=ADV,cat=adv]	tant___9999__1	Default		%default	adv
pardonne	100	v	[pred="pardoner_____1<Suj:cln|sn,Obj:(cla|sn),Obj:(cld|-sn)>",@pers,cat=v,upos=VERB,@sg.3.ind.prs.std]	pardoner_____1	Default	sg.3.ind.std	%actif	v-er
et	100	coo	[pred="et_____1<arg1,arg2>",upos=CCONJ,cat=coo]	et_____1	Default	no	%default	inv
declinet	100	v	[pred="decliner_____1<Suj:cln|sn,Obj:(cla|sn)>",@pers,cat=v,upos=VERB,@sg.3.ind.prs.std]	decliner_____1	Default	sg.3.ind.prs.std	%actif	v-er
pense	100	v	[pred="penser_____1<Suj:cln|sn,Obj:(-sn|-sinf)>",@pers,cat=v,upos=VERB,@sg.3.ind.prs.std]	penser_____1	Default	sg.3.ind.prs.std	%actif	v-er
est	100	v	[pred="estre_____1<Suj:cln|sn,Att:(sa|-sinf|-sn)>",@AttSuj,@pers,cat=v,upos=VERB,@sg.3.ind.prs.std]	estre_____1	Default	sg.3.ind.prs.std	%actif	v-unkconj
est	100	v	[pred="estre_____1<Suj:cln|sn,Att:(sn|scompl|de-sinf)>",@AttSuj,@pers,cat=v,upos=VERB,@sg.3.ind.prs.std]	estre_____1	Default	sg.3.ind.prs.std	%actif	v-unkconj
#est	400	auxEstre	[@festre,diathesis=active,@sg.3.ind.prs.std]	estre_____3	Default	sg.3.ind.prs.std	%default	v-unkconj
#est	400	auxEstre	[@festre,diathesis=passive,@sg.3.ind.prs.std]	estre_____4	Default	sg.3.ind.prs.std	%default	v-unkconj
seront	100	v	[pred="estre_____1<Suj:cln|sn,Att:sn|cla>",@pers,cat=v,upos=VERB,@sg.3.ind.prs.std]	estre_____1	Default	pl.3.ind.prs.std	%actif	v-unkconj
#voloir
veut	100	v	[pred="vouloir_____1<Suj:cln|sn,Obj:(cla|qcompl|scompl|sinf|sn)>",@CompSubj,@CtrlSujObj,@pers,cat=v,upos=VERB,@sg.3.ind.prs.std]	vouloir_____1	Default	sg.3.ind.prs.std	%actif	v-unkconj
plus	100	adv	[pred="plus_____1<Obl:(scompl|sn|sa|sinf)>",adv_kind=intens,cat=adv]	plus_____1	Default	%default	inv


# Reste FRMG


_DIMENSION	np	[cat=np,@p]	_DIMENSION	Default

densment	adv	[]	densment____20	Default

##ce		pro	[pred="ce____3",number=sg]	ce_____3	Default


##comment		pri	[pred="commentComp?_____1",case= comp]	commentComp?_____1	Default 
quel		pri	[pred="quel?_____1",case= comp,@ms]	quel?_____1	Default 
quelle		pri	[pred="quel?_____1",case= comp,@fs]	quel?_____1	Default 
quels		pri	[pred="quel?_____1",case= comp,@mp]	quel?_____1	Default 
quelles		pri	[pred="quel?_____1",case= comp,@fp]	quel?_____1	Default 
qui		pri	[pred="quiComp?_____1",case= comp]	quiComp?_____1	Default
que		pri	[pred="queComp?_____1",case= comp]	queComp?_____1	Default 
qu'		pri	[pred="queComp?_____1",case= comp]	queComp?_____1	Default 
lequel		pri	[pred="lequelComp?_____1",case= comp,@ms]	lequelComp?_____1	Default 
lesquels	pri	[pred="lequelComp?_____1",case= comp,@mp]	lequelComp?_____1	Default 
laquelle	pri	[pred="lequelComp?_____1",case= comp,@fs]	lequelComp?_____1	Default 
lesquelles	pri	[pred="lequelComp?_____1",case= comp,@fp]	lequelComp?_____1	Default 



lequel		pri	[pred="lequel?_____1",case=nom|acc,@ms]	quel?_____1	Default 
laquelle		pri	[pred="lequel?_____1",case=nom|acc,@fs]	quel?_____1	Default 
lesquels		pri	[pred="lequel?_____1",case=nom|acc,@mp]	quel?_____1	Default 
lesquelles		pri	[pred="lequel?_____1",case=nom|acc,@fp]	quel?_____1	Default 

proto-_		advPref	[pred="proto-______2<Suj:(sn)>",cat=adv]	proto-______2	Default

## Un	det	[det=+,define=-,@ms]	un_____1	Default	ms
## Un	pro	[pred="un_____1<Objde:(de-sn)>",define=-,@ms]	un_____1	Default	ms

pas encore	advneg	[pred="pas encore_____1",cat=advneg]	pas encore_____1	Default 
prs de	adv	[pred="prs de_____1",cat=adv,adv_kind=modnc]	prs de_____1	Default
prs d"	adv	[pred="prs de_____1",cat=adv,adv_kind=modnc]	prs de_____1	Default
:	poncts	[]	:_____2	Default	%default
Wikipdia	np	[cat=np]	Wikipdia_____1	Default
DGSE	np	[cat=np,@fs]	DGSE_____1	Default
DST	np	[cat=np,@fs]	DST_____1	Default

eux-mmes	100	xpro	[pred="lui-mme_____2",@3mp]	lui-mme_____1	Default	%default
elles-mmes	100	xpro	[pred="lui-mme_____2",@3fp]	lui-mme_____1	Default	%default
lui-mme	100	xpro	[pred="lui-mme_____2",@3ms]	lui-mme_____1	Default	%default
elle-mme	100	xpro	[pred="lui-mme_____2",@3fs]	lui-mme_____1	Default	%default


## comme det: divers diffrent
## Quid

Quid	np	[cat=np]	Quid_____1	Default
Gaza	np	[cat=np]	Gaza_____1	Default
Fermat	np	[cat=np]	Gaza_____1	Default
Dinant	np	[cat=np]	Gaza_____1	Default
Octave	np	[cat=np,@s]	Gaza_____1	Default
Martial	np	[cat=np,@ms]	Gaza_____1	Default
Pluton	100	np	[pred="Pluton",cat=np,@ms]	Pluton_____1	Default	ms	 %default

JO	100	np	[pred="Journal_Officiel",cat=np,@ms]	Journal Officiel_____1	Default	ms	%default

chut	pres	[pred="chut_____1"]	chut_____1	Default

divers	100	det	[define=-,det=+,@m]	divers_____1	Default m	%default
diverse	100	det	[define=-,det=+,@fs]	divers_____1	Default fs	%default
diverses	100	det	[define=-,det=+,@fp]	divers_____1	Default fp	%default

diffrent	100	det	[define=-,det=+,@ms]	diffrent_____1	Default ms	%default
diffrents	100	det	[define=-,det=+,@mp]	diffrent_____1	Default mp	%default
diffrente	100	det	[define=-,det=+,@fs]	diffrent_____1	Default fs	%default
diffrentes	100	det	[define=-,det=+,@fp]	diffrent_____1	Default fp	%default

## Construction en "X de" comme det

peu	100	predet	[pred="peu_____1",predet_kind=adv,quantity=peu]	peu_____1	Default	%default
beaucoup	100	predet	[pred="beaucoup_____1",predet_kind=adv,quantity=beaucoup]	beaucoup_____1	Default	%default
tant	100	predet	[pred="tant_____1",predet_kind=adv,quantity=beaucoup]	tant_____1	Default	%default
nombre	100	predet	[pred="nombre_____1",predet_kind=nc]	nombre_____1	Default	%default
normment	100	predet	[pred="normment_____1",predet_kind=adv,quantity=beaucoup]	normment_____1	Default	%default
tellement	100	predet	[pred="tellement_____1",predet_kind=adv,quantity=beaucoup]	tellement_____1	Default	%default
immensment	100	predet	[pred="immensment_____1",predet_kind=adv,quantity=beaucoup]	immensment_____1	Default	%default
quantit	100	predet	[pred="quantit_____1",predet_kind=nc]	quantit_____1	Default	%default
plein	100	predet	[pred="plein_____1",predet_kind=adv]	plein_____1	Default	%default
combien	100	predet	[pred="combien_____1",predet_kind=nc,wh=+]	combien_____1	Default	%default
suffisamment	100	predet	[pred="suffisamment_____1",predet_kind=adv,quantity=beaucoup]	suffisamment_____1	Default	%default
assez	100	predet	[pred="assez_____1",predet_kind=adv,quantity=beaucoup]	assez_____1	Default	%default
trop	100	predet	[pred="trop_____1",predet_kind=adv,quantity=beaucoup]	trop_____1	Default	%default
nul	100	predet	[pred="nul_____1",predet_kind=nc]	nul_____1	Default	%default
plus	100	predet	[pred="plus_____1",predet_kind=adv,quantity=beaucoup]	plus_____1	Default	%default
moins	100	predet	[pred="moins_____1",predet_kind=adv,quantity=beaucoup]	moins_____1	Default	%default
davantage	100	predet	[pred="davantage_____1",predet_kind=adv,quantity=beaucoup]	davantage_____1	Default	%default
## pas is only possible in negative contexte
## il ne veut (pas de) pomme.
## il ne veut presque (pas de) pomme.
## il veut une part avec (pas de) pommes.
pas	100	predet	[pred="pas_____1",predet_kind=advneg]	pas_____1	Default	%default
gure	100	predet	[pred="gure_____1",predet_kind=advneg]	gure_____1	Default	%default
## Ancien franais : assez, trop, plus, moult, poi, moins, point, i a. del, des.
moult	100	predet	[pred="moult_____1",predet_kind=adv,quantity=beaucoup]	moult_____1	Default	%default
molt	100	predet	[pred="molt_____1",predet_kind=adv,quantity=beaucoup]	molt_____1	Default	%default
poi	100	predet	[pred="poi_____1",predet_kind=advneg]	poi_____1	Default	%default
point	100	predet	[pred="point_____1",predet_kind=advneg]	point_____1	Default	%default

gaiement	100	advm	[pred="gaiement_____1",cat=adv,clive=+]	gaiement_____1	Default	%default

## Actually, voici and voil are in Lefff as v, but with a special subcat
## No subj
##voici	100	v	[pred="voici_____1<Suj:cln|sn,Obj:(cla|sn|scompl)>",cat=v,@imperative,@Y2s]	voici_____1	Imperative	Y2s	%actif
##revoici	100	v	[pred="revoici_____1<Suj:cln|sn,Obj:(cla|sn|scompl)>",cat=v,@imperative,@Y2s]	revoici_____1	Imperative	Y2s	%actif
##voil	100	v	[pred="voil_____1<Suj:cln|sn,Obj:(cla|sn|scompl)>",cat=v,@imperative,@Y2s]	voil_____1	Imperative	Y2s	%actif
##revoil	100	v	[pred="revoil_____1<Suj:cln|sn,Obj:(cla|sn|scompl)>",cat=v,@imperative,@Y2s]	revoil_____1	Imperative	Y2s	%actif
##quid	100	v	[pred="quid_____1<Suj:cln|sn,Objde:(de-sinf|de-sn)>",cat=v,@imperative,@Y2s]	quid_____1	Imperative	Y2s	%actif

## avec	120	prep	[pred="avec_____1<Obj:(sa|sadv|sn)>",pcas = avec]	avec_____1	Default	%default

## fait	100	v	[pred="faire_____1<Suj:cln|sn,Obj:sinfcaus>",@pers,cat=v,@P3s]	faire_____1	ThirdSing	P3s	%actif

## Tmp: en attendant de completer Lefff
soulign	100	v	[pred="souligner_____1<Obl2:(par-sn),Suj:cln|sn|sinf|scompl|qcompl>",@passive,@pers,cat=v,@Kms]	souligner_____1	PastParticiple	Kms	%passif
souligne	100	v	[pred="souligner_____1<Obl2:(par-sn),Suj:cln|sn|sinf|scompl>",@passive,@pers,cat=v,@Kfs]	souligner_____1	PastParticiple	Kfs	%passif
souligns	100	v	[pred="souligner_____1<Obl2:(par-sn),Suj:cln|sn|sinf|scompl>",@passive,@pers,cat=v,@Kmp]	souligner_____1	PastParticiple	Kmp	%passif    
soulignes	100	v	[pred="souligner_____1<Obl2:(par-sn),Suj:cln|sn|sinf|scompl>",@passive,@pers,cat=v,@Kfp]	souligner_____1	PastParticiple	Kfp	%passif

que	100	csu	[wh=que]	que_____1	Default	%default:
qu'	100	csu	[wh=que]	que_____1	Default	%default
    
_NUMBER	number	[cat=number]	_NUMBER	Default
SHQ:	epsilon	[cat=epsilon]	SHQ:____1	Default
:SHQ	epsilon	[]	:SHQ____1	Default
PRED:	epsilon	[]	PRED:____1	Default
:PRED	epsilon	[]	:PRED____1	Default
AUT CL:	epsilon	[]	AUTCL:____1	Default
:AUT CL	epsilon	[]	:AUTCL____1	Default
AUT:	epsilon	[]	AUT:____1	Default
:AUT	epsilon	[]	:AUT____1	Default
SQ:	epsilon	[cat=epsilon]	SQ:____1	Default
:SQ	epsilon	[]	:SQ____1	Default
STQ:	epsilon	[cat=epsilon]	STQ:____1	Default
:STQ	epsilon	[]	:STQ____1	Default
HQ:	epsilon	[cat=epsilon]	HQ:____1	Default
:HQ	epsilon	[]	:HQ____1	Default
TQ:	epsilon	[cat=epsilon]	TQ:____1	Default
:TQ	epsilon	[]	:TQ____1	Default
DQ:	epsilon	[cat=epsilon]	DQ:____1	Default
:DQ	epsilon	[]	:DQ____1	Default


## Pb with SxPipe: Dieu (incorrectly) corrected into dieu
dieu	100	np	[cat=np,@ms]	Dieu_____1	Default


nul	100	det	[define=-,det=+,@ms]	nul_____1	Default	ms	%default
nuls	100	det	[define=-,det=+,@mp]	nul_____1		Default	mp	%default
nulle	100	det	[define=-,det=+,@fs]	nul_____1		Default	fs	%default
nulles	100	det	[define=-,det=+,@fp]	nul_____1		Default	fp	%default

si	100	adv	[pred="si_____1",adv_kind=intensive,cat=adv]	si_____1	Default	%default
tellement	100	adv	[pred="tellement_____1",adv_kind=intensive,cat=adv]	tellement_____1	Default	%default
plutt	100	adv	[pred="plutt_____1",adv_kind=intens,cat=adv]	plutt_____1	Default	%default

## nouns witch scompl arg
motif	100	nc	[pred="motif_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj:(-sinf)>",cat=nc,@ms]	motif_____1	Default	ms	%default
souhait	100	nc	[pred="souhait_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj:(-sinf)>",cat=nc,@ms]	souhait_____1	Default	ms	%default
dsir	100	nc	[pred="dsir_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj:(-sinf)>",cat=nc,@ms]	dsir_____1	Default	ms	%default
accord	100	nc	[pred="accord_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj:(-sinf)>",cat=nc,@ms]	accord_____1	Default	ms	%default
constat	100	nc	[pred="constat_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj:(-sinf)>",cat=nc,@ms]	constat_____1	Default	ms	%default
rappel	100	nc	[pred="rappel_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj:(-sinf)>",cat=nc,@ms]	rappel_____1	Default	ms	%default
sentiment	100	nc	[pred="sentiment_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj:(-sinf)>",cat=nc,@ms]	sentiment_____1	Default	ms	%default
doute	100	nc	[pred="doute_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj:(-sinf)>",cat=nc,@ms]	doute_____1	Default	ms	%default
principe	100	nc	[pred="principe_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj:(-sinf)>",cat=nc,@ms]	principe_____1	Default	ms	%default

sorte	100	nc	[pred="sorte_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj:(-sinf)>",cat=nc,@fs]	sorte_____1	Default	fs	%default
envie	100	nc	[pred="envie_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj:(-sinf)>",cat=nc,@fs]	envie_____1	Default	fs	%default
impression	100	nc	[pred="impression_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj:(-sinf)>",cat=nc,@fs]	impression_____1	Default	fs	%default
proposition	100	nc	[pred="proposition_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj:(-sinf)>",cat=nc,@fs]	proposition_____1	Default	fs	%default
croyance	100	nc	[pred="croyance_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj:(-sinf)>",cat=nc,@fs]	croyance_____1	Default	fs	%default
assurance	100	nc	[pred="assurance_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj:(-sinf)>",cat=nc,@fs]	assurance_____1	Default	fs	%default
crainte	100	nc	[pred="crainte_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj:(-sinf)>",cat=nc,@fs]	crainte_____1	Default	fs	%default
inquitude	100	nc	[pred="inquitude_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj:(-sinf)>",cat=nc,@fs]	inquitude_____1	Default	fs	%default
pense	100	nc	[pred="pense_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj:(-sinf)>",cat=nc,@fs]	pense_____1	Default	fs	%default
condition	100	nc	[pred="condition_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj:(-sinf)>",cat=nc,@fs]	condition_____1	Default	fs	%default
annonce	100	nc	[pred="annonce_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj:(-sinf)>",cat=nc,@fs]	annonce_____1	Default	fs	%default
conclusion	100	nc	[pred="conclusion_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj:(-sinf)>",cat=nc,@fs]	conclusion_____1	Default	fs	%default


super	100	advm	[pred="super_____1",cat=adv,adv_kind=intens]	super_____1	Default	%default

#@title matre +s @m
#@title Me +s @
#@title matresse +s @f
@title docteur +s @
@title professeur +s @
@title madame mesdames @f
@title mademoiselle mesdemoiselles @f
@title monsieur messieurs @m
@title Mr MM. @m
@title M. MM. @m
@title M MM @m
@title Mlle Mlles @f
@title Mme Mmes @f
@title Monseigneur Messeigneurs @m
@title Mgr +s @m
@title Dame Dames @f
@title Sir Sirs @m
@title Lady Ladies @f
@title Mess Mess @m
@title Rvrend Rv @m
@title colonel colonel @m
@title Miss Misses @f
@title baron barons @m

aussi	adv	[pred="aussi_____1",cat=adv,adv_kind=modpro]	aussi_____1	Default
mme	adv	[pred="mme_____1",cat=adv,adv_kind=modpro]	mme_____1	Default
non plus	adv	[pred="nonplus_____1",cat=adv,adv_kind=modpro]	non plus_____1	Default

## plutt que	prep	[pred="plutt que_____1<Obj:sn|sa|sadv|sinf>"]	plutt que_____1	Default	%default

quiconque	100	prel	[pred="quiconque_____1",@pro_nom]	quiconque_____1	Default	%default

## missing in sxpipe/lefff (maybe a bug) 2010/09/28 Eric
@redirect _LOCATION2 _LOCATION
@redirect _PERSON2 _PERSON
@redirect _PERSON_f2 _PERSON_f
@redirect _PERSON_m2 _PERSON_m
@redirect _ORGANIZATION2 _ORGANIZATION
@redirect trad. traduction
@redirect rd. rdition
@redirect ed. diteur

@redirect vnement vnement

quinzaine	100	nc	[pred="quinzaine",cat=nc,@fs,@time]	quinzaine____1	Default	%default

@nc vicomt +s @f

@adj lorientais + +e +es
@adj rennais    + +e +es
@adj sedanais   + +e +es
@adj perpignanais + +e +es
@adj saskatchewanais + +e +es

@adj vallonn +s +e +es

@nc vertbr +s @m
@nc invertbr +s @m
@nc naufrag +s @m
@nc naufrage +s @f

@adj quitte +s + +s

chemin	100	cfi	[pred="chemin_____1<Suj:cln|sn>",lightverb=rebrousser]	chemin_____1	Default	%default
main-forte	100	cfi	[pred="main-forte_____1<Suj:cln|sn>",lightverb=prter]	main-forte_____1	Default	%default
main-forte	100	cfi	[pred="main-forte_____1<Suj:cln|sn>",lightverb=demander]	main-forte_____1	Default	%default
main-forte	100	cfi	[pred="main-forte_____1<Suj:cln|sn>",lightverb=chercher]	main-forte_____1	Default	%default
main-forte	100	cfi	[pred="main-forte_____1<Suj:cln|sn>",lightverb=qurir]	main-forte_____1	Default	%default
main-forte	100	cfi	[pred="main-forte_____1<Suj:cln|sn>",lightverb=requrir]	main-forte_____1	Default	%default
gain de cause	100	cfi	[pred="gain de cause_____1<Suj:cln|sn>",lightverb=obtenir]	gain de cause_____1	Default	%default
problme	100	cfi	[pred="problme_____1<Suj:cln|sn,Obj:(-sn|cld)>",lightverb=poser]	problme_____1	Default	%default
## gaffe	100	cfi	[pred="gaffe_____1<Suj:cln|sn,Obj:(-sn|y|-sinf|scompl|de-sinf)>",lightverb=faire]	gaffe_____1	Default	%default

pas grand chose	100	pro	[pred="pas grand-chose_____1<Objde:(de-sn|de-sa)>",define=-,@s]	pas grand-chose_____1	Default	%default

cens	100	v	[pred="censer_____1<Suj:cln|sn,Obl:sinf>",@CtrlSujObl,@active,@pers,@tre,cat=v,@Kms]	censer_____1	PastParticiple	Kms	%actif
cense	100	v	[pred="censer_____1<Suj:cln|sn,Obl:sinf>",@CtrlSujObl,@active,@pers,@tre,cat=v,@Kfs]	censer_____1	PastParticiple	Kfs	%actif
censs	100	v	[pred="censer_____1<Suj:cln|sn,Obl:sinf>",@CtrlSujObl,@active,@pers,@tre,cat=v,@Kmp]	censer_____1	PastParticiple	Kmp	%actif
censes	100	v	[pred="censer_____1<Suj:cln|sn,Obl:sinf>",@CtrlSujObl,@active,@pers,@tre,cat=v,@Kfp]	censer_____1	PastParticiple Kfp	%actif

suppos	100	v	[pred="supposer_____1<Suj:cln|sn,Obl:sinf>",@CtrlSujObl,@active,@pers,@tre,cat=v,@Kms]	supposer_____1	PastParticiple	Kms	%actif
suppose	100	v	[pred="supposer_____1<Suj:cln|sn,Obl:sinf>",@CtrlSujObl,@active,@pers,@tre,cat=v,@Kfs]	supposer_____1	PastParticiple	Kfs	%actif
supposs	100	v	[pred="supposer_____1<Suj:cln|sn,Obl:sinf>",@CtrlSujObl,@active,@pers,@tre,cat=v,@Kmp]	supposer_____1	PastParticiple	Kmp	%actif
supposes	100	v	[pred="supposer_____1<Suj:cln|sn,Obl:sinf>",@CtrlSujObl,@active,@pers,@tre,cat=v,@Kfp]	supposer_____1	PastParticiple Kfp	%actif

rput	100	v	[pred="rputer_____1<Suj:cln|sn,Obl:sinf>",@CtrlSujObl,@active,@pers,@tre,cat=v,@Kms]	rputer_____1	PastParticiple	Kms	%actif
rpute	100	v	[pred="rputer_____1<Suj:cln|sn,Obl:sinf>",@CtrlSujObl,@active,@pers,@tre,cat=v,@Kfs]	rputer_____1	PastParticiple	Kfs	%actif
rputs	100	v	[pred="rputer_____1<Suj:cln|sn,Obl:sinf>",@CtrlSujObl,@active,@pers,@tre,cat=v,@Kmp]	rputer_____1	PastParticiple	Kmp	%actif
rputes	100	v	[pred="rputer_____1<Suj:cln|sn,Obl:sinf>",@CtrlSujObl,@active,@pers,@tre,cat=v,@Kfp]	rputer_____1	PastParticiple Kfp	%actif

intervenu	100	v	[pred="intervenir_____1<Suj:cln|sn,Loc:(loc-sn|y)>",@active,@pers,@tre,cat=v,@Kms]	intervenir_____1	PastParticiple	Kms	%actif
intervenue	100	v	[pred="intervenir_____1<Suj:cln|sn,Loc:(loc-sn|y)>",@active,@pers,@tre,cat=v,@Kfs]	intervenir_____1	PastParticiple	Kfs	%actif
intervenus	100	v	[pred="intervenir_____1<Suj:cln|sn,Loc:(loc-sn|y)>",@active,@pers,@tre,cat=v,@Kmp]	intervenir_____1	PastParticiple	Kmp	%actif
intervenues	100	v	[pred="intervenir_____1<Suj:cln|sn,Loc:(loc-sn|y)>",@active,@pers,@tre,cat=v,@Kfp]	intervenir_____1	PastParticiple Kfp	%actif


paru	100	v	[pred="paratre_____1<Suj:cln|sn>",@active,@pers,@tre,cat=v,@Kms]	paratre_____1	PastParticiple	Kms	%actif
parue	100	v	[pred="paratre_____1<Suj:cln|sn>",@active,@pers,@tre,cat=v,@Kfs]	paratre_____1	PastParticiple	Kfs	%actif
parus	100	v	[pred="paratre_____1<Suj:cln|sn>",@active,@pers,@tre,cat=v,@Kmp]	paratre_____1	PastParticiple	Kmp	%actif
parues	100	v	[pred="paratre_____1<Suj:cln|sn>",@active,@pers,@tre,cat=v,@Kfp]	paratre_____1	PastParticiple Kfp	%actif

survenu	100	v	[pred="survenir_____1<Suj:cln|sn>",@active,@pers,@tre,cat=v,@Kms]	survenir_____1	PastParticiple	Kms	%actif
survenue	100	v	[pred="survenir_____1<Suj:cln|sn>",@active,@pers,@tre,cat=v,@Kfs]	survenir_____1	PastParticiple	Kfs	%actif
survenus	100	v	[pred="survenir_____1<Suj:cln|sn>",@active,@pers,@tre,cat=v,@Kmp]	survenir_____1	PastParticiple	Kmp	%actif
survenues	100	v	[pred="survenir_____1<Suj:cln|sn>",@active,@pers,@tre,cat=v,@Kfp]	survenir_____1	PastParticiple Kfp	%actif

apparu	100	v	[pred="apparatre_____1<Suj:cln|sn>",@active,@pers,@tre,cat=v,@Kms]	apparatre_____1	PastApparticiple	Kms	%actif
apparue	100	v	[pred="apparatre_____1<Suj:cln|sn>",@active,@pers,@tre,cat=v,@Kfs]	apparatre_____1	PastApparticiple	Kfs	%actif
apparus	100	v	[pred="apparatre_____1<Suj:cln|sn>",@active,@pers,@tre,cat=v,@Kmp]	apparatre_____1	PastApparticiple	Kmp	%actif
apparues	100	v	[pred="apparatre_____1<Suj:cln|sn>",@active,@pers,@tre,cat=v,@Kfp]	apparatre_____1	PastApparticiple Kfp	%actif

avr	100	v	[pred="avrer_____1<Suj:cln|sn>",@active,@pers,@tre,cat=v,@Kms]	avrer_____1	PastParticiple	Kms	%actif
avre	100	v	[pred="avrer_____1<Suj:cln|sn>",@active,@pers,@tre,cat=v,@Kfs]	avrer_____1	PastParticiple	Kfs	%actif
avrs	100	v	[pred="avrer_____1<Suj:cln|sn>",@active,@pers,@tre,cat=v,@Kmp]	avrer_____1	PastParticiple	Kmp	%actif
avres	100	v	[pred="avrer_____1<Suj:cln|sn>",@active,@pers,@tre,cat=v,@Kfp]	avrer_____1	PastParticiple Kfp	%actif


dbattu	100	v	[pred="dbattre_____1<Obl2:(par-sn),Suj:cln|sn>",@passive,@pers,cat=v,@Kms]	dbattre_____1	PastParticiple	Kms	%passif
dbattue	100	v	[pred="dbattre_____1<Obl2:(par-sn),Suj:cln|sn>",@passive,@pers,cat=v,@Kfs]	dbattre_____1	PastParticiple	Kfs	%passif
dbattus	100	v	[pred="dbattre_____1<Obl2:(par-sn),Suj:cln|sn>",@passive,@pers,cat=v,@Kmp]	dbattre_____1	PastParticiple	Kmp	%passif
dbattues	100	v	[pred="dbattre_____1<Obl2:(par-sn),Suj:cln|sn>",@passive,@pers,cat=v,@Kfp]	dbattre_____1	PastParticiple Kfp	%passif

paniqu	100	v	[pred="paniquer_____1<Obl2:(par-sn),Suj:cln|sn>",@passive,@pers,cat=v,@Kms]	paniquer_____1	PastParticiple	Kms	%passif
panique	100	v	[pred="paniquer_____1<Obl2:(par-sn),Suj:cln|sn>",@passive,@pers,cat=v,@Kfs]	paniquer_____1	PastParticiple	Kfs	%passif
paniqus	100	v	[pred="paniquer_____1<Obl2:(par-sn),Suj:cln|sn>",@passive,@pers,cat=v,@Kmp]	paniquer_____1	PastParticiple	Kmp	%passif
paniques	100	v	[pred="paniquer_____1<Obl2:(par-sn),Suj:cln|sn>",@passive,@pers,cat=v,@Kfp]	paniquer_____1	PastParticiple Kfp	%passif

autoproclam	100	v	[pred="autoproclamer_____1<Obl2:(par-sn),Suj:cln|sn>",@passive,@pers,cat=v,@Kms]	autoproclamer_____1	PastParticiple	Kms	%passif
autoproclame	100	v	[pred="autoproclamer_____1<Obl2:(par-sn),Suj:cln|sn>",@passive,@pers,cat=v,@Kfs]	autoproclamer_____1	PastParticiple	Kfs	%passif
autoproclams	100	v	[pred="autoproclamer_____1<Obl2:(par-sn),Suj:cln|sn>",@passive,@pers,cat=v,@Kmp]	autoproclamer_____1	PastParticiple	Kmp	%passif
autoproclames	100	v	[pred="autoproclamer_____1<Obl2:(par-sn),Suj:cln|sn>",@passive,@pers,cat=v,@Kfp]	autoproclamer_____1	PastParticiple Kfp	%passif

surnomm	100	v	[pred="surnommer_____1<Obl2:(par-sn),Suj:cln|sn,Att:(sa|sn)>",@passive,@pers,cat=v,@Kms]	surnommer_____1	PastParticiple	Kms	%passif
surnomme	100	v	[pred="surnommer_____1<Obl2:(par-sn),Suj:cln|sn,Att:(sa|sn)>",@passive,@pers,cat=v,@Kfs]	surnommer_____1	PastParticiple	Kfs	%passif
surnomms	100	v	[pred="surnommer_____1<Obl2:(par-sn),Suj:cln|sn,Att:(sa|sn)>",@passive,@pers,cat=v,@Kmp]	surnommer_____1	PastParticiple	Kmp	%passif
surnommes	100	v	[pred="surnommer_____1<Obl2:(par-sn),Suj:cln|sn,Att:(sa|sn)>",@passive,@pers,cat=v,@Kfp]	surnommer_____1	PastParticiple Kfp	%passif


modulo	120	prep	[pred="modulo_____1<Obj:(sa|sadv|sn)>",pcas = modulo]	modulo_____1	Default	%default

@redirect commes comme

@nc ataman +s @m
@nc courtine +s @f
@redirect M'sieur Monsieur


#avant	120	prep	[pred="avant_____1",pcas = loc]	avant_____1	Default	%default
#aprs	120	prep	[pred="aprs_____1",pcas = loc]	aprs_____1	Default	%default    
#derrire	120	prep	[pred="derrire_____1",pcas = loc]	derrire_____1	Default	%default

@redirect anni anniversaire

@nc accompagnant +s @m
@nc accompagnante +s @f

moyen	100	cfi	[pred="moyen_____1<Suj:cln|sn,Objde:(de-sinf|scompl)>",lightverb=avoir]	moyen_____1	Default	%default
moyen	100	cfi	[pred="moyen_____1<Suj:cln|sn,Objde:(de-sinf|scompl)>",lightverb=trouver]	moyen_____1	Default	%default

peur	100	cfi	[pred="peur_____1<Suj:cln|sn,Objde:(de-sinf|scompl|de-sn)>",lightverb=avoir]	peur_____1	Default	%default

prtexte	100	nc	[pred="prtexte_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj:(-sinf)>",cat=nc,@ms]	prtexte_____1	Default	ms	%default

trace	100	cfi	[pred="trace_____1<Suj:cln|sn,Objde:(de-sn)>",lightverb=porter]	trace_____1	Default	%default
trace	100	cfi	[pred="trace_____1<Suj:cln|sn,Objde:(de-sn)>",lightverb=garder]	trace_____1	Default	%default

pnitence	100	cfi	[pred="pnitence_____1<Suj:cln|sn,Objde:(de-sn)>",lightverb=faire]	pnitence_____1	Default	%default

garde	100	cfi	[pred="garde_____1<Suj:cln|sn,Objde:de-sn|-sn>",lightverb=avoir]	garde_____1	Default	%default
raison	100	cfi	[pred="raison_____1<Suj:cln|sn,Objde:(de-sn)>",lightverb=avoir]	raison_____1	Default	%default

ripailles	100	cfi	[pred="ripailles_____1<Suj:cln|sn>",lightverb=faire]	ripailles_____1	Default

semblant	100	cfi	[pred="semblant_____1<Suj:cln|sn,Objde:(de-sinf)>",lightverb=faire]	semblant_____1	Default

compte	100	cfi	[pred="compte_____1<Suj:cln|sn,Objde:(de-sn|scompl),Obj:(-sn)>",lightverb=rendre]	compte_____1	Default	%default

@nc sicav + @f

oh	pres	[pred="oh_____1"]	oh_____1	Default
ho	pres	[pred="ho_____1"]	ho_____1	Default
eh	pres	[pred="eh_____1"]	eh_____1	Default
he	pres	[pred="oh_____1"]	he_____1	Default
ha	pres	[pred="ha_____1"]	ha_____1	Default

pour	120	prep	[pred="pour_____1",pcas = pour]	pour_____1	Default	%default
contre	120	prep	[pred="contre_____1",pcas = loc|contre]	contre_____1	Default	%default

quitte	100	cfi	[pred="quitte_____1<Suj:cln|sn,Obj:(sa|sn),Objde:(de-sn|en)>",lightverb=tenir]	tenir_____1	Default	%default

presque	advPref	[pred="presque_____1",cat=adv]	presque_____1	Default 
presque	adjPref	[pred="presque_____1<Suj:(sn)>",cat=adj]	presque_____1	Default 

@nc agoniste +s @m

@redirect m'sieur monsieur

 ct	adv	[pred=" ct_____1",cat=adv]	 ct_____1	Default
 cts	nc	[pred=" ct_____1",cat=nc,@mp]	 ct_____1	Default

t	v	[pred="tre_____1<Suj:cln|scompl|sinf|sn,Obl:sinf>",@CtrlSujObl,@active,@pers,cat=v,@K]	tre_____1	PastParticiple	K %actif

## tmp hack for 'avoir beau' : better to implement it as a concessive construction (il a beau manger, il ne grossit pas)
beau	100	cfi	[pred="beau_____1<Suj:cln|sn,Obl:sinf>",lightverb=avoir]	beau_____1	Default	%default

chance	100	cfi	[pred="chance_____1<Suj:cln|sn,Obj:(-sn)>",lightverb=porter]	chance_____1	Default	%default

##attention	100	cfi	[pred="attention_____1<Suj:cln|sn,Obj:(-sn|cld|-sinf)>",lightverb=faire]	attention_____1	Default	%default
  
@adj record +s + +s

#voil	120	prep	[pred="voil_____1<Obj:(sn)>",pcas = tps]	voil____1	Default	%default
#voici	120	prep	[pred="voici_____1",pcas= loc, cat= prep]	voici____1	Default	%default    

# Past Conditional seconde form
et	600	auxAvoir	[@active,@favoir,@C3s]	avoir_____2	ThirdSing	C3s	%default
etes	600	auxAvoir	[@active,@favoir,@C2p]	avoir_____2	Default	C2p	%default

commande	100	cfi	[pred="commande_____1<Suj:cln|sn,Obj:(-sn|cld),Objde:(de-sn|clg)>",lightverb=passer]	commande_____1	Default	%default

matire	100	cfi	[pred="matire_____1<Suj:cln|sn,Obj:(-sn|cld)>",lightverb=trouver]	matire_____1	Default	%default
pretexte	100	cfi	[pred="pretexte_____1<Suj:cln|sn,Obj:(-sn|cld)>",lightverb=trouver]	pretexte_____1	Default	%default
    
## Some acronymes
@nc PIB + @m
@nc TVA + @f
@nc RMI + @m
@nc TGV + @m
@nc PNB + @m
@nc PIB + @m
@nc BTP + @m
@nc RMiste +s @m
@nc TUC + @m
@nc PEL + @m

## irriter +obj

@nc contre-pouvoir +s @m

/	100	ponctw	[]	/_____1	Default	%default

## tmp hack
## du	100	prep	[pred="de_____1",pcas = de]	de_____1	Default	%default

@nc smic + @m
@nc SMIC + @m

@nc pdg + @m

diable	pres	[pred="diable_____1"]	diable_____1	Default

@nc -pic + @m


conseil	100	cfi	[pred="conseil_____1<Suj:cln|sn>",lightverb=tenir]	conseil_____1	Default	%default
piti	100	cfi	[pred="piti_____1<Suj:cln|sn,Objde:(de-sn)>",lightverb=avoir]	piti_____1	Default	%default

@adj synallagmatique +s + +s

## titriser v (new)
@adj titris +s +e +es

@redirect suiv. suivant

@nc irrvocabilit +s @f
@adj conchylicole +s + +s
@adj asphaltique +s + +s
@adj notionnel +s +le +les
@nc know-how + @m
@nc inconstructibilit +s @f
@adj mytilicole +s + +s
@adj franco-belge +s + +s
@adj franco-amricain +s +e +es
@nc cubitainer +s @m
@adj laryngectomis +s +e +es
@nc compte-rendu comptes-rendus @m

@nc memorandum +s @m
@nc titrisassion +s @f

@adj macroprudentiel +s +le +les

@nc irrsistibilit +s @f

## franchiser v (new)
@adj franchis +s +e +es

@adj vosgien +s +ne +nes

@nc fiducie +s @f

## missionner v (lglex)
## conventionner v (lglex)

@adj nosocomial nosocomiaux +le +les
@adj usufructuaire +s + +s
@nc quivocit +s @f
@nc rvocabilit +s @f
@nc anonymisation +s @f
@nc objectivisation +s @f

@adj liquidatif +s liquidative liquidatives
@adj restituable +s + +s
@adj invocable +s + +s
@nc impatri +s @m
@nc trustee +s @m
@nc inexploitation +s @f
@nc inalinabilit +s @f
@adj rgularisateur +s rgularisatrice rgularisatrices
@nc demanderesse +s @f
@adj confiscatoire +s + +s
@adj individualisable +s + +s
@nc sinistrabilit +s @f
@nc dirigeable +s @m
@nc crmatorium +s @m
@adj tlchargeable +s + +s
@adj montagnard +s +e +es
@nc licit +s @f
@nc cash + @m
@nc drfrencement +s @m
@adj exonratoire +s + +s
@nc facturette +s @f
@nc prempteur +s @m
@nc premptrice +s @f
@adj modificatif +s modificative modificatives
@adj monovalent +s monovalente monovalentes
@adj subrogatif +s subrogative subrogatives
@adj subrogatoire +s + +s
@nc cash-flow cash-flows @m
@adj pondral +s pondrale pondrales
@nc dconsolidation +s @f
@nc entte +s @
@nc affactureur +s @m
@adj qualifiant +s +e +es
@adj tutoral +s +e +es
@nc harceleur +s @m
@nc ressenti +s @m
@adj validable +s + +s
@adj putatif +s putative putatives
@nc sachant +s @m
@nc aidant +s @m
@nc commettant +s @m
@nc promettant +s @m
@nc rclamant +s @m
@nc patientle +s @f
@nc financiarisation +s @f
@nc solvabilisation +s @f
@adj mortifre +s + +s
@nc mlatonine +s @f
@nc professionalit +s @f
@nc apatride +s @
@nc subventionnement +s @m

appel	100	cfi	[pred="appel_____1<Suj:cln|sn,Objde:(de-sn|en)>",lightverb=interjeter]	appel_____1	Default	%default
acqureur	100	cfi	[pred="acqureur_____1<Suj:cln|sn,Objde:(de-sn|en)>",lightverb=porter]	acqureur_____1	Default	%default

im-_	adjPref	[pred="in-______1<Suj:(sn)>",cat=adj]	in-______1	Default	%default
in-_	adjPref	[pred="in-______1<Suj:(sn)>",cat=adj]	in-______1	Default	%default
d-_	adjPref	[pred="d-______1<Suj:(sn)>",cat=adj]	d-______1	Default	%default
ds-_	adjPref	[pred="d-______1<Suj:(sn)>",cat=adj]	d-______1	Default	%default

@nc labellisation +s @f
@adj substituable +s + +s
@adj rnumratoire +s + +s
@adj facturable +s + +s
@adj valuatif +s valuative +s
@adj sanctionnable +s + +s
@adj licenciable +s + +s

actuariellement	adv	[pred="actuariellement_____1",cat=adv]	actuariellement_____1	Default

@nc identifiant +s @m
@adj qurable +s + +s
@nc externalit +s @f
@adj drogeable +s + +s
@adj curatlaire +s + +s
@nc curatlaire +s @m
@adj dosimtrique +s + +s
@adj illustratif +s illustrative illustratives
@nc sans-papier sans-papiers @
@adj rattachable +s + +s
@nc transfrabilit +s @f
@nc dflocage +s @m
@nc bilatralisation +s @f
@nc rquilibre +s @m
@adj transitionel +s +le +les
@adj topique +s + +s
@adj vaccinal +s +e +es
@nc benfluorex + @m
@nc ls +s @m
@nc lse +s @f
@adj incriminateur +s incriminatrice incriminatrices
@nc transsexuel +s @m
@nc parapente +s @m
@adj identifiant +s +e +es
@adj rgnrable +s + +s
@nc biovigilance +s @f
#@adj in_vitro in_vitro in_vitro in_vitro
@adj saisissable +s + +s
@adj sommaire +s + +s
@nc sropositivit +s @f
@nc accipiens + @m
@adj implantable +s + +s
@nc refinancement +s @m
@nc nanoparticule +s @f
@nc repreneur +s @m
@nc repreneuse +s @f
@nc dcompilation +s @f
@adj prorogeable +s + +s
@adj internalisateur +s internalisatrice internalisatrices
@nc commandit +s @m
@adj vtrinaire +s + +s
@nc compte-titres comptes-titres @m

agissant	100	v	[pred="s'agir_____2<Objde:de-scompl|de-sinf|de-sn|en|scompl>",@impers,@pron,cat=v,@G]	agir_____2	Default	G	%actif_impersonnel
rception	100	cfi	[pred="rception_____1<Suj:cln|sn,Objde:(de-sn|en)>",lightverb=accuser]	rception_____1	Default	%default

appert	100	v	[pred="apparoir_____1<Suj:de-sinf|scompl|sn,Obj:(scompl),Obj:(cld|-sn)>",@impers,cat=v,@P3s]	apparoir_____1	ThirdSing	P3s	%actif_impersonnel

@nc condensat +s @m
@adj valorisable +s + +s

considration	100	cfi	[pred="considration_____1<Suj:cln|sn>",lightverb=mriter]	considration_____1	Default	%default

contact	100	cfi	[pred="contact_____1<Suj:cln|sn,Obl:avec-sn>",lightverb=reprendre]	contact_____1	Default	%default

@nc air-bag +s @m
@nc cuisiniste +s @m
@nc recapitalisation +s @f
@nc hyperinflation +s @f
# @adj stockeur
@nc lnder +s @m
@nc triracteur +s @m
#@nc trompe-l'oeil trompes-l'oeil @m
@adj sacro-saint +s +e +es
@adj rhodanien +s +ne +nes
@nc tlpage +s @m
@nc lease-back + @m
@adj interfromtrique +s + +s
@nc garde-fous + @m
@adj eurosceptique +s + +s
@nc essuie-tout + @m
@nc conjoncturiste +s @m
#@nc chef-d'oeuvre chefs-d'oeuvre @m
@nc monospace +s @
@nc sweat +s @m
@nc sportswear +s @m
@adj spoliatoire +s + +s
@nc sraphisme +s @m
@adj rousseauiste +s + +s
@nc roastbeef + @m
@adj rsiduaire +s + +s
@adj prudentiel +s +le +les
@adj recyclable +s + +s
@adj privatisable +s + +s
#@adj prince-de-Galles + + +
@nc mulard +s @m
@adj montable +s + +s
@nc bitioniau +x @m
@nc billetiste +s @
@nc attrape-tout + @m
@nc come-back + @m
@nc be-bop + @m
@nc aromathrapie +s @f
@nc agiotateur +s @m
@nc arobic + @
@nc -coup +s @m
@adj calendaire +s + +s
@nc croissanterie +s @f
@nc entrepeunariat +s @m
@nc laisser-faire + @m
@nc savoir-faire + @m
@nc savoir-vivre + @m

@adj infracommunautaire +s + +s
@adj extracommunautaire +s + +s
@nc mobil-home +s @m

bien	adv	[adv_kind=intensive]	bien____20	Default

## FTB6

@nc stand-by + @m
@nc hoola-hoop + @m
@nc assurance-maladie assurances-maladie @f
@nc scoubidou +s @m
@nc banlieusardisation +s @f

@nc faire-savoir + @m

@nc stockeur +s @m

## faire valoir que
valoir	v	[pred="valoir_____1<Suj:cln|scompl|sinf|sn,Obj:de-sinf|sn|scompl,Obj:(cld|-sn)>",@CtrlSujObl,@active,@pers,cat=v,@W]	valoir_____1	Infinitive	W %actif

ordre	100	cfi	[pred="ordre_____1<Suj:cln|sn,Obj:-sn|y>",lightverb=mettre]	ordre_____1	Default	%default

las	pres	[pred="las_____1"]	las_____1	Default

## aller bon train
train	100	cfi	[pred="train_____1<Suj:cln|sn,Obl:(sur-sn)>",lightverb=aller]	train_____1	Default	%default

## avoir bon dos
dos	100	cfi	[pred="dos_____1<Suj:cln|sn>",lightverb=avoir]	dos_____1	Default	%default

## avoir cure de
cure	100	cfi	[pred="cure_____1<Suj:cln|sn,Objde:de-sn|en|scompl>",lightverb=avoir]	cure_____1	Default	%default

## trouver preneur
preneur	100	cfi	[pred="preneur_____1<Suj:cln|sn,Obl:(pour-sn)>",lightverb=trouver]	preneur_____1	Default	%default

@nc rural ruraux @m

## From Subtitles 
#@nc blaster +s @m
#@adj qutaine +s + +s

#@redirect ouai oui

#@redirect pouffe pouffiasse
#@nc pouffe +s @f

hyper	adv	[pred="hyper_____1",cat=adv]	hyper_____1	Default

 part	120	prep	[pred="_part_____1<Obj:sa|sadv|sinf|sn>",cat=prep]	_part____1	Default	%default    
 part	120	adj	[pred="_part_____1<Suj:(cln|sn)>",cat=adj]	_part____1	Default	%default    

## attendre confirmation
confirmation	100	cfi	[pred="confirmation_____1<Suj:cln|sn,Objde:(de-sn|scompl|de-sinf)>",lightverb=attendre]	confirmation_____1	Default	%default

@nc surqualifi +s @m
@nc surqualifie +s @f

@nc bitexte +s @m
@nc phrasme +s @m
@nc morphe +s @m
@nc metadata + @f
@nc n-gram +s @m
@nc ranalyse +s @f
@nc qualia +s @m

@adj disfluent +s +e +es
@adj voyell +s +e +es
@adj syntagmatique +s + +s
@adj positionnel +s +le +les 
@adj cooccurrent +s +e +es 
@adj n-aire +s + +s 
@adj n-meilleur +s + +s 

morpho-_	120	advPref	[pred="morpho-______2<Suj:(sn)>",cat=adv]	morpho-______2	Default

@adj sybillin +s +e +es

@nc boson +s @m
@nc flagelle +s @f
@nc dharma + @m
@nc magasine +s @m
@nc ratification +s @f
@nc burka +s @f

@adj multipartite +s + +s
@adj accroupi +s +e +es

## missing passives
## rver
## exploser

## pb on agenouiller as participle/passive
## we got a whole set of such verbs (+pron +active)
@adj accroupi +s +e +es
@adj tapi +s +e +es
@adj acoquin +s +e +es
@adj agenouill +s +e +es
@adj blotti +s +e +es
@adj mott +s +e +es
@adj prostern +s +e +es
@adj recroquevill +s +e +es
@adj rfugi +s +e +es
@adj croul +s +e +es
@adj vautr +s +e +es

## missing @pron on the following verbs
@adj pm +s +e +es

@adj dteint +s +e +es
@adj trembl +s +e +es

@adj leste +s + +s

@nc crmone +s @f
@adj souabe +s + +s
@nc plessis + @m
@nc dbine +s @f
@nc insens +s @m
@nc insense +s @f
@nc psalmodie +s @f
@nc convict +s @m
@nc pas + @m
@nc radier +s @m
@nc palanfrier +s @m
@nc canadair +s @m

@adj vellitaire +s + +s

mordieu	pres	[pred="mordieu_____1"]	mordieu_____1	Default


mot	100	cfi	[pred="mot_____1<Suj:cln|sn,Objde:(de-sn|scompl|de-sinf)>",lightverb=souffler]	mot_____1	Default	%default
attention	100	cfi	[pred="attention_____1<Suj:cln|sn,Obj:(-sn|y|-sinf)>",lightverb=prter]	attention_____1	Default	%default

## not sure the following line is the best choice
## I believe there is a more general mechanism related to 'mettre' X est hors-jeu => Y met X hors-jeu
hors-jeu	100	cfi	[pred="hors-jeu_____1<Suj:cln|sn,Obj:(sn|cla)>",lightverb=mettre]	hors-jeu_____1	Default	%default

@nc corrigendum corrigenda @m
@nc provisionnement +s @m

@adj mutagne +s + +s
@adj hexavalent +s +e +es
@adj myope +s + +s

bon mnage	100	cfi	[pred="bon_mnage_____1<Suj:cln|sn>",lightverb=faire]	bon_mnage_____1	Default	%default

@adj fantasm +s +e +es

@nc mixte +s @m
@nc exclave +s @f

@adj dcentr +s +e +es
@adj dialogu +s +e +es

@nc carcinologiste +s @
@adj trivalent +s +e +es
@nc transposon +s @m
@nc tilapia +s @m
@nc tegula tegulae @f
@nc polytope +s @m
@nc squenage +s @m
@nc moticne +s @f
@nc systmaticien +s @m

@adj macr +s +e +es
@nc microtubule +s @f
@nc pronostique +s @m
@adj vent +s +e +es
@nc exhaustion +s @f
@nc bouchain +s @m
@nc cryptanalyste +s @
@adj cadr +s +e +es
@nc tesseract +s @m
@adj traversier +s traversire traversires
@nc endormissement +s @m

##@redirect auxquel auxquels
@nc rotavirus + @m
@nc baston +s @f
@adj peureux + peureuse peureuses
@nc crnelage +s @m
@adj rflexif +s rflexive rflexives
@nc liquidateur +s @m
@nc e-commerce +s @m

@adj ci-devant + + +
@nc ci-devant + @

@redirect fuss fusse

@adj jailli +s +e +es

Internet	np	[cat=np]	Internet_____1	Default

## from EMEA error mining
@nc tubule +s @m
@nc virion +s @m
@nc lyophilisat +s @m
@nc folinate +s @m

compte	100	cfi	[pred="compte_____1<Suj:cln|sn,Objde:(de-sn|scompl)>",lightverb=tenir]	compte_____1	Default	%default
parti	100	cfi	[pred="parti_____1<Suj:cln|sn,Objde:(de-sn|scompl)>",lightverb=tirer]	parti_____1	Default	%default

## * faire appel
appel	100	cfi	[pred="appel_____1<Suj:cln|sn,Objde:(de-sn)>",lightverb=faire]	appel_____1	Default

## * faire grief
## * faire peur
peur	100	cfi	[pred="peur_____1<Suj:cln|sn>",lightverb=faire]	peur_____1	Default

## * faire obligation
## * faire long feu
## * faire machine arrire
## * faire cause
## * faire justice
## * faire jeu gal

## ** prendre cong (de lui)
cong	100	cfi	[pred="cong_____1<Suj:cln|sn,Objde:(de-sn)>",lightverb=prendre]	cong_____1	Default	%default

## ** prendre appui (sur lui)
appui	100	cfi	[pred="appui_____1<Suj:cln|sn,Obl:(sur-sn)>",lightverb=prendre]	appui_____1	Default	%default

## ** prendre ombrage
ombrage	100	cfi	[pred="ombrage_____1<Suj:cln|sn,Objde:(de-sn|scompl)>",lightverb=prendre]	ombrage_____1	Default	%default

## ** prendre note (de sa venue)
note	100	cfi	[pred="note_____1<Suj:cln|sn,Objde:(de-sn|scompl)>",lightverb=prendre]	note_____1	Default	%default

## ** tirer avantage
avantage	100	cfi	[pred="avantage_____1<Suj:cln|sn,Objde:(de-sn)>",lightverb=tirer]	avantage_____1	Default	%default

## trouver chaussure  son pied

## ** dposer plainte (contre lui)
plainte	100	cfi	[pred="plainte_____1<Suj:cln|sn,Obl:(contre-sn)>",lightverb=dposer]	plainte_____1	Default	%default

temps	adj	[pred="temps<Suj:(de-sinf|scompl|sn)>",@impers,cat=adj,@ms]	temps____1	Default

@redirect n. numro
@redirect n numro
@redirect N numro
@redirect N. numro

## from UD_French
serment	100	cfi	[pred="serment_____1<Suj:cln|sn,Obj:(-sn)>",lightverb=prter]	serment_____1	Default	%default
confiance	100	cfi	[pred="confiance_____1<Suj:cln|sn,Obl:(en-sn)>",lightverb=inspirer]	confiance_____1	Default	%default

#avec		100	adv	[pred="avec_____1",cat=adv]	avec_____1	Default	%default
sans	100	adv	[pred="sans_____1",cat=adv]	sans_____1	Default	%default

@redirect ci ici

courage	100	cfi	[pred="courage_____1<Suj:cln|sn>",lightverb=reprendre]	courage_____1	Default	%default
conscience	100	cfi	[pred="conscience_____1<Suj:cln|sn>",lightverb=reprendre]	conscience_____1	Default	%default

non	adjPref	[pred="non_____1<Suj:(sn)>",cat=adj]	non_____1	Default 

obligation	100	cfi	[pred="obligation_____1<Suj:cln|sn,Obj:(-sn),Objde:de-sinf>",lightverb=faire]	obligation_____1	Default	%default

tant	100	csu	[]	tant_____1	Default	%default
tellement	100	csu	[]	tellement_____1	Default	%default

de plus en plus	100	predet	[pred="de_plus_en_plus_____1",predet_kind=adv,quantity=beaucoup]	de_plus_en_plus_____1	Default	%default
de moins en moins	100	predet	[pred="de_moins_en_moins_____1",predet_kind=adv,quantity=beaucoup]	de_moins_en_moins_____1	Default	%default

 ## Specific ofrlex

@nc servise +s @m
@nc siecle +s @m

@nc un +s @m
@nc autre +s @

@title abbeesse +s @f
@title abbe +s @m
@title abe +s @m
@title abé +s @m
@title amie +s @f # 1
@title ami +s @m # 4
@title amí +s @m
@title amiralz +s @m
@title ángele +s @m
@title angle +s @ # 2
@title apostle +s @m
@title apostoile +s @m
@title arcediacre +s @m
@title arceveske +s @m
@title arcevesque +s @m # 1
@title archier +s @m
@title baron +s @m # 1
@title barun +s @m # 1
@title ber +s @m
@title cardinal + @m
@title chardonal + @m
@title castel + @m
@title casté + @m
@title chastel +s @m # 2
@title chastiel +s @m
@title chemin +s @m
@title cavailler +s @m
@title chevalier +s @m # 1
@title champiun +s @m
@title chapelein +s @m
@title chappellain +s @m
@title chappelle +s @f
@title chief +s @m
@title cief +s @m
@title clerc +s @m
@title cite +s @f
@title cité +s @f
@title cited +s @f
@title citéd +s @f
@title cíted +s @f
@title citeit +s @f
@title citez + @f
@title conte +s @m # 3
@title compte +s @m
@title contesse +s @f
@title conestable +s @m
@title cunestable +s @m
@title cuens +s @ # 2
@title cumpaignun +s @m # 1
@title cunte +s @m # 5
@title dan +z @m # 1
@title dame +s @f
@title dameisele +s @f
@title damoisele +s @f
@title damoiselle +s @f
@title demoisele +s @f
@title demoiselle +s @f
@title deece +s @f
@title deesse +s @f
@title deïtez + @f
@title devys +s @m
@title dol +s @m
@title duc +s +z @m # 4
@title dux + @m # 6
@title duchesse +s @f
@title duchoise +s @f
@title despos + @m
@title dictateur +s @m
@title docteur +s @m
@title doien +s @m
@title emperere +s @m # 5
@title empereor +s @m
@title empereour +s @m
@title empereur +s @m
@title enseigne +s @m # 5
@title ermite +s @m # 1
@title hermite +s @m
@title escuier +s @m
@title espee +s @f # 1
@title evesche +s @m # 1
@title evésche +s @m
@title evesqe +s @m
@title evesque +s +z
@title erchevesqe +s @m
@title fluvie +s @f
@title fontaine +s @f
@title fontainne +s @f
@title funtaine +s @f
@title forest + @f
@title fossé +s @m
@title fradre +s @m # 1
@title fredre +s @m # 1
@title frere +s @m # 3
@title freire +s @m
@title general + @m
@title gué +s @m
@title guez + @m
@title guerreier +s @m # 1
@title guerrere +s @m
@title marchis + @m # 1
@title marquis + @m
@title marchan +s @m
@title mareschal +s @m
@title mere +s @f
@title messire +s @m # 54
@title mestre +s @m # 1
@title meistre +s @m
@title maistre +s @m
@title moine +s @m
@title monseignor +s @m # 9
@title mont +s @m
@title monz + @m
@title montaigne +s @f
@title monteingne +s @f
@title nain +s @m # 2
@title norrice +s @f
@title orateur +s @m
@title paien +s @m # 5
@title palefroi +s @m
@title papa +s @m
@title pape +s @m
@title pastouret +s @m # 1
@title patriarche +s @m
@title pays + @m
@title pere +s @m
@title père +s @m
@title peuple +s @m
@title pople +s @m
@title pueple +s @m
@title pharaon +s @m
@title pont +s @m
@title porte +s @f
@title president +s @m
@title prestre +s @m
@title preudome +s @m
@title preudon +s @m
@title prodon +s @m
@title prevost +s @m
@title prevoz +s @m
@title prince +s @m
@title princesse +s @f
@title prophete +s @m
@title prophéte +s @m
@title quen +s +z @m # 42
@title quin +s @m
@title reine +s @f # 1
@title rei +s @m # 27
@title reí +s @m
@title réi +s @m
@title reyne +s @f
@title reïne +s @f
@title rey +s @m
@title roïne +s @f # 2
@title roi +s @m # 38
@title roí +s @m
@title roy +s @m
@title royne +s @f
@title roine +s @f
@title roïne +s @f
@title roÿne +s @f
@title rethorien +s @m
@title rhethorien +s @m
@title realme +s @m
@title rue +s @f
@title saluaor +s @m
@title sarrazin +s @ # 1
@title saut +s @ # 1
@title seigneur +s @m
@title seignor +s @m # 9
@title seignur +s @m # 7
@title sanior +s @m
@title seigneurie +s @f
@title seneschal + @m
@title seneschax + @m
@title serjant +s @m # 3
@title seriant +s @m
@title serianz + @m
@title sire +s @m # 30
@title seur +s @m
@title suer +s @m
@title sueur +s @m
@title soleil +s @m
@title sorciere +s @f
@title temple +s @m
@title tere +s @f # 1
@title terre +s @f
@title tesmoin +s @m
@title tiran +s @m
@title tour +s @f
@title tombe +s @f
@title traïtor +s @m
@title traitre +s @m
@title traïtre +s @m
@title trone +s @m
@title throdne +s @m
@title truant +s @m
@title uncle +s @m # 2
@title varlé +s @m
@title varlet +s @m
@title vaslez + @m
@title vicomte +s @m
@title viconte +s @m
@title visconté +s @f
@title ville +s @f

@adj grant + + +
@nc sire +s @m
@nc voie +s @f
@nc vie +s @f                                   
@nc piez + @m
@nc servise +s @m
@nc vis + @m
@nc voiz + @f
@nc genz + @m
@nc teste +s @f
@nc preudon +s @
@nc tort +s @m
@nc table +s @f
@nc compaignie +s @f
@nc oil +z @m
@nc cos + @m
@nc tans + @m
@nc drap dras @m
@nc vin +s @m
@nc talent +s @m

#@redirect tres tres

_NUMBER	nc	[cat=nc]	_NUMBER	Default
_ROMNUM	nc	[cat=nc]	_ROMNUM	Default

@nc conscience +s @f
@nc plaisance +s @f
@nc conclusion +s @f
@nc balade +s @f
@nc liesse +s @f
@nc sentence +s @f
@nc necessit +s @f
@nc abbaye +s @f
@nc besongne +s @f
@nc procureur +s @m
@nc trason +s @f
@nc vaissiau +s @m
@nc gouverneur +s @m
@nc gouvernement +s @m
@nc voyage +s @m
@nc quantit +s @f
@nc vendredi + @m
@nc exemple +s @m
@nc souvenance +s @f
@nc chasteau +x @m
@nc servage +x @m
@nc trespassement+s @m
@nc forsenerie +s @f
@nc pense +s @f
@nc connestable +s @m
@nc bastard +s @m
@nc office +s @m
@nc noblesse +s @f
@nc souspir +s @m
@nc baniere +s @f
@nc adversaire +s @m
@nc numro +s @m
@nc condicion +s @f
@nc ecclesiastique +s @m
@nc desobissance +s @f
@nc substance +s @f
@nc multitude +s @f
@nc benediction +s @f
@nc ymagination +s @f
@nc satisfactiun +s @f
@nc escarmouche +s @f
@nc considracion +s @f
@nc privation +s @f
@nc machination +s @f
@nc sabmedi +s @m
@nc dymenche +s @m
@nc election +s @f
@nc prejudice +s @m
@nc disference +s @f
@nc marchandise +s @f
@nc resistance +s @f
@nc observance +s @f
@nc escarmuche +s @f
@nc sapienche +s @f
@nc astronomie +s @f
@nc perfection +s @f
@nc privacion +s @f
@nc resurreccion +s @f
@nc punicion +s @f
@nc experience +s @f
@nc habundance +s @f
@nc enpechementz +s @m
@nc distinction +s @m
@nc privilege +s @m
@nc admiracion +s @f
@nc orfaverie +s @f
@nc indignacion +s @f
@nc inconvenien +s @m
@nc histoire +s @f
@nc outrecuidance +s @f
@nc conuersation +s @f
@nc erudition +s @f
@nc protection +s @f
@nc correction +s @f
@nc satisfactiun +s @f
@nc legation +s @f
@nc transquilit +s @f
@nc dalphin +s @m
@nc inclinacion +s @f
@nc oppression +s @f
@nc inventoire +s @m
@nc composicion +s @f
@nc rethorique +s @f
@nc sabmedy +s @m
@nc exposition +s @f
@nc predicacion +s @f
@nc ammiration +s @f
@nc simulacre +s @m
@nc diemenche +s @m
@nc malediction +s @f
@nc description +s @f
@nc elevation +s @f
@nc occupation +s @f
@nc parrinnaige +s @m
@nc discretion +s @f
@nc intercession +s @f
@nc continuacion +s @f
@nc prorogacion +s @f
@nc mirancolie +s @f
@nc complession +s @f
@nc restitucion +s @f
@nc cardinal cardilnaux @m
@nc persecution +s @f
@nc execucion +s @f
@nc providence +s @f
@nc supplicacion +s @f
@nc predecesseur +s @m
@redirect instrumentz instruments
@nc stabilit +s @f
@nc arcediacre +s @m
@nc generatiuns +s @f
@nc aministracion +s @f
@nc fortificacion +s @f
@nc curiosit +s @f
@nc prrogative +s @f
@nc delectation +s @f
@nc machination +s @f
@nc composition +s @f
@nc imperfection +s @f
@nc concepcion +s @f
@nc contemplation +s @f
@nc eslection +s @f
@nc rafreschissement +s @f
@nc preeminence +s @f
@nc difficult +z @f
@nc compositiun +s @f
@nc dispersion +s @f
@nc gallant +s @m

@redirect conme come

@nc commere +s @f

@nc couronne +s @f
@nc personne +s @f
#should add 'arrire-garde'
@nc arrire-garde +s @f
@nc arriere-garde +s @f

@redirect chappelle chapelle
@nc chapelle +s @f
@redirect comme come
@nc besongne +s @f
@nc arrest +s @m
@nc moyen +s @m
@nc corouz + @m
@nc delay +s @m
@nc chrestien +s @m

beaucoup	100	adv	[pred="beaucoup___9999__1<Obj:(scompl)>",upos=ADV,cat=adv]	beaucoup___9999__1	Default		%default	adv

@nc terce +s @f
@nc mescrean +s @m
@nc nouvelle +s @f
@nc prela +z @m
@nc trason +s @f

@nc habitant habitans @m
@nc bastille +s @f
@nc  arrire +s @m
@nc counte +s @m
@nc mye +s @f
@nc couvent +s @m
@nc besongne +s @f
@nc couleur +s @f
@nc pillier +s @m
@nc demeure +s @f
@nc convenance +s @f
@nc prisonnier +s @m
@nc exemple +s @m
@nc langage +s @m
@nc herault heraulx @m
@nc gouverneur +s @m
@nc taille +s @f
@nc tante +s @f
@nc gensdarme +s @m
@nc damoiselle +s @f
@nc siecle +s @m
@nc trompette +s @f
@nc chymyn +s @m
@nc Epistre +s @f
@nc douleur +s @f
@nc vent +s @m
@nc priere +s @f
@nc vice +s @m
@nc desert desers @m
@nc tabernacle +s @m
@nc souvenir +s @m
@nc provision +s @f
@nc coulpe +s @f
@nc meyn +s @f
@nc chose +z @f
@nc chanon +s @f
@nc quarte +s @f


@transform ans$ ant nc nc @p
@transform z$ s nc nc

@transform tion$ cion nc nc
@transform cion$ tion nc nc
@transform ion$ iun nc nc
@transform iun$ ion nc nc

@transform tions$ cions nc nc
@transform cions$ tions nc nc
@transform ions$ iuns nc nc
@transform iuns$ ions nc nc
@transform np mp nc nc

